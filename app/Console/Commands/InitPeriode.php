<?php

namespace App\Console\Commands;

use App\Http\CustomHelpers\DateHelper;
use Illuminate\Console\Command;

class InitPeriode extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'periode:init';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	for ($months = 14; $months > 2; $months--) {
    		$time = strtotime("-" . strval($months) . " months");
		    $first_day_this_month = date('Y-m-01', $time);
		    $last_day_this_month  = date('Y-m-t', $time);

		    $is_in_table = \DB::Table('periode')->where('debut_periode', $first_day_this_month)->first();
		    if ($is_in_table) {
			    continue;
		    }

		    $monthName = date('F', $time);
		    $date = DateHelper::getFrenchMonthName($monthName) . ' ' . date('Y', $time);

		    $data = [
			    'debut_periode' => $first_day_this_month,
			    'fin_periode' => $last_day_this_month,
			    'display_name' => $date
		    ];

//		    echo $data['debut_periode'] . "-" . $data['fin_periode'] . " > " . $data['display_name'] . PHP_EOL;

		    \DB::Table('periode')->insert($data);
	    }
	    return 0;
    }
}
