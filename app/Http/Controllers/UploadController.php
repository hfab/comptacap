<?php

namespace App\Http\Controllers;

use App\Http\CustomHelpers\CampaignHelper;
use App\Http\CustomHelpers\CsvHandler;
use Illuminate\Http\Request;

class UploadController extends Controller
{
	/*
	 * File type
	 * 1 : update camapign
	 * 2 : add reliquats to campaign
	 * 3 : import Campaign List
	 * 4 : import Editors
	 * 5 : import Base (also imports Editors if not exists)
	 */

	private $error = '';

	public function __construct()
	{
		$this->middleware('auth');
	}

	public function uploadView()
	{
		$periode = \DB::table('periode')
			->orderByDesc('debut_periode')
			->get();
		$campagne = \DB::table('campagnes')
			->orderBy('nom_campagne','asc')
			->get();
		return view('upload.uploadCsv')
			->with('periode',$periode)
			->with('campagne',$campagne);
	}

	public function verifyCsv(Request $request)
	{
		if (!$request->hasFile('csvFile')) {
			echo "no file added <br>";
			return 1;
		}
		$limit = $request->limit;
		$file = $request->file('csvFile');
		$filename = $file->getClientOriginalName();
		$filepath = storage_path() . "/tmp/csv/";
		$file->move($filepath, $filename);

		$fileType = intval($request->fileType);
		$now = date("Y-m-d H:i:s");

		$csv = new CsvHandler($filepath. $filename);
		$missingHeader = $this->verifyHeader($csv, $fileType);
		if ($fileType == 3) {
			$this->getCampaignImportError($csv);
		}
		$error = $csv->getError();
//		foreach ($error as $err)
//			echo $err;

		unlink($filepath . $filename);
		return view('upload.verifyContent')
			->with('headers', $csv->getHeader())
			->with('rows', $csv->getContent(intval($limit)))
			->with('date', $now)
			->with('usedCol', $this->getProcessedcolumns($fileType))
			->with('missingHeader', $missingHeader ?: null)
			->with('hideCol', $this->getVerifFilter($fileType))
			->with('csvError', $error);
	}

	public function importCsvData(Request $request)
	{
		if (!$request->hasFile('csvFile')) {
			return "nof file sent <br>";
		}
		$file = $request->file('csvFile');
		$fileType = intval($request->fileType);
		$filename = $file->getClientOriginalName();
		$filepath = storage_path() . "/upload/csv/";
//		if (file_exists($filepath . $filename)) {
//			return "File already exists.";
//		}

		$file->move($filepath, $filename);
		$now = date("Y-m-d H:i:s");
		$fileInfo[] = [
			'nom_fichier' => $filename,
			'created_at' => $now,
			'updated_at' => $now
		];
		\DB::table('fichiers')->insert($fileInfo);

		$csv = new CsvHandler($filepath. $filename);

		$result = null;
		if ($fileType === 1) {
			$id_campagne = intval($request->id_campagne);
			$id_periode = intval($request->id_periode);
			if (!$id_campagne || !$id_periode) {
				$result = 500;
				$this->error = "<p>invalid campagne / periode</p>";
			}
			else {
				$result = $this->processCampaignUpdate($csv, $id_campagne, $id_periode);
			}
		}
		else if ($fileType === 2)
			$result = $this->processCampaignImport($csv);
		else if ($fileType === 3) {
			$id_campagne = intval($request->id_campagne);
			$id_periode = intval($request->id_periode);
			$id_periode_ref = intval($request->id_periode_ref);
			if (!$id_campagne || !$id_periode || !$id_periode_ref) {
				$result = 500;
				$this->error = "<p>invalid campagne / periode / periode ref</p>";
			}
			else {
				$result = $this->processReliquatImport($csv, $id_campagne, $id_periode, $id_periode_ref);
			}
		}
		else if ($fileType === 4)
			$result = $this->processEditorImport($csv);
		else if ($fileType === 5)
			$result = $this->processBaseImport($csv);

		if ($result === 500) {
			unlink($filepath. $filename);
			return 500 . " " . $this->error;
		}
		$headers = array_shift($result);
		return view('layouts.genericTable')
			->with('headers', $headers)
			->with('rows', $result);
//			->with('error'. $csv->error ?: '');
	}

	private function processCampaignUpdate(CsvHandler $csv, int $campaign_id, int $periode_id)
	{
		$result = array(['Importé', 'Zero', 'Errors', 'Skipped'], [0, 0, 0, 0]);
		$required_headers = ['IDS'];
		foreach ($required_headers as $req_h) {
			if (!in_array($req_h, $csv->getHeader())) {
				$csv->addError("missing header [$req_h] in csv.");
				return 500;
			}
		}
		$fileInDB = \DB::table('fichiers')->orderBy('id', 'desc')->first();

		$data = $csv->getKeyValArray();
		$now = date("Y-m-d H:i:s");
		foreach ($data as $elem) {
//			if ($elem['Statut'] !== 'on') {
//				$result[1][3] += 1;
//				continue;
//			}

			$campaign = \DB::table('campagnes')
				->where('id',$campaign_id)
				->first();

			$remType = \DB::table('typerem')
				->where('id',$campaign->type_id_campagne)
				->first();

			$CPs = $this->getCPs($elem);

			$dataset = [
				'id_fichier' => $fileInDB->id ?: 1,
				'id_site_base' => $elem['IDS'],
				'id_periode' => $periode_id,
				'id_campagne' => $campaign_id,
				'CPC' => isset($CPs['CPC']) ? $CPs['CPC'] : '0',
				'CPL' => isset($CPs['CPL']) ? $CPs['CPL'] : '0',
				'CPA' => isset($CPs['CPA']) ? $CPs['CPA'] : '0',
				'CPM' => isset($CPs['CPM']) ? $CPs['CPM'] : '0',
				'com' => explode(' ', $elem['T. COM.'])[0] ?: null,
//				'devalid_CPC' => null,
//				'devalid_CPL' => null,
//				'devalid_CPA' => null,
//				'devalid_CPM' => null,
//				'devalid_com' => null,
//				'devalid_id' => 0,
//				'devalid_pourcent' => null,
//				'devalid_nombre' => 0,
//				'is_devalided' => 0,
				'created_at' => $now,
				'updated_at' => $now,
			];
//			if($remType->type == 'CPM'){
//				// $dataset['com'] = 0;
//			}
			if($remType->type == 'CPL'){
				$dataset['com'] = round($dataset['CPL'] * $campaign->rem_campagne, 2);
			}
			if($remType->type == 'CPA'){
				$dataset['com'] = round($dataset['CPA'] * $campaign->rem_campagne, 2);
			}
			if($remType->type == 'CPC'){
				$dataset['com'] = $dataset['CPC'] * $campaign->rem_campagne;
				$dataset['com'] = round($dataset['com'], 2);
			}

			echo "<br>";
			if (intval($dataset['com']) != 0) {
				\DB::table('ComptaByCampagneCsv')->insert($dataset);
				$result[1][0] += 1;
			}
			else
				$result[1][1] += 1;
		}
		return $result;
	}

	private function getCPs($data)
	{
		$result = array();

		if ($data['CPM']) {
			$result['CPM'] = str_replace(' ','',$data['CPM']);
		}

		if($data['CPC']) {
			$tmp = explode('(', $data['CPC']);
			$result['CPC'] = str_replace(' ','',$tmp[0]);
		}

		if($data['CPL']){
			preg_match_all("#(\(.+)\)#isU", $data['CPL'], $nbLead);
			$finaLead = str_replace('(','',$nbLead[0][0]);
			$finaLead = str_replace(')','',$finaLead);
			$result['CPL'] =$finaLead;
		}

		if($data['CPA']){
			preg_match_all("#(\(.+)\)#isU", $data['CPA'], $nbCPA);
			$finalCPA = str_replace('(','',$nbCPA[0][0]);
			$finalCPA = str_replace(')','',$finalCPA);
			$result['CPA'] = $finalCPA;
		}
		return $result;
	}

	private function processCampaignImport(CsvHandler $csv)
	{
		$result = array(['Success', 'Duplicate', 'missing Rem'], [0, 0, 0]);
		$required_headers = ['Campagne', 'Reversion'];
		foreach ($required_headers as $req_h) {
			if (!in_array($req_h, $csv->getHeader())) {
				$csv->addError("missing header [$req_h] in csv.");
				return 500;
			}
		}

		$data = $csv->getKeyValArray();
		foreach ($data as $elem) {
			$rem = explode(' ', $elem['Reversion']);
			if (!CampaignHelper::getRemTypeIdByString($rem[0])) {
				$csv->addError("skip: " . $elem['Campagne'] . " rem non renseigné : " . $rem[0] . PHP_EOL);
				$result[1][2]++;
				continue;
			}
			$dataset = [
				'type_id_campagne' => CampaignHelper::getRemTypeIdByString($rem[0]),
				'nom_campagne' => $elem['Campagne'],
//				'note_campagne' => $elem['note'],
				'rem_campagne' => $rem[1]
			];

			if (\DB::table('campagnes')->where('nom_campagne', $dataset['nom_campagne'])->first())
				$result[1][1] += 1;
			else {
				\DB::table('campagnes')->insert($dataset);
				$result[1][0] += 1;
			}
		}
		return $result;
	}

	private function processBaseImport(CsvHandler $csv)
	{
		$result = array(['Success', 'Duplicate', 'New Editor', 'Unknown Editor'], [0, 0, 0, 0]);
		$required_headers = ['IDSUP', 'IDS', 'URL', 'RAISON SOCIALE', "DOMAINE", "E-MAIL CONTACT"];
		foreach ($required_headers as $req_h) {
			if (!in_array($req_h, $csv->getHeader())) {
				$csv->addError("missing header [$req_h] in csv.");
				return 500;
			}
		}

		$http_prefix = 'http://';
		$data = $csv->getKeyValArray();
		foreach ($data as $elem) {
			$url = $elem['URL'];
			if (substr($url, 0, strlen($http_prefix)) == $http_prefix) {
				$url = substr($url, strlen($http_prefix));
			}
			$dataset = [
				'idsite' => $elem['IDS'] ?: null,
				'id_editeur' => $elem['IDSUP'] ?: null,
				'titre' => $url ?: null,
				'url' => $elem['DOMAINE'] ?: null,
//				'description' => $elem ?: null,
//				'categorie' => $elem ?: null,
//				'mailing' => $elem ?: null,
//				'mobile' => $elem ?: null,
//				'vu' => $elem ?: null,
//				'pap' => $elem ?: null,
//				'label' => $elem ?: null,
//				'racine' => $elem ?: null,
//				'statut' => $elem ?: null,
//				'domaine' => $elem['DOMAINE'] ?: null,
//				'commercial' => $elem ?: null,
			];
			if (!(\DB::table('editeurs')->where('ids_editeur', $dataset['id_editeur'])->first())) {
				if ($this->addEditorFromArray($elem)) {
					$result[1][2] += 1;
				}
				else {
					$result[1][3] += 1;
					continue;
				}
			}
			if (\DB::table('bases')->where('idsite', $dataset['idsite'])->first())
				$result[1][1] += 1;
			else {
				\DB::table('bases')->insert($dataset);
				$result[1][0] += 1;
			}
		}
		return $result;
	}

	private function addEditorFromArray(Array $data)
	{
		$dataset = [
			'ids_editeur' => $data['IDSUP'],
			'id_type' => 99,
			'type' => 99,
			'login' => '',
			'email' => $data['E-MAIL CONTACT'] ?: null,
			'technique' => $data['E-MAIL TECHNIQUE'] ?: null,
			'compta' => $data['E-MAIL COMPTABILITE'] ?: null,
			'skype' => $data['SKYPE'] ?: null,
			'telephone' => $data['TELEPHONE FIXE'] ?: null,
			'cellulaire' => '',
			'fax' => '',
			'nom_entreprise' => $data['RAISON SOCIALE'] ?: null,
			'siret' => '',
			'tva' => '',
			'civ' => '',
			'nom' => $data['NOM'] ?: null,
			'prenom' => $data['PRENOM'] ?: null,
			'adresse' => $data['ADRESSE'] ?: null,
			'codepostale' => $data['CODE POSTAL'] ?: null,
			'ville' => $data['VILLE'] ?: null,
			'pays' => '',
			'racine' => '',
			'commercial' => 2,
			'statut' => 1,
			'paiement' => 50,
			'paiement_type' => '',
			'iban' => '',
			'swift' => '',
			'banque' => '',
			'commentaire' => '',
			'date_creation' => date("Y-m-d H:i:s"),
		];
		return \DB::table('editeurs')->insert($dataset);
	}

	private function processEditorImport(CsvHandler $csv)
	{
		$result = array(['Success', 'Duplicate', 'Updated'], [0, 0, 0]);
		$required_headers = ['IDSUP', 'E-MAIL CONTACT', 'RAISON SOCIALE', 'NOM', "PRENOM", "ADRESSE", "CODE POSTAL", "VILLE"];
		foreach ($required_headers as $req_h) {
			if (!in_array($req_h, $csv->getHeader())) {
				$csv->addError("missing header [$req_h] in csv.");
				return 500;
			}
		}

		$data = $csv->getKeyValArray();

		foreach ($data as $elem) {
			$dataset = [
				'ids_editeur' => $elem['IDSUP'],
				'id_type' => 99,
				'type' => 99,
				'login' => '',
				'email' => $elem['E-MAIL CONTACT'] ?: null,
				'technique' => $elem['E-MAIL TECHNIQUE'] ?: null,
				'compta' => $elem['E-MAIL COMPTABILITE'] ?: null,
				'skype' => $elem['SKYPE'] ?: null,
				'telephone' => $elem['TELEPHONE FIXE'] ?: null,
				'cellulaire' => '',
				'fax' => '',
				'nom_entreprise' => $elem['RAISON SOCIALE'] ?: null,
				'siret' => '',
				'tva' => '',
				'civ' => '',
				'nom' => $elem['NOM'] ?: null,
				'prenom' => $elem['PRENOM'] ?: null,
				'adresse' => $elem['ADRESSE'] ?: null,
				'codepostale' => $elem['CODE POSTAL'] ?: null,
				'ville' => $elem['VILLE'] ?: null,
				'pays' => '',
				'racine' => '',
				'commercial' => 2,
				'statut' => 1,
				'paiement' => 50,
				'paiement_type' => '',
				'iban' => '',
				'swift' => '',
				'banque' => '',
				'commentaire' => '',
				'date_creation' => date("Y-m-d H:i:s"),
			];

			if (\DB::table('editeurs')->where('ids_editeur', $elem['IDSUP'])->first()) {
				if ($this->checkEditorUpdate($elem, $dataset))
					$result[1][2] += 1;
				else
					$result[1][1] += 1;
				continue;
			}

			\DB::table('editeurs')->insert($dataset);
			$result[1][0] += 1;
		}
		return $result;
	}

	private function checkEditorUpdate($elem, $dataset)
	{
		$ignored = [
			'ids_editeur',
			'id_type',
			'type',
			'nom_entreprise',
			'commercial',
			'statut',
			'paiement',
			'date_creation',
		];
		$editor = \DB::table('editeurs')->where('ids_editeur', $elem['IDSUP'])->first();
		$update = array();

		foreach ($dataset as $key => $value) {
			if (in_array($key, $ignored))
				continue;

			if ($editor->$key != $value)
				$update[$key] = $value;
		}

		if (count($update)) {
			\DB::table('editeurs')->where('ids_editeur', $elem['IDSUP'])->update($update);
			return true;
		}
		return false;
	}

	private function processReliquatImport(CsvHandler $csv, int $campaign_id, int $periode_id, int $periode_ref_id) {
		$result = array(['Importé', 'Zero', 'Errors', 'Skipped'], [0, 0, 0, 0]);
		$required_headers = ['IDS'];
		foreach ($required_headers as $req_h) {
			if (!in_array($req_h, $csv->getHeader())) {
				$csv->addError("missing header [$req_h] in csv.");
				return 500;
			}
		}
		$fileInDB = \DB::table('fichiers')->orderBy('id', 'desc')->first();

		$data = $csv->getKeyValArray();
		$now = date("Y-m-d H:i:s");
		foreach ($data as $elem) {
//			if ($elem['Statut'] !== 'on') {
//				$result[1][3] += 1;
//				continue;
//			}

			$campaign = \DB::table('campagnes')
				->where('id',$campaign_id)
				->first();

			$remType = \DB::table('typerem')
				->where('id',$campaign->type_id_campagne)
				->first();

			$CPs = $this->getCPs($elem);

			$dataset = [
				'id_fichier' => $fileInDB->id ?: 1,
				'id_site_base' => $elem['IDS'],
				'id_periode' => $periode_id,
				'id_campagne' => $campaign_id,
				'id_periode_reference' => $periode_ref_id,
				'CPC' => isset($CPs['CPC']) ? $CPs['CPC'] : '0',
				'CPL' => isset($CPs['CPL']) ? $CPs['CPL'] : '0',
				'CPA' => isset($CPs['CPA']) ? $CPs['CPA'] : '0',
				'CPM' => isset($CPs['CPM']) ? $CPs['CPM'] : '0',
				'com' => explode(' ', $elem['T. COM.'])[0] ?: null,
				'created_at' => $now,
				'updated_at' => $now,
			];
			if($remType->type == 'CPL'){
				$dataset['com'] = round($dataset['CPL'] * $campaign->rem_campagne, 2);
			}
			if($remType->type == 'CPA'){
				$dataset['com'] = round($dataset['CPA'] * $campaign->rem_campagne, 2);
			}
			if($remType->type == 'CPC'){
				$dataset['com'] = $dataset['CPC'] * $campaign->rem_campagne;
				$dataset['com'] = round($dataset['com'], 2);
			}

			if (intval($dataset['com']) != 0) {
				\DB::table('reliquats')->insert($dataset);
				$result[1][0] += 1;
			}
			else
				$result[1][1] += 1;
		}
		return $result;

	}

	private function getCampaignImportError(CsvHandler $csv)
	{
		$hasError = false;
		$required_headers = ['Campagne', 'Reversion'];
		foreach ($required_headers as $req_h) {
			if (!in_array($req_h, $csv->getHeader())) {
				$csv->addError("missing header [$req_h] in csv.");
				return true;
			}
		}

		$data = $csv->getKeyValArray();
		foreach ($data as $elem) {
			$rem = explode(' ', $elem['Reversion']);
			if (!CampaignHelper::getRemTypeIdByString($rem[0])) {
				$csv->addError("skip: " . $elem['Campagne'] . " rem non renseigné : " . $rem[0] . PHP_EOL);
				$hasError = true;
			}
		}
		return $hasError;
	}

	private function verifyHeader(CsvHandler $csv, int $type)
	{
		$headerlist = [
			1 => ['IDS'],
			2 => ['IDS'],
			3 => ['Campagne', 'Reversion'],
			4 => ['IDSUP', 'E-MAIL CONTACT', 'RAISON SOCIALE', 'NOM', "PRENOM", "ADRESSE", "CODE POSTAL", "VILLE"],
			5 => ['IDSUP', 'IDS', 'URL', 'RAISON SOCIALE', "DOMAINE", "E-MAIL CONTACT"],
		];
		$missing_list = array();
		foreach ($headerlist[$type] as $header) {
			if (!in_array($header, $csv->getHeader())) {
				$missing_list []= $header;
			}
		}
		if (count($missing_list))
			return $missing_list;
		return null;
	}

	private function getProcessedcolumns($fileType_id)
	{
		$headerlist = [
			1 => ['IDS', 'CPC', 'CPL', 'CPA', 'CPM', 'T. COM.'],
			2 => ['IDS'],
			3 => ['Campagne', 'Reversion'],
			4 => ['IDSUP', 'E-MAIL CONTACT', 'E-MAIL TECHNIQUE', 'E-MAIL COMPTABILITE', 'SKYPE', 'TELEPHONE FIXE', 'RAISON SOCIALE', 'NOM', "PRENOM", "ADRESSE", "CODE POSTAL", "VILLE"],
			5 => ['IDSUP', 'IDS', 'URL', "DOMAINE", 'E-MAIL CONTACT', 'E-MAIL TECHNIQUE', 'E-MAIL COMPTABILITE', 'SKYPE', 'TELEPHONE FIXE', 'RAISON SOCIALE', 'NOM', "PRENOM", "ADRESSE", "CODE POSTAL", "VILLE"],
		];
		return $headerlist[$fileType_id];
	}

	/*
	 * import verif Filter management
	 */
	public function configVerifFilter()
	{
		$dbList = \DB::table('upload_verif_filter')
			->orderBy('col_name')
			->get();
		$import_type_list = $this->getImportTypeList();

		return view('upload.config_filter')
			->with('filter_table_list', $this->buildFilterTableElemArray($dbList))
			->with('import_type_list', $import_type_list);
	}

	public function addVerifFilter()
	{
		$type_id = intval($_POST['import_type']);
		$col_name = strval($_POST['col_name']);

		$dataset = [
			'upload_type_id' => $type_id,
			'upload_name' => $this->getImportTypeList()[$type_id],
			'col_name' => $col_name,
		];

		\DB::table('upload_verif_filter')->insert($dataset);

		return redirect('/upload/config_filter');
	}

	public function removeVerifFilter()
	{
		$type = $_POST['import_type_name'];
		$col_name = $_POST['col_name'];

		\DB::table('upload_verif_filter')
			->where('upload_name', $type)
			->where('col_name', $col_name)
			->delete();

		return redirect('/upload/config_filter');
	}

	private function getVerifFilter($fileType)
	{
		$dbList = \DB::table('upload_verif_filter')
			->where('upload_type_id', $fileType)
			->get();

		$filterList = array();
		foreach ($dbList as $elem) {
			$filterList []= $elem->col_name;
		}

		return $filterList;
	}

	private function buildFilterTableElemArray($list)
	{
		$tableList = array();
		foreach ($list as $item) {
			if (!isset($tableList[$item->upload_name])) {
				$tableList[$item->upload_name] = array();
			}
			array_push($tableList[$item->upload_name], $item->col_name);
		}
		return $tableList;
	}

	private function getImportTypeList()
	{
		return [
			1 => 'mise à jour campagne',
			2 => 'ajout reliquats',
			3 => 'import campagnes',
//			4 => 'import editeurs',
			5 => 'base/editeur',
		];
	}
}
