<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class BaseController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth');
	}

	public function index(){

		$bases = \DB::table('bases')
			// ->join('editeurs', 'bases.id_editeur', '=', 'editeurs.ids_editeur')
			->get();

		return view('bases.index')
			->with('bases',$bases);
	}

	public function create()
	{
		$editeurs = \DB::table('editeurs')
			->orderBy('nom_entreprise', 'asc')
			->get();
		return view('bases.create')
			->with('editeurs',$editeurs);
	}

	public function store()
	{
		$fields = array_except(Input::all(), '_token');
		$dataset[] = ['titre' => $fields['titre'],
			'id_editeur' => $fields['id_editeur']];
		\DB::table('bases')->insert($dataset);
		return redirect('/bases');
	}


	public function edit($id)
	{

		$base = \DB::table('bases')
			->where('id',$id)
			->first();

		$editeurs = \DB::table('editeurs')
			->orderBy('nom_entreprise','asc')
			->get();

		return view('bases.edit')
			->with('base',$base)
			->with('editeurs',$editeurs);

	}

	public function update($id){

		$fields = array_except(Input::all(), '_token');
		$dataset = ['titre' => $fields['titre'],
			'id_editeur' => $fields['id_editeur']];

		\DB::table('bases')
			->where('id',$id)
			->update($dataset);
		return redirect('/bases');

	}

	public function delete($id)
	{
		\DB::table('bases')
			->where('id',$id)
			->delete();

		return redirect('/bases');
	}
}
