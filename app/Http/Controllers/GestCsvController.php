<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class GestCsvController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth');
	}


	public function uploadFileForm(){

		$periode = \DB::table('periode')->get();
		$campagne = \DB::table('campagnes')
			->orderBy('nom_campagne','asc')
			->get();
		return view('upload.uploadForm')
			->with('periode',$periode)
			->with('campagne',$campagne);
	}

	public function uploadFileFormErrorDuplicate(){

		$periode = \DB::table('periode')->get();
		$campagne = \DB::table('campagnes')->get();
		return view('upload.uploadFormDuplicateFile')
			->with('periode',$periode)
			->with('campagne',$campagne);
	}

	public function uploadFileStore()
	{
		$fields = array_except(Input::all(), '_token');
		$name = Input::file('file')->getClientOriginalName();

		if(file_exists(storage_path() . '/importbycampagne/'. $name)) {
			return redirect('file/upload/error/duplicate');
		}

		Input::file('file')->move(storage_path() . '/importbycampagne/', $name);
		$fh = fopen(storage_path() . '/importbycampagne/'.$name, 'r');

		// verif si le fichier existe
		$today = date("Y-m-d H:i:s");
		$dataset[] = [
			'nom_fichier' => $name,
			'created_at' => $today,
			'updated_at' => $today
		];

		\DB::table('fichiers')->insert($dataset);
		$file = \DB::table('fichiers')->orderBy('created_at', 'desc')->first();

		// get headers
		$headers = fgetcsv($fh, 1024, ';');
		if (!in_array('IDS', $headers)) {
			echo "No IDS, file is invalid";
		}
		$req_col_nb = count($headers);

		$periode_id = $fields['id_periode']; // todo: replace
		$campaign_id = $fields['id_campagne']; // todo: replace
		$campaign = \DB::table('campagnes')->where('id', $campaign_id)->first();
		$rem_type = \DB::table('typerem')->where('id', $campaign->type_id_campagne)->first();

		$raw_row = '';
		while ($line = fgets($fh)) {
			$raw_row .= $line;

			if (substr_count($raw_row, ';') === $req_col_nb - 1) {
				$row = $this->trim_row(explode(';', $raw_row), $headers);

				$data_elem = $this->build_data_elem($row, $this->trim_row($headers, $headers));
				if ($data_elem['id_site_base'] !== 0) {

					$data_elem['com'] = intval($data_elem[$rem_type->type]) * $campaign->rem_campagne;
					if ($rem_type->type === 'CPM')
						$data_elem['com'] /= 1000;
					$data_elem['com'] = strval($data_elem['com']);
					var_dump($data_elem['com']);

					$data_elem['id_fichier'] = $file->id;
					$data_elem['id_periode'] = $periode_id;
					$data_elem['id_campagne'] = $campaign_id;
					$data_elem['created_at'] = date("Y-m-d H:i:s");
					$data_elem['updated_at'] = date("Y-m-d H:i:s");

					echo "pour la base avec id " . $data_elem['id_site_base'] . ", de type : $rem_type->type.";
					echo '<br>';
					echo "la com est : " . $data_elem['com'] . " avec pour valeurs : nb = " .
						$data_elem[$rem_type->type] . " et rem = $campaign->rem_campagne" . PHP_EOL;
					echo '<br>';
					\DB::table('ComptaByCampagneCsv')->insert($data_elem);
				}
				$raw_row = '';
			}
			else if (substr_count($raw_row, ';') > $req_col_nb - 1) {
				echo 'Given CSV yielded a row larger than the header';
			}
		}
		return 0;
	}

	private function trim_row($row, $headers)
	{
		$ignored_columns = ["Site Web", "Note", "E-MAIL", "T. DEP.", "RPM"];

		foreach ($ignored_columns as $ignored_column) {
			$index = array_search($ignored_column, $headers);
			if ($index !== false) {
				unset($headers[$index]);
				unset($row[$index]);
			}
		}
		return $row;
	}

	private function build_data_elem($row, $headers, $id_periode = 1, $id_campaign = 1)
	{
		$data_elem = array_combine($headers, $row);
		$result_array = array('CPM' => '0', 'CPC' => '0', 'CPL' => '0', 'CPA' => '0');
		foreach ($data_elem as $key => $value) {
			if ($key === 'IDS')
				$result_array['id_site_base'] = intval(trim($value));
			if ($key === 'CPM')
				$result_array['CPM'] = trim($value);
			if ($key === 'CPC')
				$result_array['CPC'] = trim($value);
			if ($key === 'CPL')
				$result_array['CPL'] = trim($value);
			if ($key === 'CPA')
				$result_array['CPA'] = trim($value);
		}
		return $result_array;
	}

	public function uploadFileStore_static(){

		$fields = array_except(Input::all(), '_token');
		$name = Input::file('file')->getClientOriginalName();

		if(file_exists(storage_path() . '/importbycampagne/'. $name)) {
			return redirect('file/upload/error/duplicate');
		}

		Input::file('file')->move(storage_path() . '/importbycampagne/', $name);
		$fh = fopen(storage_path() . '/importbycampagne/'.$name, 'r');

		// verif si le fichier existe
		$today = date("Y-m-d H:i:s");
		$dataset[] = ['nom_fichier' => $name,
			'created_at' => $today,
			'updated_at' => $today];

		\DB::table('fichiers')->insert($dataset);

		$ligne = 0;
		while($tab=fgetcsv($fh,1024,';'))
		{
			$insetvar = false;
			$champs = count($tab);//nombre de champ dans la ligne en question
			echo "<br />";
			echo " Les " . $champs . " champs de la ligne " . $ligne . " sont :";
			echo "<br />";

			$ligne ++;
			if($ligne == 1){
				continue;
			}

			$datasetfields = array();
			//affichage de chaque champ de la ligne en question
			for($i=0; $i<$champs; $i ++)
			{
				if($i == 2 || $i == 3 || $i == 4 || $i == 12 || $i == 13){
					continue;
				} else {

					// si le 14 est pas bon "on"
					// faire les insertions et servir la page
					// id import fichier
					$datasetfields['id_fichier'] = 1;
					if($i == 0){
						$datasetfields['id_site_base'] = $tab[$i];
					}
					// bien recup l'id
					$datasetfields['id_periode'] = $fields['id_periode'];

					if($i == 5){
						$datasetfields['CPM'] = str_replace(' ','',$tab[$i]);
					}

					if($i == 6){
						$varcpc = explode('(',$tab[$i]);
						$datasetfields['CPC'] = str_replace(' ','',$varcpc[0]);
					}

					if($i == 8){
						preg_match_all("#(\(.+)\)#isU", $tab[$i], $nblead);
						$finalead = str_replace('(','',$nblead[0][0]);
						$finalead = str_replace(')','',$finalead);
						$datasetfields['CPL'] =$finalead;
					}

					if($i == 9){
						preg_match_all("#(\(.+)\)#isU", $tab[$i], $nbcpa);
						$finalcpa = str_replace('(','',$nbcpa[0][0]);
						$finalcpa = str_replace(')','',$finalcpa);
						$datasetfields['CPA'] = $finalcpa;
					}

					if($i == 11){
						$varcom = explode(' ',$tab[$i]);
						$datasetfields['com'] = $varcom[0];
					}

					if($i == 14){
						$insetvar = $tab[$i];
					}

				}
			}

			if($insetvar !== 'on'){
				continue;
			}

			$lacampagne = \DB::table('campagnes')
				->where('id',$fields['id_campagne'])
				->first();

			$letyperem = \DB::table('typerem')
				->where('id',$lacampagne->type_id_campagne)
				->first();

			if($letyperem->type == 'CPM'){
				// $datasetfields['com'] = 0;
			}

			if($letyperem->type == 'CPL'){
				$datasetfields['com'] = $datasetfields['CPL'] * $lacampagne->rem_campagne;
			}

			if($letyperem->type == 'CPA'){
				$datasetfields['com'] = $datasetfields['CPA'] * $lacampagne->rem_campagne;
			}

			if($letyperem->type == 'CPC'){
				$datasetfields['com'] = $datasetfields['CPC'] * $lacampagne->rem_campagne;
				$datasetfields['com'] = round($datasetfields['com'], 2);
			}

			$datasetfields['id_campagne'] = $fields['id_campagne'];
			$datasetfields['created_at'] = date("Y-m-d H:i:s");
			$datasetfields['updated_at'] = date("Y-m-d H:i:s");

			\DB::table('ComptaByCampagneCsv')->insert($datasetfields);

			unset($varcpc);
			unset($varcom);
			unset($datasetfields);
		}

		/*
		// il campagne type = 0 on delete les lignes
		// on supprime ce qui sert à rien
		\DB::table('ComptaByCampagneCsv')
		->where('id_periode', $fields['id_periode'])
		->where('com','=',0)
		->delete();
		*/

	}


	public function importFilebyCampagne(){




	}

	public function importFilebyEditeur(){




	}

}
