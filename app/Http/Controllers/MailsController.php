<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use SendinBlue\Client\Api\TransactionalEmailsApi;
use SendinBlue\Client\Configuration;
use SendinBlue\Client\Model as SIB;

class MailsController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth');
	}


	public function index(){

	}

	public function dbMailsPeriode($id){

		$mailsremcheck = \DB::table('ComptaByCampagneCsv')
			->where('id_periode',$id)
			->get();
		foreach ($mailsremcheck as $value) {

			$checkbases = \DB::table('bases')
				->where('idsite',$value->id_site_base)
				->first();
			$editeurs = \DB::table('editeurs')
				->where('ids_editeur',$checkbases->id_editeur)
				->first();
			$check = \DB::table('mails')
				->where('id_periode',$id)
				->where('editeur_id_mail', $checkbases->id_editeur)
				->first();

			if($check == NULL){
				$today = date("Y-m-d H:i:s");
				$dataset[] = [
					'contenu_mail' => 'vide',
					'editeur_id_mail' => $checkbases->id_editeur,
					'id_periode' => $id,
					'created_at' => $today,
					'updated_at' => $today];
				\DB::table('mails')->insert($dataset);
				unset($dataset);
			}
		}
	}


	public function dbMailsPeriodePost(){

		// on get le post
		$fields = array_except(Input::all(), '_token');

		$mailsremcheck = \DB::table('ComptaByCampagneCsv')
			->where('id_periode',$fields['id_periode'])
			->get();
		foreach ($mailsremcheck as $value) {

			$checkbases = \DB::table('bases')
				->where('idsite',$value->id_site_base)
				->first();

			if(is_null($checkbases)){
				continue;
			}

			$editeurs = \DB::table('editeurs')
				->where('ids_editeur',$checkbases->id_editeur)
				->first();

			if(is_null($editeurs)){
				continue;
			}

			$check = \DB::table('mails')
				->where('id_periode',$fields['id_periode'])
				->where('editeur_id_mail', $checkbases->id_editeur)
				->first();

			if($check == NULL){
				$today = date("Y-m-d H:i:s");
				$dataset[] = [
					'contenu_mail' => 'vide',
					'editeur_id_mail' => $checkbases->id_editeur,
					'id_periode' => $fields['id_periode'],
					'created_at' => $today,
					'updated_at' => $today];
				\DB::table('mails')->insert($dataset);
				unset($dataset);
			}
		}

		return redirect('/mails/editeurs/appelfacture');

	}

	public function listingEditeurAppel($id){

		$listediteursmails = \DB::table('mails')
			->where('id_periode',$id)
			->get();

		$periodeinfo = \DB::table('periode')
			->where('id',$id)
			->first();

		$periodeautre = \DB::table('periode')
			->where('id','<>',$id)
			->orderByDesc('debut_periode')
			->get();

		return view('mails.indexediteurperiode')
			->with('listediteursmails',$listediteursmails)
			->with('periodeinfo',$periodeinfo)
			->with('periodeautre',$periodeautre)
			->with('id',$id);

	}

	public function voirFacturePeriode($ide,$idp){

		$listbases = \DB::table('bases')
			->select('idsite')
			->where('id_editeur',$ide)
			->get();

		$editeurinfo = \DB::table('editeurs')
			->where('ids_editeur',$ide)
			->first();

		$listbases = array_pluck($listbases,'idsite');
		// var_dump($listbases);

		$periode = \DB::table('periode')
			->where('id', $idp)
			->first();

		$listerem0 = \DB::table('ComptaByCampagneCsv')
			->where('id_periode',$idp)
			->whereIn('id_site_base',$listbases)
			->where('is_devalided',0)
			->get();

		$listerem1 = \DB::table('ComptaByCampagneCsv')
			->where('id_periode',$idp)
			->whereIn('id_site_base',$listbases)
			->where('is_devalided',1)
			->get();


		$total0 = \DB::table('ComptaByCampagneCsv')
			->where('id_periode',$idp)
			->whereIn('id_site_base',$listbases)
			->where('is_devalided',0)
			->sum('com');

		$total1 = \DB::table('ComptaByCampagneCsv')
			->where('id_periode',$idp)
			->whereIn('id_site_base',$listbases)
			->where('is_devalided',1)
			->sum('devalid_com');

		$totalrel = \DB::table('reliquats')
			->where('id_periode',$idp)
			->whereIn('id_site_base',$listbases)
			->sum('com');

		$total = $total0 + $total1 + $totalrel;

		$reliquats = \DB::table('reliquats')
			->where('id_periode',$idp)
			->whereIn('id_site_base',$listbases)
			->get();

		return view('mails.appelfacture')
			->with('listerem0',$listerem0)
			->with('listerem1',$listerem1)
			->with('total',$total)
			->with('editeurinfo',$editeurinfo)
			->with('reliquats',$reliquats)
			->with('periode', $periode);

	}

	public function acceuilFacture(){
		$maxp = \DB::table('periode')
			->max('id');
		return view('mails.indexfacture')
			->with('maxp',$maxp);
	}


	public function listingGenerateAppel(){
		$lesperiodes = \DB::table('periode')
			->get();
		return view('mails.indexfacture')
			->with('lesperiodes',$lesperiodes);
	}


	public function dbGenerateChoix(){

		$lesperiodes = \DB::table('periode')
			->orderBy('id', 'desc')
			->get();

		return view('mails.choixgenerate')
			->with('lesperiodes',$lesperiodes);
	}

	public function sendMail($ide,$idp){

		$listbases = \DB::table('bases')
			->select('idsite')
			->where('id_editeur',$ide)
			->get();

		$editeurinfo = \DB::table('editeurs')
			->where('ids_editeur',$ide)
			->first();

		$listbases = array_pluck($listbases,'idsite');
		// var_dump($listbases);

		$listerem0 = \DB::table('ComptaByCampagneCsv')
			->where('id_periode',$idp)
			->whereIn('id_site_base',$listbases)
			->where('is_devalided',0)
			->get();

		$listerem1 = \DB::table('ComptaByCampagneCsv')
			->where('id_periode',$idp)
			->whereIn('id_site_base',$listbases)
			->where('is_devalided',1)
			->get();

		$reliquats = \DB::table('reliquats')
			->where('id_periode',$idp)
			->whereIn('id_site_base',$listbases)
//			->where('is_devalided',1)
			->get();

		$total0 = \DB::table('ComptaByCampagneCsv')
			->where('id_periode',$idp)
			->whereIn('id_site_base',$listbases)
			->where('is_devalided',0)
			->sum('com');

		$total1 = \DB::table('ComptaByCampagneCsv')
			->where('id_periode',$idp)
			->whereIn('id_site_base',$listbases)
			->where('is_devalided',1)
			->sum('devalid_com');

		$total = $total0 + $total1;

		$laperiodemail = \DB::table('periode')
			->where('id',$idp)
			->first();

		/*
		 * config / init / send
		 * with send in blue
		 */
		$sib_config = Configuration::getDefaultConfiguration()
			->setApiKey('api-key', env('SIB_KEY'));

		$sib_instance = new TransactionalEmailsApi(
			new Client(),
			$sib_config
		);


		/*
		 * init SIB params (dataset)
		 */
		$sender = new SIB\SendSmtpEmailSender(['email' => env('SIB_SENDER')]);
		// todo: replace with editor email when on prod
//		$to = env('SIB_RECIPIENT_TEST');
//		$recipient = new SIB\SendSmtpEmailTo(['email' => $to]);
		$to = env('SIB_RECIPIENT_TEST_1');
		$recipient1 = new SIB\SendSmtpEmailTo(['email' => $to]);
		$to = env('SIB_RECIPIENT_TEST_2');
		$recipient2 = new SIB\SendSmtpEmailTo(['email' => $to]);
		// todo: replace with editor email when on prod
		$subject = 'Appel à facture - ' . $laperiodemail->display_name;
		$html_content = view('mails.appelfacture')
			->with('listerem0',$listerem0)
			->with('listerem1',$listerem1)
			->with('total',$total)
			->with('editeurinfo',$editeurinfo)
			->with('reliquats',$reliquats)
			->with('periode', $laperiodemail)
			->render();

		/*
		 * link param and send mail
		 */
		$sendSmtpEmail = new SIB\SendSmtpEmail();
		$sendSmtpEmail->setSender($sender);
		// todo: double check on array
//		$sendSmtpEmail->setTo([$recipient]);;
		$sendSmtpEmail->setTo([$recipient1, $recipient2]);
		$sendSmtpEmail->setSubject($subject);
		$sendSmtpEmail->setHtmlContent($html_content);

		try {
			$result = $sib_instance->sendTransacEmail($sendSmtpEmail);
//			print_r($result);
		} catch (\Exception $e) {
			echo 'Exception when calling TransactionalEmailsApi->sendTransacEmail: ', $e->getMessage(), PHP_EOL;
			\Log::info($e->getMessage());
		}

		// faire le update

		$dataset = [
			'sent_at' => date("Y-m-d")
		];

		\DB::table('mails')
			->where('editeur_id_mail',$ide)
			->where('id_periode',$idp)
			->update($dataset);

		return redirect('/mails/editeurs/appelfacture/'.$idp);
	}

	public function sendBulkMail(Request $request)
	{
		$periode_id = $request->get('idp');
		$editor_ids = $request->get('ids');
		foreach ($editor_ids as $editor_id) {
			$this->sendMail($editor_id, $periode_id);
		}
		return 200;
	}
}
