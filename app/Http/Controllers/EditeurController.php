<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class EditeurController extends Controller
{

  public function __construct()
  {
      $this->middleware('auth');
  }

  public function index()
  {
    $editeurs = \DB::table('editeurs')
    ->get();
    return view('editeurs.index')
    ->with('editeurs',$editeurs);
  }

  public function create()
  {
      return view('editeurs.create');
  }

  public function edit($id)
  {
      $editeur = \DB::table('editeurs')
      ->where('id',$id)
      ->first();

      return view('editeurs.edit')
      ->with('editeur',$editeur);
  }

  public function update($id)
  {
      $fields = array_except(Input::all(), '_token');
      $dataset = [
      'ids_editeur' => $fields['ids_editeur'],

      'id_type' => 99,
      'type' => 99,

      'login' => '',

      'email' => $fields['email'],
      'technique' => $fields['technique'],
      'compta' => $fields['compta'],
      'skype' => $fields['skype'],
      'telephone' => $fields['telephone'],
      'cellulaire' => $fields['cellulaire'],
      'fax' => '',
      'nom_entreprise' => $fields['nom_entreprise'],
      'siret' => '',
      'tva' => '',
      'civ' => '',

      'nom' => $fields['nom'],
      'prenom' => $fields['prenom'],
      'adresse' => $fields['adresse'],
      'codepostale' => $fields['codepostale'],

      'ville' => $fields['ville'],
      'pays' => '',
      'racine' => '',

      'commercial' => 2,
      'statut' => 1,
      'paiement' => 50,
      'paiement_type' => '',
      'iban' => '',
      'swift' => '',
      'banque' => '',
      'commentaire' => '',
      'date_creation' => ''];

      \DB::table('editeurs')
      ->where('id',$id)
      ->update($dataset)
      ;
      return redirect('/editeurs');
  }

  public function store()
  {
      $fields = array_except(Input::all(), '_token');
      
      $dataset = [
      'ids_editeur' => $fields['ids_editeur'],

      'id_type' => 99,
      'type' => 99,

      'login' => '',

      'email' => $fields['email'],
      'technique' => $fields['technique'],
      'compta' => $fields['compta'],
      'skype' => $fields['skype'],
      'telephone' => $fields['telephone'],
      'cellulaire' => $fields['cellulaire'],
      'fax' => '',
      'nom_entreprise' => $fields['nom_entreprise'],
      'siret' => '',
      'tva' => '',
      'civ' => '',

      'nom' => $fields['nom'],
      'prenom' => $fields['prenom'],
      'adresse' => $fields['adresse'],
      'codepostale' => $fields['codepostale'],

      'ville' => $fields['ville'],
      'pays' => '',
      'racine' => '',

      'commercial' => 2,
      'statut' => 1,
      'paiement' => 50,
      'paiement_type' => '',
      'iban' => '',
      'swift' => '',
      'banque' => '',
      'commentaire' => '',
      'date_creation' => ''];

      \DB::table('editeurs')->insert($dataset);
      return redirect('/editeurs');
  }

	public function delete($id)
	{
		\DB::table('editeurs')
			->where('id',$id)
			->delete();

		return redirect('/editeurs');
	}

}
