<?php

namespace App\Http\Controllers;

use App\Http\CustomHelpers\CampagnePeriodeInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class CampagneController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function index()
	{
		$campagnes = \DB::table('campagnes')
			->orderBy('nom_campagne','asc')
			->get();
		$lastPeriode = \DB::table('periode')->orderByDesc('debut_periode')->first();
		return view('campagnes.index')
			->with('last_periode', $lastPeriode)
			->with('campagnes',$campagnes);
	}

	public function create()
	{
		$typerem = \DB::table('typerem')
			->get();
		return view('campagnes.create')
			->with('typerem',$typerem);
	}

	public function store()
	{
		$fields = array_except(Input::all(), '_token');
		if ($fields['rem_campagne'] < 0)
			return 'invalid negative rem value';

		$dataset[] = ['type_id_campagne' => $fields['type_id_campagne'],
			'nom_campagne' => $fields['nom_campagne'],
			'note_campagne' => $fields['note_campagne'],
			'type_id_campagne' => $fields['type_id_campagne'],
			'rem_campagne' => $fields['rem_campagne']];

		\DB::table('campagnes')->insert($dataset);
		return redirect('/campagnes');
	}


	public function edit($id)
	{
		$campagnes = \DB::table('campagnes')
			->where('id',$id)
			->first();

		$typerem = \DB::table('typerem')
			->get();

		return view('campagnes.edit')
			->with('campagnes',$campagnes)
			->with('typerem',$typerem);
	}

	public function update($id)
	{
		$fields = array_except(Input::all(), '_token');
		if ($fields['rem_campagne'] < 0)
			return 'invalid negative rem value';

		$dataset = ['type_id_campagne' => $fields['type_id_campagne'],
			'nom_campagne' => $fields['nom_campagne'],
			'note_campagne' => $fields['note_campagne'],
			'rem_campagne' => $fields['rem_campagne']];

		\DB::table('campagnes')
			->where('id',$id)
			->update($dataset);

		return redirect('/campagnes');
	}

	public function delete($id)
	{
		\DB::table('campagnes')
			->where('id',$id)
			->delete();

		return redirect('/campagnes');
	}

	public function infodefaultperiode($id,$idp)
	{
		$periode = \DB::table('periode')
			->where('id', $idp)
			->first();

		$periodes = \DB::table('periode')
			->orderByDesc('debut_periode')
			->limit(6)
			->get();

		$campagne = \DB::table('campagnes')
			->where('id', $id)
			->first();

		$stats = \DB::table('ComptaByCampagneCsv')
			->where('id_campagne',$id)
			->where('id_periode',$idp)
			->where('com', '>', 0)
			->orderBy('id_site_base')
			->get();
		$stats = $this->buildStatsArray($stats, $campagne);

		$reliquats = \DB::table('reliquats')
			->where('id_campagne',$id)
			->where('id_periode',$idp)
			->where('com', '>', 0)
			->get();
		$reliquats = $this->buildStatsArray($reliquats, $campagne);

		$devalidinfo = \DB::table('ComptaByCampagneCsv')
			->select('devalid_pourcent', 'devalid_nombre')
			->where('id_campagne',$id)
			->where('id_periode',$idp)
			->first();

		$reldevalidinfo = \DB::table('reliquats')
			->select('devalid_pourcent', 'devalid_nombre')
			->where('id_campagne',$id)
			->where('id_periode',$idp)
			->first();

		$totals = array();
		$totals['rem'] = $this->getColumnTotals($stats, $campagne->type_id_campagne);
		$totals['reliquats'] = $this->getColumnTotals($reliquats, $campagne->type_id_campagne);

		return view('campagnes.info3')
			->with('periode', $periode)
			->with('periodes', $periodes)
			->with('campagne', $campagne)
			->with('stats', $stats)
			->with('remduplist', $this->mark_duplicate_id($stats))
			->with('reliquats', $reliquats)
			->with('relduplist', $this->mark_duplicate_id($reliquats))
			->with('devalidinfo', $devalidinfo)
			->with('reldevalidinfo', $reldevalidinfo)
			->with('totals', $totals)
			;
	}

	private function buildStatsArray($stats, $campagne)
	{
		$arr = array();
		foreach ($stats as $stat) {
			$arr []= new CampagnePeriodeInfo($stat, $campagne);
		}
		return $arr;
	}

	private function mark_duplicate_id($stats)
	{
		$duplicate_list = array();
		foreach ($stats as $row) {
			$duplicate_list[$row->id] = false;
			foreach ($stats as $tmp){
				if ($tmp->base->idsite == $row->base->idsite
					&& $tmp->id != $row->id) {
					$duplicate_list[$row->id] = true;
				}
			}
		}

		return $duplicate_list;
	}

	private function getColumnTotals(array $stats, $rem_type_id)
	{
		$totalRem = 0;
		$totalRel = 0;
		$leadCountRem = 0;
		$leadCountRel = 0;
		foreach ($stats as $stat) {
			$totalRem += intval($stat->com);
			$totalRel += intval($stat->deval_com);
			$leadCountRem += $stat->nb;
			$leadCountRel += $stat->nb_deval;
		}
		return [$totalRem, $totalRel, $leadCountRem, $leadCountRel];
	}
}
