<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class ComptaController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth');
	}

	public function index()
	{
		return view('home');
	}


	public function devalidationProrataValue(){

		$pourcentageDevalid = 50;
		$totalValue = 6;


		$produitCroix = ($pourcentageDevalid / 100) * $totalValue;
		// $new_width = ($percentage / 100) * $totalWidth;

		var_dump($produitCroix);

		$percentage = 50;
		$totalWidth = 350;

		$new_width = ($percentage / 100) * $totalWidth;
		var_dump($new_width);

		// ajouter une colonne is_devalided pour aller remplir l'autre table
		// ajouter une table avec la nouvelle valeur et lier le devalided_id

		// from devalider % sur tel campagne + periode

		// option devalider les clics
		// ** cpm
		// ** cpl

		// table devalidation type ou dans campagne
		// + value

	}

	public function infoDevalidationCampagnePercent($id, $idp)
	{

		$campagne = \DB::table('campagnes')
			->where('id',$id)
			->first();

		$infoPeriode = \DB::table('ComptaByCampagneCsv')
			->select('id_periode')
			->where('id_campagne',$id)
			->distinct()
			->get();

		$infoPeriode = array_pluck($infoPeriode, 'id_periode');
		$periodeCampagne = \DB::table('periode')
			->whereIn('id',$infoPeriode)
			->get();

		$periodeFrom = \DB::table('periode')->where('id', $idp)->first();

		$typerem = \DB::table('typerem')
			->get();

		return view('campagnes.setdevalidation')
			->with('periodeCampagne',$periodeCampagne)
			->with('periodeFrom', $periodeFrom)
			->with('typerem',$typerem)
			->with('campagne',$campagne);

	}


	public function setDevalidationCampagnePercent($id){

		$fields = array_except(Input::all(), '_token');

		if ($fields['taux_devalidation'] < 0) {
			return "invalid negative % value";
		}

		$infocomptamodif = \DB::table('ComptaByCampagneCsv')
			->where('id_campagne',$id)
			->where('id_periode', $fields['id_periode'])
			->get();

		$campagne = \DB::table('campagnes')
			->where('id',$id)
			->first();

		// protéger si il y a déjà une davalidation
		// erase si il y a déjà un devalid nombre

		if ($fields['type_id_campagne'] == 1){
			// attention calcul différent avec le CPM
			foreach ($infocomptamodif as $inf) {
				\DB::table('ComptaByCampagneCsv')
					->where('id',$inf->id)
					->update([
						'devalid_CPM' => strval(ceil($this->reductionpercentage($fields['taux_devalidation'],intval($inf->CPM)))),
						'devalid_com' => strval(ceil($this->calculCPM(floor($this->reductionpercentage($fields['taux_devalidation'],$inf->CPM)),$campagne->rem_campagne)))
					]);
				// 'devalid_com'=> (floor($this->reductionpercentage($fields['taux_devalidation'],$inf->CPM)) * $campagne->rem_campagne)]);
				// round(1.95583, 2);
			}

		}
		if ($fields['type_id_campagne'] == 2){

			foreach ($infocomptamodif as $inf) {

				// \Log::info(json_encode($this->reductionpercentage($fields['taux_devalidation'],$inf->CPL), 0, PHP_ROUND_HALF_EVEN));


				\DB::table('ComptaByCampagneCsv')
					->where('id',$inf->id)
					->update([
						// 'devalid_CPL' => ceil( $this->reductionpercentage($fields['taux_devalidation'],$inf->CPL)),

						'devalid_CPL' => strval(round($this->reductionpercentage($fields['taux_devalidation'],$inf->CPL), 0, PHP_ROUND_HALF_EVEN)),
						'devalid_com'=> strval(round($this->reductionpercentage($fields['taux_devalidation'],$inf->CPL), 0, PHP_ROUND_HALF_EVEN) * $campagne->rem_campagne)
					]);
			}

		}
		if ($fields['type_id_campagne'] == 3){

			foreach ($infocomptamodif as $inf) {
				\DB::table('ComptaByCampagneCsv')
					->where('id',$inf->id)
					->update([
						'devalid_CPA' => strval(round($this->reductionpercentage($fields['taux_devalidation'],$inf->CPA), 0, PHP_ROUND_HALF_EVEN)),
						'devalid_com'=> strval(round($this->reductionpercentage($fields['taux_devalidation'],$inf->CPA), 0, PHP_ROUND_HALF_EVEN) * $campagne->rem_campagne)
					]);
			}

		}

		if ($fields['type_id_campagne'] == 4){

			foreach ($infocomptamodif as $inf) {
				\DB::table('ComptaByCampagneCsv')
					->where('id',$inf->id)
					->update([
						'devalid_CPC' => strval(round($this->reductionpercentage($fields['taux_devalidation'],$inf->CPC), 0, PHP_ROUND_HALF_EVEN)),
						'devalid_com'=> strval(round($this->reductionpercentage($fields['taux_devalidation'],$inf->CPC), 0, PHP_ROUND_HALF_EVEN) * $campagne->rem_campagne)
					]);
			}

		}

		\DB::table('ComptaByCampagneCsv')
			->where('id_campagne',$id)
			->where('id_periode', $fields['id_periode'])
			->update(['is_devalided'=>1,
				'devalid_pourcent' => $fields['taux_devalidation'],
				'devalid_nombre' => 0]);

		return redirect('/campagnes/info/'. $id .'/'. $fields['id_periode']);
	}


	public function infoDevalidationCampagneNb($id, $idp)
	{
		$campagne = \DB::table('campagnes')
			->where('id',$id)
			->first();

		$infoPeriode = \DB::table('ComptaByCampagneCsv')
			->select('id_periode')
			->where('id_campagne',$id)
			->distinct()
			->get();

		$infoPeriode = array_pluck($infoPeriode, 'id_periode');
		$periodeCampagne = \DB::table('periode')
			->whereIn('id',$infoPeriode)
			->get();

		$periodeFrom = \DB::table('periode')->where('id', $idp)->first();

		$typerem = \DB::table('typerem')
			->get();

		return view('campagnes.setdevalidationnb')
			->with('periodeFrom', $periodeFrom)
			->with('periodeCampagne',$periodeCampagne)
			->with('typerem',$typerem)
			->with('campagne',$campagne);
	}

	public function setDevalidationCampagneNb($id){

		$fields = array_except(Input::all(), '_token');

		if ($fields['taux_devalidation'] < 0) {
			return "invalid negative # value";
		}

		$infocomptamodif = \DB::table('ComptaByCampagneCsv')
			->where('id_campagne',$id)
			->where('id_periode', $fields['id_periode'])
			->get();

		$campagne = \DB::table('campagnes')
			->where('id',$id)
			->first();

		$valeuraenlenver =  intval($fields['taux_devalidation']);

		if ($fields['type_id_campagne'] == 1){

			$infodeleteclic = \DB::table('ComptaByCampagneCsv')
				->select('CPM')
				->where('id_campagne',$id)
				->sum('CPM');

			// attention calcul différent avec le CPM
			foreach ($infocomptamodif as $inf) {
				$res = intval($inf->CPM) - $valeuraenlenver;
				$res = $res > 0 ? $res : 0;
				$res = strval($res);
				\DB::table('ComptaByCampagneCsv')
					->where('id',$inf->id)
					->update([
						'devalid_CPM' => $res,
						'devalid_com' => ($this->calculCPM($res ,$campagne->rem_campagne))
					]);
			}

		}
		if ($fields['type_id_campagne'] == 2){

			$infodeleteclic = \DB::table('ComptaByCampagneCsv')
				->select('CPL')
				->where('id_campagne',$id)
				->sum('CPL');

			foreach ($infocomptamodif as $inf) {
				$res = intval($inf->CPL) - $valeuraenlenver;
				$res = $res > 0 ? $res : 0;
				$res = strval($res);
				\DB::table('ComptaByCampagneCsv')
					->where('id',$inf->id)
					->update([
						'devalid_CPL' => $res,
						'devalid_com' => ($res * $campagne->rem_campagne)
					]);
			}

		}
		if ($fields['type_id_campagne'] == 3){

			$infodeleteclic = \DB::table('ComptaByCampagneCsv')
				->select('CPA')
				->where('id_campagne',$id)
				->sum('CPA');

			foreach ($infocomptamodif as $inf) {
				$res = intval($inf->CPA) - $valeuraenlenver;
				$res = $res > 0 ? $res : 0;
				$res = strval($res);
				\DB::table('ComptaByCampagneCsv')
					->where('id',$inf->id)
					->update([
						'devalid_CPA' => $res,
						'devalid_com' => ($res * $campagne->rem_campagne)
					]);
			}

		}

		if ($fields['type_id_campagne'] == 4){

			$infodeleteclic = \DB::table('ComptaByCampagneCsv')
				->select('CPC')
				->where('id_campagne',$id)
				->sum('CPC');

			foreach ($infocomptamodif as $inf) {
				$res = intval($inf->CPC) - $valeuraenlenver;
				$res = $res > 0 ? $res : 0;
				$res = strval($res);
				\DB::table('ComptaByCampagneCsv')
					->where('id',$inf->id)
					->update([
						'devalid_CPC' => $res,
						'devalid_com' => ($res * $campagne->rem_campagne)
					]);
			}

		}

		\DB::table('ComptaByCampagneCsv')
			->where('id_campagne',$id)
			->where('id_periode', $fields['id_periode'])
			->update(['is_devalided'=>1,
				'devalid_pourcent' => 0,
				'devalid_nombre' => $fields['taux_devalidation']]);

		return redirect('/campagnes/info/'. $id .'/'. $fields['id_periode']);
	}


	// brouillon

	public function setDevalidationCampagneNombreClic(){

		$infodeleteclic = \DB::table('ComptaByCampagneCsv')
			->select('cpm')
			->where('id_campagne',1)
			->sum('cpm');

		$devalidnombre = 2000;

		$ourcent =  $this->pourcentage($devalidnombre, $infodeleteclic);

		var_dump($infodeleteclic);
		echo $ourcent.'%';


		$deducpourcentage = \DB::table('ComptaByCampagneCsv')
			->select('cpm')
			->where('id_campagne',1)
			->get();

		foreach ($deducpourcentage as $d) {
			// var_dump($d);
			$valeuraenlenver = ($ourcent / 100) * $d->cpm;
			var_dump($valeuraenlenver);
		}

	}

	public function pourcentage($Nombre, $Total) {
		return $Nombre * 100 / $Total;
	}

	public function reductionpercentage($percentage,$valeurdebase){
		// return ($percentage / 100) * $valeurdebase;
		return ($valeurdebase * (1 - $percentage/100));
	}

	public function calculCPM($nbimpression,$valuecpm){

		$cost = ($nbimpression * $valuecpm) / 1000;
		return $cost;
	}


	public function deletedevalid($id,$idp){
		// echo $id;
		\DB::table('ComptaByCampagneCsv')
			->where('id_campagne',$id)
			->where('id_periode',$idp)
			->update(['is_devalided'=>0,
				'devalid_pourcent' => NULL,
				'devalid_nombre' => 0,
				'devalid_com' => NULL,

				'devalid_CPA' => NULL,
				'devalid_CPC' => NULL,
				'devalid_CPM' => NULL,
				'devalid_CPL' => NULL]);

		return redirect("/campagnes/info/$id/$idp");
	}

	public function deleterowrem($idrow,$ic,$idp){

		\DB::table('ComptaByCampagneCsv')
			->where('id',$idrow)
			->delete();

		return redirect('/campagnes/info/'.$ic.'/'.$idp);

	}


	public function toreliquat($idrow,$ic,$idp){

		$typecampagne = \DB::table('campagnes')
			->select('type_id_campagne')
			->where('id',$ic)
			->first();

		$row = \DB::table('ComptaByCampagneCsv')
			->where('id',$idrow)
			->first();

		// cpm
		if($typecampagne->type_id_campagne == 1){

			$today = date("Y-m-d H:i:s");
			$dataset[] = [
				'id_site_base' => $row->id_site_base,
				'id_periode' => $row->id_periode,
				'id_campagne' => $row->id_campagne,
				'id_fichier' => 0,

				'CPM' => $row->CPM,
				'com' => $row->com,

				'id_periode_reference' => $row->id_periode,
				'created_at' => $today,
				'updated_at' => $today
			];

		}
		// CPL
		if($typecampagne->type_id_campagne == 2){

			$today = date("Y-m-d H:i:s");
			$dataset[] = [
				'id_site_base' => $row->id_site_base,
				'id_periode' => $row->id_periode,
				'id_campagne' => $row->id_campagne,
				'id_fichier' => 0,

				'CPL' => $row->CPL,
				'com' => $row->com,

				'id_periode_reference' => $row->id_periode,
				'created_at' => $today,
				'updated_at' => $today
			];

		}
		//CPA
		if($typecampagne->type_id_campagne == 3){

			$today = date("Y-m-d H:i:s");
			$dataset[] = [
				'id_site_base' => $row->id_site_base,
				'id_periode' => $row->id_periode,
				'id_campagne' => $row->id_campagne,
				'id_fichier' => 0,

				'CPA' => $row->CPA,
				'com' => $row->com,

				'id_periode_reference' => $row->id_periode,
				'created_at' => $today,
				'updated_at' => $today
			];
		}
		// CPC
		if($typecampagne->type_id_campagne == 4){

			$today = date("Y-m-d H:i:s");
			$dataset[] = [
				'id_site_base' => $row->id_site_base,
				'id_periode' => $row->id_periode,
				'id_campagne' => $row->id_campagne,
				'id_fichier' => 0,

				'CPC' => $row->CPC,
				'com' => $row->com,

				'id_periode_reference' => $row->id_periode,
				'created_at' => $today,
				'updated_at' => $today
			];

		}

		\DB::table('reliquats')->insert($dataset);

		\DB::table('ComptaByCampagneCsv')
			->where('id',$idrow)
			->delete();

		return redirect('/campagnes/info/'.$ic.'/'.$idp);

	}

	public function editrem($idrow,$ic,$idp){

		// form + view
		$row = \DB::table('ComptaByCampagneCsv')
			->where('id',$idrow)
			->first();

		$campagne = \DB::table('campagnes')
			->where('id',$ic)
			->first();

		$periode = \DB::table('periode')
			->where('id',$idp)
			->first();

		$base = \DB::table('bases')
			->where('idsite',$row->id_site_base)
			->first();

		$editeur = \DB::table('editeurs')
			->where('ids_editeur',$base->id_editeur)
			->first();

		return view('campagnes.editrem')
			->with('row',$row)
			->with('base',$base)
			->with('campagne',$campagne)
			->with('editeur',$editeur)
			->with('periode',$periode);

	}

	public function seteditrem(){

		$fields = array_except(Input::all(), '_token');
//		var_dump($fields);

		$lacampagne = \DB::table('campagnes')
			->where('id',$fields['campagne_id'])
			->first();

		// var_dump($lacampagne);

		/*if($lacampagne->type_id_campagne == 1){
		  $newrem = $datasetfields['CPL'] * $lacampagne->rem_campagne;

		}
		//   CPM
		*/
		if($lacampagne->type_id_campagne == 2){
			if ($fields['CPL'] < 0)
				return 'invalid negative # value';

			$newrem = $fields['CPL'] * $lacampagne->rem_campagne;
			\DB::table('ComptaByCampagneCsv')
				->where('id',$fields['idrow'])
				->update(['CPL'=>$fields['CPL'],
					'com' => $newrem]);
		}
		//  CPL

		if($lacampagne->type_id_campagne == 3){
			if ($fields['CPA'] < 0)
				return 'invalid negative # value';

			$newrem = $fields['CPA'] * $lacampagne->rem_campagne;

			\DB::table('ComptaByCampagneCsv')
				->where('id',$fields['idrow'])
				->update(['CPA'=>$fields['CPA'],
					'com' => $newrem]);
		}
		//  CPA

		if($lacampagne->type_id_campagne == 4){
			if ($fields['CPC'] < 0)
				return 'invalid negative # value';

			$newrem = $fields['CPC'] * $lacampagne->rem_campagne;

			\DB::table('ComptaByCampagneCsv')
				->where('id',$fields['idrow'])
				->update(['CPC'=>$fields['CPC'],
					'com' => $newrem]);

		}
		//  CPC
		return redirect('campagnes/info/'.$fields['campagne_id'].'/'.$fields['idp']);


	}

	public function editremdevalid($idrow,$ic,$idp){
		// echo 'editremdevalid';

		$row = \DB::table('ComptaByCampagneCsv')
			->where('id',$idrow)
			->first();

		$campagne = \DB::table('campagnes')
			->where('id',$ic)
			->first();

		$periode = \DB::table('periode')
			->where('id',$idp)
			->first();

		$base = \DB::table('bases')
			->where('idsite',$row->id_site_base)
			->first();

		$editeur = \DB::table('editeurs')
			->where('ids_editeur',$base->id_editeur)
			->first();

		return view('campagnes.editremdevalid')
			->with('row',$row)
			->with('base',$base)
			->with('campagne',$campagne)
			->with('editeur',$editeur)
			->with('periode',$periode);
	}

	public function seteditremdevalid(){

		// echo 'setremdevalid';

		$fields = array_except(Input::all(), '_token');
//		var_dump($fields);

		// taux devalid

		$lacampagne = \DB::table('campagnes')
			->where('id',$fields['campagne_id'])
			->first();

		if($lacampagne->type_id_campagne == 2){
			if ($fields['CPL'] < 0)
				return 'invalid negative # value';

			$newrem = $fields['CPL'] * $lacampagne->rem_campagne;
			\DB::table('ComptaByCampagneCsv')
				->where('id',$fields['idrow'])
				->update(['devalid_CPL'=>$fields['CPL'],
					'devalid_com' => $newrem]);
		}

		if($lacampagne->type_id_campagne == 3){
			if ($fields['CPA'] < 0)
				return 'invalid negative # value';

			$newrem = $fields['CPA'] * $lacampagne->rem_campagne;

			\DB::table('ComptaByCampagneCsv')
				->where('id',$fields['idrow'])
				->update(['devalid_CPA'=>$fields['CPA'],
					'devalid_com' => $newrem]);
		}

		if($lacampagne->type_id_campagne == 4){
			if ($fields['CPC'] < 0)
				return 'invalid negative # value';

			$newrem = $fields['CPC'] * $lacampagne->rem_campagne;

			\DB::table('ComptaByCampagneCsv')
				->where('id',$fields['idrow'])
				->update(['devalid_CPC'=>$fields['CPC'],
					'devalid_com' => $newrem]);

		}

		return redirect('campagnes/info/'.$fields['campagne_id'].'/'.$fields['idp']);

	}

	public function deleterowreliq($idrow,$ic,$idp){

		\DB::table('reliquats')
			->where('id',$idrow)
			->delete();

		return redirect('/campagnes/info/'.$ic.'/'.$idp);

	}

	public function editreliquat($idrow,$ic,$idp){

		$row = \DB::table('reliquats')
			->where('id',$idrow)
			->first();

		$campagne = \DB::table('campagnes')
			->where('id',$ic)
			->first();

		$periode = \DB::table('periode')
			->where('id',$idp)
			->first();

		$base = \DB::table('bases')
			->where('id',$row->id_site_base)
			->first();

		$editeur = \DB::table('editeurs')
			->where('id',$base->id_editeur)
			->first();

		return view('campagnes.editReliquat')
			->with('row',$row)
			->with('base',$base)
			->with('campagne',$campagne)
			->with('editeur',$editeur)
			->with('periode',$periode);
	}

	public function seteditreliquat(){

		$fields = array_except(Input::all(), '_token');
		var_dump($fields);

		// taux devalid

		$lacampagne = \DB::table('campagnes')
			->where('id',$fields['campagne_id'])
			->first();

		if($lacampagne->type_id_campagne == 2){
			$newrem = strval($fields['CPL'] * $lacampagne->rem_campagne);
			\DB::table('reliquats')
				->where('id',$fields['idrow'])
				->update(['CPL'=>$fields['CPL'],
					'com' => $newrem]);
		}

		if($lacampagne->type_id_campagne == 3){
			$newrem = strval($fields['CPA'] * $lacampagne->rem_campagne);

			\DB::table('reliquats')
				->where('id',$fields['idrow'])
				->update(['CPA'=>$fields['CPA'],
					'com' => $newrem]);
		}

		if($lacampagne->type_id_campagne == 4){
			$newrem = strval($fields['CPC'] * $lacampagne->rem_campagne);

			\DB::table('reliquats')
				->where('id',$fields['idrow'])
				->update(['CPC'=>$fields['CPC'],
					'com' => $newrem]);

		}

		return redirect('campagnes/info/'.$fields['campagne_id'].'/'.$fields['idp']);

	}

	public function editReliquatDevalid($idrow,$ic,$idp){

		$row = \DB::table('reliquats')
			->where('id',$idrow)
			->first();

		$campagne = \DB::table('campagnes')
			->where('id',$ic)
			->first();

		$periode = \DB::table('periode')
			->where('id',$idp)
			->first();

		$base = \DB::table('bases')
			->where('id',$row->id_site_base)
			->first();

		$editeur = \DB::table('editeurs')
			->where('id',$base->id_editeur)
			->first();

		return view('campagnes.editReliquatDevalid')
			->with('row',$row)
			->with('base',$base)
			->with('campagne',$campagne)
			->with('editeur',$editeur)
			->with('periode',$periode);
	}

	public function setEditReliquatDevalid(){

		$fields = array_except(Input::all(), '_token');
		var_dump($fields);

		// taux devalid

		$lacampagne = \DB::table('campagnes')
			->where('id',$fields['campagne_id'])
			->first();

		if($lacampagne->type_id_campagne == 2){
			$newrem = strval($fields['CPL'] * $lacampagne->rem_campagne);
			\DB::table('reliquats')
				->where('id',$fields['idrow'])
				->update(['devalid_CPL'=>$fields['CPL'],
					'devalid_com' => $newrem]);
		}

		if($lacampagne->type_id_campagne == 3){
			$newrem = strval($fields['CPA'] * $lacampagne->rem_campagne);

			\DB::table('reliquats')
				->where('id',$fields['idrow'])
				->update(['devalid_CPA'=>$fields['CPA'],
					'devalid_com' => $newrem]);
		}

		if($lacampagne->type_id_campagne == 4){
			$newrem = strval($fields['CPC'] * $lacampagne->rem_campagne);

			\DB::table('reliquats')
				->where('id',$fields['idrow'])
				->update(['devalid_CPC'=>$fields['CPC'],
					'devalid_com' => $newrem]);

		}

		return redirect('campagnes/info/'.$fields['campagne_id'].'/'.$fields['idp']);

	}

	public function addremcampagneperiode($ic,$idp){

		$bases = \DB::table('bases')
			->orderBy('titre','asc')
			->get();

		$campagne = \DB::table('campagnes')
			->where('id',$ic)
			->first();

		$periode = \DB::table('periode')
			->where('id',$idp)
			->first();

		return view('campagnes.addrem')
			->with('bases',$bases)
			->with('campagne',$campagne)
			->with('periode',$periode);


	}


	public function addremcampagneperiodego(){

		$fields = array_except(Input::all(), '_token');
		$lacampagne = \DB::table('campagnes')
			->where('id',$fields['campagne_id'])
			->first();

		// recup info base et editeur


		if($lacampagne->type_id_campagne == 2){
			if ($fields['CPL'] < 0)
				return 'invalid negative # value';

			$newrem = $fields['CPL'] * $lacampagne->rem_campagne;
			$dataset[] = [

				'com' => $newrem,
				'id_periode' => $fields['idp'],
				'id_campagne' => $fields['campagne_id'],
				'id_fichier' => 1,
				'id_site_base' => $fields['base_id'],
				'CPL' => $fields['CPL'],
				'CPC' => 0,
				'CPA' => 0,
				'CPM' => 0,
				'devalid_CPC' => NULL,
				'devalid_CPM' => NULL,
				'devalid_CPL' => NULL,
				'devalid_CPA' => NULL,
				'devalid_com' => NULL,
				'devalid_nombre' => 0,
				'is_devalided' => 0,
				'created_at' => date("Y-m-d H:i:s"),
				'updated_at' => date("Y-m-d H:i:s")
			];

			\DB::table('ComptaByCampagneCsv')->insert($dataset);
		}

		if($lacampagne->type_id_campagne == 3){
			if ($fields['CPA'] < 0)
				return 'invalid negative # value';
			$newrem = $fields['CPA'] * $lacampagne->rem_campagne;

			$dataset[] = [

				'com' => $newrem,
				'id_periode' => $fields['idp'],
				'id_campagne' => $fields['campagne_id'],
				'id_fichier' => 1,
				'id_site_base' => $fields['base_id'],
				'CPL' => 0,
				'CPC' => 0,
				'CPA' => $fields['CPA'],
				'CPM' => 0,
				'devalid_CPC' => NULL,
				'devalid_CPM' => NULL,
				'devalid_CPL' => NULL,
				'devalid_CPA' => NULL,
				'devalid_com' => NULL,
				'devalid_nombre' => 0,
				'is_devalided' => 0,
				'created_at' => date("Y-m-d H:i:s"),
				'updated_at' => date("Y-m-d H:i:s")
			];

			\DB::table('ComptaByCampagneCsv')->insert($dataset);
		}

		if($lacampagne->type_id_campagne == 4){
			if ($fields['CPC'] < 0)
				return 'invalid negative # value';

			$newrem = $fields['CPC'] * $lacampagne->rem_campagne;

			$dataset[] = [
				'com' => $newrem,
				'id_periode' => $fields['idp'],
				'id_campagne' => $fields['campagne_id'],
				'id_fichier' => 1,
				'id_site_base' => $fields['base_id'],
				'CPL' => 0,
				'CPC' => $fields['CPC'],
				'CPA' => 0,
				'CPM' => 0,
				'devalid_CPC' => NULL,
				'devalid_CPM' => NULL,
				'devalid_CPL' => NULL,
				'devalid_CPA' => NULL,
				'devalid_com' => NULL,
				'devalid_nombre' => 0,
				'is_devalided' => 0,
				'created_at' => date("Y-m-d H:i:s"),
				'updated_at' => date("Y-m-d H:i:s")
			];

			\DB::table('ComptaByCampagneCsv')->insert($dataset);
		}

		return redirect('campagnes/info/'.$fields['campagne_id'].'/'.$fields['idp']);

	}

	public function addReliquatCampagnePeriode($ic,$idp)
	{
		$bases = \DB::table('bases')
			->orderBy('titre','asc')
			->get();

		$campagne = \DB::table('campagnes')
			->where('id',$ic)
			->first();

		$periode = \DB::table('periode')
			->where('id',$idp)
			->first();

		$periodes = \DB::table('periode')
			->get();

		return view('campagnes.add_reliquat')
			->with('bases',$bases)
			->with('campagne',$campagne)
			->with('periode',$periode)
			->with('periodes',$periodes);
	}

	public function addReliquatCampagnePeriodeGo()
	{
		$fields = array_except(Input::all(), '_token');
		$lacampagne = \DB::table('campagnes')
			->where('id',$fields['campagne_id'])
			->first();

		// recup info base et editeur

		$dataset = [
			'id_campagne' => $fields['campagne_id'],
			'id_site_base' => $fields['base_id'],
			'id_periode' => $fields['idp'],
			'id_periode_reference' => $fields['id_periode_reference'],
			'CPC' => 0, // replaced later
			'CPL' => 0, // replaced later
			'CPA' => 0, // replaced later
			'CPM' => 0, // replaced later
			'com' => 0, // replaced later
			'id_fichier' => 0,
			'devalid_CPC' => null,
			'devalid_CPL' => null,
			'devalid_CPA' => null,
			'devalid_CPM' => null,
			'devalid_com' => null,
			'devalid_id' => 0,
			'devalid_pourcent' => 0,
			'devalid_nombre' => 0,
			'is_devalided' => 0,
			'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s")
		];


		if($lacampagne->type_id_campagne == 2){
			if ($fields['CPL'] < 0)
				return 'invalid negative # value';
			$newrem = $fields['CPL'] * $lacampagne->rem_campagne;

			$dataset['com'] = strval($newrem);
			$dataset['CPL'] = $fields['CPL'];
		}

		if($lacampagne->type_id_campagne == 3){
			if ($fields['CPA'] < 0)
				return 'invalid negative # value';
			$newrem = $fields['CPA'] * $lacampagne->rem_campagne;

			$dataset['com'] = strval($newrem);
			$dataset['CPA'] = $fields['CPA'];
		}

		if($lacampagne->type_id_campagne == 4){
			if ($fields['CPC'] < 0)
				return 'invalid negative # value';
			$newrem = $fields['CPC'] * $lacampagne->rem_campagne;

			$dataset['com'] = strval($newrem);
			$dataset['CPC'] = $fields['CPC'];
		}

		\DB::table('reliquats')->insert([$dataset]);

		return redirect('campagnes/info/'.$fields['campagne_id'].'/'.$fields['idp']);
	}

	/*
	 * reliquats - devalid percent
	 */
	public function devalidReliquatPercent($ic, $idp)
	{
		$campagne = \DB::table('campagnes')
			->where('id',$ic)
			->first();

		$infoPeriode = \DB::table('ComptaByCampagneCsv')
			->select('id_periode')
			->where('id_campagne',$ic)
			->distinct()
			->get();

		$infoPeriode = array_pluck($infoPeriode, 'id_periode');
		$periodeCampagne = \DB::table('periode')
			->whereIn('id',$infoPeriode)
			->get();

		$periodeFrom = \DB::table('periode')->where('id', $idp)->first();

		$typerem = \DB::table('typerem')
			->get();

		return view('campagnes.setReliquatDevalidation', [
			'campagne' => $campagne,
			'id_periode' => $idp,
			'periodeFrom' => $periodeFrom,
			'id_rem_type' => $campagne->type_id_campagne,
			'typerem' => $typerem,
			'periodeCampagne' => $periodeCampagne,
		]);
	}

	public function devalidReliquatPercentGo($ic, $idp)
	{
		$fields = array_except(Input::all(), '_token');

		if ($fields['taux_devalidation'] < 0) {
			return 'invalid negative value';
		}

		$reliquats = \DB::table('reliquats')
			->where('id_campagne', $ic)
			->where('id_periode', $idp)
			->get();

		$campagne = \DB::table('campagnes')
			->where('id', $ic)
			->first();

		if ($fields['type_id_campagne'] == 1) {
			foreach ($reliquats as $reliquat) {
				\DB::table('reliquats')
					->where('id', $reliquat->id)
					->update([
						'devalid_CPM' => strval(ceil($this->reductionpercentage($fields['taux_devalidation'],intval($reliquat->CPM)))),
						'devalid_com' => strval(ceil($this->calculCPM(floor($this->reductionpercentage($fields['taux_devalidation'],$reliquat->CPM)),$campagne->rem_campagne)))
					]);
			}
		}

		if ($fields['type_id_campagne'] == 2) {
			foreach ($reliquats as $reliquat) {
				\DB::table('reliquats')
					->where('id', $reliquat->id)
					->update([
						'devalid_CPL' => strval(round($this->reductionpercentage($fields['taux_devalidation'],$reliquat->CPL), 0, PHP_ROUND_HALF_EVEN)),
						'devalid_com'=> strval(round($this->reductionpercentage($fields['taux_devalidation'],$reliquat->CPL), 0, PHP_ROUND_HALF_EVEN) * $campagne->rem_campagne)
					]);
			}
		}

		if ($fields['type_id_campagne'] == 3) {
			foreach ($reliquats as $reliquat){
				\DB::talbe('reliquats')
					->with('id', $reliquat->id)
					->update([
						'devalid_CPA' => strval(round($this->reductionpercentage($fields['taux_devalidation'],$reliquat->CPA), 0, PHP_ROUND_HALF_EVEN)),
						'devalid_com'=> strval(round($this->reductionpercentage($fields['taux_devalidation'],$reliquat->CPA), 0, PHP_ROUND_HALF_EVEN) * $campagne->rem_campagne)
					]);
			}
		}

		if ($fields['type_id_campagne'] == 4) {
			foreach ($reliquats as $reliquat) {
				\DB::table('reliquats')
					->where('id', $reliquat->id)
					->update([
						'devalid_CPC' => strval(round($this->reductionpercentage($fields['taux_devalidation'], $reliquat->CPC), 0, PHP_ROUND_HALF_EVEN)),
						'devalid_com' => strval(round($this->reductionpercentage($fields['taux_devalidation'], $reliquat->CPC), 0, PHP_ROUND_HALF_EVEN) * $campagne->rem_campagne)
					]);
			}
		}

		\DB::table('reliquats')
			->where('id_campagne', $ic)
			->where('id_periode', $idp)
			->update([
				'is_devalided' => 1,
				'devalid_pourcent' => $fields['taux_devalidation'],
				'devalid_nombre' => 0
			]);

		return redirect('campagnes/info/'.$ic.'/'.$idp);
	}

	/*
	 * Reliquats - Devalid NB
	 */
	public function devalidReliquatNb($ic, $idp)
	{
		$campagne = \DB::table('campagnes')
			->where('id',$ic)
			->first();

		$infoPeriode = \DB::table('ComptaByCampagneCsv')
			->select('id_periode')
			->where('id_campagne',$ic)
			->distinct()
			->get();

		$infoPeriode = array_pluck($infoPeriode, 'id_periode');
		$periodeCampagne = \DB::table('periode')
			->whereIn('id',$infoPeriode)
			->get();

		$periodeFrom = \DB::table('periode')->where('id', $idp)->first();

		$typerem = \DB::table('typerem')
			->get();

		return view('campagnes.setReliquatDevalidationnb', [
			'campagne' => $campagne,
			'id_periode' => $idp,
			'periodeFrom' => $periodeFrom,
			'id_rem_type' => $campagne->type_id_campagne,
			'typerem' => $typerem,
			'periodeCampagne' => $periodeCampagne,
		]);
	}

	public function devalidReliquatNbGo($ic, $idp)
	{
		$fields = array_except(Input::all(), '_token');

		if ($fields['taux_devalidation'] < 0) {
			return "invalid negative value";
		}

		$reliquats = \DB::table('reliquats')
			->where('id_campagne', $ic)
			->where('id_periode', $idp)
			->get();

		$campange = \DB::table('campagnes')
			->where('id', $ic)
			->first();


		$remval = intval($fields['taux_devalidation']);

		// type [1:4] == CPM : CPL : CPA : CPC
		if ($fields['type_id_campagne'] == 1) {
			foreach ($reliquats as $reliquat) {
				$cpm = intval($reliquat->CPM);
				$res = $cpm - $remval;
				$res = $res > 0 ? $res : 0;
				$com = $res * intval($campange->rem_campagne);
				\DB::table('reliquats')
					->where('id', $reliquat->id)
					->update([
						'devalid_CPM' => strval($res),
						'devalid_com' => strval($com),
					]);
			}
		}
		if ($fields['type_id_campagne'] == 2) {
			foreach ($reliquats as $reliquat) {
				$cpm = intval($reliquat->CPL);
				$res = $cpm - $remval;
				$res = $res > 0 ? $res : 0;
				$com = $res * intval($campange->rem_campagne);
				\DB::table('reliquats')
					->where('id', $reliquat->id)
					->update([
						'devalid_CPL' => strval($res),
						'devalid_com' => strval($com),
					]);
			}
		}
		if ($fields['type_id_campagne'] == 3) {
			foreach ($reliquats as $reliquat) {
				$cpm = intval($reliquat->CPA);
				$res = $cpm - $remval;
				$res = $res > 0 ? $res : 0;
				$com = $res * intval($campange->rem_campagne);
				\DB::table('reliquats')
					->where('id', $reliquat->id)
					->update([
						'devalid_CPA' => strval($res),
						'devalid_com' => strval($com),
					]);
			}
		}
		if ($fields['type_id_campagne'] == 4) {
			foreach ($reliquats as $reliquat) {
				$cpm = intval($reliquat->CPC);
				$res = $cpm - $remval;
				$res = $res > 0 ? $res : 0;
				$com = $res * intval($campange->rem_campagne);
				\DB::table('reliquats')
					->where('id', $reliquat->id)
					->update([
						'devalid_CPC' => strval($res),
						'devalid_com' => strval($com),
					]);
			}
		}

		\DB::table('reliquats')
			->where('id_campagne',$ic)
			->where('id_periode', $idp)
			->update([
				'is_devalided' =>1,
				'devalid_pourcent' => 0,
				'devalid_nombre' => $fields['taux_devalidation']
				]);

		return redirect('campagnes/info/'.$ic.'/'.$idp);
	}
}
