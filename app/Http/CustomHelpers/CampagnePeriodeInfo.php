<?php


namespace App\Http\CustomHelpers;


class CampagnePeriodeInfo
{
	public $id;
	public $rem_type;
	public $base;
	public $editeur;
	public $nb;
	public $rem;
	public $com;
	public $deval_com;

	public function __construct($stat, $campagne)
	{
		$remType = \DB::table('typerem')->where('id', $campagne->type_id_campagne)->first();
		$base = \DB::table('bases')->where('idsite', $stat->id_site_base)->first();
		$editeur = \DB::table('editeurs')->where('ids_editeur', $base->id_editeur)->first();
		$remTypeName = $remType->type;
		$devalRemName = 'devalid_' . $remTypeName;

		$this->id = $stat->id;
		$this->rem_type = $remType->type;
		$this->base = $base;
		$this->editeur = $editeur;
		$this->nb = $stat->$remTypeName;
		$this->nb_deval = $stat->$devalRemName;
		$this->rem = $campagne->rem_campagne;
		$this->com = $stat->com;
		$this->deval_com = $stat->devalid_com;
	}
}