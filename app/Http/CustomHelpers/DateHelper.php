<?php


namespace App\Http\CustomHelpers;


class DateHelper
{
	public static function getFrenchMonthName($month) {
		$frenchMonths = [
			'January' => 'Janvier',
			'February' => 'Février',
			'March' => 'Mars',
			'April' => 'Avril',
			'May' => 'Mai',
			'June' => 'Juin',
			'July' => 'Juillet',
			'August' => 'Août',
			'September' => 'Septembre',
			'October' => 'Octobre',
			'November' => 'Novembre',
			'December' => 'Décembre',
		];
		return $frenchMonths[$month];
	}
}