<?php


namespace App\Http\CustomHelpers;


class CampaignHelper
{
	public static function getRemTypeIdByString($str) {
		$remlist = ['CPM' => 1, 'CPL' => 2, 'CPA' => 3, 'CPC' => 4];
		if (array_key_exists($str, $remlist))
			return $remlist[$str];
		return false;
	}

}