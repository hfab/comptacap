<?php


namespace App\Http\CustomHelpers;


class CsvHandler
{
	private $filepath;
	private $filename;

	private $header;
	private $content;
	private $delimiter;
	private $keyValArray = NULL;
	private $error = array();

	/**
	 * @return mixed
	 */
	public function getHeader()
	{
		return $this->header;
	}

	/**
	 * @return mixed
	 */
	public function getContent($len = 0)
	{
		if ($len === 0)
			return $this->content;
		return array_slice($this->content, 0, $len);
	}

	/**
	 * @return array|null
	 */
	public function getError()
	{
		if (count($this->error))
			return $this->error;
		return null;
	}

	/**
	 * @param string $error
	 */
	public function addError(string $error)
	{
		$this->error []= $error;
	}

	/**
	 * @return mixed
	 */
	public function getKeyValArray()
	{
		if (is_null($this->keyValArray))
			$this->generateKeyValueArray();
		return $this->keyValArray;
	}

	public function __construct($filepath)
	{
		if (!file_exists($filepath))
			return NULL;
		$this->filepath = $filepath;
		$this->filename = array_slice(explode('/', $filepath), -1)[0];
		$this->extractData();
	}

	private function extractData()
	{
		$fh = fopen($this->filepath, 'r');
		if ($fh === false)
			return 1;

		$rawHeader = fgets($fh);
		$rawHeader = trim($rawHeader, " \n\r\t\v\0;,");
//		$this->delimiter = strpos($rawHeader, ';') ? ';'
//			: strpos($rawHeader, ',') ? ','
//				: "err";
//		if ($this->delimiter === "err")
//			throw new \Exception('no delimiter found', 2);
		if (strpos($rawHeader, ';'))
			$this->delimiter = ';';
		else if (strpos($rawHeader, ','))
			$this->delimiter = ',';
		else
			throw new \Exception('no delimiter found', 2);
		$this->header = $this->getRow($rawHeader);
		$header_size = count($this->header);
		$line_count = 1;

		$this->content = array();
		while ($line = htmlspecialchars_decode(trim(fgets($fh)))) {
			$line_count++;
			while (substr_count($line, $this->delimiter) + 1 < $header_size) {
				$line .= htmlspecialchars_decode(trim(fgets($fh)));
			}
			$line = trim($line, ';');
			if (substr_count($line, $this->delimiter) + 1 !== $header_size) {
				$this->error []= "Skipped L." . $line_count . ": may cause problem : [$line]";
				continue;
			}
			if (mb_detect_encoding($line) != 'ASCII') {
				$line = mb_convert_encoding($line, 'UTF-8');
//				$this->error []= "Skipped L." . $line_count . ": unsupported encoding : [$line]";
//				continue;
			}
			$this->content []= $this->getRow($line);
		}
//		if (count($this->error) != 0)
//			$this->error []= "These errors are most likely due to extra '" . $this->delimiter . "' in table.";
		return 0;
	}

	private function getRow($line)
	{
		$line = trim($line, " \n\r\t\v\0;,");
		$row = explode($this->delimiter, $line);
		foreach ($row as $key => $value) {
			$row[$key] = trim($value, " \n\r\t\v\0\"");
		}
		return $row;
	}

	private function generateKeyValueArray()
	{
		$header_size = count($this->header);
		$table = array();
		foreach ($this->content as $row) {
			$data = array();
			if (count($this->header) != count($row)) {
				\Log::info($this->header);
				\Log::info($row);
			}
			for ($idx = 0; $idx < $header_size; $idx++) {
				$data[$this->header[$idx]] = $row[$idx];
			}
			$table []= $data;
		}
		$this->keyValArray = $table;
	}
}