CREATE TABLE `editeurs` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`ids_editeur` INT(11) NULL DEFAULT NULL,
	`id_type` INT(11) NULL DEFAULT NULL,
	`type` INT(11) NULL DEFAULT NULL,
	`login` VARCHAR(100) NULL DEFAULT NULL,
	`email` VARCHAR(100) NULL DEFAULT NULL,
	`technique` VARCHAR(100) NULL DEFAULT NULL,
	`compta` VARCHAR(100) NULL DEFAULT NULL,
	`skype` VARCHAR(100) NULL DEFAULT NULL,
	`telephone` VARCHAR(100) NULL DEFAULT NULL,
	`cellulaire` VARCHAR(100) NULL DEFAULT NULL,
	`fax` VARCHAR(100) NULL DEFAULT NULL,
	`nom_entreprise` VARCHAR(100) NULL DEFAULT NULL,
	`siret` VARCHAR(100) NULL DEFAULT NULL,
	`tva` VARCHAR(100) NULL DEFAULT NULL,
	`civ` VARCHAR(100) NULL DEFAULT NULL,
	`nom` VARCHAR(100) NULL DEFAULT NULL,
	`prenom` VARCHAR(100) NULL DEFAULT NULL,
	`adresse` VARCHAR(100) NULL DEFAULT NULL,
	`codepostale` VARCHAR(100) NULL DEFAULT NULL,
	`ville` VARCHAR(100) NULL DEFAULT NULL,
	`pays` VARCHAR(100) NULL DEFAULT NULL,
	`racine` VARCHAR(100) NULL DEFAULT NULL,
	`commercial` DECIMAL(10,2) NULL DEFAULT NULL,
	`statut` DECIMAL(10,2) NULL DEFAULT NULL,
	`paiement` DECIMAL(10,2) NULL DEFAULT NULL,
	`paiement_type` VARCHAR(100) NULL DEFAULT NULL,
	`iban` VARCHAR(100) NULL DEFAULT NULL,
	`swift` VARCHAR(100) NULL DEFAULT NULL,
	`banque` VARCHAR(100) NULL DEFAULT NULL,
	`commentaire` VARCHAR(100) NULL DEFAULT NULL,
	`date_creation` VARCHAR(100) NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;


/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    1, 2, 2, 'integration@capdecision.fr',
    'commercial@capdecision.fr', 'commercial@capdecision.fr',
    'commercial@capdecision.fr', 'test',
    '', '', '', 'CAPDECISION', '', '', 'Mr',
    'Metzler', 'Alexandre  (CAPDECISION)',
    '1bis bd cotte', 95880, 'Enghie',
    'fr', 'FRA', 2, 1, 25, 'Chèque', '',
    '', '', 'Compte par défaut ne pas supprimer.',
    '2013-04-22'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    3, 1, 2, 'bat@capdecision.fr', 'bat@capdecision.fr',
    'bat@capdecision.fr', 'bat@capdecision.fr',
    '', '', '', '', 'capdec editeur test',
    46465465465, '', 'Mr', 'capdec editeur test',
    'capdec editeur test', '', '', '',
    'fr', 'FRA', 2, 1, 50, 'Chèque', '',
    '', '', '', '2013-05-03'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    19, 1, 2, 'dalila2@eprogress.fr', 'fabien12394568@capdecision.fr',
    'fabien12394568@capdecision.fr',
    'fabien12394568@capdecision.fr',
    '', '', '', '', 'EPROGRESS', '', '',
    'Mr', 'Eprogress', 'Alexandre', '',
    '', '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2013-11-05'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    5, 2, 2, 'fabien@capdecision.fr', 'fabien@capdecision.fr',
    'fabien@capdecision.fr', 'fabien@capdecision.fr',
    '', 101010101, '', '', 'Capaffiliation',
    5216, '', 'Mr', 'Capaffiliation',
    'Alexandre', '', '', '', 'fr', 'FRA',
    2, 1, 50, 'Chèque', '', '', '', '', '2013-05-29'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    9, 1, 2, 'fabien9@capdecision.fr',
    'fabien9@capdecision.fr', 'fabien9@capdecision.fr',
    'fabien9@capdecision.fr', '', 101010101,
    '', '', 'SORTILEGES', '', '', 'Mr',
    'Sortileges', 'Alexandre', '17 true',
    75000, 'paris', 'fr', 'FRA', 2, 1, 50,
    'Chèque', '', '', '', '', '2013-06-18'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    7, 1, 2, 'jerome.9494@hotmail.fr',
    'jerome.9494@hotmail.fr', 'jerome.9494@hotmail.fr',
    'jerome.9494@hotmail.fr', '', '',
    '', '', 'DATARAMA &amp; co', '', '',
    'Mr', 'Jerome', '', '', '', '', 'fr',
    'FRA', 2, 1, 50, 'Chèque', '', '', '',
    '', '2013-06-05'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    8, 1, 2, 'j.sala_transatclub@yahoo.fr',
    'bat3@capdecision.fr', 'bat3@capdecision.fr',
    'bat3@capdecision.fr', '', '', '',
    '', 'aj data', '', '', 'Mr', 'Jerome',
    '', '', '', '', 'fr', 'FRA', 2, 1, 50,
    'Chèque', '', '', '', '', '2013-06-05'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    10, 1, 2, 'bertrand@sociabiliweb.com',
    'bertrand@sociabiliweb.com', 'bertrand@sociabiliweb.com',
    'bertrand@sociabiliweb.com', '',
    101010101, '', '', 'SOCIABILIWEB',
    '', '', 'Mr', 'SOCIABILIWEB', 'William',
    '17 true', 75000, 'paris', 'fr', 'FRA',
    2, 1, 50, 'Chèque', '', '', '', '', '2013-06-19'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    11, 1, 2, 'paulguillet@yahoo.fr', 'paulguillet@yahoo.fr',
    'paulguillet@yahoo.fr', 'paulguillet@yahoo.fr',
    '', '', '', '', 'EMAIL4YOU', '', '',
    'Mr', 'EMAIL4YOU', 'Paul', '', '',
    '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2013-07-08'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    12, 1, 2, 'annesophie@effi-net.com',
    'fabien281784@capdecision.fr',
    'fabien281784@capdecision.fr',
    'fabien281784@capdecision.fr',
    '', '', '', '', 'Efficiency Network',
    '', '', 'Mr', 'Efficiency Network',
    'Alexandre', '', '', '', 'fr', 'FRA',
    2, 1, 50, 'Chèque', '', '', '', '', '2013-07-09'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    13, 1, 2, 'fabien1234568@capdecision.fr',
    'fabien1234568@capdecision.fr',
    'fabien1234568@capdecision.fr',
    'fabien1234568@capdecision.fr',
    '', '', '', '', 'Comparer Les Placements',
    '', '', 'Mr', 'Comparer Les Placements',
    'Alexandre', '', '', '', 'fr', 'FRA',
    2, 1, 50, 'Chèque', '', '', '', '', '2013-07-09'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    14, 1, 2, 'fabien123456x@capdecision.fr',
    'fabien123456x@capdecision.fr',
    'fabien123456x@capdecision.fr',
    'fabien123456x@capdecision.fr',
    '', 101010101, '', '', 'LIBRE', '',
    '', 'Mr', 'LIBRE', 'Alexandre', '',
    '', '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2013-07-26'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    15, 1, 2, 'raphael@r2jmarketing.fr',
    'affiliate.manager1@r2jcompany.fr',
    'affiliate.manager1@r2jcompany.fr',
    'affiliate.manager1@r2jcompany.fr',
    '', 101010101, '', '', 'R2J', '', '',
    'Mr', 'R2J', 'Alexandre', '17 true',
    75000, 'paris', 'fr', 'FRA', 2, 1, 50,
    'Chèque', '', '', '', '', '2013-09-03'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    16, 2, 2, 'gregory@1netvision.com',
    'gregory@1netvision.com', 'gregory@1netvision.com',
    'gregory@1netvision.com', '', '',
    '', '', 'NETVISION', '529 573 73500014',
    'FR2452957373', 'Mr', 'TAATOO - NETVISION',
    '', '29 Rue de Sarre', 57070, 'Metz',
    'fr', 'FRA', 2, 1, 50, 'Virement', 'FR76 1470 7000 2430 7216 3529 725',
    'CCBPFRPPMTZ', 'BPL St Julien les Metz',
    '', '2013-10-01'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    17, 1, 2, 'fabien1234j568@capdecision.fr',
    'fabien1234j568@capdecision.fr',
    'fabien1234j568@capdecision.fr',
    'fabien1234j568@capdecision.fr',
    '', '', '', '', 'EVALOM', '', '', 'Mr',
    'EVALOM', '', '17 true', 75000, 'paris',
    'fr', 'FRA', 2, 1, 50, 'Chèque', '',
    '', '', '', '2013-10-03'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    18, 1, 2, 'affiliate1@performeclic.fr',
    'kit@performeclic.fr', 'kit@performeclic.fr',
    'kit@performeclic.fr', '', '', '',
    '', 'PERFORMCLICK', '', '', 'Mr', 'PERFORMCLICK',
    'Minh', '17 true', 75000, 'paris',
    'fr', 'FRA', 2, 1, 50, 'Chèque', '',
    '', '', '', '2013-10-08'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    20, 1, 2, 'alfredo.gosp@gmail;com',
    'jean.moulin87854@free.fr', 'jean.moulin87854@free.fr',
    'jean.moulin87854@free.fr', '',
    '', '', '', 'UAU', '', '', 'Mr', 'UAU',
    'Alexandre', '', '', '', 'fr', 'FRA',
    2, 1, 50, 'Chèque', '', '', '', '', '2013-11-05'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    21, 1, 2, 'jean.mouli415n@free.fr',
    'jean.mouli415n@free.fr', 'jean.mouli415n@free.fr',
    'jean.mouli415n@free.fr', '', '',
    '', '', 'WEBDONE', '', '', 'Mr', 'WEBDONE',
    'Alexandre', '17 true', 75000, 'brunoy',
    'fr', 'FRA', 2, 1, 50, 'Chèque', '',
    '', '', '', '2013-11-12'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    22, 2, 2, 'vanessa.gregorini@unitead.fr',
    'vanessa.gregorini@unitead.fr',
    'vanessa.gregorini@unitead.fr',
    'vanessa.gregorini@unitead.fr',
    '', '', '', '', 'UNITEAD', 50508492100038,
    'FR24 505084921', 'Mlle', 'UNITEAD',
    'Vanessa', '43, rue Castérès',
    92110, 'CLICHY', 'fr', 'FRA', 2, 1,
    50, 'Virement', 'FR76 1287 9000 0100 2210 3964 437',
    'DELUFR22XXX', 'BANQUE DELUBAC & CIE - 16, Place Saléon Terras 07160 LE CHEYLARD',
    '', '2013-11-12'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    23, 1, 2, 'bruno@leadiance.com', 'bruno@leadiance.com',
    'bruno@leadiance.com', 'bruno@leadiance.com',
    '', '', '', '', 'LEADIANCE', '', '',
    'Mr', 'LEADIANCE', 'Bruno', '17 true',
    91330, 'brunoy', 'fr', 'FRA', 2, 1,
    50, 'Chèque', '', '', '', '', '2013-11-21'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    24,
    2,
    2,
    'ludovic@1netvision.com',
    'ludovic@1netvision.com',
    'ludovic@1netvision.com',
    'ludovic@1netvision.com',
    '',
    '',
    35227861489,
    '',
    '6 KOM',
    20142223245,
    'LU27366579',
    'Mr',
    'TAATOO - 6KOM',
    'Ludovic',
    'Zone activité zare ouest n°21 ',' L - 4384 ',' EHLHRANGE ',' lu ',' FRA ',2,1,50,' Chèque ','','','','',' 2013 - 12 - 04 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 35,1,2,' contact@force - mktg.com ',' franck.benede@hotmail.fr ',' franck.benede@hotmail.fr ',' franck.benede@hotmail.fr ','','','','',' FORCE MARKETING ','','',' Mr ',' BENEDE ',' Franck ',' 17 true ',91800,' BRUNOY ',' fr ',' FRA ',2,1,50,' Chèque ','','','','',' 2014 - 03 - 27 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 25,1,2,' razorkill16789654123@gmail.com ',' razorkill16789654123@gmail.com ',' razorkill16789654123@gmail.com ',' razorkill16789654123@gmail.com ','','','','',' Gros deal ','','',' Mr ',' Gros deal ',' Alexandre ',' 17 true ',91800,'',' fr ',' FRA ',2,1,50,' Chèque ','','','','',' 2013 - 12 - 09 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 26,2,2,' ecannard@dreamlead.fr ',' eripert@dlead.fr ',' eripert@dlead.fr ',' houardi@dlead.fr ',' emilielocafilm ',' 04.78.66.77.94 ','',' 04.78.83.08.02 ',' Dreamlead Interactive ',44869437200054,' FR63448694372 ',' Mlle ',' CANNARD ',' Elodie ',' 4 place Louis Chazette ',69001,' Lyon ',' fr ',' FRA ',2,1,25,' Virement ',' FR7610096185050004456880152 ',' CMCIFRPP ',' CIC RHONE CENTRE ENTREPRISES 8 rue de la république 69001 Lyon ','',' 2013 - 12 - 09 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 27,1,2,' olivier@plandefou.com ',' olivier@plandefou.com ',' olivier@plandefou.com ',' olivier@plandefou.com ','',672546260,672546260,672546260,' PLANDEFOU ','','',' Mr ',' PLANDEFOU ',' Olivier ',' 17 true ',91800,' levallois perret ',' fr ',' FRA ',2,1,50,' Chèque ','','','','',' 2013 - 12 - 18 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 31,1,2,' eliott@net - lead.eu ',' eliott@net - lead.eu ',' razorkill16874@gmail.com ',' razorkill16874@gmail.com ','','','','',' Netlead ','','',' Mr ',' Netlead ',' Alexandre ',' 17 true ',91800,'',' fr ',' FRA ',2,1,50,' Chèque ','','','','',' 2014 - 02 - 21 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 30,1,2,' jb@liberad - media.com ',' jb@liberad - media.com ',' jb@liberad - media.com ',' jb@liberad - media.com ','','','','',' Liberad ','','',' Mme ',' Boukibza ',' Josiane ','','','',' fr ',' FRA ',2,1,50,' Chèque ','','','','',' 2014 - 01 - 22 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 29,1,2,' xavier.vangysel@webrivage.com ',' mickael.gossart@webrivage.com ',' mickael.gossart@webrivage.com ',' mickael.gossart@webrivage.com ','','','','',' Webrivage ','','',' Mr ',' Gossart ',' Mickael ',' 17 true ',91800,'',' fr ',' FRA ',2,1,50,' Chèque ','','','','',' 2014 - 01 - 16 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 32,2,2,' neezz@neezz.com ',' neezz@neezz.com ',' neezz@neezz.com ',' compta@neezz.com ','',478985853,478985853,'',' Neezz ',533030698,' FR61533030698 ',' Mr ',' GHARBI ',' ERIC ',' 2 PLACE LOUIS PRADEL ',69001,' LYON ',' fr ',' FRA ',2,1,25,' Virement ',' FR7617806005996225641177173 ',' AGRIFRPP878 ',' CREDIT AGRICOLE Centre Est ','',' 2014 - 03 - 10 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 33,1,2,' gerald.hrc@free.fr ',' gerald.hrc@free.fr ',' gerald.hrc@free.fr ',' gerald.hrc@free.fr ','','','','',' Univers VIP ','','',' Mr ',' Univers VIP ',' Gerald ',' 17 true ',91330,' BRUNOY ',' fr ',' FRA ',2,1,50,' Chèque ','','','','',' 2014 - 03 - 11 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 34,2,2,' catherine.potiron@simarket.fr ',' emailing@simarket.fr ',' emailing@simarket.fr ',' nina.samarianov@simarket.fr ','','','','',' SI Market ',50962745100024,' FR84509627451 ',' Mme ',' Samarianov ',' Nina ',' 27 RUE DU CHATEAU ',92200,' Neuilly Sur Seine ',' fr ',' FRA ',2,1,50,' Chèque ','','','','',' 2014 - 03 - 27 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 36,1,2,' cindy.dinane@tegara - media.com ',' cindy.dinane@tegara - media.com ',' cindy.dinane@tegara - media.com ',' cindy.dinane@tegara - media.com ','','','','',' Tegara Media ','','',' Mr ',' dinane ',' cindy ',' 17 true ',91800,' brunoy ',' fr ',' FRA ',2,1,50,' Virement ',' FR76 3000 4002 3200 0100 7793 885 ',' BNPAFRPPIPI ',' BNP - ILE DE FRANCE INNOVATION (02999) ','',' 2014 - 04 - 01 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 37,1,2,' paulguillet@yahoo.fr ',' paulguillet@yahoo.fr ',' paulguillet@yahoo.fr ',' paulguillet@yahoo.fr ','','','','',' Affaire en OR ','','',' Mr ',' Guillet ',' Paul ',' 17 true ',91800,' Brunoy ',' fr ',' FRA ',2,1,50,' Chèque ','','','','',' 2014 - 04 - 17 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 38,2,2,' cfabre@mailback.fr ',' cfabre@mailback.fr ',' cfabre@mailback.fr ',' cfabre@mailback.fr ','','','','',' Mailback ',52000855800014,' FR03520008558 ',' Mlle ',' fabre ',' clotilde ',' 231 rue saint honoré ',75001,' paris ',' fr ',' FRA ',2,1,50,' Virement ',' FR7630076020381976420020061 ',' NORDFRPP ',' Crédit du Nord - Paris Bac ','',' 2014 - 04 - 24 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 219,1,2,' morgane.henry@evolutioncom.eu ',' morgane.henry@evolutioncom.eu ',' morgane.henry@evolutioncom.eu ',' morgane.henry@evolutioncom.eu ','','','','',' EVOLUTION France ','','',' Mme ',' HENRY ',' Morganne ','','','',' fr ',' FRA ',2,1,50,' Chèque ','','','','',' 2017 - 02 - 20 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 40,2,2,' jhounsougbo@cupofmedia.com ',' mhanen@cupofmedia.com ',' mhanen@cupofmedia.com ',' invoice@cupofmedia.com ','','','','',' Cup of Media ',521494377,' FR19521494377 ',' Mlle ',' Brachemi ',' Amel ',' 8 Rue de l Hôtel de Ville',
    92200,
    'Neuilly sur Seine',
    'fr',
    'FRA',
    2,
    1,
    50,
    'Chèque',
    '',
    '',
    '',
    '',
    '2014-06-11'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    41, 1, 2, 'alexandre@ktp.net', 'alexandre@ktp.net',
    'alexandre@ktp.net', 'alexandre@ktp.net',
    '', '', '', '', 'KTP', '', '', 'Mr', 'Tourret',
    'Alexandre', '17 true', 91800, '',
    'fr', 'FRA', 2, 1, 50, 'Chèque', '',
    '', '', '', '2014-07-16'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    42, 1, 2, 'tratobison@sls-data.com',
    'tratobison@sls-data.com', 'tratobison@sls-data.com',
    'tratobison@sls-data.com', '', '',
    '', '', 'DMD', '', '', 'Mr', 'RATOBISON',
    'Thierry', '17 rue de paris', 75012,
    'Paris', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2014-10-17'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    43, 1, 2, 'dalila@eprogress.fr', 'dalila@eprogress.fr',
    'dalila@eprogress.fr', 'dalila@eprogress.fr',
    '', '01 69 39 37 29', '', '', 'Eprogress',
    '', '', 'Mr', 'HEBERT', 'Fabien', '',
    '', '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2014-11-26'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    44, 1, 2, 'rmattelin@theclicklab-advertising.com',
    'rmattelin@theclicklab-advertising.com',
    'rmattelin@theclicklab-advertising.com',
    'rmattelin@theclicklab-advertising.com',
    '', '', '', '', 'Conso du jour', '',
    '', 'Mr', 'Mattelin', 'Rémy', '16 avenue Marceau',
    75016, 'Paris', 'fr', 'FRA', 2, 1, 50,
    'Chèque', '', '', '', '', '2015-01-13'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    45, 1, 2, 'ndriouche@cloud-affiliation.com',
    'bat8789@capdecision.fr', 'bat8789@capdecision.fr',
    'bat8789@capdecision.fr', '', '',
    '', '', 'Axe7', '', '', 'Mlle', 'Pepato',
    'Eva', '17 rue de paris', 75016,
    '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2015-02-18'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    46, 1, 2, 'dardaine007@gmail.com',
    'bat8789rg@fbcapdecision.fr', 'bat8789rg@fbcapdecision.fr',
    'bat8789rg@fbcapdecision.fr', '',
    '', '', '', 'Acheter dans le neuf',
    '', '', 'Mr', 'Dardaine', 'Christophe',
    '', '', '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2015-02-23'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    47, 1, 2, 'samyristretto@gmail.com',
    'samyristretto@gmail.com', 'samyristretto@gmail.com',
    'samyristretto@gmail.com', '', '',
    '', '', 'Ristretto', '', '', 'Mr', 'Ristretto',
    'Samy', '', '', '', 'fr', 'FRA', 2, 1,
    50, 'Chèque', '', '', '', '', '2015-02-23'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    48, 2, 2, 'hnovarese@sls-data.com',
    'hnovarese@sls-data.com', 'hnovarese@sls-data.com',
    'hnovarese@sls-data.com', '', '',
    '', '', 'SoftDirect', '', '', 'Mr',
    'Novarese', 'Hervé', '', '', '', 'fr',
    'FRA', 2, 1, 50, 'Chèque', '', '', '',
    '', '2015-03-11'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    49, 1, 2, 'adesaintseine@gmail.com',
    'adesaintseine@gmail.com', 'adesaintseine@gmail.com',
    'adesaintseine@gmail.com', '', '',
    '', '', 'Deal Agency', '', '', 'Mr',
    'De Saint Seine', 'Artus', '', '',
    '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2015-03-12'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    50, 1, 2, 'gilles.bader57@gmail.com',
    'gilles.bader57@gmail.com', 'gilles.bader57@gmail.com',
    'gilles.bader57@gmail.com', '',
    '', '', '', 'Les Caves', '', '', 'Mr',
    'Bader', 'Gilles', 'CHEMIN LAULAGNIER',
    26740, 'ST MARCEL LES SAUZET', 'fr',
    'FRA', 2, 1, 50, 'Chèque', '', '', '',
    '', '2015-03-30'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    51, 1, 2, 'gregory@6-kom.com', 'stephane@6kom.com',
    'stephane@6kom.com', 'stephane@6kom.com',
    '', '', '', '', '6 KOM', '', '', 'Mr',
    'Grégory', 'Grégory', '17 rue de paris',
    75016, 'Paris', 'fr', 'FRA', 2, 1, 50,
    'Virement', 'LU970141454491003000',
    'CELLLULL', 'Ing Esch/Alzette',
    '', '2015-03-31'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    52, 1, 2, 'diffusion1@instamailmedia.com',
    'diffusion1@instamailmedia.com',
    'diffusion1@instamailmedia.com',
    'diffusion1@instamailmedia.com',
    '', '', '', '', 'Instalmedia', '', '',
    'Mr', 'Cherki', 'Morgan', '16 avenue Marceau',
    75016, 'Paris', 'fr', 'FRA', 2, 1, 25,
    'Chèque', '', '', '', '', '2015-04-17'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    53, 1, 2, 'miriame.l@darwin-agency.com',
    'miriame.l@darwin-agency.com',
    'miriame.l@darwin-agency.com',
    'miriame.l@darwin-agency.com',
    '', '', '', '', 'Darwin', '', '', 'Mme',
    'Miriame', 'Miriame', '17 rue de paris',
    75016, 'Paris', 'fr', 'FRA', 2, 1, 50,
    'Chèque', '', '', '', '', '2015-04-17'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    54, 1, 2, 'afremiot@k2b-webagency.com',
    'afremiot@k2b-webagency.com', 'afremiot@k2b-webagency.com',
    'afremiot@k2b-webagency.com', '',
    '', '', '', 'K2B', '', '', 'Mlle', 'Fremiot',
    'Aurelia', '17 rue de paris', 75016,
    'Paris', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2015-04-30'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    55, 1, 2, 'webmaster@ht-prive.com',
    'tranminh@r2jcompany.fr', 'tranminh@r2jcompany.fr',
    'tranminh@r2jcompany.fr', '', '',
    '', '', 'HT Privé', '', '', 'Mr', 'Minh',
    'Minh', '17 rue de paris', 75016,
    'Paris', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2015-04-30'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    56, 1, 2, 'charly@opt-e-mail.fr', 'charly@opt-e-mail.fr',
    'charly@opt-e-mail.fr', 'charly@opt-e-mail.fr',
    '', '', '', '', 'opt-e-mail', '', '',
    'Mr', 'Castille', 'Charly', '17 rue de paris',
    75016, 'Paris', 'fr', 'FRA', 2, 1, 50,
    'Chèque', '', '', '', '', '2015-06-12'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    57, 1, 2, 'mroyant@agence-mailomedia.com',
    'mroyant@agence-mailomedia.com',
    'mroyant@agence-mailomedia.com',
    'mroyant@agence-mailomedia.com',
    '', '', '', '', 'MailoMedia', '', '',
    'Mr', 'Royant', 'Mélanie', '17 rue de paris',
    75016, 'Paris', 'fr', 'FRA', 2, 1, 50,
    'Chèque', '', '', '', '', '2015-06-16'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    58, 1, 2, 'g.pommier@comandclick.com',
    'g.pommier@comandclick.com', 'g.pommier@comandclick.com',
    'g.pommier@comandclick.com', '',
    '', '', '', 'comandclick', '', '', 'Mr',
    'POMMIER', 'Guillaume', '', '', '',
    'fr', 'FRA', 2, 1, 50, 'Chèque', '',
    '', '', '', '2015-06-25'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    59, 1, 2, 'g.bader@les-caves.fr', 'g.bader@les-caves.fr',
    'g.bader@les-caves.fr', 'g.bader@les-caves.fr',
    '', '', '', '', 'Les Caves 2', '', '',
    'Mr', 'Bader', 'Gilles', '17 rue de paris',
    75016, '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2015-07-07'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    64, 2, 2, 'sarah.abid@unitead.fr',
    'sarah.abid@unitead.fr', 'sarah.abid@unitead.fr',
    'sarah.abid@unitead.fr', '', '',
    659077000, '', 'Emarketstudio', 81204912000015,
    'FR35 812049120', 'Mme', 'Chiche',
    'Fanny', '11, rue Sesto Fiorentino',
    93170, 'BAGNOLET', 'fr', 'FRA', 1,
    1, 50, 'Chèque', '', '', '', '', '2015-10-06'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    65, 2, 2, 'hadi.boudenne@distrigame.com',
    'hadi.boudenne@distrigame.com',
    'hadi.boudenne@distrigame.com',
    'hadi.boudenne@distrigame.com',
    '', '', '', '', 'Distrigame', 433313574,
    'FR84 433 313 574', 'Mr', 'Boudenne',
    'Hadi', '76 RUE DE LA POMPE', 75116,
    'PARIS', 'fr', 'FRA', 1, 1, 50, 'Virement',
    'FR7612879000010022103476430',
    'DELUFR22XXX', 'DELUBAC & CIE - 16 Place Saléon Terras 07160 Le Cheylard',
    '', '2015-10-06'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    61, 1, 2, 'bertrand4@edentify.fr',
    'Bertrand@edentify.fr', 'bertrand@edentify.fr',
    'bertrand@edentify.fr', '', '', '',
    '', 'Sponsorboost CAP', '', '', 'Mme',
    'bertrand', 'bertrand', '17 rue de paris',
    75016, 'Paris', 'fr', 'FRA', 2, 1, 50,
    'Chèque', '', '', '', '', '2015-09-07'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    62, 1, 1, 'msynquintyn@gmail.com',
    'msynquintyn@gmail.com', 'msynquintyn@gmail.com',
    'msynquintyn@gmail.com', '', '',
    613847504, '', '', '', '', 'Mr', 'synquintyn',
    'Mathieu', '79 rue gustave delory',
    59830, 'CYSOING', 'fr', 'FRA', 2, 2,
    50, 'Chèque', '', '', '', '', '2015-09-17'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    66, 2, 2, 'christian.boudjenah@adscoria.com',
    'christian.boudjenah@adscoria.com',
    'christian.boudjenah@adscoria.com',
    'christian.boudjenah@adscoria.com',
    '', '', '', '', 'ADSCORIA KEY PERFORMANCE GROUP',
    '479 638 991 00030', 'FR 704 796 38 991',
    'Mr', 'boudjenah', 'christian', '',
    '', '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2015-10-06'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    67, 1, 2, 'audrey@datalpha.fr', 'audrey@datalpha.fr',
    'audrey@datalpha.fr', 'audrey@datalpha.fr',
    '', '', '', '', 'DATALPHA', '', '', 'Mme',
    'HANZO', 'Audrey', '', '', '', 'fr',
    'FRA', 2, 1, 50, 'Chèque', '', '', '',
    '', '2015-10-06'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    68, 1, 2, 'najid.othmane@gmail.com',
    'najid.othmane@gmail.com', 'najid.othmane@gmail.com',
    'najid.othmane@gmail.com', '', '',
    '', '', 'webdev aurora', 48791, '',
    'Mr', 'Othmane', 'najid', 'CM unite 4 N160',
    4000, 'Marrakech', 'fr', 'FRA', 2,
    1, 50, 'Virement', 'MA011450000010210000327236 ',
    'BMCEMAMC ', 'BMCE Banque AV ALLAL AL FASSI OPERATION AL HOUDA MARRAKECH MAROC',
    '', '2015-10-07'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    69, 1, 2, 'katia.24media@gmail.com',
    'katia.24media@gmail.com', 'katia.24media@gmail.com',
    'katia.24media@gmail.com', '', '+41 22 518 29 59',
    '', '', '24MEDIA PTE. LTD', '201136483M',
    '', 'Mme', 'Vodanova', 'katia', '19 Keppel Road #03-05 Jit Poh Building',
    89058, 'Singapore ', 'sg', 'FRA',
    2, 1, 50, 'Chèque', '', '', '', '', '2015-10-08'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    70, 2, 2, 'sarah@lipskymedia.com',
    'sarah@lipskymedia.com', 'sarah@lipskymedia.com',
    'sarah@lipskymedia.com', '', '',
    '', '', 'Lipsky Media', 750657140,
    'FR32750657140', 'Mr', 'ASSOUS',
    'ARTHUR', '20 Bis rue Louis Philippe',
    'NEUILL', 92200, 'fr', 'FRA', 2, 1,
    50, 'Chèque', '', '', '', '', '2015-10-09'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    71, 2, 2, 'info@news-found.com', 'info@news-found.com',
    'info@news-found.com', 'info@news-found.com',
    '', '', '', '', 'Pam Communication S.r.l.s Uniperson',
    '', 2622670905, 'Mr', 'Mainas', 'Antonio',
    'Via Luigi Barzini 15', 7100, 'sassari',
    'it', 'FRA', 2, 1, 400, 'Virement',
    'IT07R0326817200052526785880',
    'SELBIT2B', 'Banca Sella S.p.a',
    '', '2015-10-12'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    72, 2, 2, 'n.fontaine@qassa.com', ' n.fontaine@qassa.com',
    ' n.fontaine@qassa.com', ' n.fontaine@qassa.com',
    '', '', '', '', 'Qassa France B.V',
    '', 'NL820681167B01', 'Mr', 'fontaine',
    'nicolas', 'Planetenveld 35', '3893 G',
    '3893 GE', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2015-10-12'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    73, 2, 2, 'lauren@ob-one.fr', 'lauren@ob-one.fr',
    'lauren@ob-one.fr', 'lauren@ob-one.fr',
    '', '', 659935403, '', 'DATALOGIE',
    49963819500028, 'FR78499638195',
    'Mme', 'Nadalié', 'Lauren', '3, cours Charlemagne - BP2597',
    69217, 'Lyon Cedex 2', 'fr', 'FRA',
    2, 1, 50, 'Virement', 'FR7630004025190001001229249',
    'BNPAFRPPXXX', 'BNP PARIBAS BD DE LA REP 33510 ANDERNOS',
    '', '2015-10-13'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    74, 2, 2, 'morgane@abacus-marketing-solutions.com ',
    'cristina@abacus-marketing.com',
    'cristina@abacus-marketing.com',
    'cristina@abacus-marketing.com',
    '', '', '', '', 'Abacus Marketing Solutions SL ',
    '', 'ES B66170135', 'Mme', 'Vizcaya ',
    'Laura', 'C/Compte de Salvatierra, 10 3º1ª',
    8008, 'Barelona', 'es', 'FRA', 2, 1,
    0, 'Virement', 'ES70 1465 0150 5719 0019 2143',
    'BIC  INGDESMMXXX', 'Bank Office  ING Direct - Diagonal Barcelona',
    '', '2015-10-13'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    75, 1, 2, 'yanis.allais@hw-agency.com',
    'yanis.allais@hw-agency.com', 'yanis.allais@hw-agency.com',
    'yanis.allais@hw-agency.com', '',
    '', '', '', 'Hw-Agency Madagascar ',
    'NIF:4001265298', '', 'Mr', 'allais',
    'yanis', '17 rue razanatseheno',
    101, 'Antananarivo', 'mg', 'FRA',
    2, 1, 25, 'Virement', 'MG4600008000050507300054385',
    'BFAVMGMG', 'BFV 14, RUE GENERAL RABEHEVITRA ANTANINARENINA BP 196 / BP 440 101 ANTANANARIVO MADAGASCAR',
    '', '2015-10-14'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    76, 2, 2, 'diffusion@moogli-media.com',
    'diffusion@moogli-media.com', 'diffusion@moogli-media.com',
    'diffusion@moogli-media.com', '',
    '', '', '', 'MOOGLI MEDIA', 'B66512815',
    'ESB66512815', 'Mr', 'perez', 'guilhem',
    '', '', '', 'fr', 'FRA', 2, 1, 50, 'Virement',
    'ES4921003020612200594704', 'CAIXESBBXXX',
    'CAIXA AV DIAGONAL', '', '2015-10-14'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    77, 2, 2, 'alouis@r-advertising.com',
    'alouis@r-advertising.com', 'alouis@r-advertising.com',
    'alouis@r-advertising.com', '',
    '', '', '', 'R SAS', 502207079, 'FR80502207079',
    'Mme', 'LOUIS', 'Audrey', '', '', '',
    'fr', 'FRA', 2, 1, 50, 'Chèque', '',
    '', '', '', '2015-10-14'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    78, 2, 2, 'alison.jumeaux@planet.fr',
    'alison.jumeaux@planet.fr', 'alison.jumeaux@planet.fr',
    'alison.jumeaux@planet.fr', '',
    '', '', '', 'Planet.fr SA', 41200154700048,
    'FR01412001547', 'Mme', 'jumeaux',
    'alison', '47 rue de la Chaussée d’Antin',
    75009, 'PARIS', 'fr', 'FRA', 2, 1, 50,
    'Virement', 'FR76 1020 7000 8421 2139 5569 141',
    'CCBPFRPPMTG', 'BANQUE POPULAIRE LE PONANT',
    '', '2015-10-14'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    79,
    2,
    2,
    'clement@emailingandco.com',
    'clement@emailingandco.com',
    'clement@emailingandco.com',
    'comptabilite@emailingandco.com',
    '',
    179752070,
    '',
    '',
    'Emailing &amp; co',
    '',
    'FR28753747591',
    'Mr',
    'brossard',
    'clement',
    '27 Boulevard de l  Ariane ',6300,' Nice ',' fr ',' FRA ',2,1,50,' Virement ',' FR19 2004 1000 0168 0093 3Z02 052 ',' PSSTFRPPPAR ',' LA BANQUE POSTALE ','',' 2015 - 10 - 15 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 80,2,2,' ripari@flipflapmedia.com ',' jose.gomez@flipflapmedia.com ',' jose.gomez@flipflapmedia.com ',' jose.gomez@flipflapmedia.com ','','','','',' flipflapmedia ','',' ESB87151924 ',' Mr ',' gomez ',' jose ',' Carrer de Borriana,
    46.CP ',8030,' Barcelona ',' es ',' FRA ',2,1,50,' Chèque ','','','','',' 2015 - 10 - 15 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 81,2,2,' luana.siciliani@medialives.com ',' luana.siciliani@medialives.com ',' luana.siciliani@medialives.com ',' luana.siciliani@medialives.com ','','','','',' Media Lives S.r.l ','',' IT11422121001 ',' Mme ',' siciliani ',' luana ',' Corso Buenos Aires,
    52 ',20124,' milan ',' it ',' FRA ',2,1,0,' Virement ',' IT46J0200801623000104244960 ',' UNCRITM1223 ',' Unicredit – Via Stradella 2 – 20129 Milano ','',' 2015 - 10 - 20 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 82,2,2,' emeline@mediabeille.com ',' emeline@mediabeille.com ',' emeline@mediabeille.com ',' emeline@mediabeille.com ','','','','',' Mediabeille ',51785550800027,' FR 83 517855508 ',' Mme ',' bino ',' emeline ','','','',' fr ',' FRA ',2,1,50,' Chèque ','','','','',' 2015 - 10 - 23 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 83,2,2,' g.colin@netpartage.fr ',' g.colin@netpartage.fr ',' g.colin@netpartage.fr ',' jm.bobay@netpartage.fr ','','','','',' Netpartage SAS ',431642651,' FR - 81 431 642 651 ',' Mr ',' Bobay ',' Jean - Marc ',' 15 rue Paul Eluard ',76290,' Montivilliers ',' fr ',' FRA ',2,1,50,' Chèque ','','','','',' 2015 - 10 - 23 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 84,2,2,' Walter@taz - media.com ',' daden@taz - media.com ',' daden@taz - media.com ',' daden@taz - media.com ','','','','',' TAZ MEDIA ',' 532 773 926 00011 ',' FR22 532 773 926 ',' Mme ',' daden ',' gwenaelle ',' 233,
    rue de la Croix Nivert ',75015 ,' paris ',' fr ',' FRA ',2,1,25,' Virement ',' FR76 3000 4008 0100 0102 4660 731 ',' BNPAFRPPXXX ',' BNP paris auteuil ','',' 2015 - 10 - 23 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 85,2,2,' m.smit@fruzzel.com ',' m.smit@fruzzel.com ',' m.smit@fruzzel.com ',' m.smit@fruzzel.com ','','','','',' 4UXL s.l ','',' ESB98547250 ',' Mr ',' smit ',' michiel ',' Calle teruel 15 – 2b ',46008,' Valencia ',' es ',' FRA ',2,1,50,' Virement ',' ES0700190411064010017722 ',' DEUTESBB ',' Deutsche Bank,
    Carrer de Sant Joan,
    22,
    12580 Benicarló,
    Spain ','',' 2015 - 10 - 27 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 86,2,2,' bat@news - diffusion.fr ',' bat@news - diffusion.fr ',' bat@news - diffusion.fr ',' bat@news - diffusion.fr ','','','','',' Yannick LYD ',824084644,' FR06824084644 ',' Mr ',' LYD ',' Yannick ',' 175 AVENUE LOUIS ANTOINE SAINT JUST 83130 ',83130,' La Garde ',' fr ',' FRA ',2,1,25,' Virement ',' FR7610096180990009538580151 ',' CMCIFRPP ',' CIC TOULON SAINT JEAN ','',' 2015 - 10 - 27 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 87,2,2,' ap@ividence.com ',' ap@ividence.com ',' ap@ividence.com ',' ap@ividence.com ','','','','',' ividence ',811097401,' FR73811097401 ',' Mme ',' gato ',' naiara ',' 20 rue Bouvier ',75011,' paris ',' fr ',' FRA ',2,1,50,' Chèque ','','','','',' 2015 - 10 - 28 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 88,2,2,' affiliation@online365.fr ',' affiliation@online365.fr ',' affiliation@online365.fr ',' affiliation@online365.fr ','','','','',' Online365 ',493814842,' FR10493814842 ',' Mme ',' guerniche ',' wapha ','','','',' fr ',' FRA ',2,1,50,' Chèque ','','','','',' 2015 - 10 - 28 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 89,1,2,' commercial@itema.email ',' gestion.commerciale@itema.email ',' gestion.commerciale@itema.email ',' gestion.commerciale@itema.email ','','','','',' itema ','','',' Mme ',' VIC ',' Audrey - Jane ','','','',' fr ',' FRA ',2,1,50,' Chèque ','','','','',' 2015 - 10 - 29 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 90,2,2,' regies@kowmedia.com ',' julien@kowmedia.com ',' julien@kowmedia.com ',' julien@kowmedia.com ','','','','',' Kow Media ','',' BE0810411640 ',' Mr ',' lilien ',' pierre ','','','',' fr ',' FRA ',2,1,25,' Virement ',' BE30 7512 0410 4511 ',' AXABBE22 ',' AXA Bank Europe S.A.-25,
    boulevard du Souverain à 1070 Bruxelles – Belgique ','',' 2015 - 11 - 02 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 91,1,2,' aurelien.bissengue@leadiya.com ',' aurelien.bissengue@leadiya.com ',' aurelien.bissengue@leadiya.com ',' aurelien.bissengue@leadiya.com ','','','','',' Leadiya Ltd ',' A162 / 09 / 13 / 7050 ','',' Mr ',' Bissengué ',' Aurélien ',' Résidence Tillel Ennasr 2 ',2034,' tunis ',' tn ',' FRA ',2,1,50,' Chèque ','','','','',' 2015 - 11 - 03 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 92,2,2,' cherita.ganga@rivieraweb - rw.com ',' cherita.ganga@rivieraweb - rw.com ',' cherita.ganga@rivieraweb - rw.com ',' cherita.ganga@rivieraweb - rw.com ','','','','',' rivieraweb ',502997059,' FR19502997059 ',' Mme ',' ganga ',' cherita ',' 41 - 43,
    rue des Chantiers ',78000,' Versailles ',' fr ',' FRA ',2,1,50,' Virement ',' FR7618707000 823092187592507 ',' CCBPFRPPVER ',' BANQUE POPULAIRE ','',' 2015 - 11 - 04 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 93,2,2,' johann@dimona - marketing.fr ',' johann@dimona - marketing.fr ',' johann@dimona - marketing.fr ',' johann@dimona - marketing.fr ','','','','',' DIMONA ',538865221,' FR77538865221 ',' Mr ',' nguelet ',' johann ','','','',' fr ',' FRA ',2,1,50,' Chèque ','','','','',' 2015 - 11 - 05 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 94,2,2,' jerome@vis - ma - ville.com ',' jerome@vis - ma - ville.com ',' jerome@vis - ma - ville.com ',' jerome@vis - ma - ville.com ','','','','',' VIS MA VILLE ',' RCS 521 110 965 ',' FR 09 521 110 965 ',' Mr ',' roux ',' nicolas ','','','',' fr ',' FRA ',2,1,50,' Chèque ','','','','',' 2015 - 11 - 06 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 95,1,2,' debbi@rapid - performance.com ',' debbi@rapid - performance.com ',' debbi@rapid - performance.com ',' debbi@rapid - performance.com ','','','','',' Rapid Performance GmbH ',' DE268551303 ','',' Mr ',' janosch karla ',' Nils ',' Alter Hohlweg 15b ',58093,' Hagen ',' fr ',' FRA ',2,1,50,' Chèque ','','','','',' 2015 - 11 - 09 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 96,2,2,' isabelle@latitudepub.com ',' isabelle@latitudepub.com ',' isabelle@latitudepub.com ',' isabelle@latitudepub.com ','','','','',' LATTITUDE PUB ',' B 493 176 655 ',' FR35493176655 ',' Mme ',' Le Mince ',' Isabelle ','','','',' fr ',' FRA ',2,1,50,' Chèque ','','','','',' 2015 - 11 - 09 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 97,1,2,' Jacopo.benelle@gmail.com ',' Jacopo.benelle@gmail.com ',' Jacopo.benelle@gmail.com ',' Jacopo.benelle@gmail.com ','','','','',' DBO llp ',' OC399550 ','',' Mr ',' benelle ',' Jacopo ',' 40 Gracechurch Street ',' london ',' EC3V 0BT ',' fr ',' FRA ',2,1,50,' Chèque ','','','','',' 2015 - 11 - 09 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 98,1,2,' btan@acheter - louer.fr ',' btan@acheter - louer.fr ',' btan@acheter - louer.fr ',' btan@acheter - louer.fr ','','','','',' ACHETER LOUER ','','',' Mr ',' rondeau ',' baptiste ','','','',' fr ',' FRA ',2,1,50,' Chèque ','','','','',' 2015 - 11 - 10 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 99,1,2,' aslmedias@gmail.com ',' aslmedias@gmail.com ',' aslmedias@gmail.com ',' aslmedias@gmail.com ','','','','',' ASL MEDIAS ',' 8141815 814 181 5252 ','',' Mr ',' elbaz ',' lenny ',' 25 avenue du 8 mai 1945 ',95200 ,' SARCELLES ',' fr ',' FRA ',2,1,50,' Chèque ','','','','',' 2015 - 11 - 12 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 100,2,2,' riccardo.zanello@across.it ',' riccardo.zanello@across.it ',' riccardo.zanello@across.it ',' riccardo.zanello@across.it ',' riccardo.zanello1 ','','','',' Across Srl ','',10599350013,' Mr ',' Zanello ',' Riccardo ',' Via Lagrange Srl ',10123,' Torino ',' it ',' FRA ',2,1,25,' Virement ',' IT55X0200801105000101703534 ',' uncritm1aa5 ',' unicredit s.p.a corso sommelier 35 ','',' 2015 - 11 - 12 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 101,2,2,' jerome.9494@hotmail.fr ',' jerome.9494@hotmail.fr ',' jerome.9494@hotmail.fr ',' jerome.9494@hotmail.fr ','','','','',' ROUSSEV TECHNOLOGY ',502195670,' FR 94 502195670 00 ',' Mr ',' ROUSSEV ',' Nicolas ','','','',' fr ',' FRA ',2,1,50,' Chèque ','','','','',' 2015 - 11 - 13 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 102,2,2,' amadoudiallo@deux - trente.com ',' amadoudiallo@deux - trente.com ',' amadoudiallo@deux - trente.com ',' amadoudiallo@deux - trente.com ','','','','',' 2 - 30 media ',' B 751 283 938 ',' FR 81751283938 ',' Mr ',' diallo ',' amadou ','','','',' fr ',' FRA ',2,1,50,' Chèque ','','','','',' 2015 - 11 - 16 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 103,1,2,' bertrand3@edentify.fr ',' Bertrand@edentify.fr ',' bertrand@edentify.fr ',' bertrand@edentify.fr ','','','','',' une adresse unique ','','',' Mr ',' Alfredo ',' une adresse unique ',' 17 rue de paris ',75016,' evry ',' fr ',' FRA ',1,1,50,' Chèque ','','','','',' 2015 - 11 - 19 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 104,2,2,' corinne@webmediarm.com ',' corinne@webmediarm.com ',' corinne@webmediarm.com ',' caroline@webmediarm.com ','',477190130,'','',' WEB MEDIA RM ',' 808 244 727 00018 ',' FR70 808 244 727 ',' Mr ',' MASSE ',' REMY ',' 2 rue des VAB ',42400,' Saint Chamond ',' fr ',' FRA ',2,1,50,' Chèque ','','','','',' 2015 - 11 - 19 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 105,2,2,' Lucie.monti@jrcg.fr ',' Lucie.monti@jrcg.fr ',' Lucie.monti@jrcg.fr ',' Lucie.monti@jrcg.fr ','','','','',' JRCG Invest ',501182356,' FR35501182356 ',' Mr ',' Gallina ',' Christopher ','','','',' fr ',' FRA ',2,1,50,' Chèque ','','','','',' 2015 - 11 - 20 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 106,2,2,' staff@atoutmail.com ',' staff@atoutmail.com ',' staff@atoutmail.com ',' staff@atoutmail.com ','','','','',' ATOUTMAIL SAS ',480888940 ,' FR91480888940 ',' Mr ',' pineau ',' pierrick ','','','',' fr ',' FRA ',2,1,50,' Chèque ','','','','',' 2015 - 11 - 20 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 107,2,2,' thomas@gaddin.com ',' thomas@gaddin.com ',' thomas@gaddin.com ',' facturation@mailcorner.com ','','','','',' MAIL CORNER FRANCE ',' 805 322 435 00019 ',' FR54805322435 ',' Mr ',' OLLIVIER ',' Matthieu ',' 14 Avenue de l Europe',
    77144,
    'MONTEVRAIN',
    'fr',
    'FRA',
    2,
    1,
    50,
    'Virement',
    'FR76 3000 4021 7300 0100 9298 922',
    'BNPAFRPPLMV',
    'BNP PARIBAS, Boulevard de Lagny, 77600 BUSSY SAINT GEORGES',
    '',
    '2015-11-23'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    108, 2, 2, 'donia@sponsorboost.com',
    'donia@sponsorboost.com', 'donia@sponsorboost.com',
    'donia@sponsorboost.com', '', '',
    '', '', 'SPONSORBOOST', '2003B00622 ',
    'FR93401486519', 'Mr', 'VASSEUR',
    'Philippe ', '9 rue Caffarelli - Le Palmeira',
    6000, 'Nice', 'fr', 'FRA', 2, 1, 50,
    'Chèque', '', '', '', '', '2015-11-24'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    109, 2, 2, 'sbernes@prismamedia.com',
    'trafic.data@prismamedia.com',
    'trafic.data@prismamedia.com',
    'trafic.data@prismamedia.com',
    '', '', '', '', 'PRISMA MEDIA', 509310785,
    '', 'Mlle', 'bernes', 'sonia', '',
    '', '', 'fr', 'FRA', 2, 1, 25, 'Virement',
    'FR7610107001100061010036413',
    'BREDFRPPXXX', 'Bred Banque populaire 93-95 avenue général de Gaulle 94000 Créteil',
    '', '2015-11-24'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    110, 1, 2, 'goaffil@gmail.com', 'goaffil@gmail.com',
    'goaffil@gmail.com', 'goaffil@gmail.com',
    '', '', '', '', 'goaffil', 570192, '',
    'Mr', 'Morand', 'Chris', 'North Point Business Park, ',
    'New Ma', 'Cork', 'ie', 'FRA', 2, 1,
    25, 'Virement', 'IE73CPAY99119966533424',
    'CPAYIE2D', 'Fire Financial Services Limited, IRLANDE',
    '', '2015-11-25'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    111, 1, 2, 'amidocharly@gmail.com',
    'amidocharly@gmail.com', 'amidocharly@gmail.com',
    'amidocharly@gmail.com', '', '',
    '', '', 'WOUNTCHOM CHARLES', 79185270000016,
    '', 'Mr', 'WOUNTCHOM ', 'CHARLES',
    'ADRESSE 31 SQUARE DE DIAGNE ',
    77186, 'NOISIEL ', 'fr', 'FRA', 2,
    1, 50, 'Chèque', '', '', '', 'SO', '2015-11-25'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    112, 2, 2, 'stephane@neonergy.lu',
    'stephane@neonergy.lu', 'stephane@neonergy.lu',
    'stephane@neonergy.lu', '', '', '',
    '', 'NEONERGY Sarl  ', 'B181189',
    'LU26618575', 'Mr', 'ELBAHTARI',
    'stephane', '', '', '', 'lu', 'FRA',
    2, 1, 50, 'Virement', 'LU640030894038230000',
    'BGLLLULL', 'BGL Luxembourg ', '',
    '2015-12-04'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    113, 1, 2, 'fissler@caloga.com', 'fissler@caloga.com',
    'fissler@caloga.com', 'fissler@caloga.com',
    '', '', '', '', 'CALOGA', '430 177 923 00025',
    '', 'Mlle', 'frederique', 'issler',
    '170 RUE RAYMOND LOSSERAND', '',
    '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2015-12-08'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    114, 2, 2, 'international@scibile.ch',
    'international@scibile.ch', 'international@scibile.ch',
    'international@scibile.ch', '',
    '', '', '', 'Scibile Network Sagl',
    '', ' CHE-110.061.446 ', 'Mr', 'mainas',
    'antonio', '', '', '', 'fr', 'FRA',
    2, 1, 50, 'Chèque', '', '', '', '', '2015-12-16'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    115, 2, 2, 'trafic@aaronbrainwave.com',
    'trafic@aaronbrainwave.com', 'trafic@aaronbrainwave.com',
    'trafic@aaronbrainwave.com', '',
    '', '', '', 'Aaron Brainwave', '',
    '', 'Mr', 'BEN YOUNES', 'Asma', '',
    '', '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2015-12-16'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    116, 2, 2, 'ludovic@tinkel.fr', 'mili@tinkel.fr',
    'mili@tinkel.fr', 'mili@tinkel.fr',
    '', '', '', '', 'SAS TINKEL ', '', 'FR20814712832',
    'Mr', 'tinkel', 'Ludovic', '', '',
    '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2015-12-18'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    117, 2, 2, 'Judith@snakeinteractive.com',
    'Judith@snakeinteractive.com',
    'Judith@snakeinteractive.com',
    'Judith@snakeinteractive.com',
    '', '', '', '', 'snakeinteractive',
    '', 'FR44 750344319', 'Mr', 'HOUNSOUGBO',
    'Judith', '', '', '', 'fr', 'FRA', 2,
    1, 50, 'Chèque', '', '', '', '', '2015-12-18'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    118, 2, 2, 'danielov@snakeinteractive.com',
    'danielov@snakeinteractive.com',
    'danielov@snakeinteractive.com',
    'comptabilite@snakeinteractive.com',
    '', 148587557, '', '', 'Snake Interactive',
    75034431900033, 'FR44 750344319',
    'Mr', 'OV', 'Daniel', '12 Rue Vivienne, Lot 3',
    75002, 'Paris', 'fr', 'FRA', 2, 1, 25,
    'Chèque', '', '', '', '', '2015-12-18'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    119, 1, 2, 'isabel@correomaster.com',
    'isabel@correomaster.com', 'isabel@correomaster.com',
    'isabel@correomaster.com', '', '',
    '', '', 'Correomaster SL', 'B54015730',
    '', 'Mme', 'diaz', 'isabel', '', '',
    '', 'fr', 'FRA', 2, 1, 50, 'Virement',
    'ES4700811003050001070316', 'BSABESBB',
    'Sabadell CAM', '', '2015-12-21'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    120, 1, 2, 'antoine@leadana.net', 'antoine@leadana.net',
    'antoine@leadana.net', 'antoine@leadana.net',
    '', '', '', '', 'LEADANA INC ', '',
    '', 'Mr', 'burnichon', 'antoine',
    '', '', '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2015-12-21'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    121, 1, 2, 'asma.benyounes@unitead.fr',
    'asma.benyounes@unitead.fr', 'asma.benyounes@unitead.fr',
    'asma.benyounes@unitead.fr', '',
    '', '', '', 'UNITEAD 2', '', '', 'Mr',
    'benyounes', 'asma', '', '', '', 'fr',
    'FRA', 2, 1, 50, 'Chèque', '', '', '',
    '', '2015-12-29'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    122, 1, 2, 'asma', 'victoria@lead-factory.net',
    'victoria@lead-factory.net', 'victoria@lead-factory.net',
    '', 672546260, '', '', 'AARON BRAINWAVE SARL',
    '', '', 'Mr', 'Brainwave', 'Aaron',
    '', '', '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2015-12-29'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    123, 1, 2, 'Lizotte@natexo.com', 'Lizotte@natexo.com',
    'Lizotte@natexo.com', 'Lizotte@natexo.com',
    '', '', '', '', 'NATEXO', '', '', 'Mlle',
    'lizotte', 'isabelle', '', '', '',
    'fr', 'FRA', 2, 1, 50, 'Chèque', '',
    '', '', '', '2016-01-05'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    124, 1, 2, 'laura@etoilemedia.com',
    'laura@etoilemedia.com', 'laura@etoilemedia.com',
    'laura@etoilemedia.com', '', '',
    '', '', 'financière media', '', '',
    'Mlle', 'BAGHDADI', 'Laura ', '',
    '', '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2016-01-07'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    125, 1, 2, 'gerli@adngames.com', 'gerli@adngames.com',
    'gerli@adngames.com', 'gerli@adngames.com',
    '', '', '', '', 'Adngames OÜ', '',
    '', 'Mme', 'Grünthal', 'Gerli', '',
    '', '', 'ee', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2016-01-07'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    126, 1, 2, 'anikou@aventers.net', 'anikou@aventers.net',
    'anikou@aventers.net', 'anikou@aventers.net',
    '', '', '', '', 'Aventers Publicité',
    '', '', 'Mr', 'Nikou', 'Alexandre',
    '86, bis rue de la république  ',
    92800, 'Puteaux', 'fr', 'FRA', 2, 1,
    50, 'Chèque', '', '', '', '', '2016-01-07'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    127, 1, 2, 'isabelle.lodin@choisir.com',
    'isabelle.lodin@choisir.com', 'isabelle.lodin@choisir.com',
    'isabelle.lodin@choisir.com', '',
    '', '', '', 'marketshot', '', '', 'Mlle',
    'lodin', 'isabelle', '', '', '', 'fr',
    'FRA', 1, 1, 50, 'Chèque', '', '', '',
    '', '2016-01-07'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    128, 1, 2, 'nd.mediapro@gmail.com',
    'nd.mediapro@gmail.com', 'nd.mediapro@gmail.com',
    'nd.mediapro@gmail.com', '', '',
    '', '', 'ND MEDIA', '', '', 'Mr', 'gosp',
    'alfredo', '', '', '', 'fr', 'FRA',
    1, 1, 50, 'Chèque', '', '', '', '', '2016-01-08'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    129, 2, 2, 'emailingco@mediastay.com',
    'emailingco@mediastay.com', 'emailingco@mediastay.com',
    'kborojevic@mediastay.com', '',
    '', '', '', 'mds performance', 80929629600035,
    'FR46809296296', 'Mr', 'Stern', 'Yohan',
    '31 rue des jeuneurs', 75002, 'PARIS',
    'fr', 'FRA', 1, 1, 25, 'Virement', 'FR76 3006 6100 3100 0201 2830 460',
    'CMCIFRPP ', 'CIC PARIS ABOUKIR  106 RUE D ABOUKIR 75002 PARIS ',
    '', '2016-01-11'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    130, 1, 2, 'lisa.meyer@rmagroup.fr',
    'lisa.meyer@rma-group.fr', 'lisa.meyer@rma-group.fr',
    'lisa.meyer@rma-group.fr', '', '',
    '', '', ' RMA GROUP', 51454937700026,
    'FR37514549377', 'Mme', 'Meyer',
    'Lisa', '55 rue de bitche', 92400,
    'Courbevoie', 'fr', 'FRA', 1, 1, 50,
    'Virement', 'FR7610207000812121185684854',
    'CCBPFRPPMTG', 'BANQUE POPULAIRE RIVES PARIS',
    '', '2016-01-14'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    131, 1, 2, 'socheata@apsaramedia.fr',
    'socheata@apsaramedia.fr', 'socheata@apsaramedia.fr',
    'socheata@apsaramedia.fr', '', '',
    '', '', 'APSARA MEDIA', ' 801 241  654 00020',
    '', 'Mme', 'NEANG ', 'Socheata', ' 147 Allées de Craponne',
    '', '', 'fr', 'FRA', 1, 1, 50, 'Chèque',
    '', '', '', '', '2016-01-14'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    132, 1, 2, 'Thibaut.amaury@orixa-media.com',
    'Thibaut.amaury@orixa-media.com',
    'Thibaut.amaury@orixa-media.com',
    'Thibaut.amaury@orixa-media.com',
    '', '', '', '', 'Orixa Media', '', '',
    'Mr', 'amaury', 'thibaut', '', '',
    '', 'fr', 'FRA', 1, 1, 50, 'Chèque',
    '', '', '', '', '2016-01-14'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    133, 1, 2, 'timars.laura@gmail.com',
    'timars.laura@gmail.com', 'timars.laura@gmail.com',
    'timars.laura@gmail.com', 'laura.timars',
    '', '', '', 'Audrey COTTE', 81259966000014,
    '', 'Mme', 'Timars', 'Laura', '1099 chemin de saint claude',
    6600, 'Antibes', 'fr', 'FRA', 0, 3,
    50, 'Chèque', '', '', '', '', '2016-01-15'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    134, 1, 2, 'grzegorz.babczyszyn@csmediadirect.com',
    ' grzegorz.babczyszyn@csmediadirect.com',
    ' grzegorz.babczyszyn@csmediadirect.com',
    ' grzegorz.babczyszyn@csmediadirect.com',
    '', '', '', '', 'universo company',
    '', '', 'Mr', 'Babczyszyn', 'Grzegorz',
    '', '', '', 'pl', 'FRA', 1, 1, 50, 'Chèque',
    '', '', '', '', '2016-01-15'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    135, 1, 1, 'support@webipub.com', 'support@webipub.com',
    'support@webipub.com', 'support@webipub.com',
    '', 34972677485, 34683296440, '',
    '', '', '', 'Mr', 'Borie', 'Jean-Marie',
    '59 Carrer Peralada', 17600, 'Figueres (Espagne)',
    'fr', 'FRA', 0, 2, 50, 'Chèque', '',
    '', '', '', '2016-01-20'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    136, 1, 2, 'alexandra@avent-media.fr',
    'alexandra@avent-media.fr', 'alexandra@avent-media.fr',
    'alexandra@avent-media.fr', '',
    '', '', '', 'AVENT MEDIA', '', '', 'Mme',
    'kahn', 'alexandra', '', '', '', 'fr',
    'FRA', 1, 1, 50, 'Chèque', '', '', '',
    '', '2016-01-28'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    137, 2, 2, 'anne@primeira-posicao.com',
    'anne@primeira-posicao.com', 'anne@primeira-posicao.com',
    'anne@primeira-posicao.com', '',
    972532308, '', '', 'Primeira Posicao',
    '', 513348913, 'Mme', 'Sembely', 'Anne',
    '', '', '', 'fr', 'FRA', 1, 1, 50, 'Virement',
    'PT 50 0010 0000 5242 3400 0010 2',
    'BBPIPTPL', 'BANCO BPI SA', 'v',
    '2016-01-28'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    138, 1, 2, 'info@odsia.com', 'info@odsia.com',
    'adam@odsia.com', 'invoicing@odsia.com',
    '', 33663675055, 663675055, 663675055,
    'odsia LTD', '', 10640231, 'Mr', 'turner',
    'Adam', 'Kemp House, 152-160 City Road',
    'EC1V 2', 'London', 'gb', 'FRA', 2,
    1, 50, 'Virement', 'FR76 3043 8001 0040 0028 7442 411 ',
    'INGBFR21XXX', 'ING DIRECT, Immeuble Lumière 40, avenue des Terroirs de France 75616 Paris Cedex 12',
    '', '2016-02-04'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    139, 1, 2, 'celine@todoleads.com',
    'celine@todoleads.com', 'celine@todoleads.com',
    'celine@todoleads.com', '', '', '',
    '', 'TODOLEADS', '', '', 'Mme', 'raoult',
    'celine', '', '', '', 'fr', 'FRA', 2,
    1, 50, 'Chèque', '', '', '', '', '2016-02-05'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    140, 1, 1, 'emma@viralitea.com', 'emma@viralitea.com',
    'emma@viralitea.com', 'emma@viralitea.com',
    '', '', '', '', '', '', '', 'Mme', 'Wang',
    'Emma', 'Jefferson Street', 1, 'Hong Kong',
    'hk', 'FRA', 0, 2, 50, 'Chèque', '',
    '', '', '', '2016-02-10'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    141, 1, 2, 'soufienne@digitalcircle360.com',
    'soufienne@digitalcircle360.com',
    'soufienne@digitalcircle360.com',
    'soufienne@digitalcircle360.com',
    '', '', '', '', 'digitalcircle360',
    '', '', 'Mr', 'Fahdy', 'Soufienne ',
    '', '', '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2016-02-16'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    142, 1, 2, 'thomas@d-medi-a.com', 'Nathalie@d-medi-a.com',
    'Nathalie@d-medi-a.com', 'Nathalie@d-medi-a.com',
    '', '', '', '', 'd-media', '', '', 'Mme',
    'omrod', 'nathalie', '', '', '', 'fr',
    'FRA', 2, 1, 50, 'Chèque', '', '', '',
    '', '2016-02-23'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    143, 2, 2, 'jeremy@azorica.fr', 'nolan@azorica.fr',
    'nolan@azorica.fr', 'finance@azorica.fr',
    '', '', '', '', 'AZORICA', '', '', 'Mr',
    'lhotte', 'jérémy', '75 Rue Paul sabatier',
    13090, 'Aix En Provence', 'fr', 'FRA',
    2, 1, 25, 'Virement', 'FR7630077048591480940020057',
    'SMCTFR2A', 'SMC - 103 cours gambetta - 84300 cavaillon',
    '', '2016-02-24'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    144, 1, 2, 'jacob@topgratuit.com',
    'jacob@topgratuit.com', 'jacob@topgratuit.com',
    'jacob@topgratuit.com', '', '', '',
    '', 'Pio Web', '', '', 'Mr', 'boukris',
    'jacob ', '', '', '', 'fr', 'FRA', 2,
    1, 50, 'Chèque', '', '', '', '', '2016-02-25'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    145, 2, 2, 'geraldine.jourdan@flinteractive.fr',
    'geraldine.jourdan@flinteractive.fr',
    'geraldine.jourdan@flinteractive.fr',
    'geraldine.jourdan@flinteractive.fr',
    '', '', '', '', 'WebMedia RM 2', 80824472700026,
    'FR70808244727', 'Mr', 'Masse', 'Remy',
    '2 Rue des V.A.B.', 42400, 'Saint-Chamond',
    'fr', 'FRA', 2, 1, 50, 'Virement', 'FR7619106006564364237728149',
    'AGRIFRPP891', 'CA - PROVENCE COTE D AZUR - AGENCE ST LAURENT ENTREPRISE',
    '', '2016-03-01'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    146, 1, 2, 'bcherouati@emailingnetwork.com',
    'bcherouati@emailingnetwork.com',
    'bcherouati@emailingnetwork.com',
    'bcherouati@emailingnetwork.com',
    '', '', '', '', 'emailing network',
    '', '', 'Mme', 'cherouati', 'bahia',
    '', '', '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2016-03-07'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    147, 1, 2, 'info@toogains.com', 'info@toogains.com',
    'info@toogains.com', 'info@toogains.com',
    '', '', '', '', 'TOOGAINS', '', '', 'Mr',
    'Iacopino', 'Alessandro ', '', '',
    '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2016-03-07'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    148, 2, 2, 'info@kooaka.com', 'info@kooaka.com',
    'info@kooaka.com', 'info@kooaka.com',
    'dlmfinances', '', 601460170, '',
    'KooaKA', '', 'FR 80788626836', 'Mr',
    'LOSARDO', 'frederic', '2 avenue des Amethystes',
    44300, 'Nantes', 'fr', 'FRA', 0, 2,
    50, 'Chèque', '', '', '', '', '2016-03-08'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    149, 1, 2, 'info@dlmfinances.com',
    'info@dlmfinances.com', 'info@dlmfinances.com',
    'info@dlmfinances.com', '', '', '',
    '', 'dlmfinances', '', '', 'Mr', 'LOSARDO',
    'frédéric', '', '', '', 'fr', 'FRA',
    2, 1, 50, 'Chèque', '', '', '', '', '2016-03-10'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    150, 1, 2, 'michael.b@mydata-agency.com',
    'michael.b@mydata-agency.com',
    'michael.b@mydata-agency.com',
    'michael.b@mydata-agency.com',
    '', '', '', '', 'A.M.D MEDIA DIGITAL LTD',
    '', '', 'Mr', 'bars', 'michael', '',
    '', '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2016-03-15'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    152, 1, 2, 'Sebastian.perez@dpinfosolution.com',
    'Sebastian.perez@dpinfosolution.com',
    'Sebastian.perez@dpinfosolution.com',
    'Sebastian.perez@dpinfosolution.com',
    '', '', '', '', 'DIGIPANTHER', '', '',
    'Mr', 'perez', 'Sebastian', '', '',
    '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2016-03-23'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    151, 1, 1, 'gregoryandrew95@gmail.com',
    'gregoryandrew95@gmail.com', 'gregoryandrew95@gmail.com',
    'gregoryandrew95@gmail.com', '',
    '', '', '', '', '', '', 'Mr', 'Gregory',
    'Andrew', '', '', '', 'af', 'FRA', 0,
    3, 50, 'Chèque', '', '', '', '', '2016-03-17'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    153, 1, 2, 'alexis@swarmiz.com', 'alexis@swarmiz.com',
    'alexis@swarmiz.com', 'alexis@swarmiz.com',
    '', '', '', '', 'SWARMIZ', '', '', 'Mr',
    'bidaut', 'alexis', '', '', '', 'fr',
    'FRA', 2, 1, 50, 'Chèque', '', '', '',
    '', '2016-03-24'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    154, 1, 1, 'admin@shareinfos.info',
    'admin@shareinfos.info', 'admin@shareinfos.info',
    'admin@shareinfos.info', '', 139022812,
    '', '', '', '', '', 'Mr', 'Avare', 'Patrick',
    '1 pl André Mignot', 78000, 'VERSAILLES',
    'fr', 'FRA', 0, 2, 50, 'Chèque', '',
    '', '', '', '2016-03-29'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    155, 1, 2, 'david.anderson@rentleads.net',
    'david.anderson@rentleads.net',
    'david.anderson@rentleads.net',
    'david.anderson@rentleads.net',
    '', '', '', '', 'RentLeads FZE', '',
    '', 'Mr', 'anderson', 'david', '',
    '', '', 'fr', 'FRA', 1, 1, 50, 'Chèque',
    '', '', '', '', '2016-04-07'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    156, 2, 2, 'coscasp@recreasoft.com',
    'coscasp@recreasoft.com', 'coscasp@recreasoft.com',
    'coscasp@recreasoft.com', 'coscasp',
    177105668, '', '', 'Recreasoft', 417934106,
    'FR21417934106', 'Mr', 'coscas',
    'patrick', '17 rue Henri Monnier',
    75009, 'Paris', 'fr', 'FRA', 1, 1, 50,
    'Virement', 'FR7630004016650001012201216',
    'BNPAFRPPPGN', 'BNP Paribas - Agence Paris Pl. Clichy',
    '', '2016-04-13'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    157, 2, 2, 'ebrochard@mailbrands.com',
    'ebrochard@mailbrands.com', 'ebrochard@mailbrands.com',
    'ebrochard@mailbrands.com', '',
    '', '', '', 'mail and brands', 53162968100036,
    'FR20531629681', 'Mme', 'brochard',
    'emilie', '6 rue du Général Clergerie',
    75116, 'Paris', 'fr', 'FRA', 1, 1, 50,
    'Virement', 'FR7630004003640001015347914',
    'BNPAFRPPPAE', 'BNP Paribas PARIS-CENTRE AFF',
    '', '2016-04-21'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    158, 1, 2, 'batt@capdecision.fr', 'batt@capdecision.fr',
    'batt@capdecision.fr', 'batt@capdecision.fr',
    '', '', '', '', '', '', '', 'Mr', 'Alexandre',
    'Metzler', '', '', '', 'fr', 'FRA',
    1, 1, 50, 'Chèque', '', '', '', '', '2016-04-28'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    159, 1, 2, 'victor@50threvenue.com',
    'victor@50threvenue.com', 'victor@50threvenue.com',
    'victor@50threvenue.com', '', '',
    '', '', '50threvenue', '', '', 'Mr',
    'Chamber ', 'victor', '', '', '', 'fr',
    'FRA', 1, 1, 50, 'Chèque', '', '', '',
    '', '2016-04-28'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    160, 1, 2, 'snehil@leadsection.com',
    'snehil@leadsection.com', 'snehil@leadsection.com',
    'snehil@leadsection.com', '', '',
    '', '', '', '', '', 'Mr', 'Ambati', 'Snehil ',
    '', '', '', 'fr', 'FRA', 1, 1, 50, 'Chèque',
    '', '', '', '', '2016-04-28'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    161, 1, 2, 'guillaume.raypro@gmail.com',
    'guillaume.raypro@gmail.com', 'guillaume.raypro@gmail.com',
    'guillaume.raypro@gmail.com', '',
    '', '', '', 'RayproMailing', '', '',
    'Mr', 'raypro', 'guillaume', '', '',
    '', 'fr', 'FRA', 1, 1, 50, 'Virement',
    'FR7630004012520001008407339',
    'BNPAFRPPPBY', 'Bnp Paribas Nation place des la Nation 75012 Paris',
    '', '2016-05-02'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    162, 1, 2, 'virginie@thedata-factory.com',
    'monetisation-fr@thedata-factory.com',
    'monetisation-fr@thedata-factory.com',
    'monetisation-fr@thedata-factory.com',
    '', '', '', '', 'the data factory',
    '', '', 'Mme', 'BALLARIN', 'Virginie',
    '', '', '', 'fr', 'FRA', 2, 1, 25, 'Virement',
    'ES5700815029170002426747', 'BSABESBBXXX',
    'Sabadell Atlantico Rbla. Cataluña 115 2º planta 08008 BARCELONA SPAIN',
    '', '2016-05-04'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    163, 2, 2, 'affiliation@welcome-media.fr',
    'jglories@welcome-media.fr', 'jglories@welcome-media.fr',
    'affiliation@welcome-media.fr',
    '', '', '', '', 'WELCOMING', 51443164200032,
    'FR 09514431642 ', 'Mr', 'glories',
    'jennifer', '6 Rue du Général Clergerie',
    75116, 'Paris', 'fr', 'FRA', 2, 1, 50,
    'Virement', 'FR76 3000 4003 6400 0101 2767 714',
    'BNPAFRPPPAE', 'BNP PARISBAS PARIS-CENTRE AFFAIRES ET CONSEIL (02890)',
    '', '2016-05-09'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    164, 2, 2, 'Tristan@elbit-medias.fr',
    'Tristan@elbit-medias.fr', 'Tristan@elbit-medias.fr',
    'Tristan@elbit-medias.fr', '', '',
    '', '', 'Elbit Medias', 78953245400020,
    'FR61789532454', 'Mr', 'Denis', 'Tristan',
    '34bis rue de picpus', 75012, 'Paris',
    'fr', 'FRA', 2, 1, 50, 'Virement', 'FR7630004012520001010172739',
    'BNPAFRPPPBU', 'BNP Paribas s ambroise',
    '', '2016-05-10'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    165, 2, 2, 'enquete-shopping@cardata.fr',
    'mt@cardata.fr, naawa@cardata.fr',
    'mt@cardata.fr, naawa@cardata.fr',
    'lb@cardata.fr, naawa@cardata.fr',
    '', '', '', '', 'cardata', '', '', 'Mme',
    'hachem', 'tatiana', '2 rue de marly le roi',
    78150, 'Le Chesnay', 'fr', 'FRA',
    2, 1, 25, 'Virement', 10107001750072600000000,
    'BREDFRPPXXX', 'BRED', '', '2016-05-20'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    166, 1, 2, 'dominique.candeille@unitead.fr',
    'vanessa.gregorini@unitead.fr',
    'vanessa.gregorini@unitead.fr',
    'vanessa.gregorini@unitead.fr',
    '', '', 678981153, '', 'Espadas Sebastien',
    '504779992 00032', '', 'Mr', 'gregorini',
    'vanessa', '59 rue voltaire', 92300,
    'LEVALOIS PERRET', 'fr', 'FRA', 2,
    1, 50, 'Chèque', '', '', '', '', '2016-05-23'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    167, 1, 1, 'n.kachouri@gmail.com',
    'n.kachouri@gmail.com', 'n.kachouri@gmail.com',
    'n.kachouri@gmail.com', 'kachouri.n',
    21624415900, 21624415900, '', '',
    '', '', 'Mr', 'KACHOURI', 'Nizar',
    'sousse', 4000, 'sousse', 'tn', 'FRA',
    0, 2, 50, 'Chèque', '', '', '', '', '2016-05-23'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    168, 1, 2, 'Msint55@gmail.com', 'Msint55@gmail.com',
    'Msint55@gmail.com', 'Msint55@gmail.com',
    '', 177380229, '', '', 'MSI', '', '',
    'Mr', 'chemla', 'serge', 'Rogozine 27/74',
    77254, 'Ashdod Israel', 'il', 'FRA',
    2, 1, 50, 'Virement', 'IL330370330000000446238',
    'FIRBILITXXX', 'FIBI Blvd Menachem BEGIN Ashdod ISRAEL',
    '', '2016-05-24'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    169, 1, 2, 'Tyler@clenchmedia.com',
    'Tyler@clenchmedia.com', 'Tyler@clenchmedia.com',
    'Tyler@clenchmedia.com', '', '',
    '', '', 'Clench Media LLC', '', '',
    'Mr', 'Lubbers', 'Tyler ', '', '',
    '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2016-05-25'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    170, 1, 2, 'tom@highmedia.fr', 'deborah@highmedia.fr',
    'deborah@highmedia.fr', 'deborah@highmedia.fr',
    '', '', '', '', 'high media', '', '',
    'Mr', 'sgaravilli', 'tom ', '', '',
    '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2016-05-31'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    171, 1, 2, 'sebastiengilbert@aujourdhui.com',
    'sebastiengilbert@aujourdhui.com',
    'sebastiengilbert@aujourdhui.com',
    'sebastiengilbert@aujourdhui.com',
    '', '', '', '', 'Anxa Limited', '',
    '', 'Mr', 'gilbert', 'sebastien',
    '', '', '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2016-06-01'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    172, 1, 2, 'affiliation@expert-contact.fr',
    'affiliation@expert-contact.fr',
    'affiliation@expert-contact.fr',
    'affiliation@expert-contact.fr',
    '', '', '', '', 'Expert contact', '',
    '', 'Mme', 'HANZO ', 'Audrey ', '',
    '', '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2016-06-10'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    173, 1, 2, 'lea.duris5@gmail.com',
    'lea.duris5@gmail.com', 'lea.duris5@gmail.com',
    'lea.duris5@gmail.com', 'leanette0',
    '', 756920243, '', 'AFX GROUP DWC-LLC',
    '', '', 'Mlle', 'Duris', 'Léa', 'Dubai World Central',
    390667, 'Dubai', 'ae', 'FRA', 0, 2,
    50, 'Chèque', '', '', '', '', '2016-06-14'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    174, 1, 2, 'l.lopez@spreadata.com',
    'l.lopez@spreadata.com', 'l.lopez@spreadata.com',
    'l.lopez@spreadata.com', '', '',
    '', '', 'SPREADATA SA', '', '', 'Mr',
    'triolet', 'Arnaud', '', '', '', 'fr',
    'FRA', 2, 1, 50, 'Virement', 'ES6800810305950001558656 ',
    'BSABESBBXX', 'BANCO SABADELL',
    '', '2016-06-16'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    175, 1, 2, 'firstindigo@orange.fr',
    'firstindigo@orange.fr', 'firstindigo@orange.fr',
    'firstindigo@orange.fr', '', '',
    '', '', 'F6KEY', '', '', 'Mr', 'MICHEL',
    'Florent ', '', '', '', 'fr', 'FRA',
    2, 1, 50, 'Chèque', '', '', '', '', '2016-06-16'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    176, 2, 2, 'rachel.laloum@unitead.fr',
    'asma.benyounes@unitead.fr', 'asma.benyounes@unitead.fr',
    'asma.benyounes@unitead.fr', '',
    '', '', '', 'Team Recrut SARL', '',
    '', 'Mr', 'CHICHE', 'Richard ', '96, boulevard de la Libération',
    94300, 'Vincennes', 'fr', 'FRA', 2,
    1, 50, 'Chèque', '', '', '', '', '2016-06-20'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    177, 1, 2, 'guillaume@mediastream.fr',
    'guillaume@mediastream.fr', 'guillaume@mediastream.fr',
    'guillaume@mediastream.fr', '',
    '', '', '', 'MEDIASTREAM', '', '', 'Mr',
    'thuillier ', 'guillaume', '', '',
    '', 'fr', 'FRA', 2, 1, 50, 'Virement',
    'FR76 1870 7000 2331 0214 1271 866',
    'CCBPFRPPVER', 'BANQUE POPULAIRE rue de paris palaiseau',
    '', '2016-06-22'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    178, 1, 2, 'jorida@myacomgroup.com',
    'jorida@myacomgroup.com', 'jorida@myacomgroup.com',
    'jorida@myacomgroup.com', '', '',
    '', '', 'Mediagency Srl', '', '', 'Mr',
    'Ilenia ', 'Enna', '', '', '', 'fr',
    'FRA', 2, 1, 50, 'Chèque', '', '', '',
    '', '2016-07-01'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    179, 1, 2, 'affiliation@naointeractive.fr',
    'account@naointeractive.fr', 'account@naointeractive.fr',
    'account@naointeractive.fr', '',
    '', '', '', 'NAOInteractive', '', '',
    'Mr', 'SERRER', 'sebastien', '', '',
    '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2016-07-01'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    180, 1, 2, 'contact@ideal-online.fr',
    'contact@ideal-online.fr', 'contact@ideal-online.fr',
    'contact@ideal-online.fr', '', '',
    '', '', 'iDeal Online', '', '', 'Mr',
    'DAGNEUX', 'Christian', '', '', '',
    'fr', 'FRA', 2, 1, 50, 'Chèque', '',
    '', '', '', '2016-07-05'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    181, 1, 2, 'hmiraoui@tradetracker.com',
    'hmiraoui@tradetracker.com', 'hmiraoui@tradetracker.com',
    'hmiraoui@tradetracker.com', '',
    '', '', '', 'tradetracker france',
    '', '', 'Mr', 'tanaka', 'dominique ',
    '', '', '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2016-07-05'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    182, 1, 2, 'affiliation@novaware.sg',
    'affiliation@novaware.sg', 'affiliation@novaware.sg',
    'affiliation@novaware.sg', '', '',
    '', '', 'Novaware', '', '', 'Mr', 'Rolandez',
    'Jonathan ', '', '', '', 'fr', 'FRA',
    2, 1, 50, 'Chèque', '', '', '', '', '2016-07-08'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    183, 1, 2, 'commercial@datasuccess.fr',
    'commercial@datasuccess.fr', 'commercial@datasuccess.fr',
    'commercial@datasuccess.fr', '',
    '', '', '', 'Data Success', '', '',
    'Mr', 'HEBERT', 'KILLIAN', '', '',
    '', 'fr', 'FRA', 1, 1, 50, 'Chèque',
    '', '', '', '', '2016-07-19'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    184, 1, 2, 'ametzler@hotmail.fr', 'ametzler@hotmail.fr',
    'ametzler@hotmail.fr', 'ametzler@hotmail.fr',
    '', '', '', '', 'ADOMOS MONETISATION',
    '', '', 'Mr', 'monetisation', 'alexandre',
    '', '', '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2016-07-20'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    185, 1, 2, 'ametzler1@hotmail.fr',
    'ametzler1@hotmail.fr', 'ametzler1@hotmail.fr',
    'ametzler1@hotmail.fr', '', '', '',
    '', 'acheter louer monetisation',
    '', '', 'Mr', 'monetisation', 'alexandre',
    '', '', '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2016-07-20'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    186, 1, 2, 'commercial@z-mailing.com',
    'commercial@z-mailing.com', 'commercial@z-mailing.com',
    'commercial@z-mailing.com', '',
    '', '', '', 'Z Mailing', '', '', 'Mr',
    'Amélineau', 'Sylvain', '', '', '',
    'fr', 'FRA', 2, 1, 50, 'Chèque', '',
    '', '', '', '2016-08-02'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    187, 1, 2, '94249@supinfo.com', '94249@supinfo.com',
    '94249@supinfo.com', '94249@supinfo.com',
    '', '', '', '', 'Weps Concept', '',
    '', 'Mr', 'seguin', 'clément', '',
    '', '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2016-08-03'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    188, 1, 2, 'emma2@viralitea.com', 'emma@viralitea.com',
    'emma@viralitea.com', 'emma@viralitea.com',
    '', '', '', '', 'Viralitea Limited',
    '', '', 'Mme', 'Wang', 'Emma', '', '',
    '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2016-08-05'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    189, 1, 2, 'affiliation.expertcontact@gmail.com',
    'emailing@acheter-au-meilleur-prix.fr',
    'emailing@acheter-au-meilleur-prix.fr',
    'emailing@acheter-au-meilleur-prix.fr',
    '', '', '', '', 'Expert contact', '',
    '', 'Mr', 'cotte', 'cyril', '', '',
    '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2016-08-10'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    190, 1, 2, 'yoni@bsd-digital.fr', 'keren@bsd-digital.fr',
    'keren@bsd-digital.fr', 'keren@bsd-digital.fr',
    '', '', '', '', 'BSD Digital', '', '',
    'Mr', 'HAGEGE', 'Yoni', '', '', '',
    'fr', 'FRA', 2, 1, 50, 'Chèque', '',
    '', '', '', '2016-08-19'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    191, 1, 2, 'marketing@pulpower.com',
    'marketing@pulpower.com', 'marketing@pulpower.com',
    'marketing@pulpower.com', '', '',
    '', '', 'Pulpower Ltd', 'UK07874543',
    '', 'Mlle', 'Picó', 'Mireia', '',
    '', '', 'es', 'FRA', 0, 2, 50, 'Chèque',
    '', '', '', '', '2016-08-24'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    192, 1, 2, 'isabel@pulpower.com', 'isabel@pulpower.com',
    'isabel@pulpower.com', 'isabel@pulpower.com',
    '', '', '', '', 'Pulpower Ltd', '',
    '', 'Mme', 'Diaz', 'Isabel', '', '',
    '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2016-08-25'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    193, 1, 2, 'contact@solu-mail.net',
    'contact@solu-mail.net', 'contact@solu-mail.net',
    'contact@solu-mail.net', '', '',
    '', '', 'Solumail', '', '', 'Mr', 'krief',
    'bruno', '', '', '', 'fr', 'FRA', 2,
    1, 50, 'Chèque', '', '', '', '', '2016-08-26'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    194, 1, 1, 'uday.websoulservices@gmail.com',
    'uday.websoulservices@gmail.com',
    'uday.websoulservices@gmail.com',
    'uday.websoulservices@gmail.com',
    'uday.websoulservices', 9811641096,
    9811641096, '', '', '', '', 'Mr', 'Jain',
    'Uday', 'B-20, 1st Floor, B - Block',
    110095, 'New Delhi', 'in', 'FRA',
    0, 2, 50, 'Chèque', '', '', '', '', '2016-09-05'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    195, 2, 2, 'reklamodawcy@emaco.info',
    'reklamodawcy@emaco.info', 'reklamodawcy@emaco.info',
    'reklamodawcy@emaco.info', '', '',
    '+48 506 498 327 ', '', 'E-ma ',
    '', 'PL5472083007', 'Mr', 'Czekaj',
    'W&amp;#322;adys&amp;#322;aw',
    'Trzebi&amp;#324;ska 40  lok. 4 ',
    '32-500', 'Chrzanów', 'pl', 'FRA',
    0, 2, 50, 'Chèque', '', '', '', '', '2016-09-13'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    196, 1, 2, 'charline@sharemyclick.com',
    'charline@sharemyclick.com', 'charline@sharemyclick.com',
    'charline@sharemyclick.com', '',
    '', '', '', 'sharemyclick', '', '',
    'Mr', 'rivera', 'charline', '', '',
    '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2016-09-14'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    197, 1, 2, 'cedric.courtinat@m6.fr',
    'cedric.courtinat@m6.fr', 'cedric.courtinat@m6.fr',
    'cedric.courtinat@m6.fr', '', '',
    '', '', 'Oxygem', '', '', 'Mr', 'Hiraclides',
    'Pierre ', '', '', '', 'fr', 'FRA',
    2, 1, 50, 'Chèque', '', '', '', '', '2016-09-20'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    198, 2, 2, 'jules.reul@m6.fr', 'jules.reul@m6.fr',
    'jules.reul@m6.fr', 'jules.reul@m6.fr',
    '', '', '', '', 'oxygem m6', 414549469,
    'FR70414549469', 'Mr', 'Hiraclides',
    'Pierre ', '125 avenue de la république',
    59110, 'La Madeleine', 'fr', 'FRA',
    2, 1, 50, 'Virement', 'fr7630003010980002076651047',
    'sogefrppllm', 'SOCIETE GENERALE PARIS',
    '', '2016-09-20'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    199, 1, 2, 'nicolas.grolee@webdone.net',
    'nicolas.grolee@webdone.net', 'nicolas.grolee@webdone.net',
    'nicolas.grolee@webdone.net', '',
    '', '', '', 'Webdone', '', '', 'Mr',
    'Grolée', 'Nicolas', '11 B RUE MADAME',
    95550, 'BESSANCOURT', 'fr', 'FRA',
    2, 1, 50, 'Chèque', '', '', '', '', '2016-09-29'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    200, 1, 2, 'lina.benmessaoud95@gmail.com',
    'lina.benmessaoud95@gmail.com',
    'lina.benmessaoud95@gmail.com',
    'lina.benmessaoud95@gmail.com',
    '', '', '', '', 'mailinmotion', '',
    '', 'Mr', 'metzler', 'alexandre',
    '', '', '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2016-09-30'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    201, 1, 2, 'nicolas@reflexemedia.com',
    'nicolas@reflexemedia.com', 'nicolas@reflexemedia.com',
    'nicolas@reflexemedia.com', '',
    '', '', '', 'REFLEXEMEDIA', '', '',
    'Mr', 'LERAY', 'Nicolas ', '', '',
    '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2016-10-13'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    202, 1, 2, 'Sherwin@sparklingadv.com',
    'Sherwin@sparklingadv.com', 'Sherwin@sparklingadv.com',
    'Sherwin@sparklingadv.com', '',
    '', '', '', 'Sparkling ADV SL', '',
    '', 'Mr', 'Quirino', 'Lorenzo ', '',
    '', '', 'fr', 'FRA', 2, 1, 400, 'Chèque',
    '', '', '', '', '2016-10-20'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    203, 1, 2, 'contact@live-com.pro',
    'contact@live-com.pro', 'contact@live-com.pro',
    'contact@live-com.pro', '', '', '',
    '', 'Livecom   ', '', '', 'Mr', 'Bensimon ',
    'Warren', '', '', '', 'fr', 'FRA', 2,
    1, 50, 'Chèque', '', '', '', '', '2016-10-31'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    204, 1, 2, 'clement.charles@verticalizer.com',
    'clement.charles@verticalizer.com',
    'clement.charles@verticalizer.com',
    'accounting@atcfm.com', '', '', '',
    '', 'Verticalizer Media Group',
    '', '', 'Mr', 'Charles', 'Clément',
    '', '', '', 'fr', 'FRA', 2, 1, 50, 'Virement',
    'CH09 0900 0000 1452 1868 4', 'POFICHBE',
    'PostFinance SA, Mingerstrasse 20, 3030 Berne, Suisse',
    '', '2016-11-03'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    205, 1, 2, 'commercial5@capdecision.fr',
    'commercial5@capdecision.fr', 'commercial5@capdecision.fr',
    'commercial5@capdecision.fr', '',
    '', '', '', '', '', '', 'Mr', 'metzler',
    'Alexandre', '', '', '', 'fr', 'FRA',
    1, 1, 50, 'Chèque', '', '', '', '', '2016-11-29'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    206, 1, 2, 'alice@regip.fr', 'alice@regip.fr',
    'alice@regip.fr', 'alice@regip.fr',
    '', '06.14.27.05.63', '', '', 'REGIP SAS',
    '', '', 'Mme', 'faget', 'alice', '',
    '', '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2016-12-06'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    207, 1, 2, 'Audrey@mediaffiliation.com',
    'Audrey@mediaffiliation.com', 'Audrey@mediaffiliation.com',
    'Audrey@mediaffiliation.com', '',
    '', '', '', 'Mediaffiliation-Point communication',
    '', '', 'Mr', 'Bogey', 'Jani ', '',
    '', '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2016-12-09'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    208, 1, 2, 'mickael@digitalks.fr',
    'mickael@digitalks.fr', 'mickael@digitalks.fr',
    'mickael@digitalks.fr', '', '', '',
    '', 'digitalks', '', '', 'Mr', 'BELHASSEN',
    'Mickaël ', '', '', '', 'fr', 'FRA',
    2, 1, 50, 'Chèque', '', '', '', '', '2016-12-09'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    209, 1, 2, 'i.barzani@autobiz.com',
    'i.barzani@autobiz.com', 'i.barzani@autobiz.com',
    'i.barzani@autobiz.com', 'ikramaffilinet',
    '01 41 44 91 00', '07 60 55 22 80',
    '01 41 44 91 01', '', '', '', 'Mme',
    'Barzani', 'Ikram', ' 15-19 Quai Gallieni',
    92150, 'Suresnes', 'fr', 'FRA', 0,
    2, 50, 'Chèque', '', '', '', '', '2016-12-13'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    210, 1, 2, 'aurelien.hoffmann@effidata.com',
    'aurelien.hoffmann@effidata.com',
    'aurelien.hoffmann@effidata.com',
    'aurelien.hoffmann@effidata.com',
    '', '', '', '', 'EFFIDATA', 41862988700013,
    'FR21418629887', 'Mme', 'HOFFNUNG',
    'Laurence ', '152, boulevard Pereire',
    75017, 'Paris', 'fr', 'FRA', 2, 1, 50,
    'Chèque', '', '', '', '', '2016-12-21'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    211, 2, 2, 'elsa.morange@unitead.fr',
    'elsa.morange@unitead.fr', 'elsa.morange@unitead.fr',
    'elsa.morange@unitead.fr', '', '',
    '', '', 'UNITEAD GLOBAL', '', 'FR24 505084921',
    'Mme', 'Morange', 'Elsa', '16, rue Palloy',
    92110, 'Clichy', 'fr', 'FRA', 2, 1,
    25, 'Virement', 'FR76 1287 9000 0100 2210 3964 437',
    'DELUFR22XXX', 'BANQUE DELUBAC & CIE 16, Place Saléon Terras 07160 LE CHEYLARD',
    '', '2016-12-29'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    212, 2, 2, 'pierrekontiki@gmail.com',
    'pierrekontiki@gmail.com', 'pierrekontiki@gmail.com',
    'pierrekontiki@gmail.com', '', '',
    '', '', 'Kontiki Media Ltd', '', 'GB159388558',
    'Mme', 'Boehm', 'Stéphanie ', '7 MULBERRY CLOSE ',
    'E48BS', 'LONDON  ', 'uk', 'FRA',
    2, 1, 50, 'Chèque', '', '', '', '', '2017-01-03'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    213, 1, 2, 'ametzler2@hotmail.fr',
    'ametzler2@hotmail.fr', 'ametzler2@hotmail.fr',
    'ametzler2@hotmail.fr', '', '', '',
    '', 'optin adresse', '', '', 'Mr',
    'metzler', 'alexandre', '', '', '',
    'fr', 'FRA', 2, 1, 50, 'Chèque', '',
    '', '', '', '2017-01-20'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    214, 1, 2, 'ametzler3@hotmail.fr',
    'ametzler3@hotmail.fr', 'ametzler3@hotmail.fr',
    'ametzler3@hotmail.fr', '', '', '',
    '', 'optin adresse', '', '', 'Mr',
    'metzler', 'alexandre', '', '', '',
    'fr', 'FRA', 2, 1, 50, 'Chèque', '',
    '', '', '', '2017-01-20'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    215, 1, 2, 'mattia.saverioberrica@yonkana.net',
    'mattia.saverioberrica@yonkana.net',
    'mattia.saverioberrica@yonkana.net',
    'mattia.saverioberrica@yonkana.net',
    '', '', '', '', 'YONKANA', '', '', 'Mr',
    'brizzo', 'sergio ', '', '', '', 'fr',
    'FRA', 2, 1, 50, 'Chèque', '', '', '',
    '', '2017-02-03'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    216, 1, 2, 'jkoskas@gmail.com', 'jkoskas@gmail.com',
    'jkoskas@gmail.com', 'jkoskas@gmail.com',
    '', '', '', '', 'Nodalys', '', '', 'Mr',
    'koskas', 'jérémy', '', '', '', 'fr',
    'FRA', 2, 1, 50, 'Chèque', '', '', '',
    '', '2017-02-06'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    217, 1, 2, 'idir@concretisez.fr', 'idir@concretisez.fr',
    'idir@concretisez.fr', 'idir@concretisez.fr',
    '', '', '', '', 'NITESCENCE INTERACTIVE',
    '', '', 'Mr', 'ADOUANE', 'IDIR', '',
    '', '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2017-02-14'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    218, 1, 2, 'guillaume.autret@gmail.com',
    'guillaume.autret@gmail.com', 'guillaume.autret@gmail.com',
    'guillaume.autret@gmail.com', '',
    '', '', '', 'EPTIC', '', '', 'Mr', 'Autret',
    'Guillaume', '', '', '', 'fr', 'FRA',
    0, 2, 50, 'Chèque', '', '', '', '', '2017-02-16'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    220, 1, 2, 'victorialeadfactory@gmail.com',
    'victorialeadfactory@gmail.com',
    'victorialeadfactory@gmail.com',
    'victorialeadfactory@gmail.com',
    '', '', '', '', 'adroll programmatique',
    '', '', 'Mr', 'chiaramonte', 'vic',
    '', '', '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2017-02-23'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    221, 1, 2, 'Mathilde@sygnature.fr',
    'Mathilde@sygnature.fr', 'Mathilde@sygnature.fr',
    'Mathilde@sygnature.fr', '', '',
    '', '', 'sygnature', '', '', 'Mr', 'DELESALLE',
    'tanguy', '', '', '', 'fr', 'FRA', 2,
    1, 50, 'Virement', 'FR7615629027280003224910125',
    'CMCIFR2A', 'CREDIT MUTUEL 55 BD du gal Leclerc 59100 Roubaix',
    '', '2017-03-02'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    222, 1, 2, 'taboola@taboola.fr', 'taboola@taboola.fr',
    'taboola@taboola.fr', 'taboola@taboola.fr',
    '', '', '', '', 'taboola', '', '', 'Mr',
    'taboola', 'test', '', '', '', 'fr',
    'FRA', 2, 1, 50, 'Chèque', '', '', '',
    '', '2017-03-06'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    223, 1, 2, 'metzleralexandre@gmail.com',
    'metzleralexandre@gmail.com', 'metzleralexandre@gmail.com',
    'metzleralexandre@gmail.com', '',
    '', '', '', 'capdecision native',
    '', '', 'Mr', 'metzler', 'alex', '6 impasse des jonquilles',
    72555, 'La Seyne Sur Mer', 'fr',
    'FRA', 2, 1, 50, 'Chèque', '', '', '',
    '', '2017-03-20'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    224, 1, 2, 'fpaquin@groupelexpress.fr',
    'fpaquin@groupelexpress.fr', 'fpaquin@groupelexpress.fr',
    'fpaquin@groupelexpress.fr', '',
    '', '', '', 'altice medias', '', '',
    'Mr', 'paquin', 'f', '', '', '', 'fr',
    'FRA', 2, 1, 50, 'Chèque', '', '', '',
    '', '2017-03-31'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    225, 1, 2, 'frederic@wbusiness.fr',
    'frederic@wbusiness.fr', 'frederic@wbusiness.fr',
    'frederic@wbusiness.fr', '', '',
    '', '', 'wbusiness', '', '', 'Mr', 'rodriguez ',
    'frederic', '', '', '', 'fr', 'FRA',
    2, 1, 50, 'Chèque', '', '', '', '', '2017-04-06'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    226, 1, 2, 'contact@emailing-promo.com  ',
    'contact@emailing-promo.com  ',
    'contact@emailing-promo.com  ',
    'contact@emailing-promo.com  ',
    '', '', '', '', 'emailing promo', '',
    '', 'Mr', 'rodriguez ', 'fréderic',
    '', '', '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2017-04-06'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    227, 1, 2, 'oprechais@actiplay.com',
    'oprechais@actiplay.com', 'oprechais@actiplay.com',
    'oprechais@actiplay.com', '', '',
    '', '', 'actiplay', '', '', 'Mr', 'prechais',
    'oceane', '', '', '', 'fr', 'FRA', 2,
    1, 50, 'Chèque', '', '', '', '', '2017-04-07'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    228, 1, 2, 'ametzler88@hotmail.fr',
    'ametzler88@hotmail.fr', 'ametzler88@hotmail.fr',
    'ametzler88@hotmail.fr', '', '',
    '', '', 'capdecision/facebook', '',
    '', 'Mr', 'metzler', 'alexandre',
    'http://Panel conso cap', 72555,
    'La Seyne Sur Mer', 'fr', 'FRA',
    2, 1, 50, 'Chèque', '', '', '', '', '2017-04-25'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    229, 1, 2, 'olivier.david@mediafoxmail.fr',
    'olivier.david@mediafoxmail.fr',
    'olivier.david@mediafoxmail.fr',
    'olivier.david@mediafoxmail.fr',
    '', '', '', '', 'MediaFox', '', '', 'Mr',
    'hageg', 'yoni ', '', '', '', 'fr',
    'FRA', 2, 1, 50, 'Chèque', '', '', '',
    '', '2017-04-26'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    230, 1, 2, 'virginie.huraux@adthink.com',
    'virginie.huraux@adthink.com',
    'virginie.huraux@adthink.com',
    'a.messioud@adthink.com', 'vhuraux',
    '', '', '', 'Adthink.com SA', '', 'CHE-228.280.267',
    'Mlle', 'Huraux', 'Virginie', 'route des Acacias 24',
    1227, 'Carouge GE', 'fr', 'FRA', 2,
    1, 50, 'Chèque', '', '', '', '', '2017-04-28'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    231, 1, 2, 'ametzler5@hotmail.fr',
    'ametzler5@hotmail.fr', 'ametzler5@hotmail.fr',
    'ametzler5@hotmail.fr', '', '', '',
    '', 'capdec-samuel', '', '', 'Mr',
    'metzler', 'alexandre', '', '', '',
    'fr', 'FRA', 2, 1, 50, 'Chèque', '',
    '', '', '', '2017-05-05'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    232, 1, 2, 'jean-frederic.hubert@tradedoubler.com',
    'jean-frederic.hubert@tradedoubler.com',
    'jean-frederic.hubert@tradedoubler.com',
    'jean-frederic.hubert@tradedoubler.com',
    '', '', '', '', 'tradedoubler', '',
    '', 'Mr', 'LAFORET', 'philippe', '',
    '', '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2017-05-11'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    233, 1, 2, 'cristina.canales@feebbo.com',
    'cristina.canales@feebbo.com',
    'cristina.canales@feebbo.com',
    'facturacion@feebbo.com', '', '',
    '', '', 'FEEBBO SOLUTIONS', '', '',
    'Mr', 'Hernandez', 'Gregorio ', '',
    '', '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2017-05-17'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    234, 1, 2, 'yoni@mesdeveloppeurs.fr',
    'yoni@mesdeveloppeurs.fr', 'yoni@mesdeveloppeurs.fr',
    'yoni@mesdeveloppeurs.fr', '', '',
    '', '', 'mesdeveloppeurs', '', '',
    'Mr', 'hagege', 'yoni', '', '', '',
    'fr', 'FRA', 2, 1, 50, 'Chèque', '',
    '', '', '', '2017-05-17'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    235, 1, 2, 'datafrance@beverlydata.com',
    'datafrance@beverlydata.com', 'datafrance@beverlydata.com',
    'datafrance@beverlydata.com', '',
    '', '', '', 'VERTIGO MEDIA S.L', '',
    '', 'Mr', 'Benichou', 'Stephane ',
    '', '', '', 'fr', 'FRA', 2, 1, 0, 'Chèque',
    '', '', '', '', '2017-05-19'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    236, 1, 2, 'ametzler6@hotmail.fr',
    'ametzler6@hotmail.fr', 'ametzler6@hotmail.fr',
    'ametzler6@hotmail.fr', '', '', '',
    '', 'capdec-maildrop', '', '', 'Mr',
    'metzler', 'alexandre', '6 impasse des jonquilles',
    72555, '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2017-05-23'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    237, 1, 2, 'ametzler7@hotmail.fr',
    'ametzler7@hotmail.fr', 'ametzler7@hotmail.fr',
    'ametzler7@hotmail.fr', '', '', '',
    '', 'club des eshoppers', '', '',
    'Mr', 'metzler', 'alexandre', '6 impasse des jonquilles',
    72555, 'La Seyne Sur Mer', 'fr',
    'FRA', 2, 1, 50, 'Chèque', '', '', '',
    '', '2017-05-30'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    267, 1, 2, 'laurent@seeinbox.fr', 'laurent@seeinbox.fr',
    'laurent@seeinbox.fr', 'laurent@seeinbox.fr',
    '', '', '', '', 'seeinbox', '', '', 'Mr',
    'seeinbox', 'laurent', '', '', '',
    'fr', 'FRA', 2, 1, 25, 'Virement', 'FR7610278079110002054710194',
    'CMCIFR2A', 'CREDIT MUTUEL 83500 LA SEYNE SUR MER',
    '', '2017-12-08'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    238, 1, 2, 'ametzler8@hotmail.fr',
    'ametzler8@hotmail.fr', 'ametzler8@hotmail.fr',
    'ametzler8@hotmail.fr', '', '', '',
    '', 'alexandre metzler', '', '', 'Mr',
    'metzler', 'alexandre', '6 impasse des jonquilles',
    72555, 'La Seyne Sur Mer', 'fr',
    'FRA', 2, 1, 50, 'Chèque', '', '', '',
    '', '2017-06-19'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    239, 1, 2, 'bdeltheil@ubik-advertising.com',
    'bdeltheil@ubik-advertising.com',
    'bdeltheil@ubik-advertising.com',
    'bdeltheil@ubik-advertising.com',
    '', '', '', '', 'UBIK ADVERTISING',
    '', '', 'Mr', 'deltheil', 'bertrand',
    '', '', '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2017-06-20'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    240, 1, 2, 'romina.sborzacchi@mredpoint.com',
    'romina.sborzacchi@mredpoint.com',
    'romina.sborzacchi@mredpoint.com',
    'romina.sborzacchi@mredpoint.com',
    '', '', '', '', 'Red Point LTD', '',
    '', 'Mr', 'sborzacchi', 'romina',
    '', '', '', 'fr', 'FRA', 2, 1, 50, 'Virement',
    'MT81STBA19116000000001047702421',
    'STBAMTMTXXX', 'Satabank plc', '',
    '2017-06-27'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    241, 1, 2, 'ametzler9@hotmail.fr',
    'ametzler9@hotmail.fr', 'ametzler9@hotmail.fr',
    'ametzler9@hotmail.fr', '', '', '',
    '', 'panel conso cap', '', '', 'Mr',
    'metzler', 'alexandre', '6 impasse des jonquilles',
    72555, 'La Seyne Sur Mer', 'fr',
    'FRA', 2, 1, 50, 'Chèque', '', '', '',
    'v', '2017-06-28'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    242, 1, 2, 'manager@leadsmakers.com',
    'manager@leadsmakers.com', 'manager@leadsmakers.com',
    'manager@leadsmakers.com', 'manager@leadsmakers.com',
    442035295640, '', '', 'MAKEMIL LIMITED',
    10739156, '', 'Mr', 'Andrew', 'Barnes',
    'Kemp House, 160 City Road', 'EC1V 2',
    'London', 'gb', 'FRA', 0, 2, 50, 'Chèque',
    '', '', '', '', '2017-06-28'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    243, 2, 2, 'juillot.alex@gmail.com',
    'juillot.alex@gmail.com', 'juillot.alex@gmail.com',
    'juillot.alex@gmail.com', '', '',
    684620745, '', 'Juillot', 52218788900020,
    'FR2522187889', 'Mr', 'Juillot',
    'Alexandre', '4 av de garnac', 11420,
    'Belpech', 'fr', 'FRA', 0, 2, 50, 'Chèque',
    '', '', '', '', '2017-07-07'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    244, 1, 2, 'Bertrand@edentify.fr',
    'Bertrand@edentify.fr', 'Bertrand@edentify.fr',
    'Bertrand@edentify.fr', '', '', '',
    '', 'EDENTIFY', '', '', 'Mr', 'lelong',
    'bertrand', '', '', '', 'fr', 'FRA',
    2, 1, 50, 'Chèque', '', '', '', '', '2017-07-12'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    245, 1, 2, 'julien@webtargetmedias.fr',
    'julien@webtargetmedias.fr', 'julien@webtargetmedias.fr',
    'julien@webtargetmedias.fr', '',
    '', '', '', 'Web Target Medias', '',
    '', 'Mme', 'Chekly', 'Jade ', '', '',
    '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2017-07-13'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    246, 2, 2, 'networks@cpx-united.com',
    'networks@cpx-united.com', 'networks@cpx-united.com',
    'networks@cpx-united.com', 'christoph_maiwald',
    ' +49 (0) 30 2089 679', '', '', 'CPX United GmbH',
    'None', 'DE264625849', 'Mr', 'Bode',
    'Nico', 'Bunzlauer Straße 32',
    53721, 'Siegburg', 'de', 'FRA', 0,
    2, 50, 'Chèque', '', '', '', '', '2017-07-14'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    247, 1, 2, 'christoph@cpx-united.com',
    'christoph@cpx-united.com', 'christoph@cpx-united.com',
    'christoph@cpx-united.com', '',
    '', '', '', 'CPX United GmbH', '',
    '', 'Mr', 'maiwald', 'christophe',
    '', '', '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2017-07-17'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    248, 1, 2, 'Bertrand2@edentify.fr',
    'Bertrand2@edentify.fr', 'Bertrand2@edentify.fr',
    'Bertrand2@edentify.fr', '', '',
    '', '', 'vertigo', '', '', 'Mr', 'lelong',
    'bertrand', '', '', '', 'fr', 'FRA',
    2, 1, 50, 'Chèque', '', '', '', '', '2017-07-18'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    249, 1, 2, 'andy@digix-media.fr', 'andy@digix-media.fr',
    'andy@digix-media.fr', 'andy@digix-media.fr',
    '', '', '', '', 'digix media', '', '',
    'Mr', 'digix', 'andy', '', '', '', 'fr',
    'FRA', 2, 1, 50, 'Chèque', '', '', '',
    '', '2017-09-08'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    250, 1, 2, 'sam@24media.org', 'sam@24media.org',
    'sam@24media.org', 'sam@24media.org',
    '', '', '', '', 'Sanberg Limited',
    '', '', 'Mr', 'majdi', 'sam', '', '',
    '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2017-09-08'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    251, 2, 2, 'monetisation@sekidigital.com',
    'monetisation@sekidigital.com',
    'monetisation@sekidigital.com',
    'monetisation@sekidigital.com',
    '', '', '', '', 'Seki Digital', '831 112 537 00012',
    'FR 56 831 112 537', 'Mr', ' Anders ',
    'Kirk', '15 Montee des Mandarines',
    6310, 'Beaulieu-sur-Mer', 'fr', 'FRA',
    2, 1, 50, 'Chèque', '', '', '', '', '2017-09-13'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    252, 1, 2, 'monika@webmilk.fr', 'monika@webmilk.fr',
    'monika@webmilk.fr', 'monika@webmilk.fr',
    '', '', '', '', 'Webmilk', '', '', 'Mr',
    'Ferraz', 'Julia', '', '', '', 'fr',
    'FRA', 2, 1, 50, 'Virement', 'FR76 3000 4015 3000 0100 7581 482',
    'BNPAFRPPXXX', 'BNPPARB PARIS PLA MEXICO',
    '', '2017-10-02'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    253, 1, 2, 'jean@jrmedia.fr', 'jean@jrmedia.fr',
    'jean@jrmedia.fr', 'jean@jrmedia.fr',
    '', '', '', '', 'JR MEDIA', '', '', 'Mr',
    'jrmedia', 'jean', '', '', '', 'fr',
    'FRA', 2, 1, 50, 'Chèque', '', '', '',
    '', '2017-10-02'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    254, 2, 2, 'redactrices@sobusygirls.fr',
    'redactrices@sobusygirls.fr', 'redactrices@sobusygirls.fr',
    'redactrices@sobusygirls.fr', '',
    '', '', '', 'SARL', '', '', 'Mr', 'Dora',
    'Sébastien', '8 Rue du Montarlier',
    89116, 'La Celle Saint Cyr', 'fr',
    'FRA', 0, 2, 50, 'Chèque', '', '', '',
    '', '2017-10-06'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    257, 2, 2, 'contact@dmconcept.fr',
    'contact@dmconcept.fr', 'contact@dmconcept.fr',
    'contact@dmconcept.fr', '', '', '',
    '', 'DMConcept', '', '', 'Mr', 'Deutscher ',
    'Matthieu', '', '', '', 'fr', 'FRA',
    2, 1, 50, 'Chèque', '', '', '', '', '2017-11-02'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    255, 1, 2, 'a.mercier@magikwebstudio.com',
    'a.mercier@magikwebstudio.com',
    'a.mercier@magikwebstudio.com',
    'a.mercier@magikwebstudio.com',
    '', '', '', '', 'Magik Web Studio',
    '', '', 'Mr', 'montigny', 'marc', '',
    '', '', 'fr', 'FRA', 2, 1, 50, 'Virement',
    'FR7612879000010022114115390',
    'DELUFR22XXX', 'DELUBAC & CIE',
    '', '2017-10-09'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    256, 1, 2, 'socheata@apsaramedia2.fr',
    'socheata@apsaramedia2.fr', 'socheata@apsaramedia2.fr',
    'socheata@apsaramedia2.fr', '',
    '', '', '', 'APSARA MEDIA SARL', '',
    '', 'Mr', 'neang', 'socheata', '',
    '', '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2017-10-25'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    258, 1, 2, 'robin@thedigitlabs.com',
    'robin@thedigitlabs.com', 'robin@thedigitlabs.com',
    'robin@thedigitlabs.com', '', '',
    '', '', 'THE DIGITALABS', '', '', 'Mr',
    'Benattar', 'Robin', '36 rue des peupliers',
    92100, 'Boulogne-Billancourt', 'fr',
    'FRA', 2, 1, 50, 'Chèque', '', '', '',
    '', '2017-11-06'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    259, 2, 2, 'Guillaume.legrand@compleo.fr',
    'romain.dupressoir@compleo.fr',
    'romain.dupressoir@compleo.fr',
    'romain.dupressoir@compleo.fr',
    'r.dupressoir', '', 688926468, '',
    'compleo', 53119083300016, 'FR57531190833',
    'Mr', 'Dupressoir', 'Romain', '11 av Daviot',
    6100, 'Nice', 'fr', 'FRA', 2, 1, 50,
    'Virement', 'FR76 1460 7004 2260 2216 2557 318',
    'CCBPFRPPMAR', 'Banque Populaire 457 Prom. des Anglais, 06200 Nice',
    '', '2017-11-07'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    260, 1, 2, 'frederique@tagadamedia.com',
    'frederique@tagadamedia.com', 'frederique@tagadamedia.com',
    'frederique@tagadamedia.com', '',
    '', '', '', 'TAGADAMEDIA', '', '', 'Mr',
    'MIMOUNI', 'ADAMS ', '', '', '', 'fr',
    'FRA', 2, 1, 50, 'Chèque', '', '', '',
    '', '2017-11-07'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    261, 1, 2, 'lea.bergamasco@team.deux-trente.com',
    'lea.bergamasco@team.deux-trente.com',
    'lea.bergamasco@team.deux-trente.com',
    'lea.bergamasco@team.deux-trente.com',
    '', '', '', '', 'deux trente medias',
    '', '', 'Mr', 'Cottin', 'Arnaud', '',
    '', '', 'fr', 'FRA', 2, 1, 25, 'Chèque',
    '', '', '', '', '2017-11-07'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    262, 1, 2, 'bdumay@veniseactivation.com',
    'bdumay@veniseactivation.com',
    'bdumay@veniseactivation.com',
    'bdumay@veniseactivation.com',
    '', '', '', '', 'venise activation',
    '', '', 'Mr', 'dumay', 'bruno ', '',
    '', '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2017-11-09'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    263, 1, 2, 'aminata.diallo@22bismedia.com',
    'aminata.diallo@22bismedia.com',
    'aminata.diallo@22bismedia.com',
    'aminata.diallo@22bismedia.com',
    '', '', '', '', '22bismedia', '', '',
    'Mr', 'diallo', 'aminata', '', '',
    '', 'fr', 'FRA', 2, 1, 50, 'Chèque',
    '', '', '', '', '2017-11-27'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    264, 1, 2, 'ametzler11@hotmail.fr',
    'ametzler11@hotmail.fr', 'ametzler11@hotmail.fr',
    'ametzler11@hotmail.fr', '', '',
    '', '', 'NEWSLETTER PLANET', '', '',
    'Mr', 'PLANET', 'NEWS', '6 impasse des jonquilles',
    72555, 'La Seyne Sur Mer', 'fr',
    'FRA', 2, 1, 50, 'Chèque', '', '', '',
    '', '2017-11-29'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    265, 1, 2, 'info@eadso.fr', 'info@eadso.fr',
    'info@eadso.fr', 'info@eadso.fr',
    'eadsopl', '', '', '', 'Eadso Olga Sobczak',
    'PL5981634881', '', 'Mme', 'Sobczak',
    'Olga', 'Wawrzyniaka 10/64', 69100,
    'Slubice', 'pl', 'FRA', 0, 2, 50, 'Chèque',
    '', '', '', '', '2017-11-30'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    266, 1, 2, 'gijs@vipresponse.nl ',
    'gijs@vipresponse.nl ', 'gijs@vipresponse.nl ',
    'gijs@vipresponse.nl ', '', '', '',
    '', 'vip response', '', '', 'Mr', 'Liekens',
    'gijs', '', '', '', 'fr', 'FRA', 2, 1,
    50, 'Chèque', '', '', '', '', '2017-12-05'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    268, 1, 2, 'account@hks-media.ch',
    'account@hks-media.ch', 'account@hks-media.ch',
    'account@hks-media.ch', '', '', '',
    '', 'HKS Media', '', '', 'Mr', 'De Pachetere',
    'Patric ', '', '', '', 'fr', 'FRA',
    2, 1, 50, 'Chèque', '', '', '', '', '2017-12-12'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    269, 1, 2, 'hua@ambremarketing.com',
    'hua@ambremarketing.com', 'hua@ambremarketing.com',
    'hua@ambremarketing.com', '', '',
    '', '', 'AMBRE MARKETING', '', '',
    'Mr', 'TANG', 'HUA', '', '', '', 'fr',
    'FRA', 2, 1, 50, 'Chèque', '', '', '',
    '', '2017-12-13'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    270, 1, 2, 'emmanuel.namer@gmail.com',
    'emmanuel.namer@gmail.com', 'emmanuel.namer@gmail.com',
    'emmanuel.namer@gmail.com', '',
    '', '', '', 'DEVOLA', '', '', 'Mr', 'namer',
    'emmanuel', '', '', '', 'fr', 'FRA',
    2, 1, 50, 'Chèque', '', '', '', '', '2017-12-14'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    271, 1, 2, 'stephane.mahi@mediazur-france.com',
    'stephane.mahi@mediazur-france.com',
    'stephane.mahi@mediazur-france.com',
    'stephane.mahi@mediazur-france.com',
    '', '', '', '', 'mediazur', '', '', 'Mr',
    'pochet', 'romain', '', '', '', 'fr',
    'FRA', 2, 1, 50, 'Chèque', '', '', '',
    '', '2017-12-28'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    272, 1, 2, 'marketing.fans.database@gmail.com',
    'marketing.fans.database@gmail.com',
    'marketing.fans.database@gmail.com',
    'marketing.fans.database@gmail.com',
    '', 420774314473, 420774314473, '',
    'carlo fano', '', '', 'Mr', 'carlo ',
    'fano', 'namesti republiky 2112/2',
    70200, 'ostrava', 'cz', 'FRA', 2, 1,
    50, 'Chèque', '', '', '', '', '2018-01-02'
  );
/* INSERT QUERY */
INSERT INTO editeurs(
  ids_editeur, id_type, type, login, email,
  technique, compta, skype, telephone,
  cellulaire, fax, nom_entreprise,
  siret, tva, civ, nom, prenom, adresse,
  codepostale, ville, pays, racine,
  commercial, statut, paiement, paiement_type,
  iban, swift, banque, commentaire,
  date_creation
)
VALUES
  (
    273,
    2,
    2,
    'Valerie@vj-interactive.com',
    'Valerie@vj-interactive.com',
    'Valerie@vj-interactive.com',
    'Valerie@vj-interactive.com',
    '',
    '',
    '',
    '',
    'VJ interactive',
    '833 663 214 00017',
    'FR 48 833663214',
    'Mr',
    'Jourdan',
    'Valérie ',
    '5 allée du Comte de la Vaulx',
    78210,
    'ST Cyr l  école ',' fr ',' FRA ',2,1,50,' Virement ',' FR76 3006 6109 0800 0202 8030 106 ',' CMCIFRPP ',' CIC ST CLOUD ','',' 2018 - 01 - 04 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 274,1,2,' mgrimaldii@yahoo.it ',' mgrimaldii@yahoo.it ',' mgrimaldii@yahoo.it ',' mgrimaldii@yahoo.it ','','','','',' GRIMALDI MARCELLO ','','',' Mr ',' GRIMALDI ',' MARCELLO ','','','',' fr ',' FRA ',2,1,50,' Chèque ','','','','',' 2018 - 01 - 04 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 275,1,2,' gabriel.beauvilain@mail.com ',' gabriel.beauvilain@mail.com ',' gabriel.beauvilain@mail.com ',' gabriel.beauvilain@mail.com ','','','','',' RAIN MAKER ','','',' Mr ',' beauvilain ',' gabriel ','','','',' fr ',' FRA ',2,1,50,' Chèque ','','','','',' 2018 - 01 - 09 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 276,1,2,' fallone@data - agency.fr ',' fallone@data - agency.fr ',' fallone@data - agency.fr ',' fallone@data - agency.fr ','','','','',' data agency ','','',' Mr ',' sgaravizzi ',' tom ','','','',' fr ',' FRA ',2,1,50,' Chèque ','','','','',' 2018 - 01 - 16 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 277,1,2,' sarah@rodeoptin.fr ',' sarah@rodeoptin.fr ',' sarah@rodeoptin.fr ',' sarah@rodeoptin.fr ','','','','',' rodeoptin ','','',' Mr ',' hagege ',' yoni ','','','',' fr ',' FRA ',2,1,50,' Chèque ','','','','',' 2018 - 01 - 18 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 278,1,2,' diffusion@instamailmedia.com ',' diffusion@instamailmedia.com ',' diffusion@instamailmedia.com ',' diffusion@instamailmedia.com ','','','','',' SAS INSTA MAIL MEDIA ','','',' Mr ',' laloum ',' jérémie ','','','',' fr ',' FRA ',2,1,50,' Chèque ','','','','',' 2018 - 01 - 23 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 279,1,2,' alizee.r@webmediarm.com ',' alizee.r@webmediarm.com ',' alizee.r@webmediarm.com ',' alizee.r@webmediarm.com ','','','','',' DISPLAY WEBMEDIA RM ','','',' Mr ',' Masse ',' Rémy ','','','',' fr ',' FRA ',2,1,50,' Chèque ','','','','',' 2018 - 01 - 23 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 280,1,2,' contact@imprimerpascher.com ',' contact@imprimerpascher.com ',' contact@imprimerpascher.com ',' contact@imprimerpascher.com ','','','','',' MAGELLAN PARTICIPATIONS ','','',' Mr ',' Habrih ',' Nadir ','','','',' fr ',' FRA ',2,1,50,' Chèque ','','','','',' 2018 - 01 - 31 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 281,1,2,' marketingstefy@gmail.com ',' marketingstefy@gmail.com ',' marketingstefy@gmail.com ',' marketingstefy@gmail.com ','','','','',' holidays times ','','',' Mme ',' palmieri ',' stefania ','','','',' fr ',' FRA ',2,1,50,' Chèque ','','','','',' 2018 - 02 - 07 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 282,1,2,' affiliate - fr@orangebuddies.com ',' affiliate - fr@orangebuddies.com ',' affiliate - fr@orangebuddies.com ',' affiliate - fr@orangebuddies.com ','','','','',' Orangebuddies Media ','','',' Mme ',' Casander - Boogers ',' Sabrine ','','','',' fr ',' FRA ',2,1,50,' Chèque ','','','','',' 2018 - 02 - 09 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 283,2,2,' laurence@traffic - labs.com ',' laurence@traffic - labs.com ',' laurence@traffic - labs.com ',' laurence@traffic - labs.com ',' laurence@traffic - lab ','','','',' Traffic Labs ','',' FR54788953297 ',' Mme ',' Regnault ',' Laurence ',' 9 rue andré darbon ',33300,' Bordeaux ',' fr ',' FRA ',2,1,25,' Virement ',' FR7610057190890009848350106 ',' CMCIFRPP ',' CIC BORDEAUX BASTIDE 9 PLACE STALINGRAD 33100 BORDEAUX ','',' 2018 - 02 - 15 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 284,1,2,' alice@arianelab.com ',' alice@arianelab.com ',' alice@arianelab.com ',' alice@arianelab.com ','','','','',' Arianelab ','','',' Mr ',' Gallienne ',' Mathieu ','','','',' fr ',' FRA ',2,1,50,' Chèque ','','','','',' 2018 - 02 - 27 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 285,1,2,' megane@gbr - media.com ',' megane@gbr - media.com ',' megane@gbr - media.com ',' megane@gbr - media.com ','','','','',' GBR diffusion ','','',' Mr ',' Ralinjatovo ',' Jakina ','','','',' fr ',' FRA ',2,1,50,' Chèque ','','','','',' 2018 - 03 - 09 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 286,1,2,' hello@ogmamedia.fr ',' hello@ogmamedia.fr ',' hello@ogmamedia.fr ',' hello@ogmamedia.fr ','','','','',' ogma media ','','',' Mr ',' lelong ',' bertrand ','','','',' fr ',' FRA ',2,1,50,' Chèque ','','','','',' 2018 - 03 - 15 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 287,2,2,' affiliati@infinitodigitale.com ',' affiliati@infinitodigitale.com ',' affiliati@infinitodigitale.com ',' affiliati@infinitodigitale.com ',' live : luigi_1742 ',3921505763,3921505763,'',' INFINITODIGITALE SOCIETA COOPERATIV ','',' IT04184420612 ',' Mr ',' Guarino ',' Luigi ',' Via Pietro Nenni,
    02 ',81030,' Lusciano ',' it ',' FRA ',0,2,50,' Chèque ','','','','',' 2018 - 03 - 22 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 288,2,2,' matt@greenbrickdigital.co.uk ',' matt@greenbrickdigital.co.uk ',' matt@greenbrickdigital.co.uk ',' matt@greenbrickdigital.co.uk ',' matt@greenbrickdigital.co.uk ',7740359936,'','','','',' 279 4217 72 ',' Mr ',' Seymour ',' Matt ',' Park Road ',' Glouce ',' GL1 1L ',' uk ',' FRA ',0,2,50,' Chèque ','','','','',' 2018 - 03 - 28 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 289,1,2,' mfrau@come - on - web.com ',' mfrau@come - on - web.com ',' mfrau@come - on - web.com ',' mfrau@come - on - web.com ','','','','',' Cloud Media ','','',' Mr ',' frau ',' matthieu ','','','',' fr ',' FRA ',2,1,50,' Chèque ','','','','',' 2018 - 04 - 11 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 290,1,2,' erica@twinkledative.com ',' erica@twinkledative.com ',' erica@twinkledative.com ',' erica@twinkledative.com ','','','','',' Twinkle SAGL ','','',' Mr ',' Milletti ',' Michele ','','','',' fr ',' FRA ',2,1,50,' Chèque ','','','','',' 2018 - 04 - 12 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 291,1,2,' vlad_pislaru@yahoo.com ',' vlad_pislaru@yahoo.com ',' vlad_pislaru@yahoo.com ',' vlad_pislaru@yahoo.com ','','','','',' S.C.PIV DEVELOPMENT SRL - D ',' RO36265890 ','',' Mr ',' PISLARU ',' Vlad ',' Aleea LIREI 1,
    FC11,
    Sc B,
    Ap.11 ',900096,' Constanta ',' ro ',' FRA ',0,2,50,' Chèque ','','','','',' 2018 - 05 - 08 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 292,1,2,' ametzler666@hotmail.fr ',' ametzler666@hotmail.fr ',' ametzler666@hotmail.fr ',' ametzler666@hotmail.fr ','','','','',' leadfactory retargarting ','','',' Mr ',' metzler ',' alexandre ',' 6 impasse des jonquilles ',72555,' La Seyne Sur Mer ',' fr ',' FRA ',2,1,50,' Chèque ','','','','',' 2018 - 05 - 31 ');
/* INSERT QUERY */INSERT INTO editeurs(ids_editeur,id_type,type,login,email,technique,compta,skype,telephone,cellulaire,fax,nom_entreprise,siret,tva,civ,nom,prenom,adresse,codepostale,ville,pays,racine,commercial,statut,paiement,paiement_type,iban,swift,banque,commentaire,date_creation) VALUES( 293,1,2,' dn.tlbr@outlook.fr ',' dn.tlbr@outlook.fr ',' dn.tlbr@outlook.fr ',' dn.tlbr@outlook.fr ','','','','',' dan Tulbure ','','',' Mr ',' Tulbure ',' dan ','','','',' fr ',' FRA ',1,1,50,' Chèque ','','','','',' 2018 - 06 - 13 ');
