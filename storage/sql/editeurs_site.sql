/* CREATE TABLE */
CREATE TABLE IF NOT EXISTS bases(
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  idsite INT(11),
  id_editeur INT(11),
  titre VARCHAR(255),
  url VARCHAR(255),
  description VARCHAR(255),
  categorie DECIMAL(10, 2),
  mailing DECIMAL(10, 2),
  mobile DECIMAL(10, 2),
  vu DECIMAL(10, 2),
  pap DECIMAL(10, 2),
  label DECIMAL(10, 2),
  racine VARCHAR(255),
  statut DECIMAL(10, 2),
  domaine VARCHAR(255),
  commercial DECIMAL(10, 2),
  PRIMARY KEY (`id`)
);
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    6, 7, 'Datarama', 'http://Datarama',
    '-----------', 2, 850, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    7, 7, 'aj data', 'http://ajdata', '--------------',
    2, 600, 0, 0, 0, 0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1, 1, 'test', 'http://www.capdecision.fr',
    '', 65, 0, 0, 0, 0, 0, 'FRA', 1, 22, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    4, 5, 'Capaffiliation', 'http://www.capaffiliation.com',
    '', 214, 0, 0, 0, 0, 3, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    8, 7, 'Transatclub', 'http://Transatclub',
    'xxxxxxxxxxxxxxxxxxx', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    9, 9, 'SORTILEGES', 'http://sortileges',
    '-----------', 2, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    10, 10, 'SOCIABILIWEB', 'http://sociabiliweb',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    11, 11, 'EMAIL4YOU', 'http://EMAIL4YOU',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    12, 12, 'Efficiency Network', 'http://Efficiency Network',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    13, 13, 'Comparer Les Placements',
    'http://clp', '', 2, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    15, 15, 'R2J', 'http://R2J', '', 2, 0,
    0, 0, 0, 0, 'FRA', 1, 154, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    16, 16, 'TAATOO', 'http://taatoo',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    17, 17, 'EVALOM', 'http://EVALOM',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    18, 18, 'PERFORMCLICK', 'http://PERFORMCLICK',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 177, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    19, 19, 'EPROGRESS', 'http://EPROGRESS',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    20, 20, 'UAU', 'http://UAU', '', 2, 0,
    0, 0, 0, 0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    21, 21, 'WEBDONE', 'http://WEBDONE',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 0, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    22, 22, 'UNITEAD', 'http://UNITEAD',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    35, 24, 'TAATOO2', 'http://TAATOO2',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    25, 23, 'Offres malines', 'http://Offresmalines',
    '..............', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    36, 25, 'Gros deal', 'http://Gros deal',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    27, 23, 'Espace deals', 'http://espace_deals',
    '..........................', 1,
    0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    28, 23, 'Conso Futee', 'http://Conso_Futee',
    '...............', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    29, 23, 'Delas Are Us', 'http://Delas_Are_Us',
    '.................', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    30, 23, 'Fait Le plein', 'http://Fait_Le_plein',
    '.................', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    31, 23, 'Idees Malines', 'http://Idees_Malines',
    '..................', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    32, 23, 'Jeconomise', 'http://Jeconomise',
    '....................', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    33, 23, 'Officiel Du Deal', 'http://Officiel_Du_Deal',
    '.......................', 1, 0,
    0, 0, 0, 0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    34, 23, 'Zone Exclusive', 'http://Zone_Exclusive',
    '.............', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    37, 26, 'Dreamlead', 'http://Dreamlead',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    38, 27, 'Plandefou', 'http://Plandefou',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 23, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    39, 23, 'programme shopping', 'http://programme shopping',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    40, 29, 'Webrivage', 'http://webrivage',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 233, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    41, 23, 'ma carte bon plan', 'http://ma_carte_bon_plan',
    '-------------------', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    42, 23, 'place aux deals', 'http://place_aux_deals',
    '------------------', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    43, 23, 'tentation privee', 'http://tentation_privee',
    '----------------', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    44, 23, 'avenue vip', 'http://avenue_vip',
    '------------------', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    45, 30, 'Liberad', 'http://Liberad',
    '-----------------', 2, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    46, 30, 'Central Marques', 'http://Central Marques',
    '------------------', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    47, 23, 'mon shopping malin', 'http://mon shopping malin',
    '------------------', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    48, 23, 'rue des offres', 'http://rue des offres',
    '---------------------', 1, 0, 0,
    0, 0, 0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    49, 23, '1 day 1 offer', 'http://1day1offer',
    'Bons plans', 10, 500, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    50, 23, 'selection privee', 'http://selection privee',
    '-------------', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    52, 31, 'Netlead', 'http://Netlead',
    'Netlead', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    53, 32, 'Neezz', 'http://neezz.com',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 208, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    54, 33, 'Univers VIP', 'http://universvip.com',
    '-----------', 2, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    58, 23, 'News and Co', 'http://News_and_Co',
    '--------------------', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    56, 23, 'Mes idees promo', 'http://Mes_idees_promo',
    '------------', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    57, 23, 'Deal Seduction', 'http://Deal_Seduction',
    '---------------------------',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    59, 34, 'REGIP', 'http://REGIP', '',
    6, 0, 0, 0, 0, 0, 'FRA', 1, 162, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    60, 24, 'Club Plus', 'http://clubplus',
    '--------------', 1, 250, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    61, 35, 'FORCE MARKETING', 'http://FORCEMARKETING',
    '-------------', 2, 0, 0, 0, 0, 0, 'FRA',
    1, 226, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    62, 36, 'Tegara Media', 'http://Tegara_Media',
    '---------', 2, 0, 0, 0, 0, 0, 'FRA',
    1, 161, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    63, 23, 'Deal Outlet', 'http://Deal_Outlet',
    '-------------', 1, 400, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    64, 23, 'L Usine Des Deals', 'http://L_Usine_Des_Deals',
    '---------------------', 1, 250,
    0, 0, 0, 0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    65, 37, 'Affaire en Or', 'http://affaire_en_or',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    66, 38, 'Meilleurs Coupons', 'http://Meilleurs_Coupons',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    67, 38, 'Meilleurs Placements', 'http://Meilleurs_Placements',
    '-----------', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    68, 34, 'Regip_2', 'http://Regip_2',
    '---------', 1, 0, 0, 0, 0, 0, 'FRA',
    0, 162, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    71, 40, 'mon shopping privé', 'http://mon_shopping_prive',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    70, 38, 'Mailback', 'http://mailback',
    '-------------', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    72, 41, 'KTP', 'http://ktp.fr', '',
    2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    73, 26, 'Base 1', 'http://base1', '------------',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    74, 26, 'Base 2', 'http://base2', '---------',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    75, 26, 'Base 3', 'http://base3', '------------',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    76, 26, 'Base 4', 'http://base4', '-------------',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    77, 26, 'Base 5', 'http://base5', '-------------',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    78, 26, 'Base 6', 'http://base6', '---------------',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    79, 26, 'Base 7', 'http://base7', '--------------',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    80, 26, 'Base 8', 'http://base8', '----------',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    81, 26, 'Base 9', 'http://base9', '----------',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    82, 33, 'Bonsplans', 'http://Bonsplans',
    '------', 1, 200, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    83, 7, 'Smartweb', 'http://Smartweb',
    'Smartweb', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    84, 27, 'comptoir des deals', 'http://comptoir_des_deals',
    '----------', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 23, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    85, 1, 'Neolane', 'http://Neolane',
    '---------', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    86, 42, 'DMD', 'http://DMD', '', 2, 0,
    0, 0, 0, 0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    87, 34, 'Regip_3', 'http://Regip_3',
    '---------', 1, 0, 0, 0, 0, 0, 'FRA',
    0, 162, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    88, 34, 'Ecomalins', 'http://Ecomalins',
    '-------------', 1, 0, 0, 0, 0, 0, 'FRA',
    0, 162, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    89, 34, 'Lesechosdushopping', 'http://Lesechosdushopping',
    '-----------', 1, 0, 0, 0, 0, 0, 'FRA',
    0, 162, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    90, 43, 'Eprogress', 'http://www.eprogress.fr',
    '----', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    91, 44, 'Conso du jour', 'http://consodujour.net',
    '----------', 2, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    92, 45, 'Axe7', 'http://axe7.com',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    93, 46, 'Acheter dans le neuf', 'http://acheter-dans-le-neuf.fr',
    '--------', 2, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    94, 47, 'Ristretto', 'http://Ristretto',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    95, 47, 'Ristretto 2', 'http://Ristretto_2',
    '---------', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    96, 30, 'CTRM', 'http://CTRM', '--------',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    97, 42, 'SLS Data', 'http://sls-data.com',
    '--------', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    98, 1, 'Capdecision Bis', 'http://capdecision-bis.com',
    '--------', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    99, 48, 'Soft Direct', 'http://soft-direct',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    100, 49, 'Deal Agency', 'http://deal-agency',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    101, 50, 'Les caves', 'http://lescaves',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 144, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    102, 34, 'Escalepro', 'http://Escalepro',
    '-----------', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 162, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    103, 34, 'Sportquik', 'http://Sportquik',
    -0, 1, 0, 0, 0, 0, 0, 'FRA', 1, 162, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    104, 34, 'Panelconso', 'http://Panelconso',
    -0, 1, 0, 0, 0, 0, 0, 'FRA', 1, 162, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    105, 51, '6 KOM', 'http://6KOM', '',
    2, 0, 0, 0, 0, 0, 'FRA', 1, 169, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    106, 27, 'espace promos', 'http://espace_promos',
    '------', 1, 0, 0, 0, 0, 0, 'FRA', 1, 23,
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    107, 27, 'palmarès des marques',
    'http://palmarès_des_marques',
    '--', 1, 0, 0, 0, 0, 0, 'FRA', 1, 23, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    108, 52, 'instalmailmedia', 'http://instalmailmedia',
    '----', 2, 0, 0, 0, 0, 0, 'FRA', 0, 176,
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    109, 53, 'darwin', 'http://darwin',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    110, 53, 'Institut DWI', 'http://InstitutDWI',
    '---', 1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    111, 53, 'Mon club privé', 'http://Mon-club-prive',
    '----', 1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    112, 53, 'Mes idées Loisirs', 'http://Mes-idees-Loisirs',
    '------', 1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    113, 53, 'Mes idées Détentes ',
    'http://Mes_idees_Detentes ', '-----',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    114, 7, 'Oh my sweet', 'http://Oh-my-sweet',
    '----', 1, 0, 0, 0, 0, 0, 'FRA', 1, 142,
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    115, 54, 'K2B', 'http://K2B', '', 2,
    0, 0, 0, 0, 0, 'FRA', 1, 207, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    116, 55, 'HT Privé', 'http://ht-prive.com',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    117, 56, 'opt-e-mail', 'http://opt-e-mail',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    118, 57, 'MailoMedia', 'http://MailoMedia',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    119, 57, 'TopAvantage ', 'http://TopAvantage ',
    '-------', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    120, 57, 'DailyReduc', 'http://DailyReduc',
    'DailyReduc', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 149, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    121, 57, 'Lookiya ', 'http://Lookiya ',
    'Lookiya ', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    122, 57, 'MelleDeal', 'http://MelleDeal',
    'MelleDeal', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 149, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    123, 58, 'comandclick', 'http://comandclick',
    '------', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    124, 59, 'Les caves 2', 'http://lescaves2',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 0, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    130, 64, 'Beetell', 'http://Beetell',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    131, 65, 'Distrigame', 'http://Distrigame',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    126, 108, 'sponsorboost', 'http://sponsorboost.com',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 0, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    127, 62, 'Mon Code Réduction', 'http://mon-code-reduction.fr/',
    'Propose aux internautes les meilleurs bons plans et codes réduction',
    95, 500, 0, 500, 2000, 0, 'FRA', 2, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    128, 11, 'Optin email', 'http://optinemail',
    '---', 1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    132, 65, 'Cash pour tous', 'http://Cash pour tous',
    '---', 1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    133, 66, 'Wearebeautiful', 'http://Wearebeautiful',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    134, 66, 'La selection de Lea', 'http://La selection de Lea',
    'hjk', 1, 0, 0, 0, 0, 0, 'FRA', 1, 196,
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    135, 67, 'defiscalisez', 'http://defiscalisez',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 202, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    136, 12, 'zannier', 'http://zannier',
    'DDF', 1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    137, 68, 'Webdev Aurora', 'http://Webdev Aurora',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    138, 69, 'un max de promo', 'http://un max de promo',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    139, 70, 'mon bon plan', 'http://mon bon plan',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 165, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    140, 71, 'news-found.com', 'http://news-found.com',
    'email', 2, 200, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    141, 72, 'qassa', 'http://qassa', '',
    2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    142, 65, 'SMS', 'http://SMS', 'sms',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    143, 65, 'plankado', 'http://plankado',
    'pkd', 1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    144, 65, 'CAPTAIN PRICE', 'http://CAPTAIN PRICE',
    'CP', 1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    145, 0, 'galeriedesmarques', 'http://galeriedesmarques',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    146, 73, 'galeriedesmarques', 'http://galeriedesmarques',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 167, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    147, 74, 'REDUCTION DE CHAMPION',
    'http://REDUCTION DE CHAMPION',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    148, 75, 'Guide-comparateur', 'http://Guide-comparateur',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    149, 75, 'information du jour', 'http://information du jour',
    'test', 1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    150, 76, 'jecible', 'http://jecible',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 238, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    151, 76, 'shopman ', 'http://shopman ',
    'shopman ', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    238, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    152, 35, 'planduweb', 'http://planduweb',
    'planduweb', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    153, 40, 'cup of media', 'http://cup of media',
    'cup of media', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 123, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    154, 27, 'club réductions', 'http://club réductions',
    'club réductions', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 23, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    155, 77, '100 Gourmets', 'http://100 Gourmets',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    156, 77, 'Shopping Women', 'http://Shopping Women ',
    'Shopping Women ', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    157, 77, 'Privilège Plus', 'http://Privilège Plus',
    'Privilège Plus', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    158, 77, 'Web Avantages', 'http://Web Avantages',
    'Web Avantages', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    159, 77, '1Jour1offre', 'http://1Jour1offre',
    '1Jour1offre', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    160, 77, 'Byzon Sport', 'http://Byzon Sport',
    'Byzon Sport', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    161, 77, 'Réponses au Quotidien ',
    'http://Réponses au Quotidien ',
    'Réponses au Quotidien ', 1, 0,
    0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    162, 78, 'FemmesPlus', 'http://FemmesPlus',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 126, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    163, 0, 'devenez malin', 'http://devenez malin',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    164, 79, 'devenez malin', 'http://devenez malin',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    165, 80, 'Market360', 'http://Market360',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    166, 80, 'Monde Merveille', 'http://Monde Merveille',
    'Monde Merveille', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    167, 74, 'Pas bete la bete', 'http://Pas bete la bete,',
    'Pas bete la bete,', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    168, 74, 'top du top', 'http://top du top',
    'top du top', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    169, 74, 'the leamonads', 'http://the leamonads',
    'the leamonads', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    170, 81, 'ediffusion.fr', 'http://.ediffusion.fr',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    171, 52, 'LAMODETOUSENSEMBLE', 'http://LAMODETOUSENSEMBLE',
    'LAMODETOUSENSEMBLE', 1, 0, 0, 0, 0,
    0, 'FRA', 0, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    172, 52, 'SHOPPERMALIN', 'http://SHOPPERMALIN',
    'SHOPPERMALIN', 1, 0, 0, 0, 0, 0, 'FRA',
    0, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    173, 52, 'ANOUSLESPETITPRIX', 'http://ANOUSLESPETITPRIX',
    'ANOUSLESPETITPRIX', 1, 0, 0, 0, 0,
    0, 'FRA', 0, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    174, 82, 'travelgo', 'http://travelgo',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    175, 82, 'lemailbonplan', 'http://lemailbonplan',
    'lemailbonplan', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    176, 82, 'oneclick', 'http://oneclick',
    'oneclick', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    177, 83, 'Ouah', 'http://Ouah', '',
    2, 0, 0, 0, 0, 0, 'FRA', 1, 120, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    178, 84, 'Avantage promo', 'http://Avantage promo', 'Paris Tél : +33 (0) 9 72 38 09 54 Capital : 15 000 €&amp;#8232;Siret :&amp;#8232;TVA intra : FR22 532 773 926',
    2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    179, 84, 'senior privilege', 'http://senior privilege',
    'senior privilege', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    180, 84, 'ideezap ', 'http://ideezap ',
    'ideezap ', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    181, 84, 'BPR', 'http://BPR', 'BPR',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    182, 84, '1 minute', 'http://1 minute',
    '1 minute', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    148, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    183, 84, 'beezclic', 'http://beezclic',
    'beezclic', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    184, 85, 'Qaolo', 'http://Qaolo', '',
    2, 0, 0, 0, 0, 0, 'FRA', 1, 211, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    185, 86, 'www.votre-mutuelle', 'http://www.votre-mutuelle',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    186, 87, 'ividence', 'http://ividence',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    187, 88, 'online365', 'http://online365',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    188, 57, 'ohmygoodeal', 'http://ohmygoodeal',
    'ohmygoodeal', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 149, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    189, 57, 'bestofdeal', 'http://bestofdeal',
    'bestofdeal', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 149, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    190, 81, 'weeky.it', 'http://weeky.it',
    'weeky.it', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    191, 89, 'itema', 'http://itema', '',
    2, 0, 0, 0, 0, 0, 'FRA', 1, 121, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    192, 57, 'dailybeauty', 'http://dailybeauty',
    'dailybeauty', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 149, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    193, 0, 'kowmedia.com', 'http://kowmedia.com',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    194, 0, 'kowmedia.com', 'http://kowmedia.com',
    '

 - 4000 Liège - Belgique
http://www.kowmedia.com',
    2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    195, 90, 'kowmedia.com', 'http://kowmedia.com',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 139, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    196, 74, 'coupon magique', 'http://coupon magique',
    'coupon magique', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    197, 84, 'realise reve', 'http://realise reve',
    'realise reve', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    198, 91, 'leadiya', 'http://leadiya',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    199, 92, 'ShoppingPrivilege', 'http://ShoppingPrivilege',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 129, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    200, 92, 'GoodDealDay ', 'http://GoodDealDay ',
    'GoodDealDay ', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 129, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    201, 92, 'UnJourUnCaddie ', 'http://UnJourUnCaddie ',
    'UnJourUnCaddie ', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 129, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    202, 91, 'mes offres privées', 'http://mes offres privées',
    'mes offres privées', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    203, 91, 'leadata', 'http://leadata',
    'leadata', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    204, 22, 'UGP Unitead', 'http://UGP Unitead',
    'UGP Unitead', 1, 0, 0, 0, 0, 0, 'FRA',
    0, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    205, 22, 'Media UNITEAD', 'http://Media UNITEAD',
    'Media UNITEAD', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    206, 22, 'Cshort', 'http://Cshort',
    'Cshort', 1, 0, 0, 0, 0, 0, 'FRA', 0, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    207, 22, 'Tedipro', 'http://Tedipro',
    'Tedipro', 1, 0, 0, 0, 0, 0, 'FRA', 0,
    'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    208, 22, 'SC2 ', 'http://SC2 ', 'SC2 ',
    1, 0, 0, 0, 0, 0, 'FRA', 0, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    209, 22, 'Wan', 'http://Wan', 'Wan',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    210, 27, 'espace reduc', 'http://espace reduc',
    'espace reduc', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 23, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    211, 93, 'Dimona', 'http://Dimona',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    212, 94, 'vis-ma-ville', 'http://vis-ma-ville',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    213, 94, 'ADN', 'http://ADN', 'ADN',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    214, 94, 'WANVMV', 'http://WANVMV',
    'WAN', 1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    215, 94, 'Enqueteo', 'http://Enqueteo',
    'Enqueteo', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    216, 84, 'promoinfo', 'http://promoinfo',
    'promoinfo', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    217, 84, 'ptit futé', 'http://ptit futé',
    'ptit futé', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    218, 95, 'Rapid FR', 'http://Rapid FR',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    219, 96, 'latitudepub', 'http://latitudepub',
    'latitudepub', 2, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    220, 96, 'Malinsmalines', 'http://Malinsmalines',
    'Malinsmalines', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    221, 97, 'coupon for you', 'http://coupon for you',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    222, 76, 'shopping attitude', 'http://shopping attitude',
    'shopping attitude', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 238, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    223, 98, 'acheter-louer.fr', 'http://acheter-louer.fr',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 143, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    224, 99, 'RV', 'http://RV', '', 2, 0,
    0, 0, 0, 0, 'FRA', 1, 146, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    225, 100, 'across', 'http://across',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    226, 101, 'Sonet', 'http://Sonet',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    227, 102, '2-30', 'http://2-30', '',
    2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    228, 92, 'BingoFacile', 'http://BingoFacile',
    'BingoFacile', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 129, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    229, 92, 'PanelConso riviera', 'http://PanelConso riviera',
    'PanelConso ', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 129, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    230, 92, 'AlerteShoppingMalin', 'http://AlerteShoppingMalin',
    'AlerteShoppingMalin', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 129, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    231, 92, 'LeShopingonline', 'http://LeShopingonline',
    'LeShopingonline', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    232, 92, 'LesAstucesShoppingDeJulie',
    'http://LesAstucesShoppingDeJulie',
    'LesAstucesShoppingDeJulie', 1,
    0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    233, 103, 'Une adresse unique', 'http://',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    234, 103, 'une adresse unique', 'http://une adresse unique',
    'une adresse unique', 1, 0, 0, 0, 0,
    0, 'FRA', 0, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    235, 104, 'MEDIA AND CO FR', 'http://MEDIA AND CO FR',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    236, 105, 'Maxipromos ', 'http://Maxipromos ',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    237, 105, 'Offre Premium', 'http://Offre Premium',
    'Offre Premium', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    238, 106, 'ATOUTMAIL ', 'http://ATOUTMAIL ',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 0, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    239, 0, 'gaddin', 'http://gaddin',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    240, 0, 'gaddin', 'http://gaddin',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    241, 107, 'gaddin.com', 'http://gaddin.com',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 138, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    242, 108, 'sponsorboost', 'http://sponsorboost123',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 0, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    243, 109, 'welcome media', 'http://welcome media',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 145, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    244, 109, 'easy voyage', 'http://easy voyage',
    'easy voyage', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 145, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    245, 109, 'voyage prive', 'http://voyage prive',
    'voyage prive', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 145, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    246, 110, 'the brico', 'http://the brico',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 122, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    247, 111, 'bestcampagne', 'http://bestcampagne',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 157, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    248, 22, 'NTL', 'http://NTL', 'NTL',
    1, 0, 0, 0, 0, 0, 'FRA', 0, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    249, 108, 'Le conseil du matin', 'http://Le conseil du matin',
    'Le conseil du matin', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    250, 108, 'BingoFacile SB', 'http://BingoFacile SB',
    'BingoFacile SB', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    251, 108, 'Online reduc', 'http://Online reduc',
    'Online reduc', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    252, 108, 'Alizébonplan', 'http://Alizébonplan',
    'Alizébonplan', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    253, 112, 'neonergy', 'http://neonergy',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    254, 113, 'caloga', 'http://caloga', ''
    , 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    255, 108, 'PanelConso2', 'http://PanelConso2 ',
    'PanelConso2 ', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    256, 108, 'shopping institut 2', 'http://shopping institut 2',
    'shopping institut 2', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    257, 114, 'Scibile', 'http://Scibile',
    'Via Lugano 18
World Trade Center
CH-6982 Agno
Nuovo codice IVA: CHE-110.061.446 IVA
Tel.: +41 91 922 82 89
Fax.: +41 91 922 82 90
E-mail: international@scibile.ch
Internet: www.scibile.ch',
    2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    258, 22, 'tedipro2', 'http://tedipro2',
    'tedipro2', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    259, 22, 'boulevard des ventes', 'http://boulevard des ventes',
    'boulevard des ventes', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    260, 22, 'enquete conso', 'http://enquete conso',
    'enquete conso', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    261, 22, 'journal des newsletters',
    'http://journal des newsletters',
    ' journal des newsletters', 1, 0,
    0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    262, 22, 'K-group', 'http://K-group',
    'K-group', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    263, 116, 'tinkel', 'http://tinkel',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 130, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    264, 116, 'bonplanshopping', 'http://bonplanshopping',
    'bonplanshopping', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    265, 118, 'ma mode à moi', 'http://',
    '',
    2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    266, 118, 'ma mode à moi', 'http://ma mode à moi',
    'ma mode à moi', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 26, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    267, 118, 'ma déco à moi', 'http://ma déco à moi',
    'ma déco à moi', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    268, 118, 'mes meilleures vacances',
    'http://mes meilleures vacances',
    'mes meilleures vacances', 1, 0,
    0, 0, 0, 0, 'FRA', 1, 26, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    269, 118, 'globe explorer', 'http://globe explorer',
    'globe explorer', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    270, 104, 'ALERTE SHOPPING MALIN',
    'http://ALERTE SHOPPING MALIN',
    'ALERTE SHOPPING MALIN', 1, 0, 0,
    0, 0, 0, 'FRA', 1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    271, 104, 'ACTUALITE MALICE ET SHOPPING   ',
    'http://ACTUALITE MALICE ET SHOPPING   ',
    'ACTUALITE MALICE ET SHOPPING

',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    272, 104, 'ACTUALITE BON PLAN ET SHOPPING',
    'http://ACTUALITE BON PLAN ET SHOPPING',
    'ACTUALITE BON PLAN ET SHOPPING',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    273, 104, 'VIS LES ASTUCES', 'http://VIS LES ASTUCES',
    'VIS LES ASTUCES', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    274, 104, 'VIS LE SHOPPING', 'http://VIS LE SHOPPING',
    'VIS LE SHOPPING', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    275, 104, 'LE PLAN DU JOUR', 'http://LE PLAN DU JOUR',
    'LE PLAN DU JOUR', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    276, 84, 'les astuces de lulu', 'http://les astuces de lulu',
    'les astuces de lulu', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    277, 84, 'REGIE SHOPPISSIME', 'http://REGIE SHOPPISSIME',
    'REGIE SHOPPISSIME', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    278, 84, 'REGIE STORE', 'http://REGIE STORE',
    'REGIE STORE', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    279, 110, 'promos24', 'http://promos24',
    'promos24', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    280, 110, 'leadana', 'http://leadana',
    'leadana', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    281, 119, 'correomaster', 'http://correomaster', ''
    , 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    282, 120, 'promos24', 'http://promos24',
    '704 N KING ST
500 STE
WILMINGTON DE 19801-3584
USA
EIN : 47-2498386',
    2, 0, 0, 0, 0, 0, 'FRA', 1, 135, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    283, 120, 'leadana2', 'http://leadana2',
    'leadana', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    135, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    284, 89, 'itemaf', 'http://itemaf',
    'itemaf', 1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    285, 73, 'Lamaisondesmarques', 'http://Lamaisondesmarques',
    'Lamaisondesmarques', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 167, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    286, 78, 'Planet Mi journée', 'http://Planet Mi journée',
    'Planet Mi journée', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 126, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    287, 92, 'LaFolieDEsMArques', 'http://LaFolieDEsMArques',
    'LaFolieDEsMArques', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 129, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    288, 92, 'IdylleShopping', 'http://IdylleShopping',
    'IdylleShopping', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 129, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    289, 73, 'caesar', 'http://caesar',
    'caesar', 1, 0, 0, 0, 0, 0, 'FRA', 1, 167,
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    290, 121, 'boulevard des ventes2',
    'http://boulevard des ventes2',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    291, 121, 'Tedi Pro2', 'http://Tedi Pro2',
    'Tedi Pro2', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    292, 121, 'journal des newsletters2',
    'http://journal des newsletters2',
    'journal des newsletters2', 1, 0,
    0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    293, 121, 'Enquêtes consommateurs2',
    'http://Enquêtes consommateurs2',
    'Enquêtes consommateurs2', 1, 0,
    0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    294, 121, 'K-Group2', 'http://K-Group2',
    'K-Group2', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    295, 122, 'AARON BRAINWAVE', 'http://AARON BRAINWAVE',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 0, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    296, 109, 'officiel des vacances',
    'http://officiel des vacances',
    'officiel des vacances', 1, 0, 0,
    0, 0, 0, 'FRA', 1, 145, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    297, 52, 'LEGUIDEFACILE', 'http://LEGUIDEFACILE',
    'LEGUIDEFACILE', 1, 0, 0, 0, 0, 0, 'FRA',
    0, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    298, 123, 'votre actu conso', 'http://votre actu conso',
    'votre actu conso', 2, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    299, 108, 'Debonmatin', 'http://Debonmatin',
    'Debonmatin', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    300, 108, 'Emilie web', 'http://Emilie web',
    'Emilie web', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    301, 108, 'Gooddealday2', 'http://Gooddealday2',
    'Gooddealday2', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    302, 108, 'Shopping astuce', 'http://Shopping astuce',
    'Shopping astuce', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    303, 108, 'Usine a marque', 'http://Usine a marque',
    'Usine a marque', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    304, 108, 'Planet chic', 'http://Planet chic',
    'Planet chic', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    305, 108, 'La newsletter du jour',
    'http://La newsletter du jour',
    'La newsletter du jour', 1, 0, 0,
    0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    306, 77, 'Jeu du Moment', 'http://Jeu du Moment',
    'Jeu du Moment', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    307, 77, 'Noé Noa', 'http://Noé Noa',
    'Noé Noa', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    308, 77, 'Pourquoi du Comment', 'http://Pourquoi du Comment',
    'Pourquoi du Comment', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    309, 77, 'Tom &amp; Julie', 'http://Tom & Julie',
    'Tom &amp; Julie', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    310, 124, '8chances', 'http://8chances',
    '8chances', 2, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    311, 108, 'deal plus3', 'http://deal plus3',
    'deal plus3', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    312, 92, 'CC', 'http://CC', 'CC', 1,
    0, 0, 0, 0, 0, 'FRA', 1, 129, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    313, 92, 'LM RIVIERA', 'http://LM RIVIERA',
    'LM RIVIERA', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 129, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    314, 92, 'WP', 'http://WP', 'WP', 1,
    0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    315, 61, '', 'http://www.test.com',
    '', 1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    316, 92, 'cdm', 'http://cdm', 'cdm',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 129, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    317, 125, 'Concoursdeluxe', 'http://Concoursdeluxe',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    318, 108, 'Mon shopping malin2', 'http://Mon shopping malin2',
    'Mon shopping malin2', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    319, 124, 'sélection exclusive',
    'http://sélection exclusive',
    'sélection exclusive', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    320, 126, 'aventers', 'http://aventers',
    '
Chef de Projets

Aventers Publicité / Groupe aventers



Tel: 01.49.67.05.53

Skype : alexandrenikou.braderie

',
    2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    321, 78, 'newsplanet', 'http://newsplanet',
    'newsplanet', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 126, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    322, 127, 'choisir.com', 'http://choisir.com',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    323, 128, 'NDMEDIA', 'http://NDMEDIA',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    330, 129, 'bananaloto', 'http://bananaloto',
    'bananaloto', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    328, 129, 'gamespassport', 'http://gamespassport',
    'gamespassport', 2, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    329, 129, 'kingoloto', 'http://kingoloto',
    'kingoloto', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    326, 124, 'idée smart', 'http://idée smart',
    'idée smart', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    327, 124, 'plans malins', 'http://plans malins',
    'plans malins', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    331, 84, 'eureka', 'http://eureka',
    'eureka', 1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    332, 126, 'braderie', 'http://braderie',
    'braderie', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    333, 126, 'Tendances&amp;Vous', 'http://Tendances&Vous',
    'Tendances&amp;Vous', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    334, 126, 'La Pie Shoppeuse', 'http://La Pie Shoppeuse',
    'La Pie Shoppeuse', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    335, 126, 'Magfemmes ', 'http://Magfemmes ',
    'Magfemmes ', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    336, 130, 'tendancy', 'http://tendancy',
    'tendancy', 2, 0, 0, 0, 0, 0, 'FRA', 0,
    'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    337, 131, 'Consomeo', 'http://Consomeo',
    '


13300 Salon de Provence



RCS Salon de Provence


TVA FR81801241654



Gerant :



Base :  ',
    2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    338, 132, 'Sex Addict', 'http://Sex Addict',
    'Raison sociale :
Email :
Nom de la base : ',
    2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    339, 133, 'IVP', 'http://immo-vente-proprio.com',
    'Annonces immoibilières', 125,
    80000, 0, 30000, 100000, 0, 'FRA', 2,
    'NULL', 0
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    340, 121, 'Encore moins cher', 'http://Encore moins cher',
    'Encore moins cher', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    341, 121, 'Stream ', 'http://Stream ',
    'Stream ', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    342, 134, 'GoodChoice', 'http://GoodChoice',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    343, 78, 'Planet à la une ', 'http://Planet à la une ',
    'Planet à la une ', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 126, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    344, 50, 'COLAD', 'http://COLAD', 'COLAD',
    1, 0, 0, 0, 0, 0, 'FRA', 0, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    345, 52, 'TADW', 'http://TADW', 'TADW',
    1, 0, 0, 0, 0, 0, 'FRA', 0, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    346, 118, 'astuce reduc', 'http://astuce reduc',
    'astuce reduc', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    347, 104, 'REGIE CODE PROMOTION',
    'http://REGIE CODE PROMOTION',
    'REGIE CODE PROMOTION', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    348, 104, 'REGIE REDUC ET BONNES AFFAIRES',
    'http://REGIE REDUC ET BONNES AFFAIRES',
    'REGIE REDUC ET BONNES AFFAIRES',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    349, 104, 'REGIE COMPARER  ET ECONOMISER',
    'http://REGIE COMPARER  ET ECONOMISER',
    'REGIE COMPARER  ET ECONOMISER',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    350, 104, 'Regie shoppissime Webmedia',
    'http://Regie shoppissime Webmedia',
    'Regie shoppissime Webmedia', 1,
    0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    351, 104, 'REGIE LES KDOS', 'http://REGIE LES KDOS',
    'REGIE LES KDOS', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    352, 136, 'Mesventespriveesdujour',
    'http://Mesventespriveesdujour',
    'Mesventespriveesdujour', 2, 0, 0,
    0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    353, 137, 'cherchersonvoyage', 'http://cherchersonvoyage',
    'cherchersonvoyage', 2, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    354, 137, 'primeira', 'http://primeira',
    'primeira', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    151, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    355, 52, 'TOPAFFAIREDUWEB', 'http://TOPAFFAIREDUWEB',
    'TOPAFFAIREDUWEB', 1, 0, 0, 0, 0, 0,
    'FRA', 0, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    356, 112, 'Nethome', 'http://Nethome',
    'Nethome', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    357, 104, 'REGIE STORE webmedia',
    'http://REGIE STORE webmedia',
    'REGIE STORE webmedia', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    358, 104, 'MEDIA AND CO webmedia',
    'http://MEDIA AND CO webmedia',
    'MEDIA AND CO webmedia', 1, 0, 0,
    0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    359, 108, 'Good deal day', 'http://Good deal day',
    'Good deal day', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    360, 138, 'Alerte les conso', 'http://Alerte les conso',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 249, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    361, 139, 'VenteFlash', 'http://VenteFlash',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 137, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    362, 104, 'bingo wm', 'http://bingo wm',
    'bingo wm', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    363, 139, 'LoisirsImmobilier', 'http://LoisirsImmobilier',
    'LoisirsImmobilier', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 137, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    364, 139, 'BonsPlans todolead', 'http://BonsPlans todolead',
    'BonsPlans todolead', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 137, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    365, 108, 'Patrick roi des bonnes affaires',
    'http://www.patrickroidesbonnesaffaires.com',
    'blablabla', 1, 40, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    366, 140, 'Viralitea', 'http://www.viralitea.com',
    'Site proposant des contenus viraux Photos/Videos/Tops',
    159, 49000, 0, 70000, 180000, 0, 'FRA',
    0, 'NULL', 0
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    367, 141, 'digitalcircle360', 'http://digitalcircle360',
    'digitalcircle360', 2, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    368, 92, 'RG riviera', 'http://RG riviera',
    'RG riviera', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    369, 138, 'datavip', 'http://datavip',
    'datavip', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    370, 50, 'colad2', 'http://colad2',
    'colad2', 1, 0, 0, 0, 0, 0, 'FRA', 0, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    371, 91, 'Dailypromo', 'http://Dailypromo',
    'Dailypromo', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    372, 91, 'FashionsWorld', 'http://FashionsWorld',
    'FashionsWorld', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    373, 65, 'folieshopping.com', 'http://folieshopping.com',
    'folieshopping.com', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    374, 75, 'info-du-jour coregistration',
    'http://info-du-jour coregistration',
    'info-du-jour coregistration',
    1, 0, 0, 0, 0, 0, 'FRA', 2, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    375, 112, 'helios', 'http://helios',
    'helios', 1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    376, 57, 'MellePrivilège', 'http://MellePrivilège',
    'MellePrivilège', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 149, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    377, 142, 'd-media', 'http://d-media',
    'd-media', 2, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    378, 109, 'prisma', 'http://prisma',
    'prisma', 1, 0, 0, 0, 0, 0, 'FRA', 1, 145,
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    379, 143, 'biozama', 'http://biozama',
    'biozama', 2, 0, 0, 0, 0, 0, 'FRA', 1,
    173, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    380, 91, 'Profile', 'http://Profile',
    'Profile', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    381, 144, 'topcom', 'http://topcom',
    'topcom', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    382, 144, 'top gratuit', 'http://top gratuit',
    'top gratuit', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 125, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    383, 144, 'destockvip', 'http://destockvip',
    'destockvip', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 125, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    384, 144, 'millebonplans', 'http://millebonplans',
    'millebonplans', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 125, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    385, 144, 'destockVip.xyz', 'http://destockVip.xyz',
    'destockVip.xyz', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 125, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    386, 31, 'web point', 'http://web point',
    'web point', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    387, 108, 'Le Plan malin', 'http://Le Plan malin',
    'Le Plan malin', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    388, 108, 'Caddie du jour ', 'http://Caddie du jour ',
    'Caddie du jour ', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    389, 84, 'Topswingo', 'http://Topswingo',
    'Topswingo', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    390, 52, 'LAMODEPOURVOUS', 'http://LAMODEPOURVOUS',
    'LAMODEPOURVOUS', 1, 0, 0, 0, 0, 0, 'FRA',
    0, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    391, 34, 'zonda', 'http://zonda', 'zonda',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 162, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    392, 34, 'mytravel', 'http://mytravel',
    'mytravel', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    162, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    393, 145, 'bingo facile', 'http://bingo facile',
    'bingo facile', 2, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    394, 145, 'mon shopping malin FLI',
    'http://mon shopping malin FLI',
    'mon shopping malin FLI', 1, 0, 0,
    0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    395, 145, 'nos offres du jour FLI',
    'http://nos offres du jour FLI',
    'nos offres du jour FLI', 1, 0, 0,
    0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    396, 145, 'destockage lingerie ',
    'http://destockage lingerie ',
    'destockage lingerie ', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    397, 52, 'LAFOLIEDUWEB', 'http://LAFOLIEDUWEB',
    'LAFOLIEDUWEB', 1, 0, 0, 0, 0, 0, 'FRA',
    0, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    398, 57, 'CBIO ', 'http://CBIO ', 'CBIO ',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    399, 79, 'todo E&amp;C', 'http://todo E&C',
    'todo E&amp;C', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    400, 79, 'Chic E&amp;C', 'http://Chic E&C',
    'Chic E&amp;C', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    401, 79, 'voyage E&amp;C', 'http://voyage E&C',
    'voyage E&amp;C', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    402, 79, 'cusine E&amp;C', 'http://cusine E&C',
    'cusine E&amp;C', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    403, 64, 'IMM beetell', 'http://IMM beetell',
    'IMM beetell', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    404, 146, 'Oferting ', 'http://Oferting ',
    'Oferting ', 2, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    405, 147, 'toogains', 'http://toogains',
    'toogains', 2, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    406, 145, 'les coups de coeur de camille',
    'http://les coups de coeur de camille',
    'les coups de coeur de camille',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    407, 148, 'creditblog', 'http://wwW.creditblog.fr',
    'portail du crédit', 84, 390000,
    190000, 190000, 300000, 0, 'FRA', 2,
    'NULL', 0
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    408, 149, 'creditblog', 'http://creditblog',
    'creditblog', 2, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    409, 57, 'Daily beauty 2', 'http://Daily beauty 2',
    'Daily beauty 2', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    410, 129, 'primoconso', 'http://primoconso',
    'primoconso', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    411, 50, 'CORAC', 'http://CORAC', 'CORAC',
    1, 0, 0, 0, 0, 0, 'FRA', 0, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    412, 145, 'coin des achats', 'http://coin des achats',
    'coin des achats', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    413, 150, 'Copine de la mode', 'http://Copine de la mode',
    'Copine de la mode', 2, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    414, 150, 'Mes plans finance', 'http://Mes plans finance',
    'Mes plans finance', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    415, 150, 'Les astuces secrètes',
    'http://Les astuces secrètes',
    'Les astuces secrètes', 1, 0, 0,
    0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    416, 150, 'Terroirs de france', 'http://Terroirs de france',
    'Terroirs de france', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    417, 151, 'acme', 'http://www.acme.com',
    'our product site', 1, 0, 0, 0, 0, 0,
    'ENG', 2, 'NULL', 0
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    418, 149, 'creditblog display', 'http://creditblog display',
    'creditblog display', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    419, 152, 'digipanther', 'http://digipanther',
    'digipanther', 2, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    420, 153, 'web plaisir', 'http://web plaisir',
    'web plaisir', 2, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    421, 153, 'panel institut swarmiz',
    'http://panel institut swarmiz',
    'panel institut swarmiz', 1, 0, 0,
    0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    422, 154, 'Budget en main', 'http://www.shareinfos.info/',
    'maîtrisons ensemble n importe quel budget afin de devenir indeependant',
    84, 0, 0, 360000, 180000, 0, 'FRA', 2,
    'NULL', 0
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    423, 108, 'emma reine des bons plans',
    'http://emma reine des bons plans',
    'emma reine des bons plans', 1,
    0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    424, 108, 'Shopping Online', 'http://Shopping Online',
    'Shopping Online', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    425, 108, 'Deal plus 2', 'http://Deal plus 2',
    'Deal plus 2', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    426, 129, 'bon plan dolive', 'http://bon plan dolive',
    'bon plan dolive', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    427, 108, 'Deal plus 4', 'http://Deal plus 4',
    'Deal plus 4', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    428, 108, ' DeBonMatin-MB', 'http://DeBonMatin-MB',
    'DeBonMatin-MB', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    429, 108, 'Newsletter maline', 'http://Newsletter maline',
    'Newsletter maline', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    430, 108, 'Mon shopping malin1', 'http://Mon shopping malin1',
    'Mon shopping malin1', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    431, 82, 'lescodespromos', 'http://lescodespromos',
    'lescodespromos', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    432, 108, 'Deal Plus 6', 'http://Deal Plus 6',
    'Deal Plus 6', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    433, 145, 'club reductions FLI', 'http://club reductions FLI',
    'club reductions FLI', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    434, 145, 'astuces shopping FLI',
    'http://astuces shopping FLI',
    'astuces shopping FLI', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    435, 145, 'plan de fou FLI', 'http://plan de fou FLI',
    'plan de fou FLI', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    436, 150, 'voyageclubexclusif', 'http://voyageclubexclusif',
    'voyageclubexclusif', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    437, 109, 'Corner promo', 'http://Corner promo',
    'Corner promo', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 145, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    438, 109, 'Actu malin', 'http://Actu malin',
    'Actu malin', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 145, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    439, 109, 'Ma minute essentielle',
    'http://Ma minute essentielle',
    'Ma minute essentielle', 1, 0, 0,
    0, 0, 0, 'FRA', 1, 145, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    440, 109, 'Zcorp', 'http://Zcorp',
    'Zcorp', 1, 0, 0, 0, 0, 0, 'FRA', 1, 145,
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    441, 155, 'rentleads', 'http://rentleads',
    'rentleads', 2, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    442, 129, 'plan du web ', 'http://plan du web ',
    'plan du web ', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    443, 82, 'Astrologa', 'http://Astrologa',
    'Astrologa', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    444, 156, 'Gratuit555.com', 'msi',
    'Gratuit555.com', 2, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    445, 108, 'expert du bon plan', 'http://expert du bon plan',
    'expert du bon plan', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    446, 123, 'gazette des malins', 'http://gazette des malins',
    'gazette des malins', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    447, 142, 'Boites a promo', 'http://Boites a promo',
    'Boites a promo', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    448, 104, 'VIS L ECONOMIE', 'http://VIS L ECONOMIE',
    'VIS L ECONOMIE', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    449, 104, 'VIS LA  PROMOTION', 'http://VIS LA  PROMOTION',
    'VIS LA  PROMOTION', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    450, 104, 'VIS LA PROMO DU JOUR',
    'http://VIS LA PROMO DU JOUR',
    'VIS LA PROMO DU JOUR', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    451, 150, 'bio naturel', 'http://bio naturel',
    'bio naturel', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    452, 157, 'mail&amp;brands', 'http://mail&amp;brands',
    'mail&amp;brands', 2, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    453, 145, 'offre vip', 'http://offre vip',
    'offre vip', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    454, 123, 'Corner Privilege', 'http://Corner Privilege',
    'Corner Privilege', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    455, 145, 'jamaisvu', 'http://jamaisvu',
    'jamaisvu', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    456, 145, 'oopad', 'http://oopad',
    'oopad', 1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    457, 145, 'openway', 'http://openway',
    'openway', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    458, 65, 'Folieskado', 'http://Folieskado',
    'Folieskado', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    459, 145, 'mode et shopping malin',
    'http://mode et shopping malin',
    'mode et shopping malin', 1, 0, 0,
    0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    460, 108, 'Panel conso 3', 'http://Panel conso 3',
    'Panel conso 3', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    461, 108, 'promoprivilege', 'http://promoprivilege',
    'promoprivilege', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    462, 150, 'decos jardin ', 'http://decos jardin ',
    'decos jardin ', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    463, 145, 'netlead FLI', 'http://netlead FLI',
    'netlead FLI', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    464, 123, 'affaires en folie', 'http://affaires en folie',
    'affaires en folie', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    465, 158, 'capdecision2', 'http://capdecision2',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    466, 159, '50threvenue', 'http://50threvenue',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    467, 160, 'leadsection', 'http://leadsection',
    'leadsection', 2, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    468, 161, 'Raypro', 'http://Raypro',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    469, 92, 'Mes emplettes VIP', 'http://Mes emplettes VIP',
    'Mes emplettes VIP', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 129, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    470, 161, 'Raypro2', 'http://Raypro2',
    'Raypro2', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    471, 1, 'cap banniere', 'http://cap banniere',
    'cap banniere', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    472, 57, 'MD2', 'http://MD2', 'MD2',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    473, 150, 'femmes moderne', 'http://femmes moderne',
    'femmes moderne', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    474, 153, 'ESI', 'http://ESI', 'ESI',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    475, 153, 'tube a essai', 'http://tube a essai',
    'tube a essai', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    476, 162, 'Offres en or', 'http://Offres en or',
    'Offres en or', 2, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    477, 162, 'Ma Sélection du Jour ',
    'http://Ma Sélection du Jour ',
    'Ma Sélection du Jour ', 1, 0, 0,
    0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    478, 162, 'Maselection premium', 'http://Maselection premium',
    'Maselection premium', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    479, 162, 'Selectionplusplus', 'http://Selectionplusplus',
    'Selectionplusplus', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    480, 162, 'Mesoffrespremium', 'http://Mesoffrespremium',
    'Mesoffrespremium', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    481, 162, 'Offre VIP du jour ', 'http://Offre VIP du jour ',
    'Offre VIP du jour ', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    482, 162, 'jeconomisesurquoiaujourdhui',
    'http://jeconomisesurquoiaujourdhui',
    'jeconomisesurquoiaujourdhui',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    483, 162, 'Je Choisis Mon Offre',
    'http://Je Choisis Mon Offre',
    'Je Choisis Mon Offre', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    484, 162, 'Les Bons Plans et Moi',
    'http://Les Bons Plans et Moi',
    'Les Bons Plans et Moi', 1, 0, 0,
    0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    485, 162, 'Simple Economique', 'http://Simple Economique',
    'Simple Economique', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    486, 162, 'Je Choisis Mon Offre Premium',
    'http://Je Choisis Mon Offre Premium',
    'Je Choisis Mon Offre Premium',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    487, 162, 'Aubaine du jour', 'http://Aubaine du jour',
    'Aubaine du jour', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    488, 162, 'Style de Vie', 'http://Style de Vie',
    'Style de Vie', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    489, 162, 'Suggestions en Or', 'http://Suggestions en Or',
    'Suggestions en Or', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    490, 162, 'Je Choisis Mon Offre DF',
    'http://Je Choisis Mon Offre DF',
    'Je Choisis Mon Offre DF', 1, 0,
    0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    491, 162, 'Sélectionné Rien Que Pour Moi   ',
    'http://Sélectionné Rien Que Pour Moi   ',
    'Sélectionné Rien Que Pour Moi',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    492, 162, 'avenue VIP', 'http://avenue VIP',
    'avenue VIP', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    493, 84, 'ma news actu', 'http://ma news actu',
    'ma news actu', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    494, 163, 'FigaroTV', 'http://FigaroTV',
    'FigaroTV', 2, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    495, 67, 'Primaximmo', 'http://www.primaximmo.com',
    'Vendeurs immobilier', 10, 200000,
    0, 50000, 100000, 0, 'FRA', 1, 202, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    496, 164, 'Elbit', 'http://Elbit',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    497, 36, 'consobuzz', 'http://consobuzz',
    'consobuzz', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    498, 121, 'TDC2', 'http://TDC2', 'TDC2',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    499, 22, 'TDC UNITEAD', 'http://TDC UNITEAD',
    'TDC UNITEAD', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    500, 129, 'club privilèges', 'http://club privilèges',
    'club privilèges', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    501, 74, 'ouitak', 'http://ouitak',
    'ouitak', 1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    502, 145, 'we mag', 'http://we mag',
    'we mag', 1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    503, 145, 'PromoLike FR', 'http://PromoLike FR',
    'PromoLike FR', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    504, 145, 'Nos-offres-du-net', 'http://Nos-offres-du-net',
    'Nos-offres-du-net', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    505, 145, 'R2J FLI', 'http://R2J FLI',
    'R2J FLI', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    506, 163, 'Absolu Féminin', 'http://Absolu Féminin',
    'Absolu Féminin', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 163, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    507, 142, 'dmediapub1', 'http://dmediapub1',
    'dmediapub1', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    508, 165, 'enquete-shopping', 'http://enquete-shopping',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    509, 142, 'dmediadisplay1', 'http://dmediadisplay1',
    'dmediadisplay1', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    510, 166, 'mtv', 'http://mtv', 'mtv',
    2, 0, 0, 0, 0, 0, 'FRA', 0, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    511, 84, 'Santé &amp; découverte',
    'http://Santé & découverte',
    'Santé &amp; découverte', 1, 0,
    0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    512, 168, 'MSI', 'http://MSI', '', 2,
    0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    513, 153, 'zalinco', 'http://zalinco',
    'zalinco', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    514, 153, 'idée shopping', 'http://idée shopping',
    ' idée shopping', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    515, 79, 'fan de voyage', 'http://fan de voyage',
    'fan de voyage', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    516, 79, 'fan de shopping', 'http://fan de shopping',
    'fan de shopping', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    517, 169, 'CM entertainment FR', 'http://CM entertainment FR',
    'CM entertainment FR', 2, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    518, 153, 'place des actus', 'http://place des actus',
    'place des actus', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    519, 150, 'planete saveurs ', 'http://planete saveurs ',
    'planete saveurs ', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    520, 145, 'Lil Life', 'http://Lil Life',
    'Lil Life', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    521, 79, 'les deux fans', 'http://les deux fans',
    'les deux fans', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    522, 121, 'BVT', 'http://BVT', 'BVT',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    523, 170, 'highmedia', 'http://highmedia',
    'highmedia', 2, 0, 0, 0, 0, 0, 'FRA',
    1, 171, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    524, 171, 'Aujourdhui.com', 'http://Aujourdhui.com',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    525, 57, 'TopdesReduc', 'http://TopdesReduc',
    'TopdesReduc', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 149, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    526, 109, 'NL Serengo', 'http://NL Serengo',
    'NL Serengo', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 145, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    527, 109, 'NL 20min', 'http://NL 20min',
    'NL 20min', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    528, 34, 'eshopdeal', 'http://eshopdeal',
    'eshopdeal', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 162, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    529, 109, 'NL Voici', 'http://NL Voici',
    'NL Voici', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    145, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    530, 142, 'dmediadisplay2', 'http://dmediadisplay2',
    'dmediadisplay2', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    531, 150, 'departement agricole',
    'http://departement agricole',
    'departement agricole', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    532, 150, 'chemin du monde', 'http://chemin du monde',
    'chemin du monde', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    533, 109, 'Tele loisirs vidéo', 'http://Tele loisirs vidéo',
    'Tele loisirs vidéo', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    534, 150, 'esprit decouverte', 'http://esprit decouverte',
    'esprit decouverte', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    535, 109, 'Cuisine Actuelle', 'http://Cuisine Actuelle',
    'Cuisine Actuelle', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    536, 109, 'nl capital', 'http://nl capital',
    'nl capital', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 145, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    537, 166, 'BOS ', 'http://BOS ', 'BOS ',
    1, 0, 0, 0, 0, 0, 'FRA', 0, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    538, 74, 'RTB abacus', 'http://RTB abacus',
    'RTB abacus', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    539, 74, 'abacus facebook', 'http://abacus facebook',
    'abacus facebook', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    540, 172, 'credit immo de france',
    'http://credit immo de france',
    'credit immo de france', 2, 0, 0,
    0, 0, 0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    541, 172, 'label artisan', 'http://label artisan',
    'label artisan', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    542, 172, 'expert contact', 'http://expert contact',
    'expert contact', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    543, 172, 'diagnostique de france',
    'http://diagnostique de france',
    'diagnostique de france', 1, 0, 0,
    0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    544, 172, 'piscine du sud ouest',
    'http://piscine du sud ouest',
    'piscine du sud ouest', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    545, 34, 'ComportoReg', 'http://ComportoReg',
    'ComportoReg', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 162, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    546, 34, 'SprintMotorsport', 'http://SprintMotorsport',
    'SprintMotorsport', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 162, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    547, 143, 'Infiniweb ', 'http://Infiniweb ',
    'Infiniweb ', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 173, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    548, 143, 'GDD azorica', 'http://GDD azorica',
    'GDD azorica', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 173, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    549, 143, 'Biglead ', 'http://Biglead ',
    'Biglead ', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    173, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    550, 143, 'Arrow', 'http://Arrow',
    'Arrow', 1, 0, 0, 0, 0, 0, 'FRA', 1, 173,
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    551, 143, 'First abo', 'http://First abo',
    'First abo', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 173, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    552, 174, 'deal en or ', 'http://deal en or ',
    'deal en or ', 2, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    553, 174, 'alerte au deal', 'http://alerte au deal',
    'alerte au deal', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    554, 174, 'freedays', 'http://freedays',
    'freedays', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    555, 175, 'F6KEY', 'http://F6KEY',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    556, 175, 'adopteruncelibataire.com',
    'http://adopteruncelibataire.com',
    'adopteruncelibataire.com', 1, 0,
    0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    557, 175, 'affinitee.com', 'http://affinitee.com',
    'sdsds', 1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    558, 175, 'best-rencontre.com', 'http://best-rencontre.com',
    'best-rencontre.com', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    559, 175, 'blog.liberteen.fr', 'http://blog.liberteen.fr',
    'blog.liberteen.fr', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    560, 175, 'blog2rencontre.fr', 'http://blog2rencontre.fr',
    'blog2rencontre.fr', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    561, 175, 'labdate.fr', 'http://labdate.fr',
    'labdate.fr', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    562, 175, 'lovepresse.fr', 'http://lovepresse.fr',
    'lovepresse.fr', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    563, 175, 'promo-rencontre.com', 'http://promo-rencontre.com',
    'promo-rencontre.com', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    564, 175, 'rencontrer-une-femme.com',
    'http://rencontrer-une-femme.com',
    'rencontrer-une-femme.com', 1, 0,
    0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    565, 175, 'socialflirt.fr', 'http://socialflirt.fr',
    'socialflirt.fr', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    566, 175, 'web-rencontre.eu', 'http://web-rencontre.eu',
    'web-rencontre.eu', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    567, 175, 'achats-malins.com', 'http://achats-malins.com',
    'achats-malins.com', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    568, 175, 'affileo.fr', 'http://affileo.fr',
    'affileo.fr', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 223, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    569, 176, 'teamrecrut.com', 'http://teamrecrut.com',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    570, 150, 'ame futer', 'http://ame futer',
    'ame futer', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    571, 82, 'bonsplansfeminins', 'http://bonsplansfeminins',
    'bonsplansfeminins', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    572, 118, 'panier astuce', 'http://panier astuce',
    'panier astuce', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    573, 118, 'revue shopping', 'http://revue shopping',
    'revue shopping', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    574, 27, 'promo en folie', 'http://promo en folie',
    'promo en folie', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 23, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    575, 177, 'MS', 'http://MS', '', 2, 0,
    0, 0, 0, 0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    576, 118, 'instant bébé', 'http://instant bébé',
    'instant bébé', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    577, 177, 'C bien', 'http://C bien',
    'C bien', 1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    578, 109, 'bnet vendeur', 'http://bnet vendeur',
    'bnet vendeur', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    579, 109, 'RTL', 'http://RTL', 'RTL',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    580, 109, 'bon plan gourmand', 'http://bon plan gourmand',
    'bon plan gourmand', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 145, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    581, 108, 'Le Courrier Malin', 'http://Le Courrier Malin',
    'Le Courrier Malin', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    582, 108, 'GDD-MB', 'http://GDD-MB',
    'GDD-MB', 1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    583, 84, 'walking leads', 'http://walking leads',
    ' walking leads', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    584, 108, 'Panel conso 5', 'http://Panel conso 5',
    'Panel conso 5', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    585, 108, 'Deal plus 5', 'http://Deal plus 5',
    'Deal plus 5', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    586, 108, 'Deal plus 8', 'http://Deal plus 8',
    'Deal plus 8', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    587, 123, 'mes idées malines', 'http://mes idées malines',
    'mes idées malines', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    588, 123, 'love my deal ', 'http://love my deal ',
    'love my deal ', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    589, 123, 'tendance premium', 'http://tendance premium',
    'tendance premium', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 191, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    590, 109, 'club des reduc', 'http://club des reduc',
    'club des reduc', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    591, 109, 'TOM prisma', 'http://TOM prisma',
    'TOM prisma', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    592, 109, 'ugc', 'http://ugc', 'ugc',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    593, 109, 'welove prisma', 'http://welove prisma',
    'welove prisma', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 145, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    594, 174, 'my smart deal ', 'http://my smart deal ',
    'my smart deal ', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    595, 109, 'Purplan ', 'http://Purplan ',
    'Purplan ', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    596, 123, 'Miss Beauty Deal', 'http://Miss Beauty Deal',
    'Miss Beauty Deal', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    597, 123, 'Affaires et Compagnie',
    'http://Affaires et Compagnie',
    'Affaires et Compagnie', 1, 0, 0,
    0, 0, 0, 'FRA', 1, 118, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    598, 123, 'Widigo', 'http://Widigo',
    'Widigo', 1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    599, 178, 'Millenya', 'http://Millenya',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    600, 179, 'NAOInteractive', 'http://NAOInteractive',
    'NAOInteractive', 2, 0, 0, 0, 0, 0, 'FRA',
    1, 164, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    601, 82, 'vivre mieux', 'http://vivre mieux',
    'vivre mieux', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    602, 180, 'coursier du web', 'http://coursier du web',
    '
90 avenue de mazargues
BAT D Res. Les Moulins du prado
13008 marseille

Nom des bases : couriser du web,Nos artisans',
    2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    603, 180, 'Nos artisans', 'http://Nos artisans',
    'Nos artisans', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    604, 168, 'WSi', 'http://WSi', 'WSi',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    605, 181, 'tradetracker france', 'http://tradetracker france',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    606, 180, 'beetell ido', 'http://beetell ido',
    'beetell ido', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    607, 181, 'zuki0707', 'http://zuki0707',
    'zuki0707', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    608, 174, 'deals online', 'http://deals online',
    'deals online', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    609, 174, 'deals planet', 'http://deals planet',
    'deals planet', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    610, 84, 'Swingoprems', 'http://Swingoprems',
    'Swingoprems', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    611, 92, 'YP RIVIERA', 'http://YP RIVIERA',
    'YP RIVIERA', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    612, 92, 'MSB', 'http://MSB', 'MSB',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    613, 181, 'cbreduction', 'http://cbreduction',
    'cbreduction', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    614, 104, 'Good deal day WM', 'http://Good deal day WM',
    'Good deal day WM', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    615, 104, 'Panel conso emailing',
    'http://Panel conso emailing',
    'Panel conso emailing', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    616, 182, 'Lettredujour.com ', 'http://Lettredujour.com ',
    'Lettredujour.com ', 2, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    617, 1, 'expert du gain argent', 'http://expert du gain argent',
    'expert du gain argent', 10, 0, 0,
    0, 0, 0, 'FRA', 2, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    618, 104, 'expert du gain dargent',
    'http://expert du gain dargent',
    'expert du gain dargent', 1, 0, 0,
    0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    619, 123, 'Place des bons plans',
    'http://Place des bons plans',
    'Place des bons plans', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    620, 108, 'Panel conso 4 ', 'http://Panel conso 4 ',
    'Panel conso 4 ', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    621, 181, 'Les Meilleures Offres',
    'http://Les Meilleures Offres',
    'Les Meilleures Offres', 1, 0, 0,
    0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    622, 181, 'Shopping Fanatique', 'http://Shopping Fanatique',
    'Shopping Fanatique', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    623, 181, 'Le Marché des Offres',
    'http://Le Marché des Offres',
    'Le Marché des Offres', 1, 0, 0,
    0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    624, 181, 'Toujours Mieux ', 'http://Toujours Mieux ',
    ' Toujours Mieux ', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    625, 181, 'Le Service des Astuces',
    'http://Le Service des Astuces',
    'Le Service des Astuces', 1, 0, 0,
    0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    626, 181, 'E-Nouveauté', 'http://E-Nouveauté',
    'E-Nouveauté', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    627, 181, 'bons deals du net', 'http://bons deals du net',
    'bons deals du net', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    628, 181, 'Chic avenue', 'http://Chic avenue',
    'Chic avenue', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    629, 163, 'La Parisienne ', 'http://La Parisienne ',
    'La Parisienne ', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 163, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    630, 181, 'Voyagespirates.fr', 'http://Voyagespirates.fr',
    'Voyagespirates.fr', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    631, 123, 'plaisirs malins', 'http://plaisirs malins',
    'plaisirs malins', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    632, 123, 'Kiss Me More ', 'http://Kiss Me More ',
    'Kiss Me More ', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    633, 162, 'Grand Soleil sur les offres   ',
    'http://Grand Soleil sur les offres',
    'Grand Soleil sur les offres


',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    634, 162, 'Le coin vip ', 'http://Le coin vip ',
    'Le coin vip ', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    635, 162, 'MEJ ', 'http://MEJ ', 'MEJ ',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    636, 162, 'Je selectionne mon Offre',
    'http://Je selectionne mon Offre',
    'Je selectionne mon Offre', 1, 0,
    0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    637, 162, 'Je choisis mon bon plan',
    'http://Je choisis mon bon plan',
    'Je choisis mon bon plan', 1, 0,
    0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    638, 177, 'CMalin', 'http://CMalin',
    'CMalin', 1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    639, 142, 'DMedia Mobile', 'http://DMedia Mobile',
    'DMedia Mobile', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    640, 183, 'Datasuccess', 'http://datasuccess.fr',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 237, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    641, 184, 'adomos', 'http://adomos',
    'ADOMOS', 3, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    642, 185, 'acheter louer cap', 'http://acheter louer cap',
    'acheter louer cap', 2, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    643, 181, 'galerie shop', 'http://galerie shop',
    'galerie shop', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    644, 181, 'seulement pour elle', 'http://seulement pour elle',
    'seulement pour elle', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    645, 180, 'deal de choc', 'http://deal de choc',
    'deal de choc', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    646, 180, 'clic surprise', 'http://clic surprise',
    'clic surprise', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    647, 180, 'affaires exclusives', 'http://affaires exclusives',
    'affaires exclusives', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    648, 104, 'vis shopping', 'http://vis shopping',
    'vis shopping', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    649, 153, 'BHE', 'http://BHE', 'BHE',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    650, 104, 'LES MEILLEURS REDUC DU WEB',
    'http://LES MEILLEURS REDUC DU WEB',
    'LES MEILLEURS REDUC DU WEB', 1,
    0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    651, 104, 'SHOPPING INSTITUT EMAIL',
    'http://SHOPPING INSTITUT EMAIL',
    'SHOPPING INSTITUT EMAIL', 1, 0,
    0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    652, 180, 'Minceur4d', 'http://Minceur4d',
    'Minceur4d', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    653, 180, 'plan du web IO', 'http://plan du web IO',
    'plan du web IO', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    654, 104, 'good shopping', 'http://good shopping',
    'good shopping', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    655, 104, 'LE TEMPS DES BONNES AFFAIRES DU WEB',
    'http://LE TEMPS DES BONNES AFFAIRES DU WEB',
    'LE TEMPS DES BONNES AFFAIRES DU WEB',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    656, 162, 'rien à envier', 'http://rien à envier',
    'rien à envier', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 18, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    657, 162, 'les aubaines malines',
    'http://les aubaines malines',
    'les aubaines malines', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    658, 162, 'mon-envie-du-jour1 ', 'http://mon-envie-du-jour1 ',
    'mon-envie-du-jour1 ', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    659, 162, 'le marché premium', 'http://le marché premium',
    'le marché premium', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    660, 162, 'La Maison des Bons Plans',
    'http://La Maison des Bons Plans',
    'La Maison des Bons Plans', 1, 0,
    0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    661, 162, 'La chance et moi ', 'http://La chance et moi ',
    'La chance et moi ', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    662, 162, 'Galeries des Bons Plans ',
    'http://Galeries des Bons Plans ',
    'Galeries des Bons Plans ', 1, 0,
    0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    663, 104, 'LE WEB DEAL', 'http://LE WEB DEAL',
    'LE WEB DEAL', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    664, 104, 'Pierre roi des bonnes affaires',
    'http://Pierre roi des bonnes affaires',
    'Pierre roi des bonnes affaires',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    665, 150, 'excursion sauvage', 'http://excursion sauvage',
    'excursion sauvage', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    666, 104, 'economies affaires chic privilege',
    'http://economies affaires chic privilege',
    'economies affaires chic privilege',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    667, 104, 'usine des marques3', 'http://usine des marques3',
    'usine des marques3', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    668, 104, 'bonnes affaires chic privilege ',
    'http://bonnes affaires chic privilege ',
    'bonnes affaires chic privilege ',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    669, 104, 'unjouruncaddie3', 'http://unjouruncaddie3',
    'unjouruncaddie3', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    670, 180, 'générique ideal-perf',
    'http://générique ideal-perf',
    'générique ideal-perf', 1, 0, 0,
    0, 0, 0, 'FRA', 1, 174, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    671, 104, 'planet shopping', 'http://planet shopping',
    'planet shopping', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    672, 150, 'culture monde', 'http://culture monde',
    'culture monde', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    673, 150, 'nana look', 'http://nana look',
    'nana look', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    674, 104, 'LES JOUR DES ECONOMIES',
    'http://LES JOUR DES ECONOMIES',
    'LES JOUR DES ECONOMIES', 1, 0, 0,
    0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    675, 186, 'Z Mailing', 'http://Z Mailing',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 160, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    676, 187, 'rcprojet', 'http://rcprojet',
    'rcprojet', 2, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    677, 104, 'Web cadeau', 'http://Web cadeau',
    'Web cadeau', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    678, 104, 'agenda du web', 'http://agenda du web',
    'agenda du web', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    679, 186, 'mail2', 'http://mail2',
    'mail2', 1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    680, 109, 'NL GEO', 'http://NL GEO',
    'NL GEO', 1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    681, 109, 'eden flirt', 'http://eden flirtge',
    'eden flirt', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 145, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    682, 104, 'Regie shop and Go', 'http://Regie shop and Go',
    'Regie shop and Go', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    683, 104, 'De bon matin WM', 'http://De bon matin WM',
    'De bon matin WM', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    684, 180, 'ptg', 'http://ptg', 'ptg',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    685, 188, 'Viralitea', 'http://Viralitea',
    'Viralitea', 2, 0, 0, 0, 0, 0, 'FRA',
    1, 204, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    686, 109, 'sublime moi', 'http://sublime moi',
    'sublime moi', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 145, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    687, 104, 'ALERTE SHOPPING MALIN3',
    'http://ALERTE SHOPPING MALIN3',
    'ALERTE SHOPPING MALIN3', 1, 0, 0,
    0, 0, 0, 'FRA', 1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    688, 189, 'expert contact', 'http://expert contact',
    '', 2, 0, 0, 0, 0, 1, 'FRA', 1, 166, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    689, 104, 'chic planet 3 ', 'http://chic planet 3 ',
    'chic planet 3 ', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    690, 104, 'Marie Reine des bons plans',
    'http://Marie Reine des bons plans',
    'Marie Reine des bons plans', 1,
    0, 0, 0, 0, 0, 'FRA', 1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    691, 104, 'univers du shopping', 'http://univers du shopping',
    'univers du shopping', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    692, 162, 'jardin des avantages',
    'http://jardin des avantages',
    'jardin des avantages', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    693, 180, 'plaisir inside', 'http://plaisir inside',
    'plaisir inside', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    694, 150, 'promenade tropicales',
    'http://promenade tropicales',
    'promenade tropicales', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    695, 153, 'IMM SWARMIZ', 'http://IMM SWARMIZ',
    'IMM SWARMIZ', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    696, 190, 'BSD', 'http://BSD', '', 2,
    0, 0, 0, 0, 0, 'FRA', 1, 27, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    697, 104, 'LEA ET SON FORUM', 'http://LEA ET SON FORUM',
    'LEA ET SON FORUM', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    698, 104, 'SHOPPING ONLINE WM', 'http://SHOPPING ONLINE WM',
    'SHOPPING ONLINE WM', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    699, 104, 'MEILLEURS PLANS', 'http://MEILLEURS PLANS',
    'MEILLEURS PLANS', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    700, 104, 'LE WEB SHOPPING', 'http://LE WEB SHOPPING',
    'LE WEB SHOPPING', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    701, 104, 'usine des marques2', 'http://usine des marques2',
    'usine des marques2', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    702, 104, 'shopping institut webmedia',
    'http://shopping institut webmedia',
    'shopping institut webmedia', 1,
    0, 0, 0, 0, 0, 'FRA', 1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    703, 104, 'panel webmedia', 'http://panel webmedia',
    'panel webmedia', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    704, 162, 'espaces promos', 'http://espaces promos',
    'espaces-promos', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 18, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    705, 162, 'occasiondumoment', 'http://occasiondumoment',
    'occasiondumoment', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    706, 162, 'simplement malin', 'http://simplement malin',
    'simplement malin', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    707, 174, 'usine des deals', 'http://usine des deals',
    'usine des deals', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    708, 174, '1j1a', 'http://1j1a', '1j1a',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    709, 121, 'ubixy', 'http://ubixy',
    'ubixy', 1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    710, 104, 'REGIE BONHEUR DU JOUR',
    'http://REGIE BONHEUR DU JOUR',
    'REGIE BONHEUR DU JOUR', 1, 0, 0,
    0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    711, 190, 'PlacementSerein', 'http://PlacementSerein',
    'PlacementSerein', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    712, 150, 'egerie feminine', 'http://egerie feminine',
    'egerie feminine', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    713, 150, 'demoiselle en vogue', 'http://demoiselle en vogue',
    'demoiselle en vogue', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    714, 108, 'LG Panel Conso', 'http://LG Panel Conso',
    'LG Panel ConsoLG Panel Conso',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    715, 108, 'GDDFR1', 'http://GDDFR1',
    'GDDFR1', 1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    716, 76, 'saga family', 'http://saga family',
    'saga family', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 238, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    717, 77, 'Enquête Conso rad', 'http://Enquête Conso rad',
    'Enquête Conso', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    718, 77, 'faites des economies', 'http://faites des economies',
    'faites des economies', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    719, 77, 'black friday', 'http://black friday',
    'black friday', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    720, 192, 'Pulpower France', 'http://Pulpower France',
    'Pulpower France', 2, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    721, 180, 'Joe&amp;Joyce', 'http://Joe&Joyce',
    'Joe&amp;Joyce', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    722, 108, 'Le club des affaires',
    'http://Le club des affaires',
    'Le club des affaires', 1, 0, 0, 0,
    0, 0, 'FRA', 0, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    723, 108, 'Les conseils de Marion',
    'http://Les conseils de Marion',
    'Les conseils de Marion', 1, 0, 0,
    0, 0, 0, 'FRA', 0, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    724, 108, 'La web party', 'http://La web party',
    'La web party', 1, 0, 0, 0, 0, 0, 'FRA',
    0, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    725, 108, 'online privilege', 'http://online privilege',
    'online privilege', 1, 0, 0, 0, 0, 0,
    'FRA', 0, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    726, 108, 'Partage des idees', 'http://Partage des idees',
    'Partage des idees', 1, 0, 0, 0, 0,
    0, 'FRA', 0, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    727, 108, 'Planete shopping', 'http://Planete shopping',
    'Planete shopping', 1, 0, 0, 0, 0, 0,
    'FRA', 0, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    728, 108, 'Shopping conseils', 'http://Shopping conseils',
    'Shopping conseils', 1, 0, 0, 0, 0,
    0, 'FRA', 0, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    729, 104, 'Shoppingconseil', 'http://Shoppingconseil',
    'Shopping conseil', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    730, 193, 'Solumail', 'http://Solumail',
    'Solumail - Koresh 8 - 75208 Rishon Lezion - Israel

Krief Bruno



Bases :

-

- traveo-devis',
    2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    731, 193, 'traveo-devis', 'http://traveo-devis',
    'traveo-devis', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    732, 108, 'VMV DEAL + ', 'http://VMV DEAL + ',
    'VMV DEAL + ', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    733, 104, 'Astuce conso', 'http://Astuce conso',
    'Astuce conso', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    734, 190, 'assuranceetprotection',
    'http://assuranceetprotection',
    'assuranceetprotection', 1, 0, 0,
    0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    735, 181, 'Correo trade', 'http://Correo trade',
    'Correo trade', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    736, 12, 'avosmails', 'http://avosmails',
    'avosmails', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    737, 12, 'sbp', 'http://sbp', 'sbp',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    738, 104, 'LE WEB DEAL France ', 'http://LE WEB DEAL France ',
    'LE WEB DEAL France ', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    739, 22, 'RMA2', 'http://RMA2', 'RMA2',
    1, 0, 0, 0, 0, 0, 'FRA', 0, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    740, 104, 'ALICES CORNER France',
    'http://ALICES CORNER France',
    'ALICES CORNER France', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    741, 194, 'Emal Marketing', 'http://www.websoulservices.com',
    'We are working in email marketing field from last 3 years and have quality database with us',
    60, 0, 0, 0, 0, 0, 'ENG', 2, 'NULL', 0
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    742, 180, 'RMA IO', 'http://RMA IO',
    'RMA IO', 1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    743, 180, 'konc', 'http://konc', 'konc',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    744, 153, 'DMS', 'http://DMS', 'DMS',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    745, 150, 'beaute luxe', 'http://beaute luxe',
    'beaute luxe', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    746, 92, 'Regip Riviera', 'http://Regip Riviera',
    'Regip Riviera', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    747, 92, 'atoutmail riviera', 'http://atoutmail riviera',
    'atoutmail riviera', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 129, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    748, 92, 'Kidikadi ', 'http://Kidikadi ',
    'Kidikadi ', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 129, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    749, 92, 'Mokito ', 'http://Mokito ',
    'Mokito ', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    225, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    750, 92, 'Pixmania ', 'http://Pixmania ',
    'Pixmania ', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 129, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    751, 92, 'OffreShopping ', 'http://OffreShopping ',
    'OffreShopping ', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 129, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    752, 181, 'buzzfrance', 'http://buzzfrance',
    'buzzfrance', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    753, 181, 'reducfr', 'http://reducfr',
    'reducfr', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    754, 180, 'topg', 'http://topg', 'topg',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    755, 195, 'into-web.com', 'http://into-web.com/',
    'Newest promotions and occasions from all around the world.',
    60, 0, 0, 100, 50, 0, 'ENG', 2, 'NULL',
    0
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    756, 162, 'Suggestion en or', 'http://Suggestion en or',
    'Suggestion en or', 1, 0, 0, 0, 0, 0,
    'FRA', 2, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    757, 162, 'VIP marques', 'http://VIP marques',
    ' VIP marques', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    758, 104, 'Le petit journal de Sophie',
    'http://Le petit journal de Sophie',
    'Le petit journal de Sophie', 1,
    0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    759, 162, 'Journée Ensoleillée',
    'http://Journée Ensoleillée',
    'Journée Ensoleillée', 1, 0, 0,
    0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    760, 196, 'sharemyclick', 'http://sharemyclick',
    'sharemyclick', 2, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    761, 150, 'ambitions-intellectuel',
    'http://ambitions-intellectuel',
    'ambitions-intellectuel', 1, 0, 0,
    0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    762, 104, 'planete shopping wm', 'http://planete shopping wm',
    'planete shopping wm', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    763, 170, 'OM1', 'http://OM1', 'OM1',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    764, 181, 'aviva', 'http://aviva',
    'aviva', 1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    765, 196, 'alertodeal', 'http://alertodeal',
    'alertodeal', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    766, 196, 'Dealvip ', 'http://Dealvip ',
    'Dealvip ', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    767, 150, 'elegance feminine', 'http://elegance feminine',
    'elegance feminine', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    768, 196, 'moncoinperso', 'http://moncoinperso',
    'moncoinperso', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    769, 196, 'Offresdefolies', 'http://Offresdefolies',
    'Offresdefolies', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    770, 104, 'acheter malin', 'http://acheter malin',
    'acheter malin', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    771, 104, 'Vis ton shopping', 'http://Vis ton shopping',
    'Vis ton shopping', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    772, 104, 'Regie acheter malin', 'http://Regie acheter malin',
    'Regie acheter malin', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    773, 198, 'Fourchette et Bikini',
    'http://cedric.courtinat@m6.fr',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    774, 198, 'Fourchette et Bikini',
    'http://Fourchette et Bikini',
    'Fourchette et Bikini', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    775, 34, 'ciel si market', 'http://ciel si market',
    'ciel si market', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 162, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    776, 104, 'Panel conso bon plan',
    'http://Panel conso bon plan',
    'Panel conso bon plan', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    777, 162, 'mon-envie-du-jour2', 'http://mon-envie-du-jour2',
    'mon-envie-du-jour2', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    778, 104, 'unjouruncaddie wm', 'http://unjouruncaddie wm',
    'unjouruncaddie wm', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    779, 104, 'LUSINE DES MARQUES', 'http://LUSINE DES MARQUES',
    'LUSINE DES MARQUES', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    780, 104, 'LE COIN DU LUXE', 'http://LE COIN DU LUXE',
    'LE COIN DU LUXE', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    781, 104, 'debon matin emailing',
    'http://debon matin emailing',
    'debon matin emailing', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    782, 104, 'bon matin shop ', 'http://bon matin shop ',
    'bon matin shop ', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    783, 104, 'panel conso WM', 'http://panel conso WM',
    'panel conso WM', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    784, 104, 'Shopping conseils WM',
    'http://Shopping conseils WM',
    'Shopping conseils WM', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    785, 104, 'Le club des affaires wm',
    'http://Le club des affaires wm',
    'Le club des affaires wm', 1, 0,
    0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    786, 104, 'Les conseils de Marion wm',
    'http://Les conseils de Marion wm',
    'Les conseils de Marion wm', 1,
    0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    787, 104, 'Partage des idees wm',
    'http://Partage des idees wm',
    'Partage des idees wm', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    788, 162, 'chic planete', 'http://chic planete',
    'chic planete', 1, 0, 0, 0, 0, 0, 'FRA',
    2, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    789, 104, 'chic planete WM', 'http://chic planete WM',
    'chic planete WM', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    790, 104, 'Online privilege wm', 'http://Online privilege wm',
    'Online privilege wm', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    791, 104, 'web party wm', 'http://web party wm',
    'web party wm', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    792, 104, 'bonnes adresses de Sophie',
    'http://bonnes adresses de Sophie',
    'bonnes adresses de Sophie', 1,
    0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    793, 104, 'Les astuces de Sophie',
    'http://Les astuces de Sophie',
    'Les astuces de Sophie', 1, 0, 0,
    0, 0, 0, 'FRA', 1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    794, 199, 'lesoffresdunet.com', 'http://lesoffresdunet.com',
    'lesoffresdunet.com', 2, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    795, 200, 'mailinmotion', 'http://mailinmotion',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    796, 163, 'ventes privées du jour',
    'http://ventes privées du jour',
    'ventes privées du jour', 1, 0,
    0, 0, 0, 0, 'FRA', 1, 163, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    797, 104, 'Mon plan du jour ', 'http://Mon plan du jour ',
    'Mon plan du jour ', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    798, 104, 'Easy bingo', 'http://Easy bingo',
    'Easy bingo', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    799, 104, 'Regie Shop and Go wm',
    'http://Regie Shop and Go wm',
    'Regie Shop and Go wm', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    800, 129, 'display mediastay', 'http://display mediastay',
    'display mediastay', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 195, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    801, 104, 'Le monde de Samuel', 'http://Le monde de Samuel',
    'Le monde de Samuel', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    802, 104, 'Le buzz du web', 'http://Le buzz du web',
    'Le buzz du web', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    803, 104, 'LES BONNES ID DE THOMAS',
    'http://LES BONNES ID DE THOMAS',
    'LES BONNES ID DE THOMAS', 1, 0,
    0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    804, 104, 'GOOD DEAL WM', 'http://GOOD DEAL WM',
    'GOOD DEAL WM', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    805, 104, 'WEB EN FOLIE', 'http://WEB EN FOLIE',
    'WEB EN FOLIE', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    806, 57, 'clicletters', 'http://clicletters',
    'clicletters ', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 149, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    807, 57, 'info deal', 'http://info deal',
    'info deal', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 149, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    808, 121, 'tendancity 2', 'http://tendancity 2',
    'tendancity 2', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    809, 121, 'Media unitead 2', 'http://Media unitead 2',
    '
Media unitead 2', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    810, 121, 'place des deals', 'http://place des deals',
    'place des deals', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    811, 121, 'TDI', 'http://TDI', 'TDI',
    1, 0, 0, 0, 0, 0, 'FRA', 0, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    812, 121, 'AARON BRAINWAVE 2 ', 'http://AARON BRAINWAVE 2 ',
    'AARON BRAINWAVE 2 ', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    813, 201, 'REFLEXEMEDIA', 'http://REFLEXEMEDIA',
    'REFLEXEMEDIA', 2, 0, 0, 0, 0, 0, 'FRA',
    1, 227, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    814, 104, 'De bon matin astuces',
    'http://De bon matin astuces',
    'De bon matin astuces', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    815, 108, 'Planète chic-EB', 'http://Planète chic-EB',
    'Planète chic-EB', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    816, 108, 'Caddie du jour-EB', 'http://Caddie du jour-EB',
    'Caddie du jour-EB', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    817, 108, 'Shopping astuces-EB', 'http://Shopping astuces-EB',
    'Shopping astuces-EB', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    818, 108, 'Emilie Web-EB', 'http://Emilie Web-EB',
    'Emilie Web-EB', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    819, 108, 'AZOglobal-Slip', 'http://AZOglobal-Slip',
    'AZOglobal-Slip', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    820, 108, 'PCHIC-EB', 'http://PCHIC-EB',
    'PCHIC-EB', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    821, 108, 'ST sponsorboost', 'http://ST sponsorboost',
    'ST sponsorboost', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    822, 108, 'UAM-EB', 'http://UAM-EB',
    'UAM-EB', 1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    823, 108, 'Online reduc-EB', 'http://Online reduc-EB',
    'Online reduc-EB', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    824, 108, 'Promoprivilege-EB', 'http://Promoprivilege-EB',
    'Promoprivilege-EB', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    825, 108, 'WMRMglobal-Slip', 'http://WMRMglobal-Slip',
    'WMRMglobal-Slip', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    826, 108, 'Deal +-EB', 'http://Deal +-EB',
    'Deal +-EB', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    827, 202, 'megaffaire.fr', 'http://megaffaire.fr',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    828, 108, 'Usines à marque eb', 'http://Usines à marque eb',
    'Usines à marque eb', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    829, 104, 'Mon offre du jour ', 'http://Mon offre du jour ',
    'Mon offre du jour ', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    830, 104, 'univers des marques ',
    'http://univers des marques ',
    'univers des marques ', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    831, 104, 'Deal and co', 'http://Deal and co',
    'Deal and co', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    832, 104, 'MATIN SHOPPING', 'http://MATIN SHOPPING',
    'MATIN SHOPPING', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    833, 181, 'theleads', 'http://theleads',
    'theleads', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    834, 104, 'VIS EN LIGNE', 'http://VIS EN LIGNE',
    'VIS EN LIGNE', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    835, 203, 'PetitJournal', 'http://PetitJournal', ''
    , 2, 0, 0, 0, 0, 0, 'FRA', 1, 147, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    836, 162, 'soldes-continues', 'http://soldes-continues',
    'soldes-continues', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 18, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    837, 108, 'WMAZOGLOBAL', 'http://WMAZOGLOBAL',
    'WMAZOGLOBAL', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    838, 76, 'Travelprix', 'http://Travelprix',
    'Travelprix', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 238, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    839, 204, 'Verticalizer ', 'http://Verticalizer ',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    840, 84, 'dressing de lili', 'http://dressing de lili',
    'dressing de lili', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    841, 121, 'offres privées unitead',
    'http://offres privées unitead',
    'offres privées', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    842, 121, 'annuaires des deals', 'http://annuaires des deals',
    'annuaires des deals', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    843, 121, 'netlead unitead', 'http://netlead unitead',
    'netlead unitead', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    844, 153, 'RDV des Actus', 'http://RDV des Actus',
    'RDV des Actus', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    845, 153, 'Usines des marque sw',
    'http://Usines des marque sw',
    'Usines des marque sw', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    846, 189, 'AAMP Immobilier', 'http://AAMP Immobilier',
    'AAMP Immobilier', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    847, 57, 'JDD', 'http://JDD', 'JDD',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    848, 109, 'the optin machine', 'http://the optin machine',
    'the optin machine', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 145, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    849, 109, 'NL GALA', 'http://NL GALA',
    'NL GALA', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    145, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    850, 92, 'MySocialBeauty ', 'http://MySocialBeauty ',
    'MySocialBeauty ', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 129, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    851, 92, 'OffreVIP ', 'http://OffreVIP ',
    'OffreVIP ', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 129, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    852, 92, 'VIPConcours', 'http://VIPConcours',
    'VIPConcours', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 129, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    853, 67, 'jour de Promo', 'http://jour de Promo',
    'jour de Promo', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 202, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    854, 67, 'labelartisan', 'http://labelartisan',
    'labelartisan', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 202, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    855, 108, 'De Bon Matin EB', 'http://De Bon Matin EB',
    'De Bon Matin EB', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    856, 108, 'Mon shopping malin EB',
    'http://Mon shopping malin EB',
    'Mon shopping malin EB', 1, 0, 0,
    0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    857, 181, 'Leadtheleads', 'http://Leadtheleads',
    'Leadtheleads', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    858, 121, 'verity', 'http://verity',
    'verity', 1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    859, 163, 'ma maison privée', 'http://ma maison privée',
    'ma maison privée', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    860, 66, 'News Tendance', 'http://News Tendance',
    'News Tendance', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    861, 66, 'Papillons du Web', 'http://http:/Papillons du Web',
    'Papillons du Web', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    862, 66, 'Guide Privilège', 'http://Guide Privilège',
    'Guide Privilège', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    863, 66, 'Femmesplus 2', 'http://Femmesplus 2',
    'Femmesplus 2', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    864, 66, 'Top Malin', 'http://Top Malin',
    'Top Malin', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 196, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    865, 181, '123vetolia', 'http://123vetolia',
    '123vetolia', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    866, 205, 'Capdecision test', 'http://',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    867, 123, 'Affaires &amp; Compagnie',
    'http://Affaires & Compagnie',
    'Affaires &amp; Compagnie', 1, 0,
    0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    868, 123, 'Votre actu immo', 'http://Votre actu immo',
    'Votre actu immo', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    869, 104, 'azo bon plan', 'http://azo bon plan',
    'azo bon plan', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    870, 180, 'coqcorico', 'http://coqcorico',
    'coqcorico', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    871, 92, 'Consochic ', 'http://Consochic ',
    'Consochic ', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 129, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    872, 121, 'lesmeilleursastuces', 'http://lesmeilleursastuces',
    'lesmeilleursastuces', 1, 0, 0, 0,
    0, 0, 'FRA', 0, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    873, 206, 'La canopée', 'http://',
    'La canopée



Tel :', 2, 0, 0, 0,
    0, 0, 'FRA', 1, 24, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    874, 206, 'La canopée', 'http://La canopée',
    'La canopée', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 24, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    875, 153, 'unjourunkady', 'http://unjourunkady',
    'unjourunkady', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    876, 153, 'clic malin', 'http://clic malin',
    'clic malin', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    877, 104, 'un jour un caddie 2', 'http://un jour un caddie 2',
    'un jour un caddie 2', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    878, 104, 'chic planete 2', 'http://chic planete 2',
    'chic planete 2', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    879, 104, 'alerte shopping malin 2',
    'http://alerte shopping malin 2',
    'alerte shopping malin 2', 1, 0,
    0, 0, 0, 0, 'FRA', 1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    880, 104, 'bingo facile wm', 'http://bingo facile wm',
    'bingo facile wm', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    881, 207, 'promonautes.com', 'http://promonautes.com', ''
    , 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    882, 208, 'digitalks', 'http://digitalks',
    '

Tel : 06 67 89 52 08
Email :
Web : www.digitalks.fr ',
    2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    883, 121, 'EBP', 'http://EBP', 'EBP',
    1, 0, 0, 0, 0, 0, 'FRA', 0, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    884, 162, 'PANELCONSO-SHOPPING', 'http://PANELCONSO-SHOPPING',
    'PANELCONSO-SHOPPING', 1, 0, 0, 0,
    0, 0, 'FRA', 2, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    885, 104, 'PANELCONSO SHOPPING', 'http://PANELCONSO SHOPPING',
    'PANELCONSO-SHOPPING', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    886, 104, 'un jour un caddie4  ',
    'http://un jour un caddie4  ',
    'un jour un caddie4  ', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    887, 104, 'Les marques outlet ', 'http://Les marques outlet ',
    'Les marques outlet ', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    888, 207, 'mediaffiliation.com', 'http://mediaffiliation.com',
    'mediaffiliation.com', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    889, 162, 'offrespremiumpourmoi.com',
    'http://offrespremiumpourmoi.com',
    'offrespremiumpourmoi.com', 1, 0,
    0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    890, 162, 'tresors du web', 'http://tresors du web',
    'tresors du web', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 18, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    891, 206, 'la petite astuce', 'http://la petite astuce',
    'la petite astuce', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 24, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    892, 121, 'PDD', 'http://PDD', 'PDD',
    1, 0, 0, 0, 0, 0, 'FRA', 0, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    893, 109, 'NL programme TV', 'http://NL programme TV',
    'NL programme TV', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 145, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    894, 104, 'alerte shopping malin 4',
    'http://alerte shopping malin 4',
    'alerte shopping malin 4', 1, 0,
    0, 0, 0, 0, 'FRA', 1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    895, 162, 'La boîte à idée', 'http://La boîte à idée',
    'La boîte à idée', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    896, 162, 'Les idées malices', 'http://Les idées malices',
    'Les idées malices', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 18, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    897, 57, 'bestofdeal 2', 'http://bestofdeal 2',
    'bestofdeal 2', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    898, 57, 'Crakeo', 'http://Crakeo',
    'Crakeo', 1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    899, 210, 'SOCIALSCOOP', 'http://SOCIALSCOOP',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    900, 211, 'ugp 47', 'http://ugp 47',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 172, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    901, 211, 'gdf 133', 'http://gdf 133',
    'gdf 133', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    172, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    902, 211, 'mda 55', 'http://mda 55',
    'mda 55', 1, 0, 0, 0, 0, 0, 'FRA', 1, 172,
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    903, 211, 'csh 116', 'http://csh 116',
    'csh 116', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    172, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    904, 211, 'SC2 58', 'http://SC2 58',
    'SC2 58', 1, 0, 0, 0, 0, 0, 'FRA', 1, 172,
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    905, 211, 'WAN 59', 'http://WAN 59',
    'WAN 59', 1, 0, 0, 0, 0, 0, 'FRA', 1, 172,
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    906, 211, 'TDI 140', 'http://TDI 140',
    'TDI 140', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    172, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    907, 211, 'ntl 64', 'http://ntl 64',
    'ntl 64', 1, 0, 0, 0, 0, 0, 'FRA', 1, 172,
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    908, 211, 'bdv 209', 'http://bdv 209',
    'bdv 209', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    172, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    909, 211, 'ted 139', 'http://ted 139',
    'ted 139', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    172, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    910, 211, 'JDN 56', 'http://JDN 56',
    'JDN 56', 1, 0, 0, 0, 0, 0, 'FRA', 1, 172,
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    911, 211, 'stm 67', 'http://stm 67',
    'stm 67', 1, 0, 0, 0, 0, 0, 'FRA', 1, 172,
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    912, 211, 'emc 71', 'http://emc 71',
    'emc 71', 1, 0, 0, 0, 0, 0, 'FRA', 1, 172,
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    913, 211, 'vrt 63', 'http://vrt 63',
    'vrt 63', 1, 0, 0, 0, 0, 0, 'FRA', 1, 172,
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    914, 211, 'aab 31', 'http://aab 31',
    'aab 31', 1, 0, 0, 0, 0, 0, 'FRA', 1, 172,
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    915, 211, 'rma 50', 'http://rma 50',
    'rma 50', 1, 0, 0, 0, 0, 0, 'FRA', 1, 172,
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    916, 211, 'IMM 1002', 'http://IMM 1002',
    'IMM 1002', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    172, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    917, 211, 'tmr 15', 'http://tmr 15',
    'tmr 15', 1, 0, 0, 0, 0, 0, 'FRA', 1, 172,
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    918, 211, 'tdc 206', 'http://tdc 206',
    'tdc 206', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    919, 211, 'ubx 1006', 'http://ubx 1006',
    'ubx 1006', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    172, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    920, 211, 'BVT 22', 'http://BVT 22',
    'BVT 22', 1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    921, 211, 'ofp 69', 'http://ofp 69',
    'ofp 69', 1, 0, 0, 0, 0, 0, 'FRA', 1, 172,
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    922, 211, 'add 70', 'http://add 70',
    'add 70', 1, 0, 0, 0, 0, 0, 'FRA', 1, 172,
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    923, 211, 'MTV 1004', 'http://MTV 1004',
    'MTV 1004', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    172, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    924, 211, 'BOS 1005', 'http://BOS 1005',
    'BOS 1005', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    172, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    925, 211, 'EQC 32', 'http://EQC 32',
    'EQC 32', 1, 0, 0, 0, 0, 0, 'FRA', 1, 172,
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    926, 211, 'LMA 483', 'http://LMA 483',
    'LMA 483', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    172, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    927, 211, 'EBP 200', 'http://EBP 200',
    'EBP 200', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    172, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    928, 211, 'PDD 68', 'http://PDD 68',
    'PDD 68', 1, 0, 0, 0, 0, 0, 'FRA', 1, 172,
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    929, 180, 'primeira ID', 'http://primeira ID',
    'primeira ID', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    930, 92, 'Planet rw', 'http://Planet rw',
    'Planet rw', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 129, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    931, 92, 'Odeals', 'http://Odeals',
    'Odeals', 1, 0, 0, 0, 0, 0, 'FRA', 1, 129,
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    932, 180, 'unitead IO', 'http://unitead IO',
    'unitead IO', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    933, 212, 'tempsprivilege.com', 'http://tempsprivilege.com',
    'tempsprivilege.com', 2, 0, 0, 0, 0,
    0, 'FRA', 1, 155, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    934, 212, 'consommermoinscher.com',
    'http://consommermoinscher.com',
    'consommermoinscher.com', 1, 0, 0,
    0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    935, 212, 'comptoirdesreducs.com',
    'http://comptoirdesreducs.com',
    'comptoirdesreducs.com', 1, 0, 0,
    0, 0, 0, 'FRA', 1, 155, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    936, 212, 'lastucieux.com', 'http://lastucieux.com',
    'lastucieux.com', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 155, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    937, 212, 'enviesdebonsplans.com',
    'http://enviesdebonsplans.com',
    'enviesdebonsplans.com', 1, 0, 0,
    0, 0, 0, 'FRA', 1, 155, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    938, 212, 'clubdesreducs.com', 'http://clubdesreducs.com',
    'clubdesreducs.com', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 155, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    939, 212, 'desirbrulant.com', 'http://desirbrulant.com',
    'desirbrulant.com', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    940, 212, 'laselectiondegedeon.com',
    'http://laselectiondegedeon.com',
    'laselectiondegedeon.com', 1, 0,
    0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    941, 212, 'reducweekend.com', 'http://reducweekend.com',
    'reducweekend.com', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 155, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    942, 212, 'shoprivilege.com', 'http://shoprivilege.com',
    'shoprivilege.com', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 155, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    943, 212, 'shoptapromo.com', 'http://shoptapromo.com',
    'shoptapromo.com', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 155, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    944, 212, 'promoenexclu.com', 'http://promoenexclu.com',
    'promoenexclu.com', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 155, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    945, 212, 'supermalin.com', 'http://supermalin.com',
    'supermalin.com', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    946, 212, 'lagendadesventesprivees.com',
    'http://lagendadesventesprivees.com',
    'lagendadesventesprivees.com',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 155, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    947, 212, 'lastucedujour.com', 'http://lastucedujour.com',
    'lastucedujour.com', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    948, 212, 'comptoirshopping.com',
    'http://comptoirshopping.com',
    'comptoirshopping.com', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 155, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    949, 212, 'lebonkdo.com', 'http://lebonkdo.com',
    'lebonkdo.com', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 155, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    950, 212, 'madestinee.com', 'http://madestinee.com',
    'madestinee.com', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 155, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    951, 212, 'lepetitruse.com', 'http://lepetitruse.com',
    'lepetitruse.com', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    952, 57, 'GMS', 'http://GMS', 'GMS',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 149, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    953, 57, 'Sourire', 'http://Sourire',
    'Sourire', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    149, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    954, 108, 'WEBCOMMERCECAP2', 'http://WEBCOMMERCECAP2',
    'WEBCOMMERCECAP2', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    955, 129, 'MDS1', 'http://MDS1', 'MDS1',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    956, 129, 'Mds2', 'http://Mds2', 'Mds2',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    957, 129, 'Mds3', 'http://Mds3', 'Mds3',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    958, 129, 'Mds4', 'http://Mds4', 'Mds4',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    959, 129, 'Mds5', 'http://Mds5', 'Mds5',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    960, 129, 'Mds6', 'http://Mds6', 'Mds6',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    961, 104, 'Les actualités de Sophie',
    'http://Les actualités de Sophie',
    'Les actualités de Sophie', 1,
    0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    962, 211, 'ADO 72', 'http://ADO 72',
    'ADO 72', 1, 0, 0, 0, 0, 0, 'FRA', 1, 172,
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    963, 109, 'happy conso', 'http://happy conso',
    'happy conso', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    964, 162, 'Je selectionne le meilleur',
    'http://Je selectionne le meilleur',
    'Je selectionne le meilleur', 1,
    0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    965, 174, 'le courriel du jour', 'http://le courriel du jour',
    'le courriel du jour', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    966, 174, 'plan des reves', 'http://plan des reves',
    'plan des reves', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    967, 174, 'mobile today', 'http://mobile today',
    'mobile today', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    968, 109, 'promo privilège', 'http://promo privilège',
    'promoprivilège', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    969, 213, 'optin adresse', 'http://optin adresse',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 152, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    970, 214, 'optin adresse', 'http://optin adresse',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 0, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    971, 108, 'CW2', 'http://CW2', 'CW2',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    972, 165, 'Autokoo', 'http://Autokoo',
    'Autokoo', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    973, 57, 'kalistaa', 'http://kalistaa',
    'kalistaa', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    974, 139, 'citymode', 'http://citymode',
    'citymode', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    137, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    975, 139, 'Newsbeauty', 'http://Newsbeauty',
    'Newsbeauty', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 137, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    976, 139, 'Cinénews ', 'http://Cinénews ',
    'Cinénews ', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 137, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    977, 162, 'selection et pas seulement',
    'http://selection et pas seulement',
    'selection et pas seulement', 1,
    0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    978, 181, 'vipb', 'http://vipb', 'vipb',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    979, 104, 'Deal du web ', 'http://Deal du web ',
    'Deal du web ', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    980, 145, 'regip bon plan', 'http://regip bon plan',
    'regip bon plan', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    981, 153, 'quick send', 'http://quick send',
    'quick send', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    982, 153, 'chic planet SW', 'http://chic planet SW',
    'chic planet SW', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    983, 212, 'le petit finaud', 'http://le petit finaud',
    'le petit finaud', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    984, 22, 'BUM', 'http://BUM', 'BUM',
    1, 0, 0, 0, 0, 0, 'FRA', 2, 127, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    985, 22, 'BML ', 'http://BML ', 'BML ',
    1, 0, 0, 0, 0, 0, 'FRA', 0, 127, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    986, 211, 'BUM 00', 'http://BUM 00',
    'BUM 00', 1, 0, 0, 0, 0, 0, 'FRA', 1, 172,
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    987, 211, 'BML 00', 'http://BML 00',
    'BML
', 1, 0, 0, 0, 0, 0, 'FRA', 1, 172,
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    988, 162, 'marchedesbonsplans', 'http://marchedesbonsplans',
    'marchedesbonsplans', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    989, 211, 'MTO', 'http://MTO', 'MTO',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 172, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    990, 211, 'TLC', 'http://TLC', 'TLC',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 172, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    991, 211, 'MCI', 'http://MCI', 'MCI',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 172, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    992, 34, 'Tivano', 'http://Tivano',
    'Tivano', 1, 0, 0, 0, 0, 0, 'FRA', 1, 162,
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    993, 215, 'yonkana', 'http://yonkana', ''
    , 2, 0, 0, 0, 0, 0, 'FRA', 1, 140, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    994, 216, 'Nodalys', 'http://Nodalys',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    995, 123, 'display JC natexo', 'http://display JC natexo',
    'display JC natexo', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 118, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    996, 92, 'CBX', 'http://CBX', 'CBX',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    997, 163, 'les echos ', 'http://les echos ',
    'les echos ', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    998, 163, 'Bourse', 'http://Bourse',
    'Bourse', 1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    999, 163, 'Lobs', 'http://Lobs', 'Lobs',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1000, 153, 'Espacevision', 'http://Espacevision',
    'Espacevision', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1001, 123, 'reduc du jour', 'http://reduc du jour',
    'reduc du jour', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1002, 123, 'affaire du web', 'http://affaire du web',
    'affaire du web', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1003, 163, 'Tribunal du net', 'http://Tribunal du net',
    'Tribunal du net', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 163, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1004, 198, 'cuisine AAZ', 'http://cuisine AAZ',
    'cuisine AAZ', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1005, 198, 'météocity', 'http://météocity',
    'météocity', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1006, 109, 'so curious', 'http://so curious',
    'so curious', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 145, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1007, 217, 'Concretisez.fr', 'http://Concretisez.fr',
    'Concretisez.fr', 2, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1008, 218, 'Mon-Credit.co', 'http://mon-credit.co',
    'Comparateur de crédit', 84, 10,
    0, 30000, 40000, 0, 'FRA', 2, 'NULL',
    0
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1009, 98, 'ma lettre immo', 'http://ma lettre immo',
    'ma lettre immo', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 143, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1010, 163, 'figaro santé ', 'http://figaro santé ',
    'figaro santé ', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1011, 219, 'LPV', 'http://LPV', 'LPV',
    2, 0, 0, 0, 0, 0, 'FRA', 1, 234, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1012, 219, 'potoroze', 'http://potoroze',
    'potoroze', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    234, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1013, 104, 'Alex vous dit tout ',
    'http://Alex vous dit tout ', 'Alex vous dit tout ',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1014, 104, 'La bande a antoine', 'http://La bande a antoine',
    'La bande a antoine', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1015, 220, 'adroll programmatique',
    'http://adroll programmatique',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1016, 170, 'bieninvesti', 'http://bieninvesti',
    'bieninvesti', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1017, 104, 'consommation nouvelle',
    'http://consommation nouvelle',
    'consommation nouvelle', 1, 0, 0,
    0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1018, 104, 'facillo bingo ', 'http://facillo bingo ',
    'facillo bingo ', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1019, 104, 'LES REDUCS DU WEB  ',
    'http://LES REDUCS DU WEB  ', 'LES REDUCS DU WEB  ',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1020, 163, 'madame figaro', 'http://madame figaro',
    'madame figaro', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 163, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1021, 104, 'Shopping and co ', 'http://Shopping and co ',
    'Shopping and co ', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1022, 221, 'ma news 7/7', 'http://ma news 7/7',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 190, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1023, 170, 'assurancemutuelle', 'http://assurancemutuelle',
    'assurancemutuelle', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1024, 222, 'taboola', 'http://taboola',
    'taboola', 2, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1025, 118, 'instant automobile', 'http://instant automobile',
    'instant automobile', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1026, 109, 'NL femme actuelle', 'http://NL femme actuelle',
    'NL femme actuelle', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 145, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1027, 118, 'Mes recettes à moi ',
    'http://Mes recettes à moi ',
    'Mes recettes à moi ', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1028, 104, 'compagnie du web', 'http://compagnie du web',
    'compagnie du web', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1029, 104, 'Panel shopping mania',
    'http://Panel shopping mania',
    'Panel shopping mania', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1030, 104, 'vivre en ligne', 'http://vivre en ligne',
    'vivre en ligne', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1031, 104, 'co and deal ', 'http://co and deal ',
    'co and deal ', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1032, 223, 'capdecision native', 'http://capdecision native',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1033, 163, 'grand mere', 'http://grand mere',
    'grand mere', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 163, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1034, 104, 'Les bons plans de Caroline',
    'http://Les bons plans de Caroline',
    'Les bons plans de Caroline', 1,
    0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1035, 104, 'Régie Shopping ', 'http://Régie Shopping ',
    'Régie Shopping ', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1036, 104, 'Les astucieux du web  ',
    'http://Les astucieux du web  ',
    'Les astucieux du web  ', 1, 0, 0,
    0, 0, 0, 'FRA', 1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1037, 104, 'Magic Coupons', 'http://Magic Coupons',
    'Magic Coupons', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1038, 123, 'base gazette des malins ',
    'http://base gazette des malins ',
    'base gazette des malins ', 1, 0,
    0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1039, 104, 'shop on the web', 'http://shop on the web',
    'shop on the web', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1040, 165, 'PROMOTION P', 'http://PROMOTION P',
    'PROMOTION P', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1041, 224, 'lexpress.fr', 'http://lexpress.fr',
    'lexpress.fr', 2, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1042, 163, 'People Addict', 'http://People Addict',
    'People Addict', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1043, 163, 'Le Parisien Meteo ', 'http://Le Parisien Meteo ',
    'Le Parisien Meteo ', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 163, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1044, 163, 'Météo France', 'http://Météo France',
    'Météo France', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1045, 163, 'Top Santé', 'http://Top Santé',
    'Top Santé ', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1046, 163, '20 min sport', 'http://20 min sport',
    '20 min sport', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1047, 170, 'evasion', 'http://evasion',
    'evasion', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1048, 204, 'banquora', 'http://banquora',
    'banquora', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1049, 225, 'FFeminin ', 'http://FFeminin ',
    'FFeminin ', 2, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1050, 225, 'MMasculin  ', 'http://MMasculin  ',
    'MMasculin  ', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1051, 225, 'emailing-promo ', 'http://emailing-promo ',
    'emailing-promo ', 1, 0, 0, 0, 0, 0,
    'FRA', 0, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1052, 226, 'emailing promo', 'http://emailing promo',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1053, 227, 'actiplay', 'http://actiplay',
    'actiplay', 2, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1054, 109, 'AJH', 'http://AJH', 'AJH',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 145, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1055, 163, 'grazia', 'http://grazia',
    'grazia', 1, 0, 0, 0, 0, 0, 'FRA', 1, 163,
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1056, 157, 'info du chef', 'http://info du chef',
    'info du chef ', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1057, 157, 'club exclusif', 'http://club exclusif',
    'club exclusif', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1058, 104, 'chic planet 4', 'http://chic planet 4',
    'chic planet 4', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1059, 27, 'lannoncedujour', 'http://lannoncedujour',
    'lannoncedujour', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 23, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1060, 211, 'MVR-79', 'http://MVR-79',
    'MVR-79', 1, 0, 0, 0, 0, 0, 'FRA', 1, 172,
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1061, 211, 'COR-82', 'http://COR-82',
    'COR-82', 1, 0, 0, 0, 0, 0, 'FRA', 1, 172,
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1062, 211, 'mem', 'http://utead.fr',
    'base généralisée', 10, 0, 0, 0,
    0, 0, 'FRA', 0, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1063, 211, 'ASD', 'http://ASD', 'ASD',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 172, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1064, 211, 'PDS', 'http://PDS', 'PDS',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 172, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1065, 211, 'lwg', 'http://lwg', 'lwg',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 172, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1066, 228, 'capdecision/facebook',
    'http://capdecision/facebook',
    'capdecision/facebook', 2, 0, 0, 0,
    0, 0, 'FRA', 1, 150, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1067, 104, 'Chaque semaine un bon plan',
    'http://Chaque semaine un bon plan',
    'Chaque semaine un bon plan', 1,
    0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1068, 104, 'Le coin des achat', 'http://Le coin des achat',
    'Le coin des achat', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1069, 229, 'MediaFox', 'http://MediaFox',
    'MediaFox', 2, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1070, 229, 'MDF1', 'http://MDF1', 'MDF1',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 159, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1071, 229, 'MDF2', 'http://MDF2', 'MDF2',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1072, 229, 'MDF3', 'http://MDF3', 'MDF3',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1073, 229, 'MDF4', 'http://MDF4', 'MDF4',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1074, 229, 'MDF5', 'http://MDF5', 'MDF5',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1075, 229, 'MDF6', 'http://MDF6', 'MDF6',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1076, 229, 'MDF7', 'http://MDF7', 'MDF7',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1077, 229, 'MDF8', 'http://MDF8', 'MDF8',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1078, 229, 'MDF9', 'http://MDF9', 'MDF9',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1079, 229, 'MDF10', 'http://MDF10',
    'MDF10', 1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1080, 229, 'MDF11', 'http://MDF11',
    'MDF11', 1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1081, 229, 'MDF12', 'http://MDF12',
    'MDF12', 1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1082, 229, 'MDF13', 'http://MDF13',
    'MDF13', 1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1083, 229, 'MDF14', 'http://MDF14',
    'MDF14', 1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1084, 229, 'MDF15', 'http://MDF15',
    'MDF15', 1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1085, 229, 'MDF16', 'http://MDF16',
    'MDF16', 1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1086, 229, 'MDF17', 'http://MDF17',
    'MDF17', 1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1087, 229, 'MDF18', 'http://MDF18',
    'MDF18', 1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1088, 229, 'MDF19', 'http://MDF19',
    'MDF19', 1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1089, 229, 'MDF20', 'http://MDF20',
    'MDF20', 1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1090, 211, 'MEM', 'http://MEM', 'MEM',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 172, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1091, 27, 'voyagemotion', 'http://voyagemotion',
    'voyagemotion', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 23, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1092, 104, 'mes experts du web', 'http://mes experts du web',
    'mes experts du web', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1093, 163, 'Top départ', 'http://Top départ',
    'Top départ', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1094, 104, 'meilleurs plans du web',
    'http://meilleurs plans du web',
    'meilleurs plans du web', 1, 0, 0,
    0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1095, 230, 'adthink', 'http://adthink',
    'adthink', 2, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1096, 74, 'idée de la journée',
    'http://idée de la journée',
    'idée de la journée', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1097, 190, 'Labonneaffaire', 'http://Labonneaffaire',
    'Labonneaffaire', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1098, 170, 'mon apéro', 'http://mon apéro',
    'mon apéro', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1099, 170, 'mieuxchezsoi', 'http://mieuxchezsoi',
    'mieuxchezsoi', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1100, 92, 'monclubshopping', 'http://monclubshopping',
    'monclubshopping', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 129, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1101, 231, 'capdec-samuel', 'http://',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1102, 231, 'capdec-samuel', 'http://capdec-samuel',
    'capdec-samuel', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1103, 74, 'piccolino', 'http://piccolino',
    'piccolino', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 156, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1104, 74, 'vincent bons plans ', 'http://vincent bons plans ',
    'vincent bons plans ', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 156, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1105, 74, 'limit edition offer', 'http://limit edition offer',
    'limit edition offer', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 156, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1106, 74, 'coin des deals', 'http://coin des deals',
    'coin des deals', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 156, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1107, 232, 'tradedoubler', 'http://tradedoubler',
    'tradedoubler', 2, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1108, 109, 'Gentside', 'http://Gentside',
    'Gentside', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    145, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1109, 57, 'kidilux', 'http://kidilux',
    'kidilux', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    149, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1110, 74, 'Le manège des offres ',
    'http://Le manège des offres ',
    'Le manège des offres ', 1, 0, 0,
    0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1111, 74, 'fête des réduc', 'http://fête des réduc',
    'fête des réduc', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 156, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1112, 233, 'FEEBBO', 'http://FEEBBO',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 19, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1113, 234, 'mesdeveloppeurs', 'http://mesdeveloppeurs',
    'mesdeveloppeurs', 2, 0, 0, 0, 0, 0,
    'FRA', 1, 232, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1114, 235, 'Mes idées shopping',
    'http://', 'Mes idées shopping',
    2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1115, 235, 'Femme Shopping', 'http://Femme Shopping',
    'Femme Shopping', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 20, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1116, 235, 'Privilège Chic', 'http://Privilège Chic',
    'Privilège Chic', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1117, 235, 'Top of promo', 'http://Top of promo',
    'Top of promo', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1118, 235, 'Tresor du Web ', 'http://Tresor du Web ',
    'Tresor du Web ', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1119, 235, 'Beauté du net', 'http://Beauté du net',
    'Beauté du net', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1120, 235, 'QEVA', 'http://QEVA', 'QEVA',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 20, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1121, 235, 'MAILAGORA', 'http://MAILAGORA',
    'MAILAGORA', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1122, 235, 'Exclusivité Shopping',
    'http://Exclusivité Shopping',
    'Exclusivité Shopping', 1, 0, 0,
    0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1123, 235, 'Mes idées shopping',
    'http://Mes idées shopping', 'Mes idées shopping',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1124, 235, 'Astuce Online', 'http://Astuce Online',
    'Astuce Online', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1125, 235, 'Les plans du net', 'http://Les plans du net',
    'Les plans du net', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1126, 235, 'Mon Shop privé ', 'http://Mon Shop privé ',
    'Mon Shop privé ', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1127, 235, 'DES PROMOS POUR LUI ',
    'http://DES PROMOS POUR LUI ',
    'DES PROMOS POUR LUI ', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1128, 235, 'Votre avantage', 'http://Votre avantage',
    'Votre avantage', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1129, 235, 'MESREDUCTIONSDUJOUR',
    'http://MESREDUCTIONSDUJOUR', 'MESREDUCTIONSDUJOUR',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1130, 236, 'capdec-maildrop', 'http://6 impasse des jonquilles',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1131, 235, 'avenue des offres', 'http://avenue des offres',
    'avenue des offres', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1132, 190, 'Madestination', 'http://Madestination',
    'Madestination', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1133, 163, 'autoplus', 'http://autoplus',
    'autoplus', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    163, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1134, 237, 'club des eshoppers', 'http://club des eshoppers',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1135, 204, 'Wellness &amp; Santé',
    'http://Wellness & Santé', 'Wellness &amp; Santé',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1136, 118, 'Instant senior ', 'http://Instant senior ',
    'Instant senior ', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1137, 118, 'Dailymel ', 'http://Dailymel ',
    'Dailymel ', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1138, 118, 'Votre hard discount',
    'http://Votre hard discount', 'Votre hard discount',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1139, 0, 'Terre de femme', 'http://Terre de femme',
    'Terre de femme', 1, 0, 0, 0, 0, 0, 'FRA',
    2, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1140, 105, 'Terredefemme', 'http://Terredefemme',
    'Terredefemme', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1141, 157, 'mechantillon', 'http://mechantillon ',
    'mechantillon ', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1142, 110, 'Plantora', 'http://Plantora',
    'Plantora', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1143, 170, 'mabellevoiture', 'http://mabellevoiture',
    'mabellevoiture', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1144, 145, 'Ozamailing', 'http://Ozamailing',
    'Ozamailing', 1, 0, 0, 0, 0, 0, 'FRA',
    2, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1145, 104, 'Ozamailing wm', 'http://Ozamailing wm',
    'Ozamailing wm', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1146, 238, 'atylia', 'http://atylia',
    'atylia', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1147, 239, 'UBIK ADVERTISING', 'http://UBIK ADVERTISING',
    'UBIK ADVERTISING
', 2, 0, 0, 0, 0,
    0, 'FRA', 1, 228, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1148, 109, 'Ohmymag', 'http://Ohmymag',
    'Ohmymag', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1149, 95, 'univers gratuit', 'http://univers gratuit',
    'univers gratuit', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 194, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1150, 107, 'enqueteenligne', 'http://enqueteenligne',
    'enqueteenligne', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1151, 240, 'Offres Directes', 'http://Offres Directes',
    'Offres Directes', 2, 0, 0, 0, 0, 0,
    'FRA', 1, 241, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1152, 240, 'Consumidor Consiente',
    'http://Consumidor Consiente',
    'Consumidor Consiente', 1, 0, 0, 0,
    0, 0, 'FRA', 2, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1153, 241, 'Panel conso cap', 'http://Panel conso cap',
    'Panel conso cap', 2, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1154, 242, 'LeadsMakers', 'http://leadsmakers.com',
    'Lead generation', 60, 2000000, 270000,
    75000, 450000, 0, 'ENG', 2, 'NULL',
    0
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1155, 104, 'topnewsletter', 'http://topnewsletter',
    'topnewsletter', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1156, 104, 'shopping sur le web',
    'http://shopping sur le web', 'shopping sur le web',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1157, 104, 'newsletter bon plan',
    'http://newsletter bon plan', 'newsletter bon plan',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1158, 104, 'journal du jour', 'http://journal du jour',
    'journal du jour', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1159, 243, 'Plusieurs source de site',
    'http://plusieurssourcedesite',
    'Plusieurs source de site personnel, ciblage possible B2B, impossible sur B2C.',
    60, 400000, 0, 0, 0, 0, 'FRA', 2, 'NULL',
    0
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1160, 110, 'FWU', 'http://FWU', 'FWU',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 122, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1161, 235, 'Smart Avenue', 'http://Smart Avenue',
    'Smart Avenue', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1162, 235, 'Bonne Promotions', 'http://Bonne Promotions',
    'Bonne Promotions', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1163, 235, 'Le Paradis du shopping',
    'http://Le Paradis du shopping',
    'Le Paradis du shopping', 1, 0, 0,
    0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1164, 235, 'Mes Meilleurs Deal ',
    'http://Mes Meilleurs Deal ', 'Mes Meilleurs Deal ',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 20, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1165, 231, 'savoir comme ça', 'http://savoir comme ça',
    'savoir comme ça', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1166, 153, 'manegedesaffaires', 'http://manegedesaffaires',
    'manegedesaffaires', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 175, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1167, 104, 'actualité du jour', 'http://actualité du jour',
    'actualité du jour', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1168, 105, 'Exclusive ADN ', 'http://Exclusive ADN ',
    'Exclusive ADN ', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1169, 105, 'My social Beauty ', 'http://My social Beauty ',
    'My social Beauty ', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1170, 118, 'Webdeals', 'http://Webdeals ',
    'Webdeals ', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 26, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1171, 118, 'VpourVacances', 'http://VpourVacances',
    'VpourVacances', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 26, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1172, 118, 'Nos tendances mode', 'http://Nos tendances mode',
    'Nos tendances mode', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 26, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1173, 244, 'focus news', 'http://focus news',
    'focus news', 2, 0, 0, 0, 0, 0, 'FRA',
    1, 140, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1174, 245, 'wtm', 'http://wtm', '',
    2, 0, 0, 0, 0, 0, 'FRA', 1, 134, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1175, 246, 'Super-Gagnant.fr', 'http://samsung-s6.super-gagnant.fr/',
    'Jeu-concours Samsung S6 Edge

Remplissez votre participation gratuite et saisissez votre chance de gagner un Samsung S6. ',
    133, 350000, 0, 500000, 700000, 0, 'FRA',
    1, 'NULL', 0
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1176, 104, 'les idees de samy', 'http://les idees de samy',
    'les idees de samy', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1177, 104, 'les echos du web', 'http://les echos du web',
    'les echos du web', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1178, 104, 'jeconomise WM', 'http://jeconomise WM',
    'jeconomise WM', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1179, 74, 'super frenchie', 'http://super frenchie',
    'super frenchie', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 156, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1180, 247, 'Super-Gagnant', 'http://Super-Gagnant',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 153, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1181, 74, 'farfouille des offres ',
    'http://farfouille des offres ',
    'farfouille des offres ', 1, 0, 0,
    0, 0, 0, 'FRA', 1, 156, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1182, 248, 'vertigo', 'http://vertigo',
    'vertigo', 2, 0, 0, 0, 0, 0, 'FRA', 0,
    'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1183, 92, 'Consomarket ', 'http://Consomarket ',
    'Consomarket ', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 129, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1184, 92, 'Kalifoo ', 'http://Kalifoo ',
    'Kalifoo ', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    129, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1185, 92, 'Jamaisvu RW', 'http://Jamaisvu RW',
    'Jamaisvu RW', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1186, 157, 'affaires du moment', 'http://affaires du moment',
    'affaires du moment', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1187, 143, 'Greedy', 'http://Greedy',
    'Greedy', 1, 0, 0, 0, 0, 0, 'FRA', 1, 173,
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1188, 212, 'Sélection de Gédéon',
    'http://Sélection de Gédéon',
    'Sélection de Gédéon', 1, 0, 0,
    0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1189, 61, 'sponsorboost CAP', 'http://sponsorboost CAP',
    'sponsorboost CAP', 1, 0, 0, 0, 0, 0,
    'FRA', 0, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1190, 74, 'la petite braderie', 'http://la petite braderie',
    'la petite braderie', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1191, 84, 'Jeune et promo ', 'http://Jeune et promo ',
    'Jeune et promo ', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1192, 84, 'Les folies du jour', 'http://Les folies du jour',
    'Les folies du jour', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1193, 143, 'deciweb', 'http://deciweb',
    'deciweb', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    173, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1194, 104, 'dans lair du temps', 'http://dans lair du temps',
    'dans lair du temps', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1195, 109, 'réserver un essai ',
    'http://réserver un essai ', 'réserver un essai ',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 145, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1196, 109, 'special mode', 'http://special mode',
    'special mode', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 145, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1197, 84, 'club des prestiges ', 'http://club des prestiges ',
    'club des prestiges ', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 148, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1198, 123, 'coin beauté ', 'http://coin beauté ',
    'coin beauté ', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 118, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1199, 143, 'MediaMail', 'http://MediaMail',
    'MediaMail', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 173, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1200, 143, 'Crystalead', 'http://Crystalead',
    'Crystalead', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 173, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1201, 143, 'Malinisphere', 'http://Malinisphere',
    'Malinisphere', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 173, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1202, 104, 'premium wm', 'http://premium wm',
    'premium wm', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1203, 244, 'optin adresse edentify',
    'http://optin adresse edentify',
    'optin adresse edentify', 1, 0, 0,
    0, 0, 0, 'FRA', 1, 140, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1204, 244, 'Atylia bis', 'http://Atylia bis',
    'Atylia bis', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 140, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1205, 244, 'club des eshoppeurs bis',
    'http://club des eshoppeurs bis',
    'club des eshoppeurs bis', 1, 0,
    0, 0, 0, 0, 'FRA', 1, 140, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1206, 244, 'savoir comme ça bis',
    'http://savoir comme ça bis',
    'savoir comme ça bis', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 140, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1207, 244, 'panel conso bis', 'http://panel conso bis',
    'panel conso bis', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 140, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1208, 244, 'une-adresse-unique', 'http://une-adresse-unique',
    'une-adresse-unique', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 140, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1209, 244, 'ajdata bis', 'http://ajdata bis',
    'ajdata bis', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 140, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1210, 244, 'vertigo bis', 'http://vertigo bis',
    'vertigo bis', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 140, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1211, 244, 'sponsorboost bis', 'http://sponsorboost bis',
    'sponsorboost bis', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 140, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1212, 74, 'foire des offres', 'http://foire des offres',
    'foire des offres', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 156, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1213, 244, 'mailback ed', 'http://mailback ed',
    'mailback ed', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 140, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1214, 249, 'universdata+', 'http://universdata+',
    'universdata+', 2, 0, 0, 0, 0, 0, 'FRA',
    1, 119, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1215, 250, 'actu et offres', 'http://actu et offres',
    'actu et offres', 2, 0, 0, 0, 0, 0, 'FRA',
    1, 136, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1216, 244, 'asl', 'http://asl', 'asl',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 140, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1217, 244, 'selectaux', 'http://selectaux',
    'selectaux', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 140, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1218, 244, 'instamail edentify', 'http://instamail edentify',
    'instamail edentify', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 140, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1219, 251, 'Seki Offer', 'http://Seki Offer',
    'Seki Offer', 2, 0, 0, 0, 0, 0, 'FRA',
    1, 188, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1220, 104, 'vip concours', 'http://vip concours',
    'vip concours', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1221, 206, 'la canopée 3', 'http://la canopée 3',
    'la canopée 3', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 24, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1222, 206, 'la canopée 6', 'http://la canopée 6',
    'la canopée 6', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 24, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1223, 206, 'la canopée 7', 'http://la canopée 7',
    'la canopée 7', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 24, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1224, 206, 'la canopée 8', 'http://la canopée 8',
    'la canopée 8', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 24, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1225, 165, 'conso-enquete', 'http://conso-enquete',
    'conso-enquete', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 25, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1226, 163, 'figaro cuisine ', 'http://figaro cuisine ',
    'figaro cuisine ', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 163, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1227, 104, 'optineo', 'http://optineo',
    'optineo', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1228, 180, 'Mailorama', 'http://Mailorama',
    'Mailorama', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1229, 252, 'Webmilk', 'http://Webmilk',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 170, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1230, 253, 'HAPPY PROMOS', 'http://HAPPY PROMOS',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 124, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1231, 144, 'top bonus', 'http://top bonus',
    'top bonus', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 125, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1232, 145, 'offre vip wm', 'http://offre vip wm',
    'offre vip wm', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1233, 104, 'offre vip wm2', 'http://offre vip wm2',
    'offre vip wm2', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1234, 254, 'So Busy Girls', 'http://sobusygirls.fr',
    'Magazine en ligne féminin généraliste proposant des articles sur des thématiques variées : mode, beauté, shopping, famille, culture, voyage, food, lifestyle...',
    172, 30000, 0, 300000, 3000000, 0, 'FRA',
    2, 'NULL', 0
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1235, 123, 'lead au tresor', 'http://lead au tresor',
    'lead au tresor', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 118, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1236, 255, 'Magik Web Studio', 'http://Magik Web Studio',
    'Magik Web Studio', 2, 0, 0, 0, 0, 0,
    'FRA', 1, 192, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1237, 244, 'sociabiliweb ed', 'http://sociabiliweb ed',
    'sociabiliweb ed', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 140, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1238, 0, 'PsychoBienEtre', 'http://www.psycho-bien-etre.be',
    'Portail santé bien etre', 172,
    20000, 0, 30000, 45000, 0, 'FRA', 2,
    'NULL', 0
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1239, 104, 'kalifoo1', 'http://kalifoo1',
    'kalifoo1', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1240, 104, 'shopping institut 1',
    'http://shopping institut 1', 'shopping institut 1',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1241, 244, 'octokom', 'http://octokom',
    'octokom', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    140, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1242, 74, 'boite des surprises', 'http://boite des surprises',
    'boite des surprises', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 156, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1243, 251, 'Gddld', 'http://Gddld',
    'Gddld', 1, 0, 0, 0, 0, 0, 'FRA', 1, 188,
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1244, 251, 'Lspdjr', 'http://Lspdjr',
    'Lspdjr', 1, 0, 0, 0, 0, 0, 'FRA', 2, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1245, 251, 'Sppgon', 'http://Sppgon',
    'Sppgon', 1, 0, 0, 0, 0, 0, 'FRA', 2, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1246, 251, 'Olnrdc', 'http://Olnrdc',
    'Olnrdc', 1, 0, 0, 0, 0, 0, 'FRA', 2, 'NULL',
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1247, 212, 'sky express', 'http://sky express',
    'sky express', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 155, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1248, 256, 'apsaramedia', 'http://apsaramedia',
    'apsaramedia', 2, 0, 0, 0, 0, 0, 'FRA',
    1, 193, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1249, 163, 'cosmopolitain', 'http://cosmopolitain',
    'cosmopolitain', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 163, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1250, 163, 'closer', 'http://closer',
    'closer', 1, 0, 0, 0, 0, 0, 'FRA', 1, 163,
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1251, 104, 'actu wm', 'http://actu wm',
    'actu wm', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1252, 143, 'DailyData', 'http://DailyData',
    'DailyData', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 173, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1253, 143, 'Elixir', 'http://Elixir',
    'Elixir', 1, 0, 0, 0, 0, 0, 'FRA', 1, 173,
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1254, 104, 'marie claire', 'http://marie claire',
    'marie claire', 1, 0, 0, 0, 0, 0, 'FRA',
    0, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1255, 163, 'marieclaire', 'http://marieclaire',
    'marieclaire', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 163, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1256, 92, 'Maminuteessentielle ',
    'http://Maminuteessentielle ',
    'Maminuteessentielle ', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 129, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1257, 257, 'chocomail', 'http://chocomail',
    'chocomail', 2, 0, 0, 0, 0, 0, 'FRA',
    1, 197, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1258, 257, 'place réservée ', 'http://place réservée ',
    ' place réservée ', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 197, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1259, 257, 'Mediaplus', 'http://Mediaplus',
    'Mediaplus', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 197, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1260, 123, 'enquête minute', 'http://enquête minute',
    'enquête minute', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1261, 251, 'Skiofr', 'http://Skiofr',
    'Skiofr', 1, 0, 0, 0, 0, 0, 'FRA', 1, 188,
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1262, 123, 'votre actu people', 'http://votre actu people',
    'votre actu people', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 191, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1263, 98, 'reponse immo', 'http://reponse immo',
    'reponse immo', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 143, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1264, 258, 'TDL', 'http://TDL', 'TDL',
    2, 0, 0, 0, 0, 0, 'FRA', 1, 128, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1265, 252, 'Hallo Info BE', 'http://Hallo Info BE',
    'Hallo Info BE', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 170, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1266, 256, 'apsara2', 'http://apsara2',
    'apsara2', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    193, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1267, 259, 'eco2energie ', 'http://eco2energie ',
    'eco2energie ', 2, 0, 0, 0, 0, 0, 'FRA',
    1, 189, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1268, 259, 'data 1 ', 'http://data 1 ',
    'data 1 ', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    189, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1269, 260, 'rue marchande', 'http://rue marchande',
    'rue marchande', 2, 0, 0, 0, 0, 0, 'FRA',
    1, 158, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1270, 260, 'rue des actus', 'http://rue des actus',
    'rue des actus', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 158, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1271, 260, 'supertoinette', 'http://supertoinette',
    'supertoinette', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 158, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1272, 260, 'peoplefolie', 'http://peoplefolie',
    'peoplefolie', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 158, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1273, 260, 'illicoecolo', 'http://illicoecolo',
    'illicoecolo', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 158, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1274, 260, 'panel opinion', 'http://panel opinion',
    'panel opinion', 1, 0, 0, 0, 0, 0, 'FRA',
    2, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1275, 260, 'rue du jour', 'http://rue du jour',
    'rue du jour', 1, 0, 0, 0, 0, 0, 'FRA',
    2, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1276, 260, 'rue des promos', 'http://rue des promos',
    'rue des promos', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 158, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1277, 260, 'rue des offres tagada',
    'http://rue des offres tagada',
    'rue des offres tagada', 1, 0, 0,
    0, 0, 0, 'FRA', 2, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1278, 66, 'My Sweat Deal ', 'http://My Sweat Deal ',
    'My Sweat Deal ', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 196, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1279, 261, 'TTFID', 'http://TTFID',
    'TTFID', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1280, 244, 'wtm edentify', 'http://wtm edentify',
    'wtm edentify', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 140, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1281, 74, 'la petite rose', 'http://la petite rose',
    'la petite rose', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 156, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1282, 259, 'compleo', 'http://compleo',
    'compleo', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    189, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1283, 262, 'venise activation', 'http://venise activation',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1284, 261, 'BELGIQUE FR', 'http://BELGIQUE FR',
    'BELGIQUE FR', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1285, 244, '3 étoiles', 'http://3 étoiles',
    '3 étoiles', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 140, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1286, 261, 'je-teste.be', 'http://www.je-teste.be',
    'Site d’échantillons qui propose des produits aux personnes qui en ont envie de tester.',
    10, 50000, 0, 100000, 200000, 0, 'FRA',
    2, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1287, 98, 'selectaux AL', 'http://selectaux AL',
    'selectaux AL', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 143, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1288, 180, 'foliedesmars ', 'http://foliedesmars ',
    'foliedesmars ', 1, 0, 0, 0, 0, 0, 'FRA',
    0, 174, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1289, 259, 'VJ', 'http://VJ', 'VJ',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 189, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1290, 263, '22bismedia', 'http://22bismedia',
    '22bismedia', 2, 0, 0, 0, 0, 0, 'FRA',
    1, 186, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1291, 263, 'Ma News Beauté', 'http://Ma News Beauté',
    'Ma News Beauté', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 210, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1292, 263, 'Consoweb', 'http://Consoweb',
    'Consoweb', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    210, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1293, 263, 'News &amp; Tendance',
    'http://News & Tendance', 'News &amp; Tendance',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 210, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1294, 263, 'Institut Consoweb', 'http://Institut Consoweb',
    'Institut Consoweb', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 210, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1295, 263, 'Yespleez', 'http://Yespleez',
    'Yespleez', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    210, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1296, 263, 'Shopping District', 'http://Shopping District',
    'Shopping District.', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 210, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1297, 264, 'NEWSLETTER PLANET', 'http://6 impasse des jonquilles',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 209, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1298, 264, 'NEWSLETTER PLANET', 'http://NEWSLETTER PLANET',
    'NEWSLETTER PLANET', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 209, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1299, 212, 'Clic et astuce', 'http://Clic et astuce',
    'Clic et astuce', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 155, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1300, 143, 'Wellolead', 'http://Wellolead',
    'Wellolead', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 173, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1301, 266, 'vip response', 'http://vip response',
    'vip response', 2, 0, 0, 0, 0, 0, 'FRA',
    1, 248, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1302, 84, 'Je paie moins cher', 'http://Je paie moins cher',
    'Je paie moins cher', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 148, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1303, 267, 'seeinbox', 'http://seeinbox',
    'seeinbox', 2, 0, 0, 0, 0, 0, 'FRA', 1,
    205, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1304, 143, 'D2D', 'http://D2D', 'D2D',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 173, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1305, 143, 'Chloé', 'http://Chloé',
    'Chloé', 1, 0, 0, 0, 0, 0, 'FRA', 1, 173,
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1306, 268, 'HKS Media', 'http://HKS Media',
    'HKS Media', 2, 0, 0, 0, 0, 0, 'FRA',
    1, 199, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1307, 269, 'jadeshopping', 'http://jadeshopping',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 203, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1308, 269, 'lignemobile', 'http://lignemobile',
    'lignemobile', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 203, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1309, 269, 'quelforfait', 'http://quelforfait',
    'quelforfait', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 203, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1310, 269, 'soglam', 'http://soglam',
    'soglam', 1, 0, 0, 0, 0, 0, 'FRA', 1, 203,
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1311, 123, 'enquête de deal', 'http://enquête de deal',
    'enquête de deal', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 191, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1312, 270, 'Bon Plan DEVOLA', 'http://Bon Plan DEVOLA',
    'Bon Plan DEVOLA', 2, 0, 0, 0, 0, 0,
    'FRA', 1, 198, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1313, 212, 'Avis De Bons Plans', 'http://Avis De Bons Plans',
    'Avis De Bons Plans', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 155, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1314, 143, 'biobulle', 'http://biobulle',
    'biobulle', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    173, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1315, 92, 'Lecoindesmarques NL', 'http://Lecoindesmarques NL',
    'Lecoindesmarques NL', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 129, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1316, 92, 'Lecoindesmarques sites',
    'http://Lecoindesmarques sites',
    'Lecoindesmarques sites', 1, 0, 0,
    0, 0, 0, 'FRA', 1, 129, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1317, 271, 'Laféedunet', 'http://Laféedunet',
    'Laféedunet', 2, 0, 0, 0, 0, 0, 'FRA',
    1, 201, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1318, 271, 'clubweb', 'http://clubweb',
    'clubweb', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    201, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1319, 271, 'allopromo', 'http://allopromo',
    'allopromo', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 201, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1320, 271, 'monexpert', 'http://monexpert',
    'monexpert', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 201, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1321, 272, 'marketingfans', 'http://marketingfans',
    'marketingfans', 2, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1322, 273, 'VJ', 'http://VJ', '', 2,
    0, 0, 0, 0, 0, 'FRA', 1, 212, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1323, 273, 'OMS', 'http://OMS', 'OMS',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 212, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1324, 273, 'E2E', 'http://E2E', 'E2E',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 212, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1325, 273, 'AVIP VJ', 'http://AVIP VJ',
    'AVIP VJ', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    212, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1326, 274, 'jeunesseglobal', 'http://jeunesseglobal',
    'jeunesseglobal', 2, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1327, 143, 'Missiweb', 'http://Missiweb',
    'Missiweb', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    173, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1328, 249, 'CARS AUTO ', 'http://CARS AUTO ',
    'CARS AUTO ', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 208, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1329, 143, 'OZALEAD', 'http://OZALEAD',
    'OZALEAD', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    173, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1330, 275, 'online reduc RM', 'http://online reduc RM',
    'online reduc RM', 2, 0, 0, 0, 0, 0,
    'FRA', 1, 206, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1331, 275, 'shopping online RM', 'http://shopping online RM',
    'shopping online RM', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 206, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1332, 275, 'bingo facile RM', 'http://bingo facile RM',
    'bingo facile RM', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 206, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1333, 123, 'kdodujour', 'http://kdodujour',
    'kdodujour', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 191, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1334, 123, 'ca me réussi ', 'http://ca me réussi ',
    'ca me réussi ', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 191, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1335, 249, 'universmode', 'http://universmode',
    'universmode', 1, 0, 0, 0, 0, 0, 'FRA',
    2, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1336, 98, 'selectaux 2', 'http://selectaux 2',
    'selectaux 2', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 143, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1337, 66, 'Girafe de lActu', 'http://Girafe de lActu',
    'Girafe de lActu', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 196, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1338, 66, 'Tendance dAujourdhui',
    'http://Tendance dAujourdhui',
    'Tendance dAujourdhui', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 196, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1339, 190, 'espace maisons', 'http://espace maisons',
    'espace maisons', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 27, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1340, 244, 'lewebchezvous', 'http://lewebchezvous',
    'lewebchezvous', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 140, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1341, 276, 'data-agency', 'http://data-agency',
    'data-agency', 2, 0, 0, 0, 0, 0, 'FRA',
    1, 235, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1342, 277, 'rodeoptin', 'http://rodeoptin',
    'rodeoptin', 2, 0, 0, 0, 0, 0, 'FRA',
    1, 198, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1343, 271, 'Maconfidence', 'http://Maconfidence',
    'Maconfidence', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 201, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1344, 271, 'dealprime', 'http://dealprime',
    'dealprime', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 201, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1345, 123, 'optimisons chaque jour ',
    'http://optimisons chaque jour ',
    'optimisons chaque jour ', 1, 0,
    0, 0, 0, 0, 'FRA', 1, 191, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1346, 212, 'La Maison Indigo', 'http://La Maison Indigo',
    'La Maison Indigo', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 155, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1347, 278, 'SAS INSTA MAIL MEDIA',
    'http://SAS INSTA MAIL MEDIA',
    'SAS INSTA MAIL MEDIA', 2, 0, 0, 0,
    0, 0, 'FRA', 1, 131, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1348, 104, 'display webmedia', 'http://display webmedia',
    'display webmedia', 1, 0, 0, 0, 0, 0,
    'FRA', 0, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1349, 279, 'DISPLAY WEBMEDIA RM',
    'http://DISPLAY WEBMEDIA RM', 'DISPLAY WEBMEDIA RM',
    2, 0, 0, 0, 0, 0, 'FRA', 1, 200, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1350, 278, 'Bienchezmoi', 'http://Bienchezmoi',
    'Bienchezmoi', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 131, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1351, 278, 'ecoplus', 'http://ecoplus',
    'ecoplus', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    131, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1352, 278, 'ideediscount', 'http://ideediscount',
    'ideediscount', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 131, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1353, 278, 'infoprice', 'http://infoprice',
    'infoprice', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 131, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1354, 278, 'marquegold', 'http://marquegold',
    'marquegold', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 131, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1355, 278, 'minuteshop', 'http://minuteshop',
    'minuteshop', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 131, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1356, 278, 'myprice', 'http://myprice',
    'myprice', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    131, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1357, 278, 'oeuvrestandart', 'http://oeuvrestandart',
    'oeuvrestandart', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 131, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1358, 278, 'votrenouvelle', 'http://votrenouvelle',
    'votrenouvelle', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 131, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1359, 278, 'votreshopping', 'http://votreshopping',
    'votreshopping', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 131, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1360, 278, 'voyagesoleil', 'http://voyagesoleil',
    'voyagesoleil', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 131, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1361, 278, 'unepromodefolie', 'http://unepromodefolie',
    'unepromodefolie', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 131, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1362, 212, 'Idée maline', 'http://Idée maline',
    'Idée maline', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 155, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1363, 190, 'Le top model', 'http://Le top model',
    'Le top model', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 27, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1364, 212, 'Les privilèges ', 'http://Les privilèges ',
    'Les privilèges ', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 155, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1365, 280, 'imprimerpascher', 'http://',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL',
    2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1366, 280, 'imprimerpascher', 'http://imprimerpascher',
    'imprimerpascher', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 161, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1367, 281, 'holidays times', 'http://holidays times',
    'holidays times', 2, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1368, 282, 'shopbuddies', 'http://shopbuddies',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 233, 0
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1369, 282, 'cashbackreduction', 'http://cashbackreduction',
    'cashbackreduction', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 233, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1370, 109, 'Daily Geek Show', 'http://Daily Geek Show',
    'Daily Geek Show', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 145, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1371, 143, 'Fantazia', 'http://Fantazia',
    'Fantazia', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    173, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1372, 143, 'inbox azorica', 'http://inbox azorica',
    'inbox azorica', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 173, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1373, 109, 'NL VSD', 'http://NL VSD',
    'NL VSD', 1, 0, 0, 0, 0, 0, 'FRA', 1, 145,
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1374, 283, 'Avenue des Bons Plans',
    'http://Avenue des Bons Plans',
    'Avenue des Bons Plans', 2, 0, 0,
    0, 0, 0, 'FRA', 1, 231, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1375, 163, 'Minute News ', 'http://Minute News ',
    'Minute News ', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 163, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1376, 211, 'test unitead', 'http://test unitead',
    'test unitead', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 172, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1377, 284, 'La selection de charlie',
    'http://La selection de charlie',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 224, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1378, 284, 'focus news arianelab',
    'http://focus news arianelab',
    'focus news arianelab', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 224, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1379, 284, 'MSE arianelab', 'http://MSE arianelab',
    'MSE arianelab', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 224, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1380, 284, 'Le clubs des e-shoppers AL',
    'http://Le clubs des e-shoppers AL',
    'Le clubs des e-shoppers AL', 1,
    0, 0, 0, 0, 0, 'FRA', 1, 224, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1381, 284, 'Panel conso AL', 'http://Panel conso AL',
    'Panel conso AL', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 224, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1382, 285, 'reducs-en-exclu.com',
    'http://reducs-en-exclu.com', 'reducs-en-exclu.com',
    2, 0, 0, 0, 0, 0, 'FRA', 1, 252, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1383, 285, 'exclusivite-et-tendance.com',
    'http://exclusivite-et-tendance.com',
    'exclusivite-et-tendance.com',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 252, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1384, 285, 'astuce-et-bon-plan.com',
    'http://astuce-et-bon-plan.com',
    'astuce-et-bon-plan.com', 1, 0, 0,
    0, 0, 0, 'FRA', 1, 252, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1385, 285, 'promo-du-jour.com', 'http://promo-du-jour.com',
    'promo-du-jour.com', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 252, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1386, 286, 'ogmamedia', 'http://ogmamedia',
    'ogmamedia', 2, 0, 0, 0, 0, 0, 'FRA',
    1, 270, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1387, 123, 'Consodeal', 'http://Consodeal',
    'Consodeal', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 191, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1388, 123, 'mon guide conso', 'http://mon guide conso',
    'mon guide conso', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 191, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1389, 287, 'INFINITODIGITALE', 'http://infinitodigitale.com',
    'Affiliate network', 103, 60000,
    0, 0, 0, 0, 'ITA', 2, 'NULL', 0
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1390, 123, 'myetselonvous', 'http://myetselonvous',
    'myetselonvous', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 251, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1391, 288, 'Healthy Freebies', 'http://www.healthyfreebies.co.uk/public/',
    'A site aimed at providing freebies in the health sector to our consumers',
    0, 195, 195, 15, 52, 0, 'ENG', 2, 'NULL',
    0
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1392, 27, 'Deals Magiques', 'http://Deals Magiques',
    'Deals Magiques', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 23, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1393, 289, 'cloud media', 'http://cloud media',
    '', 2, 0, 0, 0, 0, 0, 'FRA', 1, 230, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1394, 290, 'Twinkle', 'http://', '',
    2, 0, 0, 0, 0, 0, 'FRA', 1, 'NULL', 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1395, 290, 'Twinkle', 'http://Twinkle',
    'Twinkle', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    239, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1396, 92, 'concours riviera', 'http://concours riviera',
    'concours riviera', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 129, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1397, 78, 'sdh', 'http://sdh', 'sdh',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 126, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1398, 163, 'Le 13h actu', 'http://Le 13h actu',
    'Le 13h actu
', 1, 0, 0, 0, 0, 0, 'FRA',
    2, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1399, 163, 'info Sport', 'http://info Sport',
    'info Sport', 1, 0, 0, 0, 0, 0, 'FRA',
    2, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1400, 163, 'actu Auto', 'http://actu Auto',
    'actu Auto', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 163, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1401, 163, 'info économique', 'http://info économique',
    'info économique', 1, 0, 0, 0, 0, 0,
    'FRA', 2, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1402, 163, 'La revue féminine', 'http://La revue féminine',
    'La revue féminine
', 1, 0, 0, 0,
    0, 0, 'FRA', 1, 163, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1403, 163, 'A votre Table', 'http://A votre Table',
    'A votre Table
', 1, 0, 0, 0, 0, 0, 'FRA',
    2, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1404, 163, 'Le point Eco ', 'http://Le point Eco ',
    'Le point Eco
', 1, 0, 0, 0, 0, 0, 'FRA',
    2, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1405, 163, 'Cuisine et Tendance',
    'http://Cuisine et Tendance', 'Cuisine et Tendance
',
    1, 0, 0, 0, 0, 0, 'FRA', 2, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1406, 163, 'Votre RDV Santé', 'http://Votre RDV Santé',
    'Votre RDV Santé
', 1, 0, 0, 0, 0,
    0, 'FRA', 2, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1407, 163, 'Ce soir à la TV', 'http://Ce soir à la TV',
    'Ce soir à la TV
', 1, 0, 0, 0, 0,
    0, 'FRA', 2, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1408, 163, 'Les Astuces de Grands-Mères',
    'http://Les Astuces de Grands-Mères',
    'Les Astuces de Grands-Mères
',
    1, 0, 0, 0, 0, 0, 'FRA', 2, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1409, 163, 'Inspiration féminine',
    'http://Inspiration féminine',
    'Inspiration féminine
', 1, 0, 0,
    0, 0, 0, 'FRA', 1, 163, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1410, 163, 'actu femme avec la Parisienne',
    'http://actu femme avec la Parisienne',
    'actu femme avec la Parisienne',
    1, 0, 0, 0, 0, 0, 'FRA', 2, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1411, 163, 'La Météo avec le Parisien',
    'http://La Météo avec le Parisien',
    'La Météo avec le Parisien',
    1, 0, 0, 0, 0, 0, 'FRA', 2, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1412, 163, 'Le Point info avec les Echos ',
    'http://Le Point info avec les Echos ',
    'Le Point info avec les Echos
',
    1, 0, 0, 0, 0, 0, 'FRA', 2, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1413, 163, 'Actualité au féminin',
    'http://Actualité au féminin',
    'Actualité au féminin', 1, 0, 0,
    0, 0, 0, 'FRA', 2, 163, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1414, 163, 'La Mag féminin avec Marie Claire',
    'http://La Mag féminin avec Marie Claire',
    'La Mag féminin avec Marie Claire
',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 163, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1415, 163, 'Le Point Météo ', 'http://Le Point Météo ',
    'Le Point Météo
', 1, 0, 0, 0, 0,
    0, 'FRA', 2, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1416, 163, 'La quotidienne avec Minute News ',
    'http://La quotidienne avec Minute News ',
    'La quotidienne avec Minute News
',
    1, 0, 0, 0, 0, 0, 'FRA', 2, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1417, 163, 'Les Conseils Santé ',
    'http://Les Conseils Santé ',
    'Les Conseils Santé
', 1, 0, 0,
    0, 0, 0, 'FRA', 1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1418, 163, 'actu insolite ', 'http://actu insolite ',
    'actu insolite ', 1, 0, 0, 0, 0, 0, 'FRA',
    2, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1419, 163, 'analyse de lactu ', 'http://analyse de lactu ',
    'analyse de lactu ', 1, 0, 0, 0, 0,
    0, 'FRA', 2, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1420, 104, 'ONLINE REDUC WM', 'http://ONLINE REDUC WM',
    'ONLINE REDUC', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1421, 291, 'Meilleures Offres Enlinge',
    'http://meilleures-offres-enligne.fr/',
    'Meilleures Offres Enlinge', 60,
    1000000, 0, 0, 0, 0, 'FRA', 2, 'NULL',
    0
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1422, 170, 'Meilleurplan', 'http://Meilleurplan',
    'Meilleurplan', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 171, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1423, 143, 'webexpert', 'http://webexpert',
    'webexpert', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1424, 109, 'qapa', 'http://qapa', 'qapa',
    1, 0, 0, 0, 0, 0, 'FRA', 1, 145, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1425, 15, 'newbidder', 'http://newbidder',
    'newbidder', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 154, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1426, 212, 'une proposition', 'http://une proposition',
    'une proposition', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 155, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1427, 275, 'expert en finance', 'http://expert en finance',
    'expert en finance', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 206, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1428, 292, 'leadfactory retargarting',
    'http://6 impasse des jonquilles',
    'leadfactory retargarting', 2, 0,
    0, 0, 0, 0, 'FRA', 1, 266, 2
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1429, 292, 'leadfactory retargarting',
    'http://leadfactory retargarting',
    'leadfactory retargarting', 1, 0,
    0, 0, 0, 0, 'FRA', 1, 266, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1430, 23, 'deals and forme', 'http://deals and forme',
    'deals and forme', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 229, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1431, 143, 'trend line', 'http://trend line',
    'trend line', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 173, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1432, 143, 'Action web', 'http://Action web',
    'Action web', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 173, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1433, 143, 'Service du web', 'http://Service du web',
    'Service du web', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 173, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1435, 109, 'emprunis', 'http://emprunis',
    'emprunis', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    'NULL', 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1436, 293, 'Placedesexperts', 'http://Placedesexperts',
    'Placedesexperts', 2, 0, 0, 0, 0, 0,
    'FRA', 1, 269, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1437, 123, 'offre de camille', 'http://offre de camille',
    'offre de camille', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 191, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1438, 244, 'adomos ed', 'http://adomos ed',
    'adomos ed', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 140, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1439, 123, 'sages du web', 'http://sages du web',
    'sages du web', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 191, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1440, 244, 'gdd ed', 'http://gdd ed',
    'gdd ed', 1, 0, 0, 0, 0, 0, 'FRA', 1, 140,
    1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1441, 104, 'les infos de lucie', 'http://les-infos-de-lucie.com/',
    'les infos de lucie', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1442, 104, 'Pannel shopping', 'http://vosnews-3.es',
    'Pannel shopping', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1443, 211, 'EGS 2018', 'EGS 2018  ',
    'EGS 2018', 1, 0, 0, 0, 0, 0, 'FRA', 1,
    172, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1444, 244, 'le coin des marques ',
    'http://le coin des marques ',
    'le coin des marques ', 1, 0, 0,
    0, 0, 0, 'FRA', 1, 140, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1445, 143, 'Cest le pied', 'http://Cest le pied',
    'Cest le pied', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 173, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1446, 104, 'jamais vu', 'http://jamais vu',
    'jamais vu', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1447, 104, 'chic deal', 'http://chic deal',
    'chic deal', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1448, 104, 'offres vip ', 'http://offres vip ',
    'offres vip ', 1, 0, 0, 0, 0, 0, 'FRA',
    1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1449, 104, 'Panel conso Lena', 'http://Panel conso Lena',
    'Panel conso Lena', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1450, 244, 'Shopping privilege', 'http://Shopping privilege',
    'Shopping privilege', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 140, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1451, 285, 'astuce et shopping', 'http://astuce et shopping',
    'astuce et shopping', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 252, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1452, 104, 'shopping de lea', 'http://shopping de lea',
    'shopping de lea', 1, 0, 0, 0, 0, 0,
    'FRA', 1, 284, 1
  );
/* INSERT QUERY */
INSERT INTO bases(
  idsite, id_editeur, titre, url, description,
  categorie, mailing, mobile, vu, pap,
  label, racine, statut, domaine, commercial
)
VALUES
  (
    1453, 104, 'shopping institut', 'http://shopping institut lena',
    'shopping institut', 1, 0, 0, 0, 0,
    0, 'FRA', 1, 284, 1
  );
