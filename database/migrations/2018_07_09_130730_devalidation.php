<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Devalidation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('devalidationaupourcent', function (Blueprint $table) {

        $table->increments('id');
        $table->integer('type_rem_devalid');
        $table->integer('id_campagne');
        $table->integer('pourcent_devalidation');

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
