<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Campagne extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campagnes', function (Blueprint $table) {
        // envoyée par une base rattaché à un editeur
        $table->increments('id');
        $table->integer('type_id_campagne');
        $table->string('nom_campagne',255);
        $table->string('note_campagne',255);
        $table->float('rem_campagne',255);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
