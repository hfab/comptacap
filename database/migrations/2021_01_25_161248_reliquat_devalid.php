<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReliquatDevalid extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('reliquats', function (Blueprint $table) {
	         $table->string('devalid_CPC',25)->nullable();
	         $table->string('devalid_CPL',25)->nullable();
	         $table->string('devalid_CPA',25)->nullable();
	         $table->string('devalid_CPM',25)->nullable();
	         $table->string('devalid_com',25)->nullable();

	         $table->integer('devalid_id')->default(0);
	         $table->integer('devalid_pourcent')->default(0);
	         $table->integer('devalid_nombre')->default(0);
	         $table->integer('is_devalided')->default(0);
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
