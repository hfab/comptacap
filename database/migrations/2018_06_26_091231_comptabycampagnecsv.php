<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Comptabycampagnecsv extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('ComptaByCampagneCsv', function (Blueprint $table) {
        $table->increments('id');

        $table->integer('id_fichier');
        $table->integer('id_site_base');
        $table->integer('id_periode');
        $table->integer('id_campagne');

        $table->string('CPC',25);
        $table->string('CPL',25);
        $table->string('CPA',25);
        $table->string('CPM',25);
        $table->string('com',25);

        $table->string('devalid_CPC',25)->nullable();
        $table->string('devalid_CPL',25)->nullable();
        $table->string('devalid_CPA',25)->nullable();
        $table->string('devalid_CPM',25)->nullable();
        $table->string('devalid_com',25)->nullable();

        $table->integer('devalid_id')->default(0);
        $table->integer('devalid_pourcent')->default(0);
        $table->integer('devalid_nombre')->default(0);
        $table->integer('is_devalided')->default(0);


        // ajouter le bon nombre entre les ()
        // ajouter les pourcentages éditeurs

        $table->timestamps();

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
