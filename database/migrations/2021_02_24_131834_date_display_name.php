<?php

use App\Http\CustomHelpers\DateHelper;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DateDisplayName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('periode', function (Blueprint $table) {
        	$table->string('display_name');
        });

	    $periodes = \DB::table('periode')->get();
	    foreach ($periodes as $periode) {
		    $timestamp = strtotime($periode->debut_periode);
		    $month = date('F', $timestamp);
		    $date = DateHelper::getFrenchMonthName($month) . ' ' . date('Y', $timestamp);
		    \DB::table('periode')->where('id', $periode->id)->update(['display_name' => $date]);
	    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('periode', function (Blueprint $table) {
        	$table->dropColumn('display_name');
        });
    }
}
