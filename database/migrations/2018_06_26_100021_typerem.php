<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Typerem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('typerem', function (Blueprint $table) {
      // envoyée par une base rattaché à un editeur
      $table->increments('id');
      $table->string('type',255);

      });

      $dataset[] = ['type' => 'CPM'];
      $dataset[] = ['type' => 'CPL'];
      $dataset[] = ['type' => 'CPA'];
      $dataset[] = ['type' => 'CPC'];

      \DB::table('typerem')->insert($dataset);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
