<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Mails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('mails', function (Blueprint $table) {
        // promo en folie
        // espace reducs
        $table->increments('id');
        $table->text('contenu_mail');
        $table->integer('editeur_id_mail');
        $table->integer('id_periode');
        $table->integer('sented')->default(0);
        $table->date('sent_at')->nullable();
        $table->timestamps();

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
