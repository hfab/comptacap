<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableReliquat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('reliquats', function (Blueprint $table) {
        $table->increments('id');

        $table->integer('id_site_base');
        $table->integer('id_periode');
        $table->integer('id_campagne');

        $table->string('CPC',25)->nullable();
        $table->string('CPL',25)->nullable();
        $table->string('CPA',25)->nullable();
        $table->string('CPM',25)->nullable();
        $table->string('com',25)->nullable();

        $table->timestamps();

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
