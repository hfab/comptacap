<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemMails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('remumails', function (Blueprint $table) {
        // promo en folie
        // espace reducs
        $table->increments('id');
        $table->integer('id_mail');
        $table->integer('id_campagne');
        $table->integer('id_base');
        $table->float('ligne_remuneration');

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
