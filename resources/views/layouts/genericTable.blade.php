<table class="table">
    <thead>
        <tr>
            @foreach($headers as $header)
                <th class="row">{{ $header }}</th>
            @endforeach
        </tr>
    </thead>
    <tbody>
    @foreach($rows as $row)
        <tr>
            @foreach($row as $elem)
                <td>{{ $elem }}</td>
            @endforeach
        </tr>
    @endforeach
    </tbody>
</table>