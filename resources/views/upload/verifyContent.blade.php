@if ($missingHeader)
    <div>
        <p style="color: orangered">Colonne(s) manquant(s) :</p>
        @foreach($missingHeader as $headerName)
            <p style="color: orangered">{{ $headerName }}</p>
        @endforeach
    </div>
@endif

@if ($csvError)
    <div id="verify_error_container" class="container">
        <button id="verify_error_toggle" onclick="toggleErrorBox()">
            Erreurs
        </button>
        <div id="verify_error_content" class="container pre-scrollable">
            @foreach ($csvError as $error)
                <p>{{ $error }}</p>
            @endforeach
        </div>
    </div>
@endif

<div class="pre-scrollable">
    <table class="table">
        <thead>
        <tr>
			<?php
			$hideIdList = array();
			?>
            @foreach($headers as $key => $header)
                @if(in_array($header, $hideCol))
					<?php
					$hideIdList []= $key;
					?>
                @else
                    <th class="row">
                        @if(in_array($header, $usedCol))
                            <span style="color: #2ca02c"> {{ $header }} </span>
                        @else
                            {{ $header }}
                        @endif
                    </th>
                @endif
            @endforeach
        </tr>
        </thead>
        <tbody>
        @foreach($rows as $row)
            <tr>
                @foreach($row as $key => $elem)
                    @if(!in_array($key, $hideIdList))
                        <td>{{ $elem }}</td>
                    @endif
                @endforeach
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
