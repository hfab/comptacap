@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="">

                <a href="/upload/config_filter">
                    <p class="text-right">configurer le filtre <span class="glyphicon glyphicon-cog"></span></p>
                </a>

                <div class="form-group panel panel-default">
                    <div class="panel-heading">Envoyer un fichier</div>

                    <div class="container">
                        <input id='select_base' name="fileTypeSelector" type="radio" value=5 onclick="fileTypeSelect('base')">
                        <label for='select_base'>Ajouter des bases / editeurs</label>
                    </div>
                    <div class="container">
                        <input id='select_editor' name="fileTypeSelector" type="radio" value=4 onclick="fileTypeSelect('editor')">
                        <label for='select_editor'>Ajouter des editeurs seuls</label>
                    </div>
                    <div class="container">
                        <input id='select_campaign' name="fileTypeSelector" type="radio" value=3 onclick="fileTypeSelect('campaign')">
                        <label for='select_campaign'>Importer des campagnes</label>
                    </div>

                    <div class="container">
                        <input id='select_reliquat' name="fileTypeSelector" type="radio" value=2 onclick="fileTypeSelect('reliquat')">
                        <label for='select_reliquat'>Ajouter des reliquats à une campagne</label>
                    </div>
                    <div class="container">
                        <input id='select_update' name="fileTypeSelector" type="radio" value=1 onclick="fileTypeSelect('update')">
                        <label for='select_update'>Importer les stats editeur</label>
                    </div>


                    <div class="container">
                        <label class="custom-file">
                            <input type="file" id="file" name="file" class="custom-file-input" required>
                            <span class="custom-file-control"></span>
                        </label>
                        <input id="verify5" type="button" value="verify 5" onclick="verifyFile(5)">
                        <input id="verify-all" type="button" value="all" onclick="verifyFile(0)">
                    </div>
                </div>

                <!-- update data form -->
                <div id="update_form" class="form-group" hidden>
                    <div class="form-group">
                        <label for="id_periode">Choix de la période :</label>
                        <select class="form-control" id="id_periode" name="id_periode">
                            @foreach($periode as $p)
                                <option value="{{$p->id}}">{{$p->display_name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="id_campagne">Choix de campagne :</label>
                        <select class="form-control" id="id_campagne" name="id_campagne">
                            @foreach($campagne as $c)
                                <option value="{{$c->id}}">{{$c->nom_campagne}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div id="update_send_btn" hidden>
                        <button class="btn btn-success" onclick="send_csv(1)">Envoyer</button>
                    </div>
                </div>

                <!-- import campaign data form -->
                <div id="campaign_form" class="container" hidden>
                    <div id="campaign_send_btn" hidden>
                        <button id="campaign_send_btn" class="btn btn-success" onclick="send_csv(2)">Envoyer</button>
                    </div>
                </div>

                <!-- import reliquat form -->
                <div id="reliquat_form" class="form-group" hidden>
                    <div class="form-group">
                        <label for="id_reliquat_periode">Choix de la période :</label>
                        <select class="form-control" id="id_reliquat_periode" name="id_periode">
                            @foreach($periode as $p)
                                <option value="{{$p->id}}">{{$p->display_name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="id_reliquat_periode_ref">Période de référence :</label>
                        <select class="form-control" id="id_reliquat_periode_ref" name="id_periode">
                            @foreach($periode as $p)
                                <option value="{{$p->id}}">{{$p->display_name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="id_reliquat_campagne">Choix de campagne :</label>
                        <select class="form-control" id="id_reliquat_campagne" name="id_campagne">
                            @foreach($campagne as $c)
                                <option value="{{$c->id}}">{{$c->nom_campagne}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div id="reliquat_send_btn" hidden>
                        <button class="btn btn-success" onclick="send_csv(3)">Envoyer</button>
                    </div>
                </div>

                <!-- import editor and base data form -->
                <div id="editor_form" class="container" hidden>
                    <div id="editor_send_btn" hidden>
                        <button class="btn btn-success" onclick="send_csv(4)">Envoyer</button>
                    </div>
                </div>

                <!-- import base and base data form -->
                <div id="base_form" class="container" hidden>
                    <div id="base_send_btn" hidden>
                        <button class="btn btn-success" onclick="send_csv(5)">Envoyer</button>
                    </div>
                </div>

                <div id="verify_response_box" class="container" hidden></div>

            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        function fileTypeSelect(id) {
            document.getElementById('update_form').style.display = (id === 'update' ? 'block' : 'none')
            document.getElementById('campaign_form').style.display = (id === 'campaign' ? 'block' : 'none')
            document.getElementById('editor_form').style.display = (id === 'editor' ? 'block' : 'none')
            document.getElementById('reliquat_form').style.display = (id === 'reliquat' ? 'block' : 'none')
            document.getElementById('base_form').style.display = (id === 'base' ? 'block' : 'none')

            hideSendButtons()
            document.getElementById('verify_response_box').innerHTML = ''
        }

        function verifyFile(mode) {
            const file = document.getElementById('file').files[0]
            const fileType = document.querySelector('input[name="fileTypeSelector"]:checked').value

            if (file === undefined) {
                document.getElementById('file').style.color = 'red'
                return
            }

            let data = new FormData()
            data.append('csvFile', file)
            data.append('fileType', fileType)
            data.append('limit', mode)

            let request = new XMLHttpRequest()
            request.onreadystatechange = function ()  {
                if (this.readyState === 4) {
                    document.getElementById("verify_response_box").innerHTML = request.responseText

                    document.getElementById('update_send_btn').style.display = 'block'
                    document.getElementById('campaign_send_btn').style.display = 'block'
                    document.getElementById('editor_send_btn').style.display = 'block'
                    document.getElementById('reliquat_send_btn').style.display = 'block'
                    document.getElementById('base_send_btn').style.display = 'block'

                    if (document.getElementById('verify_error_toggle')) {
                        document.getElementById('verify_error_toggle').addEventListener('click', toggleErrorBox())
                    }
                }
            }
            document.getElementById('verify_response_box').innerHTML = "<div class=\"loader\"></div>"
            document.getElementById('verify_response_box').style.display = "block"
            request.open("POST", "/upload/verify", true);
            request.setRequestHeader("X-CSRF-TOKEN", "{{ csrf_token() }}")
            request.send(data);
        }

        function send_csv(fileType) {
            const file = document.getElementById('file').files[0]

            if (file === undefined) {
                document.getElementById('file').style.color = 'red'
                return
            }

            let data = new FormData()
            data.append('csvFile', file)
            data.append('fileType', fileType)
            if (fileType === 1) {
                const id_campagne = document.getElementById('id_campagne').value;
                data.append('id_campagne', id_campagne)
                const id_periode = document.getElementById('id_periode').value;
                data.append('id_periode', id_periode)
            }
            if (fileType === 3) {
                const id_campagne = document.getElementById('id_reliquat_campagne').value
                data.append('id_campagne', id_campagne)
                const id_periode = document.getElementById('id_reliquat_periode').value
                data.append('id_periode', id_periode)
                const id_periode_ref = document.getElementById('id_reliquat_periode_ref').value
                data.append('id_periode_ref', id_periode_ref)
            }

            let request = new XMLHttpRequest()
            request.onreadystatechange = function ()  {
                if (this.readyState === 4) {
                    hideSendButtons()
                    document.getElementById("verify_response_box").innerHTML = request.responseText;
                }
            }
            document.getElementById('verify_response_box').innerHTML = "<div class=\"loader\"></div>"
            document.getElementById('verify_response_box').style.display = "block"
            request.open("POST", "/upload/importCsv", true);
            request.setRequestHeader("X-CSRF-TOKEN", "{{ csrf_token() }}")
            request.send(data);

            hideSendButtons()
        }

        function hideSendButtons() {
            document.getElementById('update_send_btn').style.display = 'none'
            document.getElementById('campaign_send_btn').style.display = 'none'
            document.getElementById('editor_send_btn').style.display = 'none'
            document.getElementById('reliquat_send_btn').style.display = 'none'
            document.getElementById('base_send_btn').style.display = 'none'
        }

        // pre-load
        function toggleErrorBox() {
            const error_div = document.getElementById('verify_error_content');
            error_div.style.display = error_div.style.display === "none" ? "block" : "none"
        }

    </script>
@endsection

@section('styles')
    <style>
        .loader {
            border: 16px solid #f3f3f3; /* Light grey */
            border-top: 16px solid #3498db; /* Blue */
            border-radius: 50%;
            width: 120px;
            height: 120px;
            animation: spin 2s linear infinite;
        }

        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }
    </style>
@endsection