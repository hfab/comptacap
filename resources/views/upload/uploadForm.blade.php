@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Envoyer un fichier</div>

                <div class="panel-body">
                  <div class="form-group">
                  <form method="POST" action="/file/upload" accept-charset="UTF-8" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />

                    <label class="custom-file">
                      <input type="file" id="file" name="file" class="custom-file-input" required>
                      <span class="custom-file-control"></span>
                    </label>

                    <div class="form-group">
                      <label for="id_editeur">Choix de la période :</label>
                        <select class="form-control" id="id_periode" name="id_periode">
                        @foreach($periode as $p)
                          <option value="{{$p->id}}">{{$p->display_name}}</option>
                        @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                      <label for="id_editeur">Choix de campagne :</label>
                        <select class="form-control" id="id_campagne" name="id_campagne">
                        @foreach($campagne as $c)
                          <option value="{{$c->id}}">{{$c->nom_campagne}}</option>
                        @endforeach
                        </select>
                    </div>
                    <button type="submit" class="btn btn-success">Envoyer</button>

                  </form>

                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
