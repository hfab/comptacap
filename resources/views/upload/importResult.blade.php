<table class="table">
    <thead>
    <tr>
        <th class="row">imported</th>
        <th class="row">duplicates</th>
        <th class="row">unknown editor</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>{{ $result['ok'] }}</td>
        <td>{{ $result['duplicate'] }}</td>
        <td>{{ $result['unknown_editor'] }}</td>
    </tr>
    </tbody>
</table>