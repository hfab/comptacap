@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @foreach($filter_table_list as $importName => $elem)
                <div class="col-sm-3">
                    <p>{{ $importName }}</p>
                    <ul class="list-group">
                        @foreach($elem as $filteredColName)
                            <li class="list-group-item">
                                <form method="post" action="/upload/remove_filter">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                    <button type="submit" class="btn btn-xs btn-warning">
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                    </button>
                                    <label>
                                        <input type="hidden" name="import_type_name" value="{{ $importName }}">
                                        <input type="hidden" name="col_name" value="{{ $filteredColName }}">
                                    </label>
                                    {{ $filteredColName }}
                                </form>
                            </li>
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endforeach
        </div>
        <div class="row">
            <form method="post" action="/upload/add_filter">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                <label for="import_type_selector">ajouter un filtre : </label>
                <select id="import_type_selector" name="import_type">
                    <option value="1">mise à jour campagne</option>
                    <option value="2">ajout reliquats</option>
                    <option value="3">import campagnes</option>
                    <option value="4">import editeurs</option>
                    <option value="5">import base/editeur</option>
                </select>
                <label for="col_name"> > colonne à cacher</label>
                <input id="col_name" name="col_name" type="text" required>
                <input type="submit" value="ajouter">
            </form>
        </div>
    </div>

@endsection