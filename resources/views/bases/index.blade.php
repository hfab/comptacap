@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Listing des bases</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <a href="/bases/create">
                                    <button type="button" class="btn btn-success">Ajouter une base</button>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>ID site</th>
                                <th>Nom</th>
                                <th>Editeur de la base</th>
                                <th>Options</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($bases as $b)

                                <tr>
                                    <td>{{$b->idsite}}</td>
                                    <td>{{$b->titre}}</td>
                                    <td>
										<?php

										$editor = \DB::table('editeurs')
											->where('ids_editeur', $b->id_editeur)
											->first();

										if(isset($editor)){
											echo $editor->nom_entreprise;
										}

										?>

                                    </td>
                                    <td>
                                        <a href="./bases/edit/{{$b->id}}">
                                            <button type="button" class="btn btn-xs btn-info">
                                                Modifier
                                            </button>
                                        </a>
                                        <button type="button" class="btn btn-xs btn-danger"disabled>Supprimer</button>
{{--                                        <a onclick="return confirm('supprimer {{ $b->titre }}')" href="./bases/delete/{{$b->id}}">--}}
{{--                                            <button type="button" class="btn btn-xs btn-danger"disabled>Supprimer</button>--}}
{{--                                        </a>--}}
                                    </td>
                                </tr>

                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
