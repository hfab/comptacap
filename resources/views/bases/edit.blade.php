@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Modifier une base</div>

                <div class="panel-body">

                  <form method="POST" action="/bases/update/{{$base->id}}" accept-charset="UTF-8">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}" />

                  <div class="form-group">
                    <label for="nom_editeur">Nom :</label>
                    <input type="text" class="form-control" id="titre" name="titre" value="{{$base->titre}}">
                  </div>

                  <div class="form-group">
                    <label for="id_editeur">Editeur de la base :</label>
                      <select class="form-control" id="id_editeur" name="id_editeur">
                      @foreach($editeurs as $e)

                        @if($e->ids_editeur == $base->id_editeur)
                          <option value="{{$e->ids_editeur}}" selected>{{$e->nom_entreprise}}</option>
                        @else
                          <option value="{{$e->ids_editeur}}">{{$e->nom_entreprise}}</option>
                        @endif

                      @endforeach
                      </select>
                  </div>


                 <button type="submit" class="btn btn-success">Modifier</button>
                 </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
