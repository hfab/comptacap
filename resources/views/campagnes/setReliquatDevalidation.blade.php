@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">

                    <div class="panel-heading">Dévalidation sur la campagne</div>

                    <div class="panel-body">

                        <form method="POST" action="/campagnes/reliquatDevalid/{{$campagne->id}}/{{$id_periode}}/go" accept-charset="UTF-8">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />

                            <h4><span class="label label-warning">{{$campagne->nom_campagne}}</span></h4>

                            <div class="form-group">
                                <label for="id_editeur">Type :</label>
                                <select class="form-control" id="type_id_campagne" name="type_id_campagne">
                                    @foreach($typerem as $t)
                                        @if($t->id == $campagne->type_id_campagne)
                                            <option value="{{$t->id}}" selected>{{$t->type}}</option>
                                        @else
                                            <option value="{{$t->id}}">{{$t->type}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="nom_editeur">Dévalidation pourcentage (%) :</label>
                                <input type="number" step="0.01" class="form-control" min="0" id="taux_devalidation" name="taux_devalidation" required>
                            </div>

                            <div class="form-group">
                                <label for="id_editeur">Choix de la période :</label>
                                <select class="form-control" id="id_periode" name="id_periode">
                                    @foreach($periodeCampagne as $p)
                                        @if($p->id == $periodeFrom->id)
                                            <option value="{{$p->id}}" selected>{{$p->display_name}}</option>
                                        @else
                                            <option value="{{$p->id}}">{{$p->display_name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>

                            @if(count($periodeCampagne))
                                <button type="submit" class="btn btn-success">Valider</button>
                            @else
                                Aucune période disponnible.
                            @endif

                        </form>

                    </div>
                </div>
            </div>
        </div>

@endsection
