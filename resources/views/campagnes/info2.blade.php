@extends('layouts.app')

@section('content')

  <div>
    <div class="row">
      <div class="panel-body">Ajouter une ligne de rémunération
        <a href="/campagnes/addrem/{{$id}}/{{$idp}}">
          <button type="button" class="btn btn-xs btn-info">
              <span class="glyphicon glyphicon-plus" aria-hidden="true">
              </span>
          </button>
        </a>
      </div>
    </div>

    <div class="row">
      <div class="panel-body">Ajouter une ligne de Reliquat
        <a href="/campagnes/addReliquat/{{$id}}/{{$idp}}">
          <button type="button" class="btn btn-xs btn-info">
              <span class="glyphicon glyphicon-plus" aria-hidden="true">
              </span>
          </button>
        </a>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-6">
      <div class="panel-body">
        <div class="row">
          <h1>Période Actuelle</h1>
          <table class="table table-hover">
            <thead>
            <tr>
              <th>ID</th>
              <th>Type</th>
              <th>Base</th>
              <th>Editeur</th>
              <th>Nombre</th>
              <th>Rémunération</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($campagnestats as $stats)
              <tr>
                <td>
                  {{$stats->id}}
                </td>
                <td>
                  @if($remcampagne == 1)
                    CPM
                  @elseif($remcampagne == 2)
                    CPL
                  @elseif($remcampagne == 3)
                    CPA
                  @elseif($remcampagne == 4)
                    CPC
                  @endif
                </td>
                <td>
                  <?php
                  $base = \DB::table('bases')
                          ->where('idsite', $stats->id_site_base)
                          ->first();

                  if($base !== NULL){
                    echo($base->titre);
                  }
                  ?>
                  @if($remduplist[$stats->id])
                    <span class="glyphicon glyphicon-warning-sign" style="color: red" aria-hidden="true"></span>
                  @endif
                </td>
                <td>
                  <?php
                  if($base !== NULL){
                    $editeur = \DB::table('editeurs')
                            ->where('ids_editeur', $base->id_editeur)
                            ->first();

                    echo $editeur->nom_entreprise;
                  }
                  ?>
                </td>
                <td>
                  @if($remcampagne == 1)
                    {{$stats->CPM}}
                  @elseif($remcampagne == 2)
                    {{$stats->CPL}}
                  @elseif($remcampagne == 3)
                    {{$stats->CPA}}
                  @elseif($remcampagne == 4)
                    {{$stats->CPC}}
                  @endif
                </td>

                <td>
                  {{$stats->com}} €
                </td>
                <td>
                  <!-- relicat -->
                  <a href="/campagnes/reliquat/{{$stats->id}}/{{$id}}/{{$idp}}">
                    <button type="button" class="btn btn-xs btn-warning">
                                <span class="glyphicon glyphicon-time" aria-hidden="true">
                                </span>
                    </button>
                  </a>
                  <!-- supprimer -->
                  <a href="/campagnes/deleterem/{{$stats->id}}/{{$id}}/{{$idp}}">
                    <button type="button" class="btn btn-xs btn-danger">
                                <span class="glyphicon glyphicon-trash" aria-hidden="true">
                                </span>
                    </button>
                  </a>

                  <a href="/campagnes/editrem/{{$stats->id}}/{{$id}}/{{$idp}}">
                    <button type="button" class="btn btn-xs btn-info">
                                <span class="glyphicon glyphicon-pencil" aria-hidden="true">
                                </span>
                    </button>
                  </a>
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>

      <div>
              <span>
                Stats des périodes CSV
                @foreach($periodeautre as $p)
                  <a href="/campagnes/info/{{$id}}/{{$p->id}}"><button type="button" class="btn btn-xs btn-success">{{$p->display_name}}</button></a>
                @endforeach
              </span>
      </div>
    </div>

    <div class="col-md-6">
      <div class="panel-body">
        <div class="row">
          <h1>Devalidé</h1>
          <table class="table table-hover">
            <thead>
            <tr>
              <th>ID</th>
              <th>Type</th>
              <th>Base</th>
              <th>Editeur</th>
              <th>Nombre</th>
              <th>Rémunération</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($campagnestats as $stats)
              <tr>
                <td>{{$stats->id}}</td>
                <td>
                  @if($remcampagne == 1)
                    CPM
                  @elseif($remcampagne == 2)
                    CPL
                  @elseif($remcampagne == 3)
                    CPA
                  @elseif($remcampagne == 4)
                    CPC
                  @endif
                </td>
                <td>
                  <?php
                  $base = \DB::table('bases')
                          ->where('idsite', $stats->id_site_base)
                          ->first();

                  if($base !== NULL){
                    echo($base->titre);
                  }
                  ?>
                </td>
                <td>
                  <?php
                  if($base !== NULL){
                    $editeur = \DB::table('editeurs')
                            ->where('ids_editeur', $base->id_editeur)
                            ->first();

                    echo $editeur->nom_entreprise;
                  }
                  ?>
                </td>
                <td style="color: #2ab27b">
                  @if($remcampagne == 1)
                    {{$stats->devalid_CPM}}
                  @elseif($remcampagne == 2)
                    {{$stats->devalid_CPL}}
                  @elseif($remcampagne == 3)
                    {{$stats->devalid_CPA}}
                  @elseif($remcampagne == 4)
                    {{$stats->devalid_CPC}}
                  @endif
                </td>

                <td>{{$stats->devalid_com}} €</td>
                <td>
                  <!-- modifier -->
                  <a href="/campagnes/editremdevalid/{{$stats->id}}/{{$id}}/{{$idp}}">
                    <button type="button" class="btn btn-xs btn-info">
                             <span class="glyphicon glyphicon-pencil" aria-hidden="true">
                             </span>
                    </button>
                  </a>
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>

      <div class="col-md-6">
        @if($devalidinfo->devalid_pourcent !== 0)
          Devalidation ({{$devalidinfo->devalid_pourcent}} %)
          - <a href="/campagnes/devalid/delete/{{$id}}/{{$idp}}"><button type="button" class="btn btn-xs btn-danger">Supprimer la dévalidation {{$p->display_name}}</button></a>
        @elseif($devalidinfo->devalid_nombre !== 0)
          Devalidation (- {{$devalidinfo->devalid_nombre}} action > CPL / CPC / CPA)
          - <a href="/campagnes/devalid/delete/{{$id}}/{{$idp}}"><button type="button" class="btn btn-xs btn-danger">Supprimer la dévalidation {{$p->display_name}}</button></a>
        @endif
      </div>
    </div>
  </div>

  <div class="row">
  <div class="col-md-6">
    <div class="row">
      <div class="panel-body">
        <h1>Reliquats</h1>
        <table class="table table-hover">
          <thead>
          <tr>
            <th>ID</th>
            <th>Type</th>
            <th>Base</th>
            <th>Editeur</th>
            <th>Nombre</th>
            <th>Rémunération</th>
            <th>Action</th>
          </tr>
          </thead>
          <tbody>
          @foreach($reliquats as $stats)
            <tr>
              <td>{{$stats->id}}</td>
              <td>
                @if($remcampagne == 1)
                  CPM
                @elseif($remcampagne == 2)
                  CPL
                @elseif($remcampagne == 3)
                  CPA
                @elseif($remcampagne == 4)
                  CPC
                @endif
              </td>
              <td>
                <?php
                $base = \DB::table('bases')
                        ->where('idsite', $stats->id_site_base)
                        ->first();

                if($base !== NULL){
                  echo($base->titre);
                }
                ?>
                @if($relduplist[$stats->id])
                  <span class="glyphicon glyphicon-warning-sign" style="color: red" aria-hidden="true"></span>
                @endif
              </td>
              <td>
                <?php
                if($base !== NULL){
                  $editeur = \DB::table('editeurs')
                          ->where('ids_editeur', $base->id_editeur)
                          ->first();

                  echo $editeur->nom_entreprise;
                }
                ?>
              </td>
              <td>
                @if($remcampagne == 1)
                  {{$stats->CPM}}
                @elseif($remcampagne == 2)
                  {{$stats->CPL}}
                @elseif($remcampagne == 3)
                  {{$stats->CPA}}
                @elseif($remcampagne == 4)
                  {{$stats->CPC}}
                @endif
              </td>

              <td>{{$stats->com}} €</td>
              <td>
                <!-- supprimer -->
                <a href="/campagnes/deletereliq/{{$stats->id}}/{{$id}}/{{$idp}}">
                  <button type="button" class="btn btn-xs btn-danger">
                                <span class="glyphicon glyphicon-trash" aria-hidden="true">
                                </span>
                  </button>
                </a>

                <!-- modifier -->
                <a href="/campagnes/editReliquat/{{$stats->id}}/{{$id}}/{{$idp}}">
                  <button type="button" class="btn btn-xs btn-info">
                             <span class="glyphicon glyphicon-pencil" aria-hidden="true">
                             </span>
                  </button>
                </a>
              </td>
            </tr>
          @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>

  <div class="col-md-6">
    <div class="panel-body">
      <div class="row">
        <h1>Devalidé</h1>
        <table class="table table-hover">
          <thead>
          <tr>
            <th>ID</th>
            <th>Type</th>
            <th>Base</th>
            <th>Editeur</th>
            <th>Nombre</th>
            <th>Rémunération</th>
            <th>Action</th>
          </tr>
          </thead>
          <tbody>
          @foreach($reliquats as $stats)
            <tr>
              <td>{{$stats->id}}</td>
              <td>
                @if($remcampagne == 1)
                  CPM
                @elseif($remcampagne == 2)
                  CPL
                @elseif($remcampagne == 3)
                  CPA
                @elseif($remcampagne == 4)
                  CPC
                @endif
              </td>
              <td>
                <?php
                $base = \DB::table('bases')
                        ->where('idsite', $stats->id_site_base)
                        ->first();

                if($base !== NULL){
                  echo($base->titre);
                }
                ?>
              </td>
              <td>
                <?php
                if($base !== NULL){
                  $editeur = \DB::table('editeurs')
                          ->where('ids_editeur', $base->id_editeur)
                          ->first();

                  echo $editeur->nom_entreprise;
                }
                ?>
              </td>
              <td style="color: #2ab27b">
                @if($remcampagne == 1)
                  {{$stats->devalid_CPM}}
                @elseif($remcampagne == 2)
                  {{$stats->devalid_CPL}}
                @elseif($remcampagne == 3)
                  {{$stats->devalid_CPA}}
                @elseif($remcampagne == 4)
                  {{$stats->devalid_CPC}}
                @endif
              </td>

              <td>{{$stats->devalid_com}} €</td>
              <td>
                <!-- modifier -->
                <a href="/campagnes/editReliquatDevalid/{{$stats->id}}/{{$id}}/{{$idp}}">
                  <button type="button" class="btn btn-xs btn-info">
                             <span class="glyphicon glyphicon-pencil" aria-hidden="true">
                             </span>
                  </button>
                </a>
              </td>
            </tr>
          @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>

  <div class="col-md-6">

    <div class="row">
      <div class="panel-body">Devalidation des reliquats
        <a href="/campagnes/reliquatDevalid/{{$id}}/{{$idp}}">
          <button type="button" class="btn btn-xs btn-info">
              <span class="glyphicon" style="font-family: Arial, sans-serif"><strong>%</strong>
              </span>
          </button>
        </a>
        <a href="/campagnes/reliquatDevalidnb/{{$id}}/{{$idp}}">
          <button type="button" class="btn btn-xs btn-info">
              <span class="glyphicon glyphicon-minus" aria-hidden="true">
              </span>
          </button>
        </a>
      </div>
    </div>
  </div>
  </div>

@endsection

<?php
function _count_in_array($haystack, $elem)
{
  $count = 0;
  foreach ($haystack as $item) {
    if ($elem->id_site_base == $item->id_site_base)
      $count++;
  }
  return $count;
}
?>