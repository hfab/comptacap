@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-heading">Modifier une campagnes</div>

                <div class="panel-body">

                  <form method="POST" action="/campagnes/update/{{$campagnes->id}}" accept-charset="UTF-8">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}" />

                  <div class="form-group">
                    <label for="nom_editeur">Nom :</label>
                    <input type="text" class="form-control" id="nom_campagne" name="nom_campagne" value="{{$campagnes->nom_campagne}}">
                  </div>

                  <div class="form-group">
                    <label for="id_editeur">Type :</label>
                      <select class="form-control" id="type_id_campagne" name="type_id_campagne" value="">
                      @foreach($typerem as $t)
                        @if($t->id == $campagnes->type_id_campagne)
                          <option value="{{$t->id}}" selected>{{$t->type}}</option>
                        @else
                          <option value="{{$t->id}}">{{$t->type}}</option>
                        @endif
                      @endforeach
                      </select>
                  </div>

                  <div class="form-group">
                    <label for="id_editeur">Valeur rémunération :</label>
                    <input type="number" class="form-control" id="rem_campagne" name="rem_campagne" min="0" step="0.00001" value="{{$campagnes->rem_campagne}}">
                  </div>

                  <div class="form-group">
                    <label for="comment">Commentaire / Note :</label>
                    <textarea class="form-control" rows="5" id="note_campagne" name="note_campagne">{{$campagnes->note_campagne}}</textarea>
                  </div>

                  <button type="submit" class="btn btn-success">Modifier</button>
                  </form>
            </div>
        </div>
    </div>
</div>
@endsection
