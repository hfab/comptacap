@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">

                    <div class="panel-heading">Ajouter valeur ligne</div>

                    <div class="panel-body">

                        <form method="POST" action="/campagnes/addReliquat/{{$campagne->id}}/{{$periode->id}}/go" accept-charset="UTF-8">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />

                            <h4><span class="label label-warning">{{$campagne->nom_campagne}} </span> - <span class="label label-success">Période {{$periode->display_name}}</span></h4>

                            <div class="form-group">
                                <label for="nb">
                                    Nombre :
                                    @if($campagne->type_id_campagne == 1)
                                        CPM
                                    @elseif($campagne->type_id_campagne == 2)
                                        CPL
                                    @elseif($campagne->type_id_campagne == 3)
                                        CPA
                                    @elseif($campagne->type_id_campagne == 4)
                                        CPC
                                    @endif
                                </label>

                                @if($campagne->type_id_campagne == 1)
                                    <input type="number" step="1" min="0" class="form-control" id="CPM" name="CPM" value="" required>
                                @elseif($campagne->type_id_campagne == 2)
                                    <input type="number" step="1" min="0" class="form-control" id="CPL" name="CPL" value="" required>
                                @elseif($campagne->type_id_campagne == 3)
                                    <input type="number" step="1" min="0" class="form-control" id="CPA" name="CPA" value="" required>
                                @elseif($campagne->type_id_campagne == 4)
                                    <input type="number" step="1" min="0" class="form-control" id="CPC" name="CPC" value="" required>
                                @endif

                                <input type="hidden" name="campagne_id" value="{{$campagne->id}}" />
                                <input type="hidden" name="idp" value="{{$periode->id}}" />
                            </div>

                            <div class="form-group">
                                <label for="id_editeur">Base :</label>
                                <select class="form-control" id="base_id" name="base_id">
                                    @foreach($bases as $b)
                                        <option value="{{$b->idsite}}">{{$b->titre}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="id_periode_reference">Période de Référence :</label>
                                <select class="form-control" id="id_periode_reference" name="id_periode_reference">
                                    @foreach($periodes as $p)
                                        <option value="{{$p->id}}">{{$p->display_name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <button type="submit" class="btn btn-success">Valider</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
