@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <h1 class="text-center">{{ $campagne->nom_campagne }} - {{ $periode->display_name }}</h1>
        </div>

        <div class="row">
            <div class="row">
                <div class="col-md-6">
                    <div class="panel-body">Ajouter une rémunération
                        <a href="/campagnes/addrem/{{$campagne->id}}/{{$periode->id}}">
                            <button type="button" class="btn btn-xs btn-info">
                                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                            </button>
                        </a>
                        Devalidation
                        <a href="/campagnes/setdevalidation/periode/{{$campagne->id}}/{{$periode->id}}">
                            <button type="button" class="btn btn-xs btn-info">
                                %
                            </button>
                        </a>
                        <a href="/campagnes/setdevalidationnb/periode/{{$campagne->id}}/{{$periode->id}}">
                            <button type="button" class="btn btn-xs btn-info">
                                #
                            </button>
                        </a>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel-body">
                        @if(isset($reldevalidinfo) && intval($devalidinfo->devalid_pourcent) != 0)
                            Devalidation -
                            <button type="button" class="btn btn-xs btn-danger">{{$devalidinfo->devalid_pourcent}} %</button>
                        @elseif(isset($reldevalidinfo) && intval($devalidinfo->devalid_nombre) !== 0)
                            Devalidation -
                            <button type="button" class="btn btn-xs btn-danger">{{$devalidinfo->devalid_nombre}}</button>
                        @else
                            Devalidation - Aucune Dévalidation - <button type="button" class="btn btn-xs btn-danger">{{$periode->display_name}}</button>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <h2>Periode Actuelle - {{ $totals['rem'][0] }} € ({{$totals['rem'][2]}})</h2>
                    <div class="panel-body">
                        <div class="row">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>IDS</th>
                                    <th>Type</th>
                                    <th>Base</th>
                                    <th>Editeur</th>
                                    <th>Nombre</th>
                                    <th>Rémunération</th>
                                    <th>Total</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($stats as $stat)
                                    <tr>
                                        <td>{{ $stat->base->id }}</td>
                                        <td>{{ $stat->rem_type }}</td>
                                        <td>{{ $stat->base->titre }}
                                            @if($remduplist[$stat->id])
                                                <span class="glyphicon glyphicon-warning-sign" style="color: red" aria-hidden="true"></span>
                                            @endif
                                        </td>
                                        <td>{{ $stat->editeur->nom_entreprise }}</td>
                                        <td>{{ $stat->nb }}</td>
                                        <td>{{ $stat->rem }} €</td>
                                        <td>{{ $stat->com }} €</td>
                                        <td>
                                            <!-- supprimer -->
                                            <a href="/campagnes/deleterem/{{$stat->id}}/{{$campagne->id}}/{{$periode->id}}">
                                                <button type="button" class="btn btn-xs btn-danger">
                                <span class="glyphicon glyphicon-trash" aria-hidden="true">
                                </span>
                                                </button>
                                            </a>

                                            <a href="/campagnes/editrem/{{$stat->id}}/{{$campagne->id}}/{{$periode->id}}">
                                                <button type="button" class="btn btn-xs btn-info">
                                <span class="glyphicon glyphicon-pencil" aria-hidden="true">
                                </span>
                                                </button>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <h2>Devalidé - {{ $totals['rem'][1] }}€ ({{ $totals['rem'][3] }})</h2>
                    <div class="panel-body">
                        <div class="row">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Type</th>
                                    <th>Base</th>
                                    <th>Editeur</th>
                                    <th>Nombre</th>
                                    <th>Rémunération</th>
                                    <th>Total</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($stats as $stat)
                                    <tr>
                                        <td>{{ $stat->base->id }}</td>
                                        <td>{{ $stat->rem_type }}</td>
                                        <td>{{ $stat->base->titre }}</td>
                                        <td>{{ $stat->editeur->nom_entreprise }}</td>
                                        <td style="color: #2ab27b">{{ $stat->nb_deval }}</td>
                                        <td>{{ $stat->rem }} €</td>
                                        <td>{{ $stat->deval_com }} €</td>
                                        <td>
                                            <!-- modifier -->
                                            <a href="/campagnes/editremdevalid/{{$stat->id}}/{{$campagne->id}}/{{$periode->id}}">
                                                <button type="button" class="btn btn-xs btn-info">
                                                <span class="glyphicon glyphicon-pencil" aria-hidden="true">
                                                </span>
                                                </button>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="row">
                <div class="col-md-6">
                    <div class="panel-body">Ajouter un reliquat
                        <a href="/campagnes/addReliquat/{{$campagne->id}}/{{$periode->id}}">
                            <button type="button" class="btn btn-xs btn-info">
                                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                            </button>
                        </a>
                        Devalidation
                        <a href="/campagnes/reliquatDevalid/{{$campagne->id}}/{{$periode->id}}">
                            <button type="button" class="btn btn-xs btn-info">
                                %
                            </button>
                        </a>
                        <a href="/campagnes/reliquatDevalidnb/{{$campagne->id}}/{{$periode->id}}">
                            <button type="button" class="btn btn-xs btn-info">
                                #
                            </button>
                        </a>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel-body">
                        @if(isset($reldevalidinfo) && intval($reldevalidinfo->devalid_pourcent) !== 0)
                            Devalidation -
                            <button type="button" class="btn btn-xs btn-danger">{{$reldevalidinfo->devalid_pourcent}} %</button>
                        @elseif(isset($reldevalidinfo) && intval($reldevalidinfo->devalid_nombre) !== 0)
                            Devalidation -
                            <button type="button" class="btn btn-xs btn-danger">{{$reldevalidinfo->devalid_nombre}}</button>
                        @else
                            Devalidation - Aucune Dévalidation - <button type="button" class="btn btn-xs btn-danger">{{$periode->display_name}}</button>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <h2>Reliquats - {{ $totals['reliquats'][0] }}€ ({{ $totals['reliquats'][2] }})</h2>
                    <div class="panel-body">
                        <div class="row">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Type</th>
                                    <th>Base</th>
                                    <th>Editeur</th>
                                    <th>Nombre</th>
                                    <th>Rémunération</th>
                                    <th>Total</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($reliquats as $stat)
                                    <tr>
                                        <td>{{ $stat->base->id }}</td>
                                        <td>{{ $stat->rem_type }}</td>
                                        <td>{{ $stat->base->titre }}</td>
                                        <td>{{ $stat->editeur->nom_entreprise }}</td>
                                        <td>{{ $stat->nb }}</td>
                                        <td>{{ $stat->rem }} €</td>
                                        <td>{{ $stat->com }} €</td>
                                        <td>
                                            <!-- supprimer -->
                                            <a href="/campagnes/deletereliq/{{$stat->id}}/{{$campagne->id}}/{{$periode->id}}">
                                                <button type="button" class="btn btn-xs btn-danger">
                                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                                </button>
                                            </a>

                                            <!-- modifier -->
                                            <a href="/campagnes/editReliquat/{{$stat->id}}/{{$campagne->id}}/{{$periode->id}}">
                                                <button type="button" class="btn btn-xs btn-info">
                                                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                                </button>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <h2>Devalidé - {{ $totals['reliquats'][1] }}€ ({{ $totals['reliquats'][3] }})</h2>
                    <div class="panel-body">
                        <div class="row">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Type</th>
                                    <th>Base</th>
                                    <th>Editeur</th>
                                    <th>Nombre</th>
                                    <th>Rémunération</th>
                                    <th>Total</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($reliquats as $stat)
                                    <tr>
                                        <td>{{ $stat->base->id }}</td>
                                        <td>{{ $stat->rem_type }}</td>
                                        <td>{{ $stat->base->titre }}</td>
                                        <td>{{ $stat->editeur->nom_entreprise }}</td>
                                        <td style="color: #2ab27b">{{ $stat->nb_deval }}</td>
                                        <td>{{ $stat->rem }} €</td>
                                        <td>{{ $stat->deval_com }} €</td>
                                        <td>
                                            <!-- modifier -->
                                            <a href="/campagnes/editReliquatDevalid/{{$stat->id}}/{{$campagne->id}}/{{$periode->id}}">
                                                <button type="button" class="btn btn-xs btn-info">
                                                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                                </button>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <p>
                    Autres periodes:
                    @foreach($periodes as $p)
                        <a href="/campagnes/info/{{$campagne->id}}/{{$p->id}}">
                            @if($p->id === $periode->id)
                                <button type="button" class="btn btn-xs btn-primary">{{ $p->display_name }}</button>
                            @else
                                <button type="button" class="btn btn-xs btn-success">{{ $p->display_name }}</button>
                            @endif
                        </a>
                    @endforeach
                </p>
            </div>
        </div>
    </div>


@endsection