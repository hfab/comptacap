@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
          <div class="panel-heading">Listing des campagnes</div>
          <div class="panel-body">
            <div class="row">
              <div class="col-sm-6">
                <a href="./campagnes/create"><button type="button" class="btn btn-success">Ajouter une campagne</button></a>
              </div>
            </div>
          </div>
          <div class="panel-body">
            <table class="table table-hover">
              <thead>
              <tr>
                <th>ID</th>
                <th>Type</th>
                <th>Nom</th>
                <th>Rémunération</th>
                <th>Note</th>
                <th>Options</th>
              </tr>
              </thead>
              <tbody>
              @foreach($campagnes as $c)
                <tr>
                  <td>{{$c->id}}</td>
                  <td>
                    <?php
                    if($c->type_id_campagne == 1){
                      echo 'CPM';
                    }
                    if($c->type_id_campagne == 2){
                      echo 'CPL';
                    }
                    if($c->type_id_campagne == 3){
                      echo 'CPA';
                    }
                    if($c->type_id_campagne == 4){
                      echo 'CPC';
                    }
                    ?>
                  </td>

                  <td>{{$c->nom_campagne}}</td>
                  <td>{{$c->rem_campagne}} €</td>
                  <td>{{$c->note_campagne}}</td>
                  <td>
                  <!--
                          <a href="./campagnes/info/{{$c->id}}">
                            <button type="button" class="btn btn-xs btn-success">
                              Voir détails
                            </button>
                          </a>
                        -->
                    <a href="./campagnes/info/{{$c->id}}/{{$last_periode->id}}">
                      <button type="button" class="btn btn-xs btn-success">
                        Info
                      </button>
                    </a>

                    <a href="./campagnes/edit/{{$c->id}}">
                      <button type="button" class="btn btn-xs btn-warning">
                        Modifier
                      </button>
                    </a>
                    <a onclick="return confirm('supprimer {{ $c->nom_campagne }}')" href="./campagnes/delete/{{$c->id}}">
                      <button type="button" class="btn btn-xs btn-danger">
                        Supprimer
                      </button>
                    </a>
                  </td>
                </tr>
              @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
