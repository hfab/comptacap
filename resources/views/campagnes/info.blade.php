@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel col-md-6">

                <div class="panel-heading">Stats CSV</div>

                <div class="panel-body">

                  <div class="row">
                    <table class="table table-hover">
                      <thead>
                        <tr>
                          <th>ID</th>
                          <th>Type</th>
                          <th>Base</th>
                          <th>Editeur</th>
                          <th>Nombre</th>
                          <th>Rémunération</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($campagnestats as $stats)
                        <tr>
                          <td>{{$stats->id}}</td>
                          <td>
                            @if($remcampagne == 1)
                              CPM
                            @elseif($remcampagne == 2)
                              CPL
                            @elseif($remcampagne == 3)
                              CPA
                            @elseif($remcampagne == 4)
                              CPC
                            @endif
                          </td>
                          <td>
                            <?php
                              $base = \DB::table('bases')
                              ->where('idsite', $stats->id_site_base)
                              ->first();

                              if($base !== NULL){
                              echo($base->titre);
                              }
                          ?>
                          </td>
                          <td>
                            <?php
                              if($base !== NULL){
                                $editeur = \DB::table('editeurs')
                                ->where('ids_editeur', $base->id_editeur)
                                ->first();

                                echo $editeur->nom_entreprise;
                              }
                            ?>
                          </td>
                          <td>
                            @if($remcampagne == 1)
                              {{$stats->CPM}}
                            @elseif($remcampagne == 2)
                              {{$stats->CPL}}
                            @elseif($remcampagne == 3)
                              {{$stats->CPA}}
                            @elseif($remcampagne == 4)
                              {{$stats->CPC}}
                            @endif
                          </td>

                          <td>{{$stats->com}} €</td>
                        </tr>
                        @endforeach
                        </tbody>
                      </table>

                  </div>

            </div>
        </div>

        <div class="panel col-md-6">

            <div class="panel-heading">
              @if($devalidinfo->devalid_pourcent !== 0)
              Devalidation ({{$devalidinfo->devalid_pourcent}} %)
              @elseif($devalidinfo->devalid_nombre !== 0)
              Devalidation (- {{$devalidinfo->devalid_nombre}} action > CPL / CPC / CPA)
              @endif
            </div>

            <div class="panel-body">

              <div class="row">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Type</th>
                      <th>Base</th>
                      <th>Editeur</th>
                      <th>Nombre</th>
                      <th>Rémunération</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($campagnestats as $stats)
                    <tr>
                      <td>{{$stats->id}}</td>
                      <td>
                        @if($remcampagne == 1)
                          CPM
                        @elseif($remcampagne == 2)
                          CPL
                        @elseif($remcampagne == 3)
                          CPA
                        @elseif($remcampagne == 4)
                          CPC
                        @endif
                      </td>
                      <td>
                        <?php
                          $base = \DB::table('bases')
                          ->where('idsite', $stats->id_site_base)
                          ->first();

                          if($base !== NULL){
                          echo($base->titre);
                          }
                      ?>
                      </td>
                      <td>
                        <?php
                          if($base !== NULL){
                            $editeur = \DB::table('editeurs')
                            ->where('ids_editeur', $base->id_editeur)
                            ->first();

                            echo $editeur->nom_entreprise;
                          }
                        ?>
                      </td>
                      <td>
                        @if($remcampagne == 1)
                          {{$stats->devalid_CPM}}
                        @elseif($remcampagne == 2)
                          {{$stats->devalid_CPL}}
                        @elseif($remcampagne == 3)
                          {{$stats->devalid_CPA}}
                        @elseif($remcampagne == 4)
                          {{$stats->devalid_CPC}}
                        @endif
                      </td>

                      <td>{{$stats->devalid_com}} €</td>
                    </tr>
                    @endforeach
                    </tbody>
                  </table>


              </div>

        </div>
    </div>

    </div>
</div>
@endsection
