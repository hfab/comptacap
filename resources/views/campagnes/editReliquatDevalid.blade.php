@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

              <div class="panel-heading">Modifier valeur ligne reliquat</div>

              <div class="panel-body">

                <form method="POST" action="/campagnes/editReliquatDevalid/go" accept-charset="UTF-8">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />

                <h4><span class="label label-warning">{{$campagne->nom_campagne}} par {{$base->titre}} ({{$editeur->nom_entreprise}})</span> - <span class="label label-success">Période {{$periode->display_name}}</span> - <span class="label label-info">Reliquat</span></h4>

                <div class="form-group">
                  <label for="nb">Nombre :
                    @if($campagne->type_id_campagne == 1)
                      CPM
                    @elseif($campagne->type_id_campagne == 2)
                      CPL
                    @elseif($campagne->type_id_campagne == 3)
                      CPA
                    @elseif($campagne->type_id_campagne == 4)
                      CPC
                    @endif</label>


                    @if($campagne->type_id_campagne == 1)
                      <input type="number" step="1" class="form-control" id="CPM" name="CPM" value="{{$row->devalid_CPM}}" required>
                    @elseif($campagne->type_id_campagne == 2)
                      <input type="number" step="1" class="form-control" id="CPL" name="CPL" value="{{$row->devalid_CPL}}" required>
                    @elseif($campagne->type_id_campagne == 3)
                      <input type="number" step="1" class="form-control" id="CPA" name="CPA" value="{{$row->devalid_CPA}}" required>
                    @elseif($campagne->type_id_campagne == 4)
                      <input type="number" step="1" class="form-control" id="CPC" name="CPC" value="{{$row->devalid_CPC}}" required>
                    @endif

                    <input type="hidden" name="campagne_id" value="{{$campagne->id}}" />
                    <input type="hidden" name="idrow" value="{{$row->id}}" />
                    <input type="hidden" name="idp" value="{{$row->id_periode}}" />

                </div>

                <button type="submit" class="btn btn-success">Valider</button>

              </form>
            </div>
        </div>
    </div>
  </div>

@endsection
