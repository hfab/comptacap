<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset='UTF-8'>

	<title>Appel à Facture</title>

	<link rel='stylesheet' href='css/style.css'>
	<link rel='stylesheet' href='css/print.css' media="print">


	<style>

		* { margin: 0; padding: 0; }
		body { font: 14px/1.4 Georgia, serif; }
		#page-wrap { width: 600px; margin: 0 auto; }

		textarea { border: 0; font: 14px Georgia, Serif; overflow: hidden; resize: none; }
		table { border-collapse: collapse; }
		table td, table th { border: 1px solid black; padding: 5px; }

		#header { height: 15px; width: 100%; margin: 20px 0; background: #222; text-align: center; color: white; font: bold 15px Helvetica, Sans-Serif; text-decoration: uppercase; letter-spacing: 20px; padding: 8px 0px; }

		#address { width: 250px; height: 150px; float: left; }
		#customer { overflow: hidden; }

		#logo { text-align: right; float: right; position: relative; margin-top: 25px; border: 1px solid #fff; max-width: 540px; max-height: 100px; overflow: hidden; }

		#logoctr { display: none; }

		#logohelp { text-align: left; display: none; font-style: italic; padding: 10px 5px;}
		#logohelp input { margin-bottom: 5px; }
		#customer-title { font-size: 15px; font-weight: bold; float: left; }

		#meta { margin-top: 1px; width: 300px; float: right; }
		#meta td { text-align: right;  }
		#meta td.meta-head { text-align: left; background: #eee; }
		#meta td textarea { width: 100%; height: 20px; text-align: right; }

		.items { clear: both; width: 100%; margin: 30px 0 0 0; border: 1px solid black; }
		.items th { background: #eee; }
		.items textarea { width: 80px; height: 50px; }
		.items tr.item-row td { border: 0; vertical-align: top; }
		.items td.description { width: 120px; }
		.items td.item-name { width: 120px; }
		.items td.description textarea, #items td.item-name textarea { width: 100%; }
		.items td.total-line { border-right: 0; text-align: right; }
		.items td.total-value { border-left: 0; padding: 10px; }
		.items td.total-value textarea { height: 20px; background: none; }
		.items td.total-detail { width: 70px }
		.items td.balance { background: #eee; }
		.items td.blank { border: 0; }

		#bottom { width: 100%; }
		#reliquats { width: 100%; float: left; }

		.reliquat_items { width: 100%; margin: 30px 0 0 0; border: 1px solid black; }
		.reliquat_items th { background: #eee; }
		.reliquat_items textarea { width: 80px; height: 50px; }
		.reliquat_items tr.item-row td { border: 0; vertical-align: top; }
		.reliquat_items td.description { width: 80px; }
		.reliquat_items td.item-name { width: 100px; }
		.reliquat_items td.description textarea, #items td.item-name textarea { width: 100%; }
		/*.reliquat_items td.total-line { border-right: 0; text-align: right; }*/
		/*.reliquat_items td.total-value { border-left: 0; padding: 10px; }*/
		.reliquat_items td.total-value textarea { height: 20px; background: none; }
		.reliquat_items td.total-detail { width: 70px }
		/*.reliquat_items td.balance { background: #eee; }*/
		.reliquat_items td.blank { border: 0; }

		.total_item { width: 100%; margin: 30px 0 0 0; border: 1px solid black; }
		.total_item td.total-line { border-right: 0; text-align: right; }
		.total_item td.total-value { border-left: 0; padding: 10px; }
		.total_item td.balance { background: #eee; }

		#total_container { margin-left: 70%; }

		#terms { text-align: center; margin: 20px 0 0 0; }
		#terms h5 { text-transform: uppercase; font: 13px Helvetica, Sans-Serif; letter-spacing: 10px; border-bottom: 1px solid black; padding: 0 0 8px 0; margin: 0 0 8px 0; }
		#terms textarea { width: 100%; text-align: center;}


		.delete-wpr { position: relative; }
		.delete { display: block; color: #000; text-decoration: none; position: absolute; background: #EEEEEE; font-weight: bold; padding: 0px 3px; border: 1px solid; top: -6px; left: -22px; font-family: Verdana; font-size: 12px; }

		#hiderow,
		.delete {
			display: none;
		}

	</style>
</head>

<body>

<div id="page-wrap">

	<div id="header">Appel à Facture</div>

	<div id="identity">
		<div id="address">
			12, rue Camille Desmoulins<br />
			92300 LEVALLOIS-PERRET<br />
		</div>

		<div id="logo">
			<div id="logoctr">
			</div>
			<div id="logohelp">
			</div>
			<img id="image" src="https://plateforme.lead-factory.net/logo.png" alt="logo" />
		</div>
	</div>

	<div style="clear:both"></div>

	<div id="customer">

		<div id="customer-title">
			{{$editeurinfo->nom_entreprise}} <br />
			{{$editeurinfo->nom}} <br />
			{{$editeurinfo->prenom}} <br />
			{{$editeurinfo->adresse}} <br />
			{{$editeurinfo->ville}} <br />
			{{$editeurinfo->codepostale}} <br />
		</div>

		<table id="meta">
			<tr>
				<td class="meta-head">Appel #</td>
				<td>000123</td>
			</tr>
			<tr>
				<td class="meta-head">Date</td>
				<td>{{date('d-m-Y')}}</td>
			</tr>
			<tr>
				<td class="meta-head">Période</td>
				<td>{{ $periode->display_name }}</td>
			</tr>
			<tr>
				<td class="meta-head">C. Commercial</td>
				<td>{{ $editeurinfo->email }}</td>
			</tr>
			<tr>
				<td class="meta-head">C. Comptable</td>
				<td>{{ $editeurinfo->compta }}</td>
			</tr>
		</table>
	</div>

	<table id="items" class="items">
		<tr>
			<th>base</th>
			<th>campagne</th>
			<th>type</th>
			<th>quantité</th>
			<th>prix unitaire</th>
			<th>total</th>
		</tr>

		@foreach ($listerem0 as $lr0)
			<tr class="item-row">
				<td class="item-name">
					<div class="delete-wpr">

						<?php
						$baserem = \DB::table('bases')
								->select('titre')
								->where('idsite',$lr0->id_site_base)
								->first();
						echo $baserem->titre;
						?>
					</div>
				</td>
				<td class="description">
					<?php
					$campagneinfo = \DB::table('campagnes')
							->select('nom_campagne', 'type_id_campagne', 'rem_campagne')
							->where('id',$lr0->id_campagne)
							->first();
					echo $campagneinfo->nom_campagne;
					?>
				</td>

				<td>
					<?php
					$typerem = \DB::table('typerem')
							->select('type')
							->where('id',$campagneinfo->type_id_campagne)
							->first();
					echo $typerem->type;
					?>
				</td>

				<td>
					<span class="price">
					<?php
						if($campagneinfo->type_id_campagne == 1){
							echo $lr0->CPM;
						}
						if($campagneinfo->type_id_campagne == 2){
							echo $lr0->CPL;
						}
						if($campagneinfo->type_id_campagne == 3){
							echo $lr0->CPA;
						}
						if($campagneinfo->type_id_campagne == 4){
							echo $lr0->CPC;
						}
						?>
					</span>
				</td>

				<td>
					{{ $campagneinfo->rem_campagne }} €
				</td>

				<td>{{$lr0->com}} €</td>
			</tr>
		@endforeach

		@foreach ($listerem1 as $lr1)
			<tr class="item-row">
				<td class="item-name">
					<div class="delete-wpr">
						<?php
						$baserem = \DB::table('bases')
								->select('titre')
								->where('idsite',$lr1->id_site_base)
								->first();
						echo $baserem->titre;
						?>
					</div>
				</td>
				<td class="description">
					<?php
					$campagneinfo = \DB::table('campagnes')
							->select('nom_campagne','type_id_campagne', 'rem_campagne')
							->where('id',$lr1->id_campagne)
							->first();
					echo $campagneinfo->nom_campagne;
					?>
				</td>

				<td>
					<?php
					$typerem = \DB::table('typerem')
							->select('type')
							->where('id',$campagneinfo->type_id_campagne)
							->first();
					echo $typerem->type;
					?>
				</td>

				<td>
					<span class="price">
						<?php
						if($campagneinfo->type_id_campagne == 1){
							echo $lr1->devalid_CPM;
						}
						if($campagneinfo->type_id_campagne == 2){
							echo $lr1->devalid_CPL;
						}
						if($campagneinfo->type_id_campagne == 3){
							echo $lr1->devalid_CPA;
						}
						if($campagneinfo->type_id_campagne == 4){
							echo $lr1->devalid_CPC;
						}
						?>
					</span>
				</td>

				<td>
					{{ $campagneinfo->rem_campagne }} €
				</td>

				<td class="total-detail">
					{{$lr1->devalid_com}} €
				</td>

			</tr>
		@endforeach
	</table>


	<div id="bottom">
		<div id="reliquats">
			<table id="reliquats_table" class="reliquat_items">
				<tr>
					<th>Reliquats</th>
				</tr>
				<tr>
					<th>Base</th>
					<th>Campagne</th>
					<th>Type</th>
					<th>quantité</th>
					<th>total</th>
					<th>période source</th>
				</tr>

				@foreach($reliquats as $r)
					<tr>
						<td class="item-name">
							<?php
							$baserem = \DB::table('bases')
									->select('titre')
									->where('idsite',$r->id_site_base)
									->first();
							echo $baserem->titre;
							?>
						</td>
						<td class="description">
							<?php
							$campagneinfo = \DB::table('campagnes')
									->select('nom_campagne','type_id_campagne')
									->where('id',$r->id_campagne)
									->first();
							echo $campagneinfo->nom_campagne;
							?>
						</td>

						<td>
							<?php
							$typerem = \DB::table('typerem')
									->select('type')
									->where('id',$campagneinfo->type_id_campagne)
									->first();
							echo $typerem->type;
							?>
						</td>

						<td>
					<span class="price">
						<?php
						if($campagneinfo->type_id_campagne == 1){
							echo $r->CPM;
						}
						if($campagneinfo->type_id_campagne == 2){
							echo $r->CPL;
						}
						if($campagneinfo->type_id_campagne == 3){
							echo $r->CPA;
						}
						if($campagneinfo->type_id_campagne == 4){
							echo $r->CPC;
						}
						?>
				</span>
						</td>

						<td>
							<?php
							echo $r->com . ' €';
							?>
						</td>
						<td>
							<?php
								$id_rel_periode = $r->id_periode_reference;
								$periode = \DB::table('periode')->where('id', $id_rel_periode)->first();
								echo $periode->display_name;
							?>
						</td>
					</tr>

				@endforeach
			</table>
		</div>
	</div>

	<div style="clear:both"></div>

	<div id="total_container">
		<table id="total_table" class="total_item">
			<tr>
				<td colspan="2" class="total-line">Sous-total</td>
				<td class="total-value"><div id="subtotal">{{$total}} €</div></td>
			</tr>
			<tr>
				<td colspan="2" class="total-line">Total</td>
				<td class="total-value"><div id="total">{{$total}} €</div></td>
			</tr>
			<tr>
				<td colspan="2" class="total-line">Total réglé</td>
				<td class="total-value"><div id="paid">0.00 €</div></td>
			</tr>
			<tr>
				<td colspan="2" class="total-line balance">Total HT</td>
				<td class="total-value balance"><div class="due">{{$total}} €</div></td>
			</tr>
		</table>
	</div>

	<div style="clear:both"></div>

	<div id="page-wrap">
		<br />
		Nous vous informons que si des campagnes n'apparaissent pas dans l'appel à facture ci-dessous, elles apparaitront sur le suivant en reliquat.
		<br />
		Les paiements sont effectués lorsque la campagne est réglée par le client final.
	</div>

	<div id="terms">
		<h5>Mentions Légales</h5>
		<div>Lead Factory - 12 rue Camille Desmoulins - 92300 - Levallois Perret - RCS Nanterre 812 706 257 TVA FR 18 812706257</div>
	</div>
</div>
</body>
</html>
