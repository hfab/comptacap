@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
          <div class="panel-heading">Listing des appel à facture éditeurs</div>
          <div class="panel-body">
            <div class="row">
              <div class="col-sm-12">
                Appel à facture sur la période : <b>{{$periodeinfo->display_name}}</b>
              </div>
              <hr>
              <div class="col-sm-12">
                @foreach($periodeautre as $p)
                  <a href="/mails/editeurs/appelfacture/{{$p->id}}"><button type="button" class="btn btn-xs btn-success">{{$p->display_name}}</button></a>
                @endforeach
              </div>

            </div>
          </div>
          <div class="panel-body">
            <table class="table table-hover">
              <thead>
              <tr>
                <th>IDS</th>
                <th>Entreprise</th>
                <th>Apercu</th>
                <th>Mail</th>
                <th>
                  <input type="checkbox" onclick="selectAll()">
                  <button onclick="sendbulk()">
                    <span class="glyphicon glyphicon-send"></span>
                  </button>
                </th>
              </tr>
              </thead>
              <tbody>

              @foreach($listediteursmails as $e)

                <?php
                $editor = \DB::table('editeurs')
                        ->where('ids_editeur',$e->editeur_id_mail)
                        ->first();
                ?>

                <tr>
                  <td>{{$editor->ids_editeur}}</td>
                  <td>{{$editor->nom_entreprise}}</td>

                  <td>
                    <a href="/mails/editeurs/voirfacture/{{$editor->ids_editeur}}/{{$id}}">
                      <button type="button" class="btn btn-xs btn-info">
                        Voir Appel à facture
                      </button>
                    </a>
                  </td>
                  <td>
                    @if($e->sent_at == NULL)
                      <a href="/envoi/mailappelfacture/{{$editor->ids_editeur}}/{{$id}}">
                        <button type="button" class="btn btn-xs btn-warning">
                          Envoyer seul
                        </button>
                      </a>
                    @else
                      <button type="button" class="btn btn-xs btn-warning disabled">
                        Envoyé le {{$e->sent_at}}
                      </button>
                    @endif
                  </td>

                  <td>
                    @if($e->sent_at == NULL)
                      <input id="select{{$editor->ids_editeur}}" type="checkbox" name="selection" value="{{$editor->ids_editeur}}" onclick="store_id({{$editor->ids_editeur}})">
                    @endif
                  </td>
                </tr>

              @endforeach

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

  <script>
    let check_list = []
    let is_checked = false;

    function store_id(id) {
      if (check_list.includes(id))
        check_list.splice(check_list.indexOf(id), 1)
      else
        check_list.push(id)
      // console.log(check_list)
    }

    function sendbulk() {
      if (check_list.length === 0) {
        alert('Aucun mail sélectionné.')
        return
      }
      if (!confirm("send mail to " + check_list.length + " contacts ?"))
        return

      let data = JSON.stringify({'ids': check_list, 'idp': {{$id}}})

      var request = new XMLHttpRequest()
      request.open("POST", '/envoi/mail_send_bulk', true)
      request.setRequestHeader("X-CSRF-TOKEN", "{{ csrf_token() }}")
      request.setRequestHeader("content-type", "application/json")
      request.onreadystatechange = function() {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
          // console.log(this.response)
          alert("Envoi terminé.")
          location.reload()
        }
      }
      request.send(data)
    }

    function selectAll() {
      is_checked = !is_checked
      var _check_list = []
      var checkboxes = document.getElementsByName('selection')
      // console.log(checkboxes)
      for (var checkbox of checkboxes) {
        checkbox.checked = is_checked
        if (is_checked)
          _check_list.push(checkbox.value)
        else
          _check_list = []
      }
      check_list = _check_list
      // console.log(check_list)
    }
  </script>
@endsection

