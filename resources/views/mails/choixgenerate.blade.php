@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

              <div class="panel-heading">Générer les factures</div>

              <div class="panel-body">

                <form method="POST" action="/mails/generate/periode/launch" accept-charset="UTF-8">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />

                <h4></h4>

                <div class="form-group">
                  <label for="id_editeur">Choix de la période :</label>
                    <select class="form-control" id="id_periode" name="id_periode">
                    @foreach($lesperiodes as $p)
                      <option value="{{$p->id}}">{{$p->display_name}}</option>
                    @endforeach
                    </select>
                </div>


                <button type="submit" class="btn btn-success">Valider</button>


            </div>
        </div>
    </div>
  </div>

@endsection
