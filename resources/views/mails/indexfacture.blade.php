@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Acceuil - Appel à facture</div>
                <div class="panel-body">
                  <div class="row">

                  </div>
                </div>
                <div class="panel-body">
                  <a href="/mails/generate/choix"><button type="button" class="btn btn-danger">Générer les appels à facture</button></a>
                  <a href="/mails/editeurs/appelfacture/{{$maxp}}"><button type="button" class="btn btn-success">Consulter les appels à facture</button></a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
