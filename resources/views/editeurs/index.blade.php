@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Listing des éditeurs</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <a href="/editeurs/create"><button type="button" class="btn btn-success">Ajouter un éditeur</button></a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>IDS</th>
                                <th>Nom</th>
                                <th>Email</th>
                                <th>Options</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($editeurs as $e)

                                <tr>
                                    <td>{{$e->ids_editeur}}</td>
                                    <td>{{$e->nom_entreprise}}</td>
                                    <td>{{$e->email}}</td>
                                    <td><a href="./editeurs/edit/{{$e->id}}">
                                            <button type="button" class="btn btn-xs btn-info">
                                                Modifier
                                            </button>
                                        </a>
                                        <button type="button" class="btn btn-xs btn-danger" disabled>Supprimer</button>
{{--                                        <a onclick="return confirm('supprimer {{ $e->nom_entreprise }}')" href="./editeurs/delete/{{$e->id}}">--}}
{{--                                            <button type="button" class="btn btn-xs btn-danger">Supprimer</button>--}}
{{--                                        </a>--}}
                                    </td>
                                </tr>

                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
