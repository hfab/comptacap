@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Modifier un editeur</div>

                <div class="panel-body">

                  <form method="POST" action="/editeurs/update/{{$editeur->id}}" accept-charset="UTF-8">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}" />

                  <div class="form-group">
                    <label for="nom_editeur">Entreprise :</label>
                    <input type="text" class="form-control" id="nom_entreprise" name="nom_entreprise" value="{{$editeur->nom_entreprise}}">
                  </div>

                  <div class="form-group">
                    <label for="tel_portable_editeur">Nom : </label>
                    <input type="text" class="form-control" id="nom" name="nom" value="{{$editeur->nom}}">
                  </div>

                  <div class="form-group">
                    <label for="tel_portable_editeur">Prenom :</label>
                    <input type="text" class="form-control" id="prenom" name="prenom" value="{{$editeur->prenom}}">
                  </div>


                  <div class="form-group">
                     <label for="ids_editeur">ID IDS :</label>
                     <input type="number" class="form-control" id="ids_editeur" name="ids_editeur" value="{{$editeur->ids_editeur}}">
                  </div>
                  <div class="form-group">
                    <label for="mail_editeur">Email de contact :</label>
                    <input type="text" class="form-control" id="email" name="email" value="{{$editeur->email}}">
                  </div>

                  <div class="form-group">
                    <label for="mail_editeur">Email Tecnhique :</label>
                    <input type="text" class="form-control" id="technique" name="technique" value="{{$editeur->technique}}">
                  </div>

                  <div class="form-group">
                    <label for="mail_editeur">Email Compta :</label>
                    <input type="text" class="form-control" id="compta" name="compta" value="{{$editeur->compta}}">
                  </div>


                  <div class="form-group">
                    <label for="skype_editeur">Skype :</label>
                    <input type="text" class="form-control" id="skype" name="skype" value="{{$editeur->skype}}">
                  </div>


                  <div class="form-group">
                    <label for="tel_fix_editeur">Tel. fixe :</label>
                    <input type="text" class="form-control" id="telephone" name="telephone" value="{{$editeur->telephone}}">
                  </div>
                  <div class="form-group">
                    <label for="tel_portable_editeur">Tel. Portable :</label>
                    <input type="text" class="form-control" id="cellulaire" name="cellulaire" value="{{$editeur->cellulaire}}">
                  </div>


                  <div class="form-group">
                    <label for="ville_editeur">Ville :</label>
                    <input type="text" class="form-control" id="ville" name="ville" value="{{$editeur->ville}}">
                  </div>
                  <div class="form-group">
                    <label for="code_postal_editeur">Code Postal :</label>
                    <input type="text" class="form-control" id="codepostale" name="codepostale" value="{{$editeur->codepostale}}">
                  </div>
                  <div class="form-group">
                    <label for="adresse_editeur">Adresse :</label>
                    <input type="text" class="form-control" id="adresse" name="adresse" value="{{$editeur->adresse}}">
                  </div>

                 <button type="submit" class="btn btn-success">Modifier</button>
                 </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
