<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
    return redirect('/home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// upload
Route::get('/upload/upload', 'UploadController@uploadView')->name('Import fichier');
//Route::post('/file/upload', 'GestCsvController@verifyContent')->name('Import fichier');
Route::post('/upload/verify', 'UploadController@verifyCsv')->name('Import fichier');
Route::post('/upload/importCsv', 'UploadController@importCsvData')->name('Import fichier');
Route::get('/upload/config_filter', 'UploadController@configVerifFilter');
Route::post('/upload/add_filter', 'UploadController@addVerifFilter');
Route::post('/upload/remove_filter', 'UploadController@removeVerifFilter');
Route::get('/file/upload/error/duplicate', 'GestCsvController@uploadFileFormErrorDuplicate');


// editeurs
Route::get('/editeurs', 'EditeurController@index')->name('Listing editeurs');
Route::get('/editeurs/create', 'EditeurController@create')->name('Créer editeur');
Route::get('/editeurs/edit/{id}', 'EditeurController@edit')->name('Editer editeur');
Route::post('/editeurs/store', 'EditeurController@store');
Route::post('/editeurs/update/{id}', 'EditeurController@update');
Route::get('/editeurs/delete/{id}', 'EditeurController@delete');

// bases
Route::get('/bases', 'BaseController@index')->name('Listing des bases');
Route::get('/bases/create', 'BaseController@create')->name('Créer base');
Route::get('/bases/edit/{id}', 'BaseController@edit')->name('Editer base');
Route::post('/bases/store', 'BaseController@store');
Route::post('/bases/update/{id}', 'BaseController@update');
Route::get('/bases/delete/{id}', 'BaseController@delete');

// campagnes
Route::get('/campagnes', 'CampagneController@index')->name('Listing des campagnes');
Route::get('/campagnes/create', 'CampagneController@create')->name('Créer une campagne');
Route::get('/campagnes/edit/{id}', 'CampagneController@edit')->name('Modifier une campagne');
Route::post('/campagnes/store', 'CampagneController@store');
Route::post('/campagnes/update/{id}', 'CampagneController@update');
Route::get('/campagnes/delete/{id}', 'CampagneController@delete');

// mails
Route::get('/mails/editeurs/appelfacture/{id}', 'MailsController@listingEditeurAppel');
Route::get('/mails/editeurs/voirfacture/{ide}/{idp}', 'MailsController@voirFacturePeriode');

Route::get('/mails/generate/choix', 'MailsController@dbGenerateChoix');
Route::post('/mails/generate/periode/launch', 'MailsController@dbMailsPeriodePost');

Route::get('/mails/generate/periode/{id}', 'MailsController@dbMailsPeriode');
Route::get('/mails/editeurs/appelfacture', 'MailsController@acceuilFacture');

// calculs
Route::get('/set/prorata', 'ComptaController@devalidationProrataValue');
Route::get('/campagnes/setdevalidation/periode/{id}/{idp}', 'ComptaController@infoDevalidationCampagnePercent');
Route::post('/campagnes/applydevalidation/periode/{id}', 'ComptaController@setDevalidationCampagnePercent');

Route::get('/campagnes/setdevalidationnb/periode/{id}/{idp}', 'ComptaController@infoDevalidationCampagneNb');
Route::post('/campagnes/applydevalidationnb/periode/{id}', 'ComptaController@setDevalidationCampagneNb');

Route::get('/campagnes/devalid/delete/{id}/{idp}', 'ComptaController@deletedevalid');
Route::get('/campagnes/deleterem/{idrow}/{ic}/{idp}', 'ComptaController@deleterowrem');

Route::get('/campagnes/editrem/{idrow}/{ic}/{idp}', 'ComptaController@editrem');
Route::post('/campagnes/editrem/go', 'ComptaController@seteditrem');

Route::get('/campagnes/editremdevalid/{idrow}/{ic}/{idp}', 'ComptaController@editremdevalid');
Route::post('/campagnes/editremdevalid/go', 'ComptaController@seteditremdevalid');

// Reliquats
Route::get('/campagnes/reliquat/{idrow}/{ic}/{idp}', 'ComptaController@toreliquat');
Route::get('/campagnes/deletereliq/{idrow}/{ic}/{idp}', 'ComptaController@deleterowreliq');
Route::get('/campagnes/editReliquat/{idrow}/{ic}/{idp}', 'ComptaController@editreliquat');
Route::post('/campagnes/editreliquat/go', 'ComptaController@seteditreliquat');
Route::get('/campagnes/editReliquatDevalid/{idrow}/{ic}/{idp}', 'ComptaController@editReliquatDevalid');
Route::post('/campagnes/editReliquatDevalid/go', 'ComptaController@setEditReliquatDevalid');

Route::get('/campagnes/addrem/{ic}/{idp}', 'ComptaController@addremcampagneperiode');
Route::post('/campagnes/addrem/{ic}/{idp}/go', 'ComptaController@addremcampagneperiodego');
Route::get('/campagnes/addReliquat/{ic}/{idp}', 'ComptaController@addReliquatCampagnePeriode');
Route::post('/campagnes/addReliquat/{ic}/{idp}/go', 'ComptaController@addReliquatCampagnePeriodeGo');
// deval reliquats
Route::get('/campagnes/reliquatDevalid/{ic}/{idp}', 'ComptaController@devalidReliquatPercent');
Route::post('/campagnes/reliquatDevalid/{ic}/{idp}/go', 'ComptaController@devalidReliquatPercentGo');
Route::get('/campagnes/reliquatDevalidnb/{ic}/{idp}', 'ComptaController@devalidReliquatNb');
Route::post('/campagnes/reliquatDevalidnb/{ic}/{idp}/go', 'ComptaController@devalidReliquatNbGo');


// infos
Route::get('/campagnes/info/{id}', 'CampagneController@infodefault');
Route::get('/campagnes/info/{id}/{idp}', 'CampagneController@infodefaultperiode');

Route::get('/test/mail/{ide}/{idp}', 'MailsController@sendMail');
Route::get('/envoi/mailappelfacture/{ide}/{idp}', 'MailsController@sendMail');
Route::post('/envoi/mail_send_bulk', 'MailsController@sendBulkMail');
